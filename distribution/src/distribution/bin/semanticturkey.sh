#!/bin/sh

# determine the script directory (resolving symbolic links)
ST_BASE=$(dirname "$(dirname "$(readlink -f "$0")")")

# include the variable definitions
. "${ST_BASE}/bin/setvars.in.sh" || exit

# define the JAVA environment variable depending on the value of
# the JAVA_HOME environment variable
if [ -z "${JAVA_HOME}" ]; then
  JAVA=$(which java 2> /dev/null)
else
  JAVA="${JAVA_HOME}/bin/java"
fi

if [ ! -f "$JAVA" ]; then
  echo "Java binary ($JAVA) not found" >&2
  exit 1
fi

# change directory to ST_BASE
cd "$ST_BASE"

# define the Semantic Turkey classpath
ST_CLASSPATH="lib/*"

"${JAVA}" ${JAVA_OPTS} -cp "${ST_CLASSPATH}" it.uniroma2.art.semanticturkey.app.Application ${ST_OPTS}
exit $?