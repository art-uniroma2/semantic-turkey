@echo off

:: include variable definitions
setlocal enabledelayedexpansion

call "%~dp0"\setvars.in.bat || exit /b 1

setlocal disabledelayedexpansion

if "%JAVA_HOME%" == "" (
  set "JAVA=java"
  ) else (
  set "JAVA=%JAVA_HOME%/bin/java"
  )

:: bin directory
set ST_BIN=%~dp0

:: base directory
for %%I in ("%ST_BIN%..") do set "ST_BASE=%%~dpfI"

:: classpath
set "ST_CLASSPATH=%ST_BASE%\lib\*"

:: change the current working directory to the base directory
cd "%ST_BASE%"

:: start the application
"%JAVA%" %JAVA_OPTS% -cp "%ST_CLASSPATH%" it.uniroma2.art.semanticturkey.app.Application %ST_OPTS%
