package it.uniroma2.art.semanticturkey.mdr.services;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class MetadataRegistryServicesPlugin extends STPlugin {

    public MetadataRegistryServicesPlugin(PluginWrapper wrapper) {
        super(wrapper,"it.uniroma2.art.semanticturkey/st-metadata-registry-services", true);
    }

}
