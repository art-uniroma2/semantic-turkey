package it.uniroma2.art.semanticturkey.zthes;

public enum RelationType {
	NT(TermType.PT, TermType.PT), //Narrower term
	BT(TermType.PT, TermType.PT), //Broader term
	RT(TermType.PT, TermType.PT), //Related term
	LE(TermType.PT, TermType.PT), //Lexical Equivalence
	USE(TermType.ND, TermType.PT), //Use instead
	UF(TermType.PT, TermType.ND); //Use for
	
	private final TermType termTypeFrom;
	private final TermType termTypeTo;
	
	RelationType(TermType termTypeFrom, TermType termTypeTo) {
		this.termTypeFrom = termTypeFrom;
		this.termTypeTo = termTypeTo;
	}
	
	public TermType getTermTypeFrom() {
		return this.termTypeFrom;
	}
	
	public TermType getTermTypeTo() {
		return this.termTypeTo;
	}
}
