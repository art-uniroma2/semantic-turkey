package it.uniroma2.art.semanticturkey.extension.impl.urigen.template;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class NativeTemplateBasedURIGeneratorPlugin extends STPlugin {
    public NativeTemplateBasedURIGeneratorPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
