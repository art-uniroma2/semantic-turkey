package it.uniroma2.art.semanticturkey.extension.impl.urigen.coda;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class CODAURIGeneratorPlugin extends STPlugin {
    public CODAURIGeneratorPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
