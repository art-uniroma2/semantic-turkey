package it.uniroma2.art.semanticturkey.extension.impl.rdflifter.zthesdeserializer;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class ZthesDeserializerLifterPlugin extends STPlugin {
    public ZthesDeserializerLifterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
