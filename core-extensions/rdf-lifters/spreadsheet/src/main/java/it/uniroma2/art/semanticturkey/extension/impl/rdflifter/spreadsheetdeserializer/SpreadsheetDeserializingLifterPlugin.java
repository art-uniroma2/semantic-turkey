package it.uniroma2.art.semanticturkey.extension.impl.rdflifter.spreadsheetdeserializer;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class SpreadsheetDeserializingLifterPlugin extends STPlugin {
    public SpreadsheetDeserializingLifterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
