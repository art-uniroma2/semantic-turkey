package it.uniroma2.art.semanticturkey.extension.impl.rdflifter.rdfdeserializer;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class RDFDeserializerLifterPlugin extends STPlugin {
    public RDFDeserializerLifterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
