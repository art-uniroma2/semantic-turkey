package it.uniroma2.art.semanticturkey.extension.impl.rdftransformer.updateproperty;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class UpdatePropertyValueRDFTransformerPlugin extends STPlugin {
    public UpdatePropertyValueRDFTransformerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
