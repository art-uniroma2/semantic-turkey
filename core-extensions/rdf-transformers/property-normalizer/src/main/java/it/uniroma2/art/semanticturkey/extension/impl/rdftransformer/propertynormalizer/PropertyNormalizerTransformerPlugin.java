package it.uniroma2.art.semanticturkey.extension.impl.rdftransformer.propertynormalizer;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class PropertyNormalizerTransformerPlugin extends STPlugin {
    public PropertyNormalizerTransformerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
