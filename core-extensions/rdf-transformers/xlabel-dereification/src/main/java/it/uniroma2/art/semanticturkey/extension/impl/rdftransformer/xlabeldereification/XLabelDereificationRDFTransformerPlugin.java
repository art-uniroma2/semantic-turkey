package it.uniroma2.art.semanticturkey.extension.impl.rdftransformer.xlabeldereification;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class XLabelDereificationRDFTransformerPlugin extends STPlugin {
    public XLabelDereificationRDFTransformerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
