package it.uniroma2.art.semanticturkey.extension.impl.rdftransformer.edoalstdflat;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class EDOAL2StdFlatFormatsTransformerPlugin extends STPlugin {
    public EDOAL2StdFlatFormatsTransformerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
