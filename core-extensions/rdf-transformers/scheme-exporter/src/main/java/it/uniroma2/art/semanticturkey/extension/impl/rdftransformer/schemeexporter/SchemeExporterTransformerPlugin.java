package it.uniroma2.art.semanticturkey.extension.impl.rdftransformer.schemeexporter;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class SchemeExporterTransformerPlugin extends STPlugin {
    public SchemeExporterTransformerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
