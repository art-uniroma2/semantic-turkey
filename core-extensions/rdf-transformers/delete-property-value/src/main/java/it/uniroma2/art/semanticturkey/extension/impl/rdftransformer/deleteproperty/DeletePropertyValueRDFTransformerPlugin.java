package it.uniroma2.art.semanticturkey.extension.impl.rdftransformer.deleteproperty;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class DeletePropertyValueRDFTransformerPlugin extends STPlugin {
    public DeletePropertyValueRDFTransformerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
