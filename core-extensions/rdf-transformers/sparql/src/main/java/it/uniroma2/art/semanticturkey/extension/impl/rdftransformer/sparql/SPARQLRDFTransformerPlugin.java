package it.uniroma2.art.semanticturkey.extension.impl.rdftransformer.sparql;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class SPARQLRDFTransformerPlugin extends STPlugin {
    public SPARQLRDFTransformerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
