package it.uniroma2.art.semanticturkey.extension.impl.rdftransformer.stdflatedoal;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class StdFlatFormats2EDOALTransformerPlugin extends STPlugin {
    public StdFlatFormats2EDOALTransformerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
