package it.uniroma2.art.semanticturkey.extension.impl.customservice.sparql;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class SPARQLCustomServiceBackendPlugin extends STPlugin {
    public SPARQLCustomServiceBackendPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
