package it.uniroma2.art.semanticturkey.extension.impl.datasetcatalog.ontoportal;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class OntoPortalConnectorPlugin extends STPlugin {
    public OntoPortalConnectorPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
