package it.uniroma2.art.semanticturkey.extension.impl.datasetcatalog.showvoc;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import it.uniroma2.art.semanticturkey.extension.ExtensionPointManager;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetcatalog.DatasetCatalogConnector;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetcatalog.DatasetDescription;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetcatalog.DatasetSearchResult;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetcatalog.SearchResultsPage;
import it.uniroma2.art.semanticturkey.i18n.I18NConstants;
import it.uniroma2.art.semanticturkey.mvc.RequestMappingHandlerAdapterPostProcessor;
import jakarta.annotation.Nullable;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * An {@link DatasetCatalogConnector} for <a href="https://data.europa.eu/euodp">Public Multilingual Knowledge
 * Infrastructure</a> (PMKI).
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 */
public class ShowVocConnector implements DatasetCatalogConnector {

	private static final Logger logger = LoggerFactory.getLogger(ShowVocConnector.class);

	private final ShowVocConnectorConfiguration conf;

	private final ExtensionPointManager exptManager;

	public ShowVocConnector(ShowVocConnectorConfiguration conf, ExtensionPointManager exptManager) {
		this.conf = conf;
		this.exptManager = exptManager;
	}

	protected ShowVocClient createShowVocClient() throws MalformedURLException {
		return new ShowVocClient(exptManager, conf.apiBaseURL);
	}

	@Override
	public SearchResultsPage<DatasetSearchResult> searchDataset(String query,
			Map<String, List<String>> facets, int page) throws IOException {
		if (page < 0) {
			throw new IllegalArgumentException("Page number is negative: " + page);
		}

		try (ShowVocClient showVocClient = createShowVocClient()) {
			try {
				return showVocClient.searchDataset(query, facets, page);
			} finally {
				showVocClient.doLogout();
			}
		}
	}

	@Override
	public DatasetDescription describeDataset(String id) throws IOException {
		try (ShowVocClient showVocClient = createShowVocClient()) {
			try {
				return showVocClient.describeDataset(id, conf.apiBaseURL, conf.frontendBaseURL);
			} finally {
				showVocClient.doLogout();
			}
		}
	}

	public static void main(String[] args) throws IOException {
//		ShowVocConnectorConfiguration conf = new ShowVocConnectorConfiguration();
//		conf.apiBaseURL = "http://localhost:1979/semanticturkey/";
//		conf.frontendBaseURL = "http://localhost:1979/showvoc/";
//		ShowVocConnector connector = new ShowVocConnectorFactory().createInstance(conf);
//
//		SearchResultsPage<DatasetSearchResult> results = connector.searchDataset("test",
//				Collections.emptyMap(), 0);
//		System.out.println(results);

		SailRepository sailRepo = new SailRepository(new MemoryStore());
		sailRepo.init();
		System.out.println(sailRepo.getValueFactory().createBNode());

		sailRepo.shutDown();
	}

}

class ShowVocClient implements AutoCloseable {

	public static final String AUTH_SERVICE_CLASS = "Auth";

	public static final String CORE_SERVICES_PATH = "it.uniroma2.art.semanticturkey/st-core-services/";

	public static final String SHOWVOC_SERVICE_CLASS = "ShowVoc";

	private final String apiBaseURL;
	private final CloseableHttpClient httpClient;
	private final ObjectMapper objectMapper;

	public ShowVocClient(ExtensionPointManager exptManager, String apiBaseURL)
			throws MalformedURLException {
		this.apiBaseURL = apiBaseURL;

		URL apiBaseURLobj = new URL(apiBaseURL);
		BasicCookieStore cookieStore = new BasicCookieStore();
		BasicClientCookie cookie = new BasicClientCookie(I18NConstants.LANG_COOKIE_NAME,
				LocaleContextHolder.getLocale().toString());
		cookie.setDomain(apiBaseURLobj.getHost());
		cookie.setPath(apiBaseURLobj.getPath());
		cookieStore.addCookie(cookie);
		this.httpClient = HttpClientBuilder.create().useSystemProperties().setDefaultCookieStore(cookieStore)
				.build();
		this.objectMapper = RequestMappingHandlerAdapterPostProcessor.createObjectMapper(exptManager);
	}

	@Override
	public void close() throws IOException {
		httpClient.close();
	}

	@SuppressWarnings("unchecked")
	private <T> T execute(HttpUriRequest httpRequest, JavaType valueType)
			throws ClientProtocolException, IOException {
		try (CloseableHttpResponse httpReponse = httpClient.execute(httpRequest)) {
			int statusCode = httpReponse.getStatusLine().getStatusCode();
			if ((statusCode / 100) != 2) {
				throw new IOException(
						"The request to the remote machine failed: " + httpReponse.getStatusLine());
			}

			@Nullable
			HttpEntity entity = httpReponse.getEntity();

			if (entity != null) {
				ContentType contentType = ContentType.getOrDefault(entity);
				String mimeType = contentType.getMimeType();

				if (ContentType.APPLICATION_JSON.getMimeType().equals(mimeType)) {
					try (InputStream is = entity.getContent(); InputStreamReader reader = new InputStreamReader(is,
							Optional.ofNullable(contentType.getCharset()).orElse(StandardCharsets.UTF_8))) {
						JsonNode reponseTree = this.objectMapper.readTree(reader);
						JsonNode stresponseJson = reponseTree.get("stresponse");

						if (stresponseJson != null) {
							JsonNode exceptionJson = stresponseJson.get("exception");
							JsonNode msgJson = stresponseJson.get("msg");
							if (exceptionJson != null && msgJson != null) {
								throw new IOException("Remote machine error: " + msgJson.textValue());
							}
						}

						JsonNode responseContentJson = reponseTree.get("result");

						if (responseContentJson != null) {
							if (valueType.hasRawClass(ObjectNode.class)) {
								return (T) (ObjectNode) responseContentJson;
							}

							return (T) objectMapper.readValue(objectMapper.treeAsTokens(responseContentJson),
									valueType);

						} else {
							if (!valueType.equals(TypeFactory.defaultInstance().constructType(void.class))) {
								throw new IOException(

										"The response from the remote machine doesn't contain a result");
							} else {
								return null;
							}
						}

					}
				} else {
					throw new IOException(
							"The remote machine returned an unexpected content type: " + contentType);
				}
			} else {
				throw new IOException("No entity returned by the remote machine");
			}
		}
	}

	public void doLogout() throws IOException {
		String requestURL = UriComponentsBuilder.fromHttpUrl(apiBaseURL).path(CORE_SERVICES_PATH)
				.path(AUTH_SERVICE_CLASS).path("/logout").build().toUriString();
		HttpPost httpPost = new HttpPost(requestURL);
		execute(httpPost, TypeFactory.defaultInstance().constructType(void.class));
	}

	public SearchResultsPage<DatasetSearchResult> searchDataset(String query,
			Map<String, List<String>> facets, int page) throws IOException {
		String requestURL = UriComponentsBuilder.fromHttpUrl(apiBaseURL).path(CORE_SERVICES_PATH)
				.path(SHOWVOC_SERVICE_CLASS).path("/searchDataset").queryParam("query", query)
				.queryParam("facets", objectMapper.writeValueAsString(facets)).queryParam("page", page)
				.build(false).encode().toUriString();
		HttpGet httpGet = new HttpGet(requestURL);
		SearchResultsPage<DatasetSearchResult> resultsPage = execute(httpGet, TypeFactory.defaultInstance()
				.constructParametricType(SearchResultsPage.class, DatasetSearchResult.class));
		return resultsPage;
	}

	public DatasetDescription describeDataset(String id, @Nullable String apiBaseURL,
			@Nullable String frontendBaseURL) throws IOException {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(apiBaseURL);
		uriBuilder.path(CORE_SERVICES_PATH).path(SHOWVOC_SERVICE_CLASS).path("/describeDataset").queryParam("id",
				id);
		if (apiBaseURL != null) {
			uriBuilder.queryParam("apiBaseURL", apiBaseURL);
		}

		if (frontendBaseURL != null) {
			uriBuilder.queryParam("frontendBaseURL", frontendBaseURL);
		}

		String requestURL = uriBuilder.build(false).encode().toUriString();
		HttpGet httpGet = new HttpGet(requestURL);
		DatasetDescription datasetMetadata = execute(httpGet,
				TypeFactory.defaultInstance().constructType(DatasetDescription.class));

		return datasetMetadata;
	}

}