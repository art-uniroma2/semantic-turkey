package it.uniroma2.art.semanticturkey.extension.impl.datasetcatalog.dataeu;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class DataEUConnectorPlugin extends STPlugin {
    public DataEUConnectorPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
