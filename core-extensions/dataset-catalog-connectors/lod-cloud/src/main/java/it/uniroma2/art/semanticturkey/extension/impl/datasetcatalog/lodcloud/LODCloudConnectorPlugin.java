package it.uniroma2.art.semanticturkey.extension.impl.datasetcatalog.lodcloud;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class LODCloudConnectorPlugin extends STPlugin {
    public LODCloudConnectorPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
