package it.uniroma2.art.semanticturkey.extension.impl.datasetcatalog.lov;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class LOVConnectorPlugin extends STPlugin {
    public LOVConnectorPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
