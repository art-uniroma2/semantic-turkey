package it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.spreadsheetserializer;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class SpreadsheetSerializingExporterPlugin extends STPlugin {
    public SpreadsheetSerializingExporterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
