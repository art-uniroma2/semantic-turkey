package it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.zthesserializer;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class ZthesSerializingExporterPlugin extends STPlugin {
    public ZthesSerializingExporterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
