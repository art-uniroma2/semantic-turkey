package it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.rdfserializer;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class RDFSerializingExporterPlugin extends STPlugin {
    public RDFSerializingExporterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
