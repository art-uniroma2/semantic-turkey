package it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.sdmxserializer;

public class SdmxXmlVocabulary {

    public static final String XML_LANG = "xml:lang";
    public static final String URN = "urn";
    public static final String ID = "id";
    public static final String ISEXTERNALREFERENCE = "isExternalReference";
    public static final String AGENCYID = "agencyID";
    public static final String VALIDFROM = "validFrom";
    public static final String VALIDTO = "validTo";
    public static final String VERSION = "version";


    public static final String MESSAGE_STRUCTURE = "message:Structure";

    //HEADER
    public static final String MESSAGE_HEADER = "message:Header";
    public static final String MESSAGE_ID = "message:ID";
    public static final String MESSAGE_TEST = "message:Test";
    public static final String MESSAGE_PREPARED = "message:Prepared";



    public static final String MESSAGE_STRUCTURES = "message:Structures";
    public static final String STR_CODELISTS = "str:Codelists";
    public static final String STR_CODELIST = "str:Codelist";

    public static final String COM_ANNOTATIONS = "com:Annotations";
    public static final String COM_ANNOTATION = "com:Annotation";
    public static final String COM_ANNOTATIONTYPE = "com:AnnotationType";
    public static final String COM_ANNOTATIONTEXT = "com:AnnotationText";
    public static final String COM_ANNOTATIONTITLE = "com:AnnotationTitle";
    public static final String COM_ANNOTATIONURL = "com:AnnotationURL";

    public static final String COM_NAME = "com:Name";
    public static final String COM_DESCRIPTION = "com:Description";
    public static final String STR_CODE = "str:Code";
    public static final String STR_PARENT = "str:Parent";


    //the SDMX values for <com:AnnotationType>
    public static final String COUNTRY_ORDER_ANN = "COUNTRY_ORDER";
    public static final String COVERAGE_LABEL_ANN = "COVERAGE_LABEL";
    public static final String DEPRECATED_ANN = "DEPRECATED"; // special case, this is used also for the sub-propeties of skos:mapping
    public static final String EXPLAN_CASELAW_ANN = "EXPLAN_CASELAW";
    public static final String EXPLAN_EXCLUDES_ANN = "EXPLAN_EXCLUDES";
    public static final String EXPLAN_INCLUDES_ANN = "EXPLAN_INCLUDES";
    public static final String EXPLAN_INCLUDES_ALSO_ANN = "EXPLAN_INCLUDES_ALSO";
    public static final String EXPLAN_LABEL_ANN = "EXPLAN_LABEL";
    public static final String FAMILY_LABEL_ANN = "FAMILY_LABEL";
    public static final String FOLLOWS_ANN = "FOLLOWS";
    public static final String HAS_VARIANT_ANN = "HAS_VARIANT";
    public static final String HIER_LEVEL_ANN = "HIER_LEVEL";
    public static final String LEGAL_BASIS_ANN = "LEGAL_BASIS";
    public static final String MAP_REFER_ANN = "MAP_REFER"; // special case, this is used also for the sub-propeties of skos:mapping
    public static final String ORDER_ANN = "ORDER";
    public static final String ORIGINAL_CODE_ANN = "ORIGINAL CODE";
    public static final String PRODUCTION_TYPE_ANN = "PRODUCTION_TYPE";
    public static final String REGION_ORDER_ANN = "REGION_ORDER";
    public static final String SHORT_LABEL_ANN = "SHORT_LABEL";
    public static final String SUPERSEED_ANN = "SUPERSEED";
    public static final String UNIT_MEASURE_LABEL_ANN = "UNIT_MEASURE_LABEL";



    //properties URIs (and namespace) used in the SPARQL queries to get the values

    //NAMESPACES:
    public static final String CV_NAMESPACE = "http://data.europa.eu/m8g/";
    public static final String DC_NAMESPACE = "http://purl.org/dc/elements/1.1/";
    public static final String DCT_NAMESPACE = "http://purl.org/dc/terms/";
    public static final String ELI_NAMESPACE = "http://data.europa.eu/eli/ontology#";
    public static final String NACE_NAMESPACE = "http://data.europa.eu/ux2/nace2/";
    public static final String NUTS_NAMESPACE = "http://data.europa.eu/nuts/";
    public static final String OWL_NAMESPACE = "http://www.w3.org/2002/07/owl#";
    public static final String XKOS_NAMESPACE = "http://rdf-vocabulary.ddialliance.org/xkos#";
    public static final String SCHEMA_NAMESPACE = "http://schema.org/";
    public static final String SKOS_NAMESPACE = "http://www.w3.org/2004/02/skos/core#";

    //PROPERTY URIs
    public static final String ADDITIONALCONTENTNOTE_URI= XKOS_NAMESPACE+"additionalContentNote";
    public static final String ADDITIONALCONTENTNOTE_URI_NT = "<"+ADDITIONALCONTENTNOTE_URI+">";

    public static final String ALTLABEL_URI = SKOS_NAMESPACE+"altLabel";
    public static final String ALTLABEL_URI_NT = "<"+ALTLABEL_URI+">";

    public static final String BASEDON_URI = ELI_NAMESPACE+"based_on";
    public static final String BASEDON_URI_NT = "<"+BASEDON_URI+">";

    public static final String BELONGSTO_URI = XKOS_NAMESPACE+"belongsTo";
    public static final String BELONGSTO_URI_NT = "<"+BELONGSTO_URI+">" ;

    public static final String BROADER_URI = SKOS_NAMESPACE+"broader";
    public static final String BROADER_URI_NT = "<"+BROADER_URI+">";

    public static final String CASELAW_URI= XKOS_NAMESPACE+"caseLaw";
    public static final String CASELAW_URI_NT = "<"+CASELAW_URI+">";

    public static final String CORECONTENTNOTE_URI = XKOS_NAMESPACE+"coreContentNote";
    public static final String CORECONTENTNOTE_URI_NT = "<"+CORECONTENTNOTE_URI+">";

    public static final String COUNTRYORDER_URI = NUTS_NAMESPACE+"countryOrder";
    public static final String COUNTRYORDER_URI_NT = "<"+COUNTRYORDER_URI+">";

    public static final String COVERS_URI = XKOS_NAMESPACE+"covers";
    public static final String COVERS_URI_NT = "<"+COVERS_URI+">";

    public static final String CREATOR_URI = DCT_NAMESPACE+"creator";
    public static final String CREATOR_URI_NT = "<"+CREATOR_URI+">";

    public static final String DEPRECATED_URI = OWL_NAMESPACE+"deprecated"; // only the value "true"
    public static final String DEPRECATED_URI_NT = "<"+DEPRECATED_URI+">";

    public static final String DEPTH_URI = XKOS_NAMESPACE+"depth";
    public static final String DEPTH_URI_NT = "<"+DEPTH_URI+">";

    public static final String DESCRIPTION_URI = DCT_NAMESPACE+"description";
    public static final String DESCRIPTION_URI_NT = "<"+DESCRIPTION_URI+">";

    public static final String ENDDATE_URI = SCHEMA_NAMESPACE+"endDate";
    public static final String ENDDATE_URI_NT = "<"+ENDDATE_URI+">";

    public static final String EXCLUSIONNOTE_URI = XKOS_NAMESPACE+"exclusionNote";
    public static final String EXCLUSIONNOTE_URI_NT = "<"+EXCLUSIONNOTE_URI+">";

    public static final String FOLLOWS_URI = XKOS_NAMESPACE+"follows";
    public static final String FOLLOWS_URI_NT = "<"+FOLLOWS_URI+">";

    public static final String IDENTIFIER_DC_URI = DC_NAMESPACE+"identifier";
    public static final String IDENTIFIER_DC_URI_NT = "<"+ IDENTIFIER_DC_URI +">";

    public static final String IDENTIFIER_DCTERMS_URI = DCT_NAMESPACE+"identifier";
    public static final String IDENTIFIER_DCTERMS_URI_NT = "<"+ IDENTIFIER_DCTERMS_URI +">";

    public static final String INCLUSIONNOTE_URI = XKOS_NAMESPACE+"inclusionNote";
    public static final String INCLUSIONNOTE_URI_NT = "<"+INCLUSIONNOTE_URI+">";

    public static final String LEVELS_URI = XKOS_NAMESPACE+"levels";
    public static final String LEVELS_URI_NT = "<"+ LEVELS_URI +">";

    public static final String MAPPING_URI = SKOS_NAMESPACE+"mapping"; // its subproperties should be considered as well
    public static final String MAPPING_URI_NT = "<"+MAPPING_URI+">";

    public static final String MEMBER_URI = SKOS_NAMESPACE+"member";
    public static final String MEMBER_URI_NT = "<"+MEMBER_URI+">";

    public static final String NOTATION_URI = SKOS_NAMESPACE+"notation";
    public static final String NOTATION_URI_NT = "<"+NOTATION_URI+">";

    public static final String NOTE_URI = SKOS_NAMESPACE+"note";
    public static final String NOTE_URI_NT = "<"+NOTE_URI+">";

    public static final String ORDER_URI = NACE_NAMESPACE+"order";
    public static final String ORDER_URI_NT = "<"+ORDER_URI+">";

    public static final String PREFLABEL_URI = SKOS_NAMESPACE+"prefLabel";
    public static final String PREFLABEL_URI_NT = "<"+PREFLABEL_URI+">";

    public static final String REGIONORDER_URI = NUTS_NAMESPACE+"regionOrder";
    public static final String REGIONORDER_URI_NT = "<"+REGIONORDER_URI+">";

    public static final String SCOPENOTE_URI = SKOS_NAMESPACE+"scopeNote";
    public static final String SCOPENOTE_URI_NT = "<"+SCOPENOTE_URI+">";

    public static final String STARTDATE_URI = SCHEMA_NAMESPACE+"startDate";
    public static final String STARTDATE_URI_NT = "<"+STARTDATE_URI+">";

    public static final String STATUNITMEASURE_URI = CV_NAMESPACE+"statUnitMeasure" ;
    public static final String STATUNITMEASURE_URI_NT = "<"+STATUNITMEASURE_URI+">";

    public static final String SUPERSEDES_URI = XKOS_NAMESPACE+"supersedes";
    public static final String SUPERSEDES_URI_NT = "<"+SUPERSEDES_URI+">";

    public static final String TYPE_URI = DCT_NAMESPACE+"type";
    public static final String TYPE_URI_NT = "<"+SUPERSEDES_URI+">";

    public static final String VARIANT_URI = XKOS_NAMESPACE+"variant";
    public static final String VARIANT_URI_NT = "<"+VARIANT_URI+">";


}
