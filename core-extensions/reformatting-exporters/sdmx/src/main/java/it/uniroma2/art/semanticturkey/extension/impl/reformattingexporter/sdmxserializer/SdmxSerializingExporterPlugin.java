package it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.sdmxserializer;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;


public class SdmxSerializingExporterPlugin extends STPlugin {
    public SdmxSerializingExporterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
