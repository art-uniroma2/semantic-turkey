package it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.sdmxserializer.struct;

import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SchemeSDMXInfo {

    private final String URN_FIRST_PART = "urn:sdmx:org.sdmx.infomodel.codelist.Codelist=";

    private final String schemeUri;

    private String id1; // from dc:identifier
    private String id2; // from dcterms:identifier
    //private String urn;
    private String agencyId;
    private String version;
    private String validFrom;
    private String validTo;
    private final List<Literal> nameList = new ArrayList<>();
    private final List<Literal> descriptionList = new ArrayList<>();

    private final Map<String, List<Value>> annotationTypeToValueMap = new HashMap<>();

    public SchemeSDMXInfo(String schemeUri) {
        this.schemeUri = schemeUri;
    }

    public void setId1(String id) {
        this.id1 = id;
    }

    public void setId2(String id) {
        this.id2 = id;
    }


    public void setAgencyId(String agencyId) {
        this.agencyId = agencyId;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public void addName(Value name) {
        if (!name.isLiteral()) {
            return;
        }
        if (!nameList.contains((Literal)name)) {
            nameList.add((Literal) name);
        }
    }

    public void addDescription(Value description) {
        if (!description.isLiteral()) {
            return;
        }
        if (!descriptionList.contains((Literal)description)) {
            descriptionList.add((Literal)description);
        }
    }

    public void addValueToAnnotationType(Value value, String annotationType) {
        if (!annotationTypeToValueMap.containsKey(annotationType)) {
            annotationTypeToValueMap.put(annotationType, new ArrayList<>());
        }
        List<Value> valueList = annotationTypeToValueMap.get(annotationType);
        if (!valueList.contains(value)) {
            valueList.add(value);
        }
    }

    public String getSchemeUri() {
        return schemeUri;
    }

    public String getId() {
        // this will return "id1", if it is not null, or id2 if id1 is null
        return id1!=null ? id1 : id2;
    }

    public String getUrn() {
        return URN_FIRST_PART+getAgencyId()+":"+getId()+"("+getVersion()+")";
    }

    public String isExternalReference() {
        return "false";
    }

    public String getAgencyId() {
        return agencyId;
    }

    public String getVersion() {
        return version;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public List<Literal> getNameList() {
        return nameList;
    }

    public List<Literal> getDescriptionList() {
        return descriptionList;
    }

    public Map<String, List<Value>> getAnnotationTypeToValueMap() {
        return annotationTypeToValueMap;
    }
}
