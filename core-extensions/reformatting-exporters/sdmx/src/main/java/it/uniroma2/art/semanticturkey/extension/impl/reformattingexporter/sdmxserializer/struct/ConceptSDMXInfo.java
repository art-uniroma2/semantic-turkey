package it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.sdmxserializer.struct;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConceptSDMXInfo {

    private final String URN_FIRST_PART = "urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ESTAT:";
    private SchemeSDMXInfo schemeSDMXInfo;

    private final String conceptUri;

    private String id;
    private List<Literal> nameList = new ArrayList<>();
    private List<Literal> backupNameList = new ArrayList<>(); // this should be used (READ) only when the nameList is empty

    private List<Literal> descriptionList = new ArrayList<>();
    private List<IRI> parentList = new ArrayList<>();

    private Map<String, List<String>> mapPropToRdfResourceMap = new HashMap<>();

    private final Map<String, List<Value>> annotationTypeToValueMap = new HashMap<>();

    public ConceptSDMXInfo(String conceptUri) {
        this.conceptUri = conceptUri;
    }

    public void setSchemeSDMXInfo(SchemeSDMXInfo schemeSDMXInfo) {
        this.schemeSDMXInfo = schemeSDMXInfo;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void addName(Value name) {
        if (!name.isLiteral()) {
            return;
        }
        if (!nameList.contains((Literal) name)) {
            nameList.add((Literal)name);
        }
    }

    public void addBackupName(Value name) {
        if (!name.isLiteral()) {
            return;
        }
        if (!backupNameList.contains((Literal) name)) {
            backupNameList.add((Literal)name);
        }
    }

    public void addDescription(Value description) {
        if (!description.isLiteral()) {
            return;
        }
        if (!descriptionList.contains((Literal)description)) {
            descriptionList.add((Literal)description);
        }
    }

    public void addParent(Value parent) {
        if (!parent.isIRI()) {
            return;
        }
        if (!parentList.contains((IRI)parent)) {
            parentList.add((IRI)parent);
        }
    }

    public void addValueToAnnotationType(Value value, String annotationType) {
        if (!annotationTypeToValueMap.containsKey(annotationType)) {
            annotationTypeToValueMap.put(annotationType, new ArrayList<>());
        }
        List<Value> valueList = annotationTypeToValueMap.get(annotationType);
        if (!valueList.contains(value)) {
            valueList.add(value);
        }
    }

    public void addValueToMappingProperties(Value value, IRI mappingProperty) {
        String mappingPropLC = mappingProperty.getLocalName();

        if (!value.isIRI()) {
            return;
        }

        if (!mapPropToRdfResourceMap.containsKey(mappingPropLC)) {
            mapPropToRdfResourceMap.put(mappingPropLC, new ArrayList<>());
        }
        List<String> rdfResourceList = mapPropToRdfResourceMap.get(mappingPropLC);
        if (!rdfResourceList.contains(value.stringValue())) {
            rdfResourceList.add(value.stringValue());
        }

    }

    public String getConceptUri() {
        return conceptUri;
    }

    public String getId() {
        return id;
    }

    public String getUrn() {
        return schemeSDMXInfo.getUrn()+"."+getId();
    }

    public List<Literal> getNameList() {
        return nameList;
    }

    public List<Literal> getBackupNameList() {
        return backupNameList;
    }

    public List<Literal> getDescriptionList() {
        return descriptionList;
    }

    public List<IRI> getParentList() {
        return parentList;
    }

    public Map<String, List<Value>> getAnnotationTypeToValueMap() {
        return annotationTypeToValueMap;
    }

    public Map<String, List<String>> getMapPropToRdfResourceMap() {
        return mapPropToRdfResourceMap;
    }
}
