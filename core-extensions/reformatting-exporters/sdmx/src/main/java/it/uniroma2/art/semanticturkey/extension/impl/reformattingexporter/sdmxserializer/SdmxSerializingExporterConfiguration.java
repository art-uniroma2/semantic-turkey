package it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.sdmxserializer;

import java.util.Set;

import it.uniroma2.art.semanticturkey.config.Configuration;
import it.uniroma2.art.semanticturkey.constraints.HasRole;
import it.uniroma2.art.semanticturkey.data.role.RDFResourceRole;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import org.eclipse.rdf4j.model.IRI;

public class SdmxSerializingExporterConfiguration implements Configuration {

	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.sdmxserializer.SdmxSerializingExporterConfiguration";

		public static final String shortName = keyBase + ".shortName";
		public static final String altlabelsInsteadOfPreflabels$description = keyBase + ".altlabelsInsteadOfPreflabels.description";
		public static final String altlabelsInsteadOfPreflabels$displayName = keyBase + ".altlabelsInsteadOfPreflabels.displayName";

		public static final String targetScheme$description = keyBase + ".targetScheme.description";
		public static final String targetScheme$displayName = keyBase + ".targetScheme.displayName";

		public static final String agencyId$description = keyBase + ".agencyId.description";
		public static final String agencyId$displayName = keyBase + ".agencyId.displayName";

	}

	@Override
	public String getShortName() {
		return "{" + MessageKeys.shortName + "}";
	}

	@STProperty(description = "{" + MessageKeys.altlabelsInsteadOfPreflabels$description + "}", displayName = "{" + MessageKeys.altlabelsInsteadOfPreflabels$displayName + "}")
	@Required
	public Boolean altlabelsInsteadOfPreflabels;

	@STProperty(description = "{" + MessageKeys.targetScheme$description + "}", displayName = "{" + MessageKeys.targetScheme$displayName + "}")
	@Required
	@HasRole(RDFResourceRole.conceptScheme)
	public IRI targetScheme;

	@STProperty(description = "{" + MessageKeys.agencyId$description + "}", displayName = "{" + MessageKeys.agencyId$displayName + "}")
	public String agencyId;
	
}

