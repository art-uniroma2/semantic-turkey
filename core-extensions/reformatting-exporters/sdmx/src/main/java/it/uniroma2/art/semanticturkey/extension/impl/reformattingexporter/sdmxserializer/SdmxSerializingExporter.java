package it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.sdmxserializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.TimeZone;

import it.uniroma2.art.semanticturkey.extension.extpts.reformattingexporter.ReformattingException;
import it.uniroma2.art.semanticturkey.extension.extpts.reformattingexporter.ReformattingWrongLexModelException;
import it.uniroma2.art.semanticturkey.extension.extpts.reformattingexporter.ReformattingWrongModelException;
import it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.sdmxserializer.struct.ConceptSDMXInfo;
import it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.sdmxserializer.struct.SchemeSDMXInfo;
import it.uniroma2.art.semanticturkey.i18n.STMessageSource;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.utilities.XMLHelp;
import jakarta.annotation.Nullable;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.semanticturkey.extension.extpts.reformattingexporter.ClosableFormattedResource;
import it.uniroma2.art.semanticturkey.extension.extpts.reformattingexporter.ExporterContext;
import it.uniroma2.art.semanticturkey.extension.extpts.reformattingexporter.ReformattingExporter;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * A {@link ReformattingExporter} that serializes RDF data in Sdmx format
 * 
 * @author <a href="mailto:turbati@info.uniroma2.it">Andrea Turbati</a>
 */
public class SdmxSerializingExporter implements ReformattingExporter {

	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.sdmxserializer.SdmxSerializingExporter";

		public static final String exceptionModel$messsage = keyBase + ".exceptionModel.message";
		public static final String exceptionLexModel$messsage = keyBase + ".exceptionLexModel.message";
	}

	private SdmxSerializingExporterConfiguration config;

	private final Map<String, List<String>> rdfPropToSdmxAnnTypeMap_forScheme = new HashMap<>();
	private final Map<String, List<String>> rdfPropToSdmxAnnTypeMap_forConcept = new HashMap<>();

	private final Map<String, List<String>> inverseRdfPropToSdmxAnnTypeMap_forConcept = new HashMap<>();


	public SdmxSerializingExporter(SdmxSerializingExporterConfiguration config) {
		this.config = config;
	}

	@Override
	public ClosableFormattedResource export(RepositoryConnection sourceRepositoryConnection, IRI[] graphs,
			@Nullable String format, ExporterContext exporterContext) throws IOException, ReformattingException {
		Objects.requireNonNull(format, "Format must be specified");


		if(!exporterContext.getProject().getModel().equals(Project.SKOS_MODEL)){
			//only SKOS Model should be used
			throw new ReformattingWrongModelException(STMessageSource.getMessage(MessageKeys.exceptionModel$messsage));
		}

		IRI lexModel = exporterContext.getProject().getLexicalizationModel();
		if(!lexModel.equals(Project.SKOS_LEXICALIZATION_MODEL) && !lexModel.equals(Project.SKOSXL_LEXICALIZATION_MODEL)) {
			//only SKOS and SKOSXL lexicalization are supported
			throw new ReformattingWrongLexModelException(STMessageSource.getMessage(MessageKeys.exceptionLexModel$messsage));
		}


		try {

			//initialize the maps about the RDF properties to the SDMX AnnotationType
			initializeRdfToSDMXMaps();

			//check if the target scheme exists
			if(!isAScheme(this.config.targetScheme, sourceRepositoryConnection)) {
				throw new ReformattingException(NTriplesUtil.toNTriplesString(this.config.targetScheme)+" is not a valid scheme");
			}


			//get the data about the scheme
			SchemeSDMXInfo schemeSDMXInfo = readScheme(this.config.targetScheme, sourceRepositoryConnection,
					this.config.altlabelsInsteadOfPreflabels);

			//get the labels (prefLabel) for the object of the property SdmxXmlVocabulary.STATUNITMEASURE_URI
			//Map<String, List<Literal>> stUnitIriToPrefLabelListMap = getLabelsForObjectOfProp(
			//		SdmxXmlVocabulary.STATUNITMEASURE_URI_NT, sourceRepositoryConnection);



			//get the data about the concepts
			Map<String, ConceptSDMXInfo> conceptIriToConceptSDMXInfoMap =  readConcepts(this.config.targetScheme,
					sourceRepositoryConnection, schemeSDMXInfo, this.config.altlabelsInsteadOfPreflabels);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();

			addDataIntoDoc(doc, schemeSDMXInfo, conceptIriToConceptSDMXInfoMap);


			// write the content into xml file
			TransformerFactory transformerFactory = XMLHelp.getSecuredTransformerFactory();
			Transformer transformer = transformerFactory.newTransformer();
			Properties outputProps = new Properties();
			outputProps.setProperty("encoding", "UTF-8");
			outputProps.setProperty("indent", "yes");
			outputProps.setProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.setOutputProperties(outputProps);
			DOMSource source = new DOMSource(doc);

			File tempServerFile = File.createTempFile("sdmx", ".xml");
			try (FileOutputStream os = new FileOutputStream(tempServerFile)) {
				StreamResult result = new StreamResult(os);
				transformer.transform(source, result);
				return new ClosableFormattedResource(tempServerFile, "xml", "application/xml",
						StandardCharsets.UTF_8, null);
			}

		} catch (ParserConfigurationException | TransformerException e) {
			throw new ReformattingWrongModelException(e);
		}

	}


	private void initializeRdfToSDMXMaps() {
		//initialize the map for the scheme
		rdfPropToSdmxAnnTypeMap_forScheme.put(SdmxXmlVocabulary.FOLLOWS_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forScheme.get(SdmxXmlVocabulary.FOLLOWS_URI).add(SdmxXmlVocabulary.FOLLOWS_ANN);
		rdfPropToSdmxAnnTypeMap_forScheme.put(SdmxXmlVocabulary.VARIANT_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forScheme.get(SdmxXmlVocabulary.VARIANT_URI).add(SdmxXmlVocabulary.HAS_VARIANT_ANN);
		rdfPropToSdmxAnnTypeMap_forScheme.put(SdmxXmlVocabulary.SUPERSEDES_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forScheme.get(SdmxXmlVocabulary.SUPERSEDES_URI).add(SdmxXmlVocabulary.SUPERSEED_ANN);
		rdfPropToSdmxAnnTypeMap_forScheme.put(SdmxXmlVocabulary.BELONGSTO_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forScheme.get(SdmxXmlVocabulary.BELONGSTO_URI).add(SdmxXmlVocabulary.FAMILY_LABEL_ANN);
		rdfPropToSdmxAnnTypeMap_forScheme.put(SdmxXmlVocabulary.COVERS_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forScheme.get(SdmxXmlVocabulary.COVERS_URI).add(SdmxXmlVocabulary.COVERAGE_LABEL_ANN);
		rdfPropToSdmxAnnTypeMap_forScheme.put(SdmxXmlVocabulary.BASEDON_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forScheme.get(SdmxXmlVocabulary.BASEDON_URI).add(SdmxXmlVocabulary.LEGAL_BASIS_ANN);
		rdfPropToSdmxAnnTypeMap_forScheme.put(SdmxXmlVocabulary.LEVELS_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forScheme.get(SdmxXmlVocabulary.LEVELS_URI).add(SdmxXmlVocabulary.HIER_LEVEL_ANN);
		rdfPropToSdmxAnnTypeMap_forScheme.put(SdmxXmlVocabulary.IDENTIFIER_DC_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forScheme.get(SdmxXmlVocabulary.IDENTIFIER_DC_URI).add(SdmxXmlVocabulary.ORIGINAL_CODE_ANN);
		rdfPropToSdmxAnnTypeMap_forScheme.put(SdmxXmlVocabulary.NOTATION_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forScheme.get(SdmxXmlVocabulary.NOTATION_URI).add(SdmxXmlVocabulary.SHORT_LABEL_ANN);


		//initialize the map for the concept
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.ORDER_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.ORDER_URI).add(SdmxXmlVocabulary.ORDER_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.REGIONORDER_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.REGIONORDER_URI).add(SdmxXmlVocabulary.REGION_ORDER_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.COUNTRYORDER_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.COUNTRYORDER_URI).add(SdmxXmlVocabulary.COUNTRY_ORDER_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.STATUNITMEASURE_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.STATUNITMEASURE_URI).add(SdmxXmlVocabulary.UNIT_MEASURE_LABEL_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.NOTATION_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.NOTATION_URI).add(SdmxXmlVocabulary.ORIGINAL_CODE_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.DEPTH_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.DEPTH_URI).add(SdmxXmlVocabulary.HIER_LEVEL_ANN);
		//rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.ALTLABEL_URI, new ArrayList<>());
		//rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.ALTLABEL_URI).add(SdmxXmlVocabulary.SHORT_LABEL_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.SCOPENOTE_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.SCOPENOTE_URI).add(SdmxXmlVocabulary.EXPLAN_LABEL_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.NOTE_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.NOTE_URI).add(SdmxXmlVocabulary.EXPLAN_LABEL_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.INCLUSIONNOTE_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.INCLUSIONNOTE_URI).add(SdmxXmlVocabulary.EXPLAN_INCLUDES_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.CORECONTENTNOTE_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.CORECONTENTNOTE_URI).add(SdmxXmlVocabulary.EXPLAN_INCLUDES_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.EXCLUSIONNOTE_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.EXCLUSIONNOTE_URI).add(SdmxXmlVocabulary.EXPLAN_EXCLUDES_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.ADDITIONALCONTENTNOTE_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.ADDITIONALCONTENTNOTE_URI).add(SdmxXmlVocabulary.EXPLAN_INCLUDES_ALSO_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.CASELAW_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.CASELAW_URI).add(SdmxXmlVocabulary.EXPLAN_CASELAW_ANN);
		//rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.MAPPING_URI, new ArrayList<>());
		//rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.MAPPING_URI).add(SdmxXmlVocabulary.MAP_REFER_ANN);
		//rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.DEPRECATED_URI, new ArrayList<>());
		//rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.DEPRECATED_URI).add(SdmxXmlVocabulary.DEPRECATED_ANN);
		rdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.TYPE_URI, new ArrayList<>());
		rdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.TYPE_URI).add(SdmxXmlVocabulary.PRODUCTION_TYPE_ANN);


		inverseRdfPropToSdmxAnnTypeMap_forConcept.put(SdmxXmlVocabulary.MEMBER_URI, new ArrayList<>());
		inverseRdfPropToSdmxAnnTypeMap_forConcept.get(SdmxXmlVocabulary.MEMBER_URI).add(SdmxXmlVocabulary.HIER_LEVEL_ANN);

	}

	private boolean isAScheme(IRI targetScheme, RepositoryConnection sourceRepositoryConnection) {
		String query = "SELECT *" +
				"\nWHERE { " +
				"\n"+NTriplesUtil.toNTriplesString(targetScheme)+" a " + NTriplesUtil.toNTriplesString(SKOS.CONCEPT_SCHEME) + "." +
				"\n"+NTriplesUtil.toNTriplesString(targetScheme)+" a ?type ." +
				"\n}" +
				"\nLIMIT 1";
		TupleQuery tq = sourceRepositoryConnection.prepareTupleQuery(query);

		try (TupleQueryResult result = tq.evaluate()) {
			return result.hasNext();
		}
	}

	private SchemeSDMXInfo readScheme(IRI targetScheme, RepositoryConnection sourceRepositoryConnection,
							boolean useAltLabelInsteadofPrefLabel) {
		SchemeSDMXInfo schemeSDMXInfo = new SchemeSDMXInfo(targetScheme.stringValue());
		schemeSDMXInfo.setVersion("1.0");

		//read the wanted scheme, from the dataset, get ALL its associated value
		// (so get the RDF triples in which the scheme is the subject )
		// and compare the properties with the values in rdfPropToSdmxAnnTypeMap_forScheme, if they match, add the value
		// to the SchmeSDMXInfo
		String query = "SELECT ?pred ?obj " +
				"\nWHERE {" +
				"\n"+NTriplesUtil.toNTriplesString(targetScheme)+" ?pred ?obj ." +
				"\n}";
		TupleQuery tq = sourceRepositoryConnection.prepareTupleQuery(query);

		try (TupleQueryResult result = tq.evaluate()) {
			while (result.hasNext()) {
				BindingSet bindingSet = result.next();
				//check if it is a special case. If so, manage the special case, otherwise manage the normal case
				if (isSpecialCaseForScheme(bindingSet)) {
					manageSpecialCaseForScheme(bindingSet, schemeSDMXInfo);
				} else {
					manageNormalCaseForScheme(bindingSet, schemeSDMXInfo);
				}
			}
		}

		//if a default value for the AgencyId is provided, then use such value (so override any other possible value
		// used for setAgencyId, if any)
		if (this.config.agencyId!=null && !this.config.agencyId.isEmpty()) {
			schemeSDMXInfo.setAgencyId(this.config.agencyId);
		}

		return schemeSDMXInfo;
	}

	private boolean isSpecialCaseForScheme(BindingSet bindingSet) {

		if (!bindingSet.getValue("pred").isIRI()) {
			// if "pred" is not an IRI, return false immediately
			return false;
		}

		//it is a special case if the value for "pred" is one of the following:
		// - dc:identifier
		// - dcterms:identifier  // added since it seems that some schemes use this and not the dc:identifier
		// - dct:creator
		// - schema:startDate
		// - schema:endDate
		// -skos:altLabel or skos:prefLabel (depends on the value of useAltLabelInteadOfPrefLabel)
		// -dct:description or skos:scopeNote
		String propUriString = bindingSet.getValue("pred").stringValue();
		return propUriString.equals(SdmxXmlVocabulary.IDENTIFIER_DC_URI) || propUriString.equals(SdmxXmlVocabulary.IDENTIFIER_DCTERMS_URI)
				|| propUriString.equals(SdmxXmlVocabulary.CREATOR_URI) ||
				propUriString.equals(SdmxXmlVocabulary.STARTDATE_URI) || propUriString.equals(SdmxXmlVocabulary.ENDDATE_URI) ||
				propUriString.equals(SdmxXmlVocabulary.PREFLABEL_URI) ||
				propUriString.equals(SdmxXmlVocabulary.DESCRIPTION_URI) || propUriString.equals(SdmxXmlVocabulary.SCOPENOTE_URI) ;
	}

	private void manageSpecialCaseForScheme(BindingSet bindingSet, SchemeSDMXInfo schemeSDMXInfo) {
		// it is a special case, so, depending on the value of "pref", fill the corresponding value of SchemeSDMXInfo
		String propUriString = bindingSet.getValue("pred").stringValue();
		Value obj = bindingSet.getValue("obj");

		if (!bindingSet.getValue("pred").isIRI()) {
			// if "pred" is not an IRI, return false immediatelly
			return;
		}

		/*
		//do a check if useAltLabelInsteadofPrefLabel is true and propUriString is skos:prefLabel or if
		// useAltLabelInsteadofPrefLabel is false and propUriString is skos:altLabel, in these cases, just return
		if ((useAltLabelInsteadofPrefLabel && propUriString.equals(SdmxXmlVocabulary.PREFLABEL_URI)) ||
				(!useAltLabelInsteadofPrefLabel && propUriString.equals(SdmxXmlVocabulary.ALTLABEL_URI))) {
			return;
		}
		 */

		switch (propUriString) {
			case SdmxXmlVocabulary.IDENTIFIER_DC_URI ->  schemeSDMXInfo.setId1(obj.stringValue().toUpperCase(Locale.ROOT));
			case SdmxXmlVocabulary.IDENTIFIER_DCTERMS_URI ->  schemeSDMXInfo.setId2(sanitizeIdFromDcterms(obj.stringValue()));
			case SdmxXmlVocabulary.CREATOR_URI ->   schemeSDMXInfo.setAgencyId(obj.isIRI() ? ((IRI) obj).getLocalName() : obj.stringValue());
			case SdmxXmlVocabulary.STARTDATE_URI -> schemeSDMXInfo.setValidFrom(obj.stringValue());
			case SdmxXmlVocabulary.ENDDATE_URI -> schemeSDMXInfo.setValidTo(obj.stringValue());
			case SdmxXmlVocabulary.PREFLABEL_URI -> schemeSDMXInfo.addName(obj);
			case SdmxXmlVocabulary.DESCRIPTION_URI, SdmxXmlVocabulary.SCOPENOTE_URI -> schemeSDMXInfo.addDescription(obj);
		}
	}

	private String sanitizeIdFromDcterms(String id) {
		//sanitize the value, by taking the string after the last "/" and set to upper case
		int posLastSlash = id.lastIndexOf("/");
		return id.substring(posLastSlash+1).toUpperCase(Locale.ROOT);
	}

	private void manageNormalCaseForScheme(BindingSet bindingSet, SchemeSDMXInfo schemeSDMXInfo) {

		String propUriString = bindingSet.getValue("pred").stringValue();
		Value obj = bindingSet.getValue("obj");

		if (!bindingSet.getValue("pred").isIRI()) {
			// if "pred" is not an IRI, return false immediately
			return;
		}

		//check if the "propUriString" is the key rdfPropToSdmxAnnTypeMap_forScheme, if not, just return

		List<String> annTypeList = rdfPropToSdmxAnnTypeMap_forScheme.get(propUriString);
		if (annTypeList == null || annTypeList.isEmpty()) {
			return;
		}
		for (String annType : annTypeList) {
			schemeSDMXInfo.addValueToAnnotationType(obj, annType);
		}
	}

	/*
	private Map<String, List<Literal>> getLabelsForObjectOfProp(String propIriNT,
															   RepositoryConnection sourceRepositoryConnection) {
		Map<String, List<Literal>> stunitIriToPrefLabelListMap = new HashMap<>();

		String query = "SELECT DISTINCT ?obj ?label" +
				"\nWHERE {" +
				"\n?subj "+propIriNT+" ?obj ." +
				"\n?obj "+NTriplesUtil.toNTriplesString(SKOS.PREF_LABEL)+" ?label ." +
				"\n}";

		TupleQuery tq = sourceRepositoryConnection.prepareTupleQuery(query);
		try (TupleQueryResult result = tq.evaluate()) {
			while (result.hasNext()) {
				BindingSet bindingSet = result.next();
				if (!bindingSet.getValue("obj").isIRI()) {
					// the obj is not an IRI, so skip it
					continue;
				}
				if (!bindingSet.getValue("label").isLiteral()) {
					// the label is not a Literal, so skip it
					continue;
				}
				String obj = bindingSet.getValue("obj").stringValue();
				Literal label = (Literal) bindingSet.getValue("label");

				if (!stunitIriToPrefLabelListMap.containsKey(obj)) {
					stunitIriToPrefLabelListMap.put(obj, new ArrayList<>());
				}
				stunitIriToPrefLabelListMap.get(obj).add(label);
			}
		}
		return stunitIriToPrefLabelListMap;
	}
	*/


	private Map<String, ConceptSDMXInfo> readConcepts(IRI targetScheme, RepositoryConnection sourceRepositoryConnection,
													  SchemeSDMXInfo schemeSDMXInfo, boolean useAltLabelInsteadofPrefLabel) {

		Map<String, ConceptSDMXInfo> conceptIriToConceptSDMXInfoMap = new HashMap<>();
		// read the concepts, belonging to the wanted scheme from the dataset, get ALL their assocaited value
		// (so get the RDF triples in which the scheme is the subject ) and compare the properties with the values in
		// rdfPropToSdmxAnnTypeMap_forConcept, if they match, add the value to the ConceptSDMXInfoList

		//get the list of mapping properties
		List<IRI> mappingProperiesList = getMappingSkosProperties(sourceRepositoryConnection);


		String query = "SELECT ?concept ?pred ?obj " +
				"\nWHERE {" +
				"\n?concept a "+NTriplesUtil.toNTriplesString(SKOS.CONCEPT)+" . " +
				"\n?concept "+NTriplesUtil.toNTriplesString(SKOS.IN_SCHEME)+" "+NTriplesUtil.toNTriplesString(targetScheme)+" ." +
				"\n?concept ?pred ?obj ." +
				"\n}";
		TupleQuery tq = sourceRepositoryConnection.prepareTupleQuery(query);
		try (TupleQueryResult result = tq.evaluate()) {
			while (result.hasNext()) {
				BindingSet bindingSet = result.next();
				manageConceptData(conceptIriToConceptSDMXInfoMap, bindingSet, schemeSDMXInfo,
						useAltLabelInsteadofPrefLabel, mappingProperiesList);
			}
		}

		//there is the special case of the use of SdmxXmlVocabulary.MEMBER_URI in which the concept is the OBJECT of
		// the property and not the subject, so do a SPARQL query to manage such case
		query = "SELECT ?subj ?pred ?concept" +
				"\nWHERE {"+
				"\n?concept a "+NTriplesUtil.toNTriplesString(SKOS.CONCEPT)+" . " +
				"\n?subj "+SdmxXmlVocabulary.MEMBER_URI_NT+" ?concept ." + // single special case (very efficient)
				"\n?subj ?pred ?concept ." +
				"\n}";
		tq = sourceRepositoryConnection.prepareTupleQuery(query);
		try (TupleQueryResult result = tq.evaluate()) {
			while (result.hasNext()) {
				BindingSet bindingSet = result.next();
				manageConceptDataInverseProp(conceptIriToConceptSDMXInfoMap, bindingSet, schemeSDMXInfo);
			}
		}

		return conceptIriToConceptSDMXInfoMap;
	}

	public List<IRI> getMappingSkosProperties(RepositoryConnection sourceRepositoryConnection) {
		List<IRI> mappingPropList = new ArrayList<>();
		String query = "SELECT ?mappingProp " +
				"\nWHERE {" +
				"\n?mappingProp "+NTriplesUtil.toNTriplesString(RDFS.SUBPROPERTYOF)+"+ "
						+ NTriplesUtil.toNTriplesString(SKOS.MAPPING_RELATION)+" ." +
				"\n}";
		TupleQuery tq = sourceRepositoryConnection.prepareTupleQuery(query);
		try (TupleQueryResult result = tq.evaluate()) {
			while (result.hasNext()) {
				BindingSet bindingSet = result.next();
				if (bindingSet.getValue("mappingProp").isIRI()) {
					continue;
				}
				mappingPropList.add((IRI) bindingSet.getValue("mappingProp"));
			}
		}
		return mappingPropList;
	}


	private void manageConceptData(Map<String, ConceptSDMXInfo> conceptIriToConceptSDMXInfoMap,
								   BindingSet bindingSet, SchemeSDMXInfo schemeSDMXInfo,
								   boolean useAltLabelInsteadofPrefLabel, List<IRI> mappingProperiesList) {

		if (!bindingSet.getValue("concept").isIRI()) {
			// the "concept" is not an IRI, so return
			return;
		}
		if (!bindingSet.getValue("pred").isIRI()) {
			// if "pred" is not an IRI, so return
			return;
		}



		String conceptUriString = bindingSet.getValue("concept").stringValue();
		IRI propUri = (IRI) bindingSet.getValue("pred");
		String propUriString = propUri.stringValue();

		Value obj = bindingSet.getValue("obj");

		//get the ConceptSDMXInfo (or create one) from conceptIriToConceptSDMXInfoMap for conceptUriString
		if (!conceptIriToConceptSDMXInfoMap.containsKey(conceptUriString)) {
			ConceptSDMXInfo conceptSDMXInfo = new ConceptSDMXInfo(conceptUriString);
			conceptSDMXInfo.setSchemeSDMXInfo(schemeSDMXInfo);
			conceptIriToConceptSDMXInfoMap.put(conceptUriString, conceptSDMXInfo);
		}
		ConceptSDMXInfo conceptSDMXInfo = conceptIriToConceptSDMXInfoMap.get(conceptUriString);

		//manage the special cases
		switch (propUriString) {
			case SdmxXmlVocabulary.IDENTIFIER_DC_URI -> conceptSDMXInfo.setId(obj.stringValue());
			case SdmxXmlVocabulary.NOTATION_URI -> conceptSDMXInfo.addName(obj);
			case SdmxXmlVocabulary.BROADER_URI -> conceptSDMXInfo.addParent(obj);
			// this will be considered only when the NOTATION_URI is not present in that concept
			case SdmxXmlVocabulary.PREFLABEL_URI -> conceptSDMXInfo.addBackupName(obj);
		}

		//manage the Description case (if useAltLabelInsteadofPrefLabel is true, SdmxXmlVocabulary.ALTLABEL_URI
		// otherwise use SdmxXmlVocabulary.PREFLABEL_URI)
		if (useAltLabelInsteadofPrefLabel) {
			if (propUriString.equals(SdmxXmlVocabulary.ALTLABEL_URI)) {
				conceptSDMXInfo.addDescription(obj);
			}
		} else {
			if (propUriString.equals(SdmxXmlVocabulary.PREFLABEL_URI )) {
				conceptSDMXInfo.addDescription(obj);
			}
		}

		//now manage the other cases, using the rdfPropToSdmxAnnTypeMap_forConcept
		List<String> annTypeList = rdfPropToSdmxAnnTypeMap_forConcept.get(propUriString);
		if (annTypeList == null || annTypeList.isEmpty()) {
			return;
		}
		for (String annType : annTypeList) {
			conceptSDMXInfo.addValueToAnnotationType(obj, annType);
		}


		/*
		//manage the special case of SdmxXmlVocabulary.STATUNITMEASURE_URI
		if (propUriString.equals(SdmxXmlVocabulary.STATUNITMEASURE_URI)) {
			//this is a special case, so use the stUnitIriToPrefLabelListMap
			if (obj.isIRI()) {
				List<Literal> labelList = stUnitIriToPrefLabelListMap.get(obj.stringValue());
				if (labelList != null) {
					for (Literal label : labelList) {
						conceptSDMXInfo.addValueToAnnotationType(label, SdmxXmlVocabulary.UNIT_MEASURE_LABEL_ANN);
					}
				} else {
					// a Label was not found for that valud of SdmxXmlVocabulary.STATUNITMEASURE_URI, so use the
					// local name of the object
					conceptSDMXInfo.addValueToAnnotationType((IRI)obj)., SdmxXmlVocabulary.UNIT_MEASURE_LABEL_ANN);
				}
			}
		}

		 */

		//special case for SdmxXmlVocabulary.DEPRECATED_URI (the obj must be "true")
		if (propUriString.equals(SdmxXmlVocabulary.DEPRECATED_URI) && obj.stringValue().equalsIgnoreCase("true")) {
			conceptSDMXInfo.addValueToAnnotationType(obj, SdmxXmlVocabulary.DEPRECATED_ANN);
		}

		//special case for the SdmxXmlVocabulary.MAP_REFER_ANN (use mappingProperiesList)
		if (mappingProperiesList.contains(propUri)) {
			conceptSDMXInfo.addValueToMappingProperties(obj, propUri);
		}
	}

	private void manageConceptDataInverseProp(Map<String, ConceptSDMXInfo> conceptIriToConceptSDMXInfoMap,
											  BindingSet bindingSet, SchemeSDMXInfo schemeSDMXInfo) {

		if (!bindingSet.getValue("subj").isIRI()) {
			// if "subj" is not an IRI, return false immediately
			return;
		}
		if (!bindingSet.getValue("pred").isIRI()) {
			// if "pred" is not an IRI, so return
			return;
		}
		if (!bindingSet.getValue("concept").isIRI()) {
			// the "concept" is not an IRI, so return
			return;
		}


		String propUriString = bindingSet.getValue("pred").stringValue();
		Value subj = bindingSet.getValue("subj");
		String conceptUriString = bindingSet.getValue("concept").stringValue();

		//get the ConceptSDMXInfo (or create one) from conceptIriToConceptSDMXInfoMap for conceptUriString
		if (!conceptIriToConceptSDMXInfoMap.containsKey(conceptUriString)) {
			ConceptSDMXInfo conceptSDMXInfo = new ConceptSDMXInfo(conceptUriString);
			conceptSDMXInfo.setSchemeSDMXInfo(schemeSDMXInfo);
			conceptIriToConceptSDMXInfoMap.put(conceptUriString, conceptSDMXInfo);
		}
		ConceptSDMXInfo conceptSDMXInfo = conceptIriToConceptSDMXInfoMap.get(conceptUriString);

		List<String> annTypeList = inverseRdfPropToSdmxAnnTypeMap_forConcept.get(propUriString);
		if (annTypeList == null || annTypeList.isEmpty()) {
			return;
		}
		for (String annType : annTypeList) {
			conceptSDMXInfo.addValueToAnnotationType(subj, annType);
		}

	}

	private void addDataIntoDoc(Document doc, SchemeSDMXInfo schemeSDMXInfo,
								Map<String, ConceptSDMXInfo> conceptIriToConceptSDMXInfoMap) {



		//List<IRI> conceptList = getConceptAndHierarchyList(sourceRepositoryConnection);

		//create the message:Structure
		Element mesStructureElem = doc.createElement(SdmxXmlVocabulary.MESSAGE_STRUCTURE);
		mesStructureElem.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		mesStructureElem.setAttribute("xmlns:message", "http://www.sdmx.org/resources/sdmxml/schemas/v3_0/message");
		mesStructureElem.setAttribute("xmlns:str", "http://www.sdmx.org/resources/sdmxml/schemas/v3_0/structure");
		mesStructureElem.setAttribute("xmlns:com", "http://www.sdmx.org/resources/sdmxml/schemas/v3_0/common");
		doc.appendChild(mesStructureElem);


		//create the message:Header
		Element mesHeaderElem = doc.createElement(SdmxXmlVocabulary.MESSAGE_HEADER);
		mesStructureElem.appendChild(mesHeaderElem);
		Element mesIdElem = doc.createElement(SdmxXmlVocabulary.MESSAGE_ID);
		String randPartFromTimeMillis = System.currentTimeMillis()+"";
		randPartFromTimeMillis = randPartFromTimeMillis.substring(5);
		mesIdElem.setTextContent("IREF"+randPartFromTimeMillis);// TODO add a real value
		mesHeaderElem.appendChild(mesIdElem);
		Element mesTestElem = doc.createElement(SdmxXmlVocabulary.MESSAGE_TEST);
		mesTestElem.setTextContent("false");
		mesHeaderElem.appendChild(mesTestElem);
		Element mesPreparedElem = doc.createElement(SdmxXmlVocabulary.MESSAGE_PREPARED);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		mesPreparedElem.setTextContent(sdf.format(Calendar.getInstance().getTime()));
		mesHeaderElem.appendChild(mesPreparedElem);

		//create the message:Structures
		Element mesStructuresElem = doc.createElement(SdmxXmlVocabulary.MESSAGE_STRUCTURES);
		mesStructureElem.appendChild(mesStructuresElem);

		//create the str:Codelists
		Element strCodelistsElem = doc.createElement(SdmxXmlVocabulary.STR_CODELISTS);
		mesStructuresElem.appendChild(strCodelistsElem);


		//create the str:Codelist
		Element strCodelistElem = doc.createElement(SdmxXmlVocabulary.STR_CODELIST);
		strCodelistsElem.appendChild(strCodelistElem);
		//add all the data into the element strCodelistElem
		//first the attribute
		strCodelistElem.setAttribute(SdmxXmlVocabulary.URN, schemeSDMXInfo.getUrn());
		strCodelistElem.setAttribute(SdmxXmlVocabulary.ISEXTERNALREFERENCE, schemeSDMXInfo.isExternalReference());
		strCodelistElem.setAttribute(SdmxXmlVocabulary.AGENCYID, schemeSDMXInfo.getAgencyId());
		strCodelistElem.setAttribute(SdmxXmlVocabulary.ID, schemeSDMXInfo.getId());
		strCodelistElem.setAttribute(SdmxXmlVocabulary.VALIDFROM, schemeSDMXInfo.getValidFrom());
		strCodelistElem.setAttribute(SdmxXmlVocabulary.VERSION, schemeSDMXInfo.getVersion());
		strCodelistElem.setAttribute(SdmxXmlVocabulary.VALIDTO, schemeSDMXInfo.getValidTo());
		// then the Annotations
		Element strAnnotationsElem = doc.createElement(SdmxXmlVocabulary.COM_ANNOTATIONS);
		strCodelistElem.appendChild(strAnnotationsElem);
		for (String annType : schemeSDMXInfo.getAnnotationTypeToValueMap().keySet()) {
			List<Value> valueList = schemeSDMXInfo.getAnnotationTypeToValueMap().get(annType);
			for (Value value : valueList) {
				createAnnotationElement(annType, value, doc, strAnnotationsElem, schemeSDMXInfo.getId());
			}
		}

		//finally the <com:Name> and <com:Description>
		for (Literal literal : schemeSDMXInfo.getNameList()) {
			addElement(SdmxXmlVocabulary.COM_NAME, literal, doc, strCodelistElem);
		}
		for (Literal literal : schemeSDMXInfo.getDescriptionList()) {
			addElement(SdmxXmlVocabulary.COM_DESCRIPTION, literal, doc, strCodelistElem);
		}



		//add the concepts/<str:Code>
		for (String concept : conceptIriToConceptSDMXInfoMap.keySet()) {
			ConceptSDMXInfo conceptSDMXInfo = conceptIriToConceptSDMXInfoMap.get(concept);

			//create the <str:Code> and add its attribute
			Element strCodeElem = doc.createElement(SdmxXmlVocabulary.STR_CODE);
			strCodelistElem.appendChild(strCodeElem);
			strCodeElem.setAttribute(SdmxXmlVocabulary.URN, conceptSDMXInfo.getUrn());
			strCodeElem.setAttribute(SdmxXmlVocabulary.ID, conceptSDMXInfo.getId());

			//add the Annotations
			strAnnotationsElem = doc.createElement(SdmxXmlVocabulary.COM_ANNOTATIONS);
			strCodeElem.appendChild(strAnnotationsElem);
			for (String annType : conceptSDMXInfo.getAnnotationTypeToValueMap().keySet()) {
				List<Value> valueList = conceptSDMXInfo.getAnnotationTypeToValueMap().get(annType);
				for (Value value : valueList) {
					createAnnotationElement(annType, value, doc, strAnnotationsElem, conceptSDMXInfo.getId());
				}
			}

			//finally the <com:Name>, <com:Description> and <strParent>
			//for SdmxXmlVocabulary.COM_NAME, if conceptSDMXInfo.getNameList() is empty, then use conceptSDMXInfo.getBackupNameList()
			if (!conceptSDMXInfo.getNameList().isEmpty()) {
				for (Literal literal : conceptSDMXInfo.getNameList()) {
					addElement(SdmxXmlVocabulary.COM_NAME, literal, doc, strCodeElem);
				}
			} else {
				for (Literal literal : conceptSDMXInfo.getBackupNameList()) {
					addElement(SdmxXmlVocabulary.COM_NAME, literal, doc, strCodeElem);
				}
			}
			for (Literal literal : conceptSDMXInfo.getDescriptionList()) {
				addElement(SdmxXmlVocabulary.COM_DESCRIPTION, literal, doc, strCodeElem);
			}
			for (IRI iri : conceptSDMXInfo.getParentList()) {
				addElement(SdmxXmlVocabulary.STR_PARENT, iri, doc, strCodeElem);
			}

		}

	}

	private void createAnnotationElement(String annType, Value value, Document doc, Element strAnnotationsElem,
										 String idForAnnotation) {
		boolean toAdd = false;
		Element strAnnotationElem = doc.createElement(SdmxXmlVocabulary.COM_ANNOTATION);

		//add the id to the COM_ANNOTATION
		strAnnotationElem.setAttribute(SdmxXmlVocabulary.ID, idForAnnotation);

		Element strAnnotationTypeElem = doc.createElement(SdmxXmlVocabulary.COM_ANNOTATIONTYPE);
		strAnnotationTypeElem.setTextContent(annType);
		strAnnotationElem.appendChild(strAnnotationTypeElem);

		//the value is a Literal
		if (value.isLiteral()) {
			toAdd = true;
			Literal literal = (Literal) value;
			Element strAnnotationTextElem = doc.createElement(SdmxXmlVocabulary.COM_ANNOTATIONTEXT);
			strAnnotationTextElem.setTextContent(literal.stringValue());
			strAnnotationElem.appendChild(strAnnotationTextElem);
			if (literal.getLanguage().isPresent()) {
				strAnnotationTextElem.setAttribute(SdmxXmlVocabulary.XML_LANG, literal.getLanguage().get());
			} else if (literal.getDatatype()!=null && literal.getDatatype().isIRI() &&
					!literal.getDatatype().stringValue().isEmpty()) {
				Element strAnnotationTitleElem = doc.createElement(SdmxXmlVocabulary.COM_ANNOTATIONTITLE);
				strAnnotationTitleElem.setTextContent(literal.getDatatype().stringValue());
				strAnnotationElem.appendChild(strAnnotationTitleElem);
			}
		} else if (value.isIRI()) {
			IRI iri = (IRI) value;
			toAdd = true;
			Element strAnnotationUrlElem = doc.createElement(SdmxXmlVocabulary.COM_ANNOTATIONURL);
			strAnnotationUrlElem.setTextContent(iri.stringValue());
			strAnnotationElem.appendChild(strAnnotationUrlElem);
			Element strAnnotationTextElem = doc.createElement(SdmxXmlVocabulary.COM_ANNOTATIONTEXT);
			strAnnotationTextElem.setTextContent(iri.getLocalName());
			strAnnotationElem.appendChild(strAnnotationTextElem);
		}

		if (toAdd) {
			strAnnotationsElem.appendChild(strAnnotationElem);
		}
	}

	private void addElement(String elemName, Literal literal, Document doc, Element parentElem) {
		Element element = doc.createElement(elemName);
		element.setTextContent(literal.stringValue());
		if (literal.getLanguage().isPresent()) {
			element.setAttribute(SdmxXmlVocabulary.XML_LANG, literal.getLanguage().get());
		}
		parentElem.appendChild(element);
	}

	private void addElement(String elemName, IRI iri, Document doc, Element parentElem) {
		Element element = doc.createElement(elemName);
		element.setTextContent(iri.stringValue());
		parentElem.appendChild(element);
	}


	/*
	//list of method to get the data from the current project
	private Map<IRI, SchemeSDMXInfo_OLD> getSchemeInfo(RepositoryConnection sourceRepositoryConnection) {
		//do a SPARQL query to get ALL the scheme, and, for each scheme, get the following properties (they might
		// not be in there):
		// - dc:identifier

		String query = "SELECT ?scheme ?id ?broader ?narrower\n" +
				"WHERE { \n" +
				"?concept a " + NTriplesUtil.toNTriplesString(SKOS.CONCEPT) + ".\n" +
				"OPTIONAL{?concept " + NTriplesUtil.toNTriplesString(SKOS.BROADER) + " ?broader .}\n" +
				"OPTIONAL{?concept " + NTriplesUtil.toNTriplesString(SKOS.NARROWER) + " ?broader .}\n" +
				"}";
		TupleQuery tq = sourceRepositoryConnection.prepareTupleQuery(query);
	}

	private Map<IRI, List<IRI>> getConceptAndHierarchyList(RepositoryConnection sourceRepositoryConnection) {
		String query = "SELECT ?concept ?broader ?narrower\n" +
				"WHERE { \n" +
				"?concept a " + NTriplesUtil.toNTriplesString(SKOS.CONCEPT) + ".\n" +
				"OPTIONAL{?concept " + NTriplesUtil.toNTriplesString(SKOS.BROADER) + " ?broader .}\n" +
				"OPTIONAL{?concept " + NTriplesUtil.toNTriplesString(SKOS.NARROWER) + " ?broader .}\n" +
				"}";
		TupleQuery tq = sourceRepositoryConnection.prepareTupleQuery(query);

		Map<IRI, List<IRI>> conceptToBroaderListMap = new HashMap<>();

		try (TupleQueryResult result = tq.evaluate()) {
			while (result.hasNext()) {
				conceptList.add((IRI) result.next().getValue("concept"));
			}
		}

		return conceptList;
	}
	*/

}
