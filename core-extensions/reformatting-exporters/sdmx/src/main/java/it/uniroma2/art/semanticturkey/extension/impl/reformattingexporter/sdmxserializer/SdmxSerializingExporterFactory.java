package it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.sdmxserializer;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import it.uniroma2.art.semanticturkey.extension.ConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.PUScopedConfigurableComponent;
import it.uniroma2.art.semanticturkey.extension.extpts.commons.io.FormatCapabilityProvider;
import it.uniroma2.art.semanticturkey.i18n.STMessageSource;
import it.uniroma2.art.semanticturkey.resources.DataFormat;
import org.springframework.stereotype.Component;

/**
 * Factory for the instantiation of {@link SdmxSerializingExporter}.
 * 
 * @author <a href="mailto:turbati@info.uniroma2.it">Andrea Turbati</a>
 */
@Component
public class SdmxSerializingExporterFactory
	implements ConfigurableExtensionFactory<SdmxSerializingExporter, SdmxSerializingExporterConfiguration>,
		PUScopedConfigurableComponent<SdmxSerializingExporterConfiguration>, FormatCapabilityProvider {

	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.reformattingexporter.smdxserializer.SdmxSerializingExporterFactory";
		private static final String name = keyBase + ".name";
		private static final String description = keyBase + ".description";
	}

	@Override
	public String getName() {
		return STMessageSource.getMessage(MessageKeys.name);
	}

	@Override
	public String getDescription() {
		return STMessageSource.getMessage(MessageKeys.description);
	}

	@Override
	public SdmxSerializingExporter createInstance(SdmxSerializingExporterConfiguration conf) {
		return new SdmxSerializingExporter(conf);
	}
	
	@Override
	public Collection<SdmxSerializingExporterConfiguration> getConfigurations() {
		return Arrays.asList(new SdmxSerializingExporterConfiguration());
	}

	@Override
	public List<DataFormat> getFormats() {
		return Arrays.asList(new DataFormat("XML", "application/xml", "xml"));
	}

}
