package it.uniroma2.art.semanticturkey.extension.impl.repositoryimplconfigurer.predefined;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class PredefinedRepositoryImplConfigurerPlugin extends STPlugin {
    public PredefinedRepositoryImplConfigurerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
