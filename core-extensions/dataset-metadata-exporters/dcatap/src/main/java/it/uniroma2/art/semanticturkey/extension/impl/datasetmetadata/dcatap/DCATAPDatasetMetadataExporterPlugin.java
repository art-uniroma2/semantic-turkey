package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.dcatap;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class DCATAPDatasetMetadataExporterPlugin extends STPlugin {
    public DCATAPDatasetMetadataExporterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
