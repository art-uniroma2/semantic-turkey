package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.dcat;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class DCATDatasetMetadataExporterPlugin extends STPlugin {
    public DCATDatasetMetadataExporterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
