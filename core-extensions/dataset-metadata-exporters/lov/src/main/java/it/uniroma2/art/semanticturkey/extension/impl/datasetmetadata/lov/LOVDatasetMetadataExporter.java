package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.lov;

import it.uniroma2.art.semanticturkey.extension.extpts.datasetmetadata.DatasetMetadataExporter;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetmetadata.DatasetMetadataExporterException;
import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.mdr.bindings.STMetadataRegistryBackend;
import it.uniroma2.art.semanticturkey.mdr.core.DatasetMetadata;
import it.uniroma2.art.semanticturkey.mdr.core.MetadataRegistryStateException;
import it.uniroma2.art.semanticturkey.mdr.core.NoSuchDatasetMetadataException;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.resources.Scope;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.ObjectUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.query.impl.SimpleDataset;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.query.QueryStringUtil;

import jakarta.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A {@link DatasetMetadataExporter} for the <a href="https://lov.linkeddata.es/Recommendations_Vocabulary_Design.pdf">Metadata Recommendations For Linked
 * Open Data Vocabularies</a>
 * 
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">manuel Fiorelli</a>
 */
public class LOVDatasetMetadataExporter implements DatasetMetadataExporter {

	private final LOVDatasetMetadataExporterFactory factory;

	public LOVDatasetMetadataExporter(LOVDatasetMetadataExporterFactory factory) {
		this.factory = factory;
	}

	@Override
	public Model produceDatasetMetadata(Project project, RepositoryConnection conn, IRI dataGraph)
			throws DatasetMetadataExporterException {
		throw new NotImplementedException("LOV metadata are stored inside the ontology itself");
	}

	@Override
	public void addMetadataToDataset(Project project, RepositoryConnection conn, IRI dataGraph)
			throws DatasetMetadataExporterException {

		try {
			ValueFactory vf = conn.getValueFactory();

			LOVDatasetMetadataExporterSettings pluginSettings = factory.getProjectSettings(project);

			// vann:preferredNamespacePrefix
			Literal preferredNamespacePrefix = vf.createLiteral(pluginSettings.preferredNamespacePrefix);

			// vann:preferredNamespaceUri
			Literal preferredNamespaceUri = vf.createLiteral(project.getDefaultNamespace());

			// dct:title
			List<Literal> title = pluginSettings.title;

			// dct:description
			List<Literal> description = pluginSettings.description;

			// dct:issued
			Literal issued = vf.createLiteral(pluginSettings.issued);

			// dct:modified
			Literal modified = vf.createLiteral(pluginSettings.modified);

			// owl:versionInfo
			Literal versionInfo = vf.createLiteral(pluginSettings.versionInfo);

			// dct:creator
			List<IRI> creator = ObjectUtils.firstNonNull(pluginSettings.creator, Collections.emptyList());

			// dct:contributor
			List<IRI> contributor = ObjectUtils.firstNonNull(pluginSettings.contributor, Collections.emptyList());

			// dct:publisher
			List<IRI> publisher = ObjectUtils.firstNonNull(pluginSettings.publisher, Collections.emptyList());

			// dct:rights
			String rights = pluginSettings.rights;

			// cc:license
			IRI license = pluginSettings.license;

			// voaf:similar
			List<IRI> similarVoc = ObjectUtils.firstNonNull(pluginSettings.similarVocabulary, Collections.emptyList());

			//voaf:classNumber
			Literal classNumber = null;
			
			//voaf:propertyNumber
			Literal propertyNumber = null;

			//voaf:metadataVoc
			List<IRI> metadataVoc = Collections.emptyList();

			//voaf:extends
			List<IRI> extendedVoc = Collections.emptyList();

			//voaf:specializes
			List<IRI> specializedVoc = Collections.emptyList();

			//voaf:generalizes
			List<IRI> generalizedVoc = Collections.emptyList();

			//voaf:hasEquivalencesWith
			List<IRI> hasEquivalencesWithVoc = Collections.emptyList();

			//voaf:hasDisjunctionsWith
			List<IRI> hasDisjunctionsWithVoc = Collections.emptyList();

			if (pluginSettings.computeMetadata) {
				SimpleDataset datasetForEditableGraph = new SimpleDataset();
				datasetForEditableGraph.addDefaultGraph(dataGraph);

				//voaf:classNumber
				TupleQuery classNumberQuery = conn.prepareTupleQuery("PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" +
						"PREFIX owl:<http://www.w3.org/2002/07/owl#>\n" +
						"PREFIX voaf:<http://purl.org/vocommons/voaf#>\n" +
						"\n" +
						"SELECT (COUNT(distinct ?class) AS ?nbClass)\n" +
						"WHERE{\n" +
						"    {?class a rdfs:Class.}\n" +
						"    UNION{?class a owl:Class.}\n" +
						"    ?class a ?type.\n" +
						"    FILTER(?type!=owl:DeprecatedClass)\n" +
						"} ");
				classNumberQuery.setIncludeInferred(false);
				classNumberQuery.setDataset(datasetForEditableGraph);
				try (TupleQueryResult tupleQueryResult = classNumberQuery.evaluate()) {
					classNumber = (Literal) QueryResults.singleResult(tupleQueryResult).getValue("nbClass");
				}
				
				//voaf:propertyNumber
				TupleQuery propertyNumberQuery = conn.prepareTupleQuery("PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
						"PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" +
						"PREFIX owl:<http://www.w3.org/2002/07/owl#>\n" +
						"PREFIX voaf:<http://purl.org/vocommons/voaf#>\n" +
						"\n" +
						"SELECT (COUNT(distinct ?prop) AS ?nbProp)\n" +
						"WHERE{\n" +
						"{?prop a rdf:Property.}\n" +
						"UNION{?prop a owl:ObjectProperty.}\n" +
						"UNION{?prop a owl:DatatypeProperty.}\n" +
						"UNION{?prop a owl:AnnotationProperty.}\n" +
						"UNION{?prop a owl:FunctionalProperty.}\n" +
						"UNION{?prop a owl:OntologyProperty.}\n" +
						"UNION{?prop a owl:AsymmetricProperty.}\n" +
						"UNION{?prop a owl:InverseFunctionalProperty.}\n" +
						"UNION{?prop a owl:IrreflexiveProperty.}\n" +
						"UNION{?prop a owl:ReflexiveProperty.}\n" +
						"UNION{?prop a owl:SymmetricProperty.}\n" +
						"UNION{?prop a owl:TransitiveProperty.}\n" +
						"?prop a ?type.\n" +
						"FILTER(?type!=owl:DeprecatedProperty)\n" +
						"}\n");
				propertyNumberQuery.setIncludeInferred(false);
				propertyNumberQuery.setDataset(datasetForEditableGraph);
				try (TupleQueryResult tupleQueryResult = propertyNumberQuery.evaluate()) {
					propertyNumber = (Literal) QueryResults.singleResult(tupleQueryResult).getValue("nbProp");
				}

				//voaf:metadataVoc
				TupleQuery metadataVocQuery = conn.prepareTupleQuery("PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" +
						"PREFIX owl:<http://www.w3.org/2002/07/owl#>\n" +
						"PREFIX voaf:<http://purl.org/vocommons/voaf#>\n" +
						"\n" +
						"SELECT DISTINCT ?vocab2\n" +
						"WHERE{\n" +
						"  GRAPH ?g {\n" +
						"    ?elem1 ?elem2 ?o.\n" +
						"    ?vocab1 a owl:Ontology .\n" +
						"  }\n" +
						"  GRAPH ?g2 {\n" +
						"    ?elem2 a [] .\n" +
						"    OPTIONAL { ?elem2 rdfs:isDefinedBy ?vocab2 . }\n" +
						"    ?vocab2 a owl:Ontology . \n" +
						"  " +
						"}\n" +
						"FILTER(?vocab1!=?vocab2)\n" +
						"} ");
				metadataVocQuery.setIncludeInferred(false);
				metadataVocQuery.setBinding("g", dataGraph);
				try (TupleQueryResult tupleQueryResult = metadataVocQuery.evaluate();
					 	Stream<BindingSet> bindingSetStream = QueryResults.stream(tupleQueryResult)) {
					metadataVoc = bindingSetStream.map(bs -> (IRI) bs.getValue("vocab2")).collect(Collectors.toList());
				}

				//voaf:extends
				TupleQuery extendedVocQuery = conn.prepareTupleQuery(
						"PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" +
								"PREFIX owl:<http://www.w3.org/2002/07/owl#>\n" +
								"SELECT DISTINCT ?vocab2\n" +
								"WHERE{\n" +
								"    GRAPH ?g { ?vocab1 a owl:Ontology . {?elem1 owl:inverseOf ?elem2. FILTER(!isBlank(?elem2))}\n" +
								"      UNION{?elem1 rdfs:domain ?elem2. FILTER(!isBlank(?elem2))}\n" +
								"      UNION{?elem1 rdfs:range ?elem2. FILTER(!isBlank(?elem2))}\n" +
								"      UNION{?elem2 rdfs:domain ?elem1. FILTER(!isBlank(?elem1))}\n" +
								"      UNION{?elem2 rdfs:range ?elem1. FILTER(!isBlank(?elem1))} \n" +
								"    }\n" +
								"    GRAPH ?g2 {\n" +
								"        ?elem2 a [].\n" +
								"        OPTIONAL { ?elem2 rdfs:isDefinedBy ?vocab2 . }\n" +
								"        ?vocab2 a owl:Ontology.\n" +
								"    }\n" +
								"    FILTER(?vocab1!=?vocab2)\n" +
								"} \n");
				extendedVocQuery.setIncludeInferred(false);
				extendedVocQuery.setBinding("g", dataGraph);
				try (TupleQueryResult tupleQueryResult = extendedVocQuery.evaluate();
					 	Stream<BindingSet> bindingSetStream = QueryResults.stream(tupleQueryResult)) {
					extendedVoc = bindingSetStream.map(bs -> (IRI) bs.getValue("vocab2")).collect(Collectors.toList());
				}

				//voaf:specializes
				TupleQuery specializedVocQuery = conn.prepareTupleQuery(
						"PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" +
								"PREFIX owl:<http://www.w3.org/2002/07/owl#>\n" +
								"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n" +
								"SELECT DISTINCT ?vocab2\n" +
								"WHERE{\n" +
								"    GRAPH ?g { \n" +
								"        ?vocab1 a owl:Ontology . \n" +
								"        {?elem1 rdfs:subPropertyOf ?elem2. FILTER(!isBlank(?elem2))}\n" +
								"        UNION{?elem1 rdfs:subClassOf ?elem2. FILTER(!isBlank(?elem2))}\n" +
								"        " +
								"UNION{?elem1 skos:broadMatch ?elem2. FILTER(!isBlank(?elem2))}\n" +
								"    }\n" +
								"    GRAPH ?g2 {\n" +
								"        ?elem2 a [].\n" +
								"        OPTIONAL { ?elem2 rdfs:isDefinedBy ?vocab2 ." +
								" " +
								"}\n" +
								"        ?vocab2 a owl:Ontology.\n" +
								"    }\n" +
								"    FILTER(?vocab1!=?vocab2)\n" +
								"} \n");
				specializedVocQuery.setIncludeInferred(false);
				specializedVocQuery.setBinding("g", dataGraph);
				try (TupleQueryResult tupleQueryResult = specializedVocQuery.evaluate();
					 	Stream<BindingSet> bindingSetStream = QueryResults.stream(tupleQueryResult)) {
					specializedVoc = bindingSetStream.map(bs -> (IRI) bs.getValue("vocab2")).collect(Collectors.toList());
				}

				//voaf:generalizes
				TupleQuery generalizedVocQuery = conn.prepareTupleQuery(
						"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
								"PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" +
								"PREFIX owl:<http://www.w3.org/2002/07/owl#>\n" +
								"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n" +
								"SELECT DISTINCT ?vocab2\n" +
								"WHERE{\n" +
								"    GRAPH ?g { \n" +
								"        ?vocab1 a owl:Ontology . \n" +
								"        {\n" +
								"            ?elem1 skos:narrowMatch ?elem2. FILTER(!isBlank(?elem2))\n" +
								"        } " +
								"UNION {\n" +
								"            ?elem2 rdfs:subPropertyOf ?elem1.\n" +
								"        } " +
								"UNION{\n" +
								"            ?elem1 a owl:Class. \n" +
								"            ?elem1 owl:unionOf/rdf:rest*/rdf:first ?elem2.\n" +
								"        " +
								"}\n" +
								"        FILTER(!isBlank(?elem2))\n" +
								"    }\n" +
								"    GRAPH ?g2 {\n" +
								"        ?elem2 a [].\n" +
								"        OPTIONAL { ?elem2 rdfs:isDefinedBy ?vocab2 ." +
								" " +
								"}\n" +
								"        ?vocab2 a owl:Ontology.\n" +
								"    }\n" +
								"    FILTER(?vocab1!=?vocab2)\n" +
								"} \n");
				generalizedVocQuery.setIncludeInferred(false);
				generalizedVocQuery.setBinding("g", dataGraph);
				try (TupleQueryResult tupleQueryResult = generalizedVocQuery.evaluate();
					 Stream<BindingSet> bindingSetStream = QueryResults.stream(tupleQueryResult)) {
					generalizedVoc = bindingSetStream.map(bs -> (IRI) bs.getValue("vocab2")).collect(Collectors.toList());
				}

				//voaf:hasEquivalencesWith
				TupleQuery hasEquivalencesWithVocQuery = conn.prepareTupleQuery(
						"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
								"PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" +
								"PREFIX owl:<http://www.w3.org/2002/07/owl#>\n" +
								"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n" +
								"SELECT DISTINCT ?vocab2\n" +
								"WHERE{\n" +
								"    GRAPH ?g { \n" +
								"        ?vocab1 a owl:Ontology . \n" +
								"        {?elem1 owl:equivalentProperty ?elem2.}\n" +
								"        UNION{?elem1 owl:sameAs ?elem2.}\n" +
								"        UNION{?elem1 owl:equivalentClass ?elem2.}\n" +
								"        UNION{?elem1 skos:exactMatch ?elem2.}\n" +
								"        UNION{?elem2 skos:exactMatch ?elem1.}\n" +
								"        " +
								"FILTER(!isBlank(?elem2))\n" +
								"    }\n" +
								"    GRAPH ?g2 {\n" +
								"        ?elem2 a [].\n" +
								"        OPTIONAL { ?elem2 rdfs:isDefinedBy ?vocab2 ." +
								" " +
								"}\n" +
								"        ?vocab2 a owl:Ontology.\n" +
								"    }\n" +
								"    FILTER(?vocab1!=?vocab2)\n" +
								"} \n");
				hasEquivalencesWithVocQuery.setIncludeInferred(false);
				hasEquivalencesWithVocQuery.setBinding("g", dataGraph);
				try (TupleQueryResult tupleQueryResult = hasEquivalencesWithVocQuery.evaluate();
					 Stream<BindingSet> bindingSetStream = QueryResults.stream(tupleQueryResult)) {
					hasEquivalencesWithVoc = bindingSetStream.map(bs -> (IRI) bs.getValue("vocab2")).collect(Collectors.toList());
				}

				//voaf:hasDisjunctionsWith
				TupleQuery hasDisjunctionsWithVocQuery = conn.prepareTupleQuery(
						"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
								"PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" +
								"PREFIX owl:<http://www.w3.org/2002/07/owl#>\n" +
								"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n" +
								"SELECT DISTINCT ?vocab2\n" +
								"WHERE{\n" +
								"    GRAPH ?g { \n" +
								"        ?vocab1 a owl:Ontology . \n" +
								"        ?elem1 owl:disjointWith ?elem2.\n" +
								"        FILTER(!isBlank(?elem2))\n" +
								"    }\n" +
								"    GRAPH ?g2 {\n" +
								"        ?elem2 a [].\n" +
								"        OPTIONAL { ?elem2 rdfs:isDefinedBy ?vocab2 ." +
								" " +
								"}\n" +
								"        ?vocab2 a owl:Ontology.\n" +
								"    }\n" +
								"    FILTER(?vocab1!=?vocab2)\n" +
								"} \n");
				hasDisjunctionsWithVocQuery.setIncludeInferred(false);
				hasDisjunctionsWithVocQuery.setBinding("g", dataGraph);


				try (TupleQueryResult tupleQueryResult = hasDisjunctionsWithVocQuery.evaluate();
						Stream<BindingSet> bindingSetStream = QueryResults.stream(tupleQueryResult)) {
					hasDisjunctionsWithVoc = bindingSetStream.map(bs -> (IRI) bs.getValue("vocab2")).collect(Collectors.toList());
				}

			}

			Update vocabMetadataUpdate = conn.prepareUpdate(
					"PREFIX dct: <http://purl.org/dc/terms/>\n" +
					"PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
					"PREFIX vann: <http://purl.org/vocab/vann/>\n" +
					"PREFIX voaf: <http://purl.org/vocommons/voaf#>\n" +
					"PREFIX cc: <http://creativecommons.org/ns#>\n" +
					"\n" +
					"WITH <" + dataGraph +">\n" +
					"DELETE {\n" +
					"    ?vocab vann:preferredNamespacePrefix ?oldPreferredNamespacePrefix .\n" +
					"    ?vocab vann:preferredNamespaceUri ?oldPreferredNamespaceUri .\n" +
					"    ?vocab dct:title ?oldTitle .\n" +
					"    ?vocab dct:description ?oldDescription .\n" +
					"    ?vocab dct:issued ?oldIssued .\n" +
					"    ?vocab dct:modified ?oldModified .\n" +
					"    ?vocab owl:versionInfo ?oldVersionInfo .\n" +
					"    ?vocab dct:creator ?oldCreator .\n" +
					"    ?vocab dct:contributor ?oldContributor .\n" +
					"    ?vocab dct:publisher ?oldPublisher .\n" +
					"    ?vocab dct:rights ?oldRights .\n" +
					"    ?vocab cc:license ?oldLicense .\n" +
					"    ?vocab voaf:classNumber ?oldClassNumber .\n" +
					"    ?vocab voaf:propertyNumber ?oldPropertyNumber .\n" +
					"    ?vocab voaf:metadataVoc ?oldMetadataVoc .\n" +
					"    ?vocab voaf:extends ?oldExtendedVoc .\n" +
					"    ?vocab voaf:specializes ?oldSpecializedVoc .\n" +
					"    ?vocab voaf:generalizes ?oldGeneralizedVoc .\n" +
					"    ?vocab voaf:hasEquivalencesWith ?oldHasEquivalencesWithVoc .\n" +
					"    ?vocab voaf:hasDisjunctionsWith ?oldHasDisjunctionsWithVoc .\n" +
					"    ?vocab voaf:similar ?oldSimilarVoc .\n" +
					"}\n" +
					"INSERT {\n" +
					"    ?vocab a voaf:Vocabulary .\n" +
					"    ?vocab vann:preferredNamespacePrefix ?preferredNamespacePrefix .\n" +
					"    ?vocab vann:preferredNamespaceUri ?preferredNamespaceUri .\n" +
					"    ?vocab dct:title ?title .\n" +
					"    ?vocab dct:description ?description .\n" +
					"    ?vocab dct:issued ?issued .\n" +
					"    ?vocab dct:modified ?modified .\n" +
					"    ?vocab owl:versionInfo ?versionInfo .\n" +
					"    ?vocab dct:creator ?creator .\n" +
					"    ?vocab dct:contributor ?contributor .\n" +
					"    ?vocab dct:publisher ?publisher .\n" +
					"    ?vocab dct:rights ?rights .\n" +
					"    ?vocab cc:license ?license .\n" +
					"    ?vocab voaf:classNumber ?classNumber .\n" +
					"    ?vocab voaf:propertyNumber ?propertyNumber .\n" +
					"    ?vocab voaf:metadataVoc ?metadataVoc .\n" +
					"    ?vocab voaf:extends ?extendedVoc .\n" +
					"    ?vocab voaf:specializes ?specializedVoc .\n" +
					"    ?vocab voaf:generalizes ?generalizedVoc .\n" +
					"    ?vocab voaf:hasEquivalencesWith ?hasEquivalencesWithVoc .\n" +
					"    ?vocab voaf:hasDisjunctionsWith ?hasDisjunctionsWithVoc .\n" +
					"    ?vocab voaf:similar ?similarVoc .\n" +
					"}\n" +
					"WHERE {\n" +
					"    {\n" +
					"        ?vocab vann:preferredNamespacePrefix ?oldPreferredNamespacePrefix .\n" +
					"    } UNION {\n" +
					"        ?vocab vann:preferredNamespaceUri ?oldPreferredNamespacePrefix .\n" +
					"    } UNION {\n" +
					"        ?vocab dct:title ?oldTitle .\n" +
					"    } UNION {\n" +
					"        ?vocab dct:description ?oldDescription .\n" +
					"    } UNION {\n" +
					"        ?vocab dct:issued ?oldIssued .\n" +
					"    } UNION {\n" +
					"        ?vocab dct:modified ?oldModified .\n" +
					"    } UNION {\n" +
					"        ?vocab owl:versionInfo ?oldVersionInfo .\n" +
					"    } UNION {\n" +
					"        ?vocab dct:creator ?oldCreator .\n" +
					"    } UNION {\n" +
					"        ?vocab dct:contributor ?oldContributor .\n" +
					"    } UNION {\n" +
					"        ?vocab dct:publisher ?oldPublisher .\n" +
					"    } UNION {\n" +
					"        ?vocab dct:rights ?oldRights .\n" +
					"    } UNION {\n" +
					"        ?vocab cc:license ?oldLicense .\n" +
					"    } UNION {\n" +
					"        ?vocab voaf:classNumber ?oldClassNumber .\n" +
					"    } UNION {\n" +
					"        ?vocab voaf:propertyNumber ?oldPropertyNumber .\n" +
					"    } UNION {\n" +
					"        ?vocab voaf:metadataVoc ?oldMetadataVoc .\n" +
					"    } UNION {\n" +
					"        ?vocab voaf:extends ?oldExtendedVoc .\n" +
					"    } UNION {\n" +
					"        ?vocab voaf:specializes ?oldSpecializedVoc .\n" +
					"    } UNION {\n" +
					"        ?vocab voaf:generalizes ?oldGeneralizedVoc .\n" +
					"    } UNION {\n" +
					"        ?vocab voaf:hasEquivalencesWith ?oldHasEquivalencesWithVoc .\n" +
					"    } UNION {\n" +
					"        ?vocab voaf:hasDisjunctionsWith ?oldHasDisjunctionsWithVoc .\n" +
					"    } UNION {\n" +
					"        ?vocab voaf:similar ?oldSimilarVoc .\n" +
					"    } UNION {\n" +
					"      VALUES(?title){" + title.stream().map(l -> "(" + QueryStringUtil.valueToString(l) + ")").collect(Collectors.joining()) + "}\n" +
					"    } UNION {\n" +
					"      VALUES(?description){" + description.stream().map(l -> "(" + QueryStringUtil.valueToString(l) + ")").collect(Collectors.joining()) + "}\n" +
					"    } UNION {\n" +
					"        VALUES(?creator){" + creator.stream().map(l -> "(" + QueryStringUtil.valueToString(l) + ")").collect(Collectors.joining()) + "}\n" +
					"    } UNION {\n" +
					"        VALUES(?contributor){" + contributor.stream().map(l -> "(" + QueryStringUtil.valueToString(l) + ")").collect(Collectors.joining()) + "}\n" +
					"    } UNION {\n" +
					"        VALUES(?publisher){" + publisher.stream().map(l -> "(" + QueryStringUtil.valueToString(l) + ")").collect(Collectors.joining()) + "}\n" +
					"    } UNION {\n" +
					"        VALUES(?metadataVoc){" + metadataVoc.stream().map(l -> "(" + QueryStringUtil.valueToString(l) + ")").collect(Collectors.joining()) + "}\n" +
					"    } UNION {\n" +
					"        VALUES(?extendedVoc){" + extendedVoc.stream().map(l -> "(" + QueryStringUtil.valueToString(l) + ")").collect(Collectors.joining()) + "}\n" +
					"    } UNION {\n" +
					"        VALUES(?specializedVoc){" + specializedVoc.stream().map(l -> "(" + QueryStringUtil.valueToString(l) + ")").collect(Collectors.joining()) + "}\n" +
					"    } UNION {\n" +
					"        VALUES(?generalizedVoc){" + generalizedVoc.stream().map(l -> "(" + QueryStringUtil.valueToString(l) + ")").collect(Collectors.joining()) + "}\n" +
					"    } UNION {\n" +
					"        VALUES(?hasEquivalencesWithVoc){" + hasEquivalencesWithVoc.stream().map(l -> "(" + QueryStringUtil.valueToString(l) + ")").collect(Collectors.joining()) + "}\n" +
					"    } UNION {\n" +
					"        VALUES(?hasDisjunctionsWithVoc){" + hasDisjunctionsWithVoc.stream().map(l -> "(" + QueryStringUtil.valueToString(l) + ")").collect(Collectors.joining()) + "}\n" +
					"    } UNION {\n" +
					"        VALUES(?similarVoc){" + similarVoc.stream().map(l -> "(" + QueryStringUtil.valueToString(l) + ")").collect(Collectors.joining()) + "}\n" +
					"    }\n" +
					"}");
			vocabMetadataUpdate.setBinding("vocab", vf.createIRI(project.getBaseURI()));
			vocabMetadataUpdate.setBinding("preferredNamespacePrefix", preferredNamespacePrefix);
			vocabMetadataUpdate.setBinding("preferredNamespaceUri", preferredNamespaceUri);
			vocabMetadataUpdate.setBinding("issued", issued);
			vocabMetadataUpdate.setBinding("modified", modified);
			vocabMetadataUpdate.setBinding("versionInfo", versionInfo);

			if (rights != null) {
				vocabMetadataUpdate.setBinding("rights", Values.literal(rights));
			}

			if (license != null) {
				vocabMetadataUpdate.setBinding("license", license);
			}

			if (classNumber != null) {
				vocabMetadataUpdate.setBinding("classNumber", classNumber);
			}

			if (propertyNumber != null) {
				vocabMetadataUpdate.setBinding("propertyNumber", propertyNumber);
			}

			vocabMetadataUpdate.execute();
		} catch (STPropertyAccessException e) {
			throw new DatasetMetadataExporterException(e);
		}
	}

	@Override
	public Settings importFromMetadataRegistry(Project project, Scope scope) throws NoSuchDatasetMetadataException, MetadataRegistryStateException {
		if (scope != Scope.PROJECT) return null;

		STMetadataRegistryBackend metadataRegistryBackend = factory.getMetadataRegistryBackend();

		@Nullable  IRI mdrIRI = metadataRegistryBackend.findDatasetForProject(project);

		LOVDatasetMetadataExporterSettings settings = new LOVDatasetMetadataExporterSettings();

		if (mdrIRI != null) {
			DatasetMetadata datasetMetadata = metadataRegistryBackend.getDatasetMetadata(mdrIRI);
			/*
			datasetMetadata.getTitle().ifPresent(s -> settings.dataset_title = s);
			datasetMetadata.getDescription().ifPresent(s -> settings.dataset_description = s);
			*/
		}
		return settings;
	}

	public Literal generateLiteralWithLang(String inputLiteral, ValueFactory valueFactory){
		Literal literal;
		
		String[] inputSplit = inputLiteral.split("@");
		String label;
		if(inputSplit[0].startsWith("\"") && inputSplit[0].startsWith("\"")){
			label = inputSplit[0].substring(1, inputSplit[0].length()-1);
		} else{
			label = inputSplit[0];
		}
		
		if(inputSplit.length == 2){
			literal = valueFactory.createLiteral(label, inputSplit[1]);
		} else {
			literal = valueFactory.createLiteral(label);
		}
		
		return literal;
	}
	
	public Literal generateLiteralWithType(String inputLiteral, ValueFactory valueFactory){
		Literal literal;
		String[] inputSplit = inputLiteral.split("^^");
		literal = valueFactory.createLiteral(inputSplit[0], valueFactory.createIRI(inputSplit[1]));
		return literal;
	}

}
