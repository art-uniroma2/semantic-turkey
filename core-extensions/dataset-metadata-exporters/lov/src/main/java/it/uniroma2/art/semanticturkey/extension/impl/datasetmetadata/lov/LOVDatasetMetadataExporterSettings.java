package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.lov;

import it.uniroma2.art.semanticturkey.constraints.HasDatatype;
import it.uniroma2.art.semanticturkey.constraints.LanguageTaggedString;
import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.properties.Enumeration;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

import java.util.List;


/**
 * 
 * @author <a href="mailto:turbati@info.uniroma2.it">Andrea Turbati </a>
 *
 */

public class LOVDatasetMetadataExporterSettings implements Settings {

	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.lov.LOVDatasetMetadataExporterSettings";

		public static final String shortName = keyBase + ".shortName";
		public static final String preferredNamespacePrefix$description = keyBase + ".preferredNamespacePrefix.description";
		public static final String preferredNamespacePrefix$displayName = keyBase + ".preferredNamespacePrefix.displayName";
		public static final String title$description = keyBase + ".title.description";
		public static final String title$displayName = keyBase + ".title.displayName";
		public static final String description$description = keyBase + ".description.description";
		public static final String description$displayName = keyBase + ".description.displayName";
		public static final String issued$description = keyBase + ".issued.description";
		public static final String issued$displayName = keyBase + ".issued.displayName";
		public static final String modified$description = keyBase + ".modified.description";
		public static final String modified$displayName = keyBase + ".modified.displayName";
		public static final String versionInfo$description = keyBase + ".versionInfo.description";
		public static final String versionInfo$displayName = keyBase + ".versionInfo.displayName";
		public static final String creator$description = keyBase + ".creator.description";
		public static final String creator$displayName = keyBase + ".creator.displayName";
		public static final String contributor$description = keyBase + ".contributor.description";
		public static final String contributor$displayName = keyBase + ".contributor.displayName";
		public static final String publisher$description = keyBase + ".publisher.description";
		public static final String publisher$displayName = keyBase + ".publisher.displayName";
		public static final String rights$description = keyBase + ".rights.description";
		public static final String rights$displayName = keyBase + ".rights.displayName";
		public static final String license$description = keyBase + ".license.description";
		public static final String license$displayName = keyBase + ".license.displayName";
		public static final String similarVocabulary$description = keyBase + ".similarVocabulary.description";
		public static final String similarVocabulary$displayName = keyBase + ".similarVocabulary.displayName";
		public static final String computeMetadata$description = keyBase + ".computeMetadata.description";
		public static final String computeMetadata$displayName = keyBase + ".computeMetadata.displayName";

	}

	@Override
	public String getShortName() {
		return "{" + MessageKeys.shortName + "}";
	}

	@Required
	@STProperty(description = "{" + MessageKeys.preferredNamespacePrefix$description + "}", displayName = "{" + MessageKeys.preferredNamespacePrefix$displayName + "}")
	public String preferredNamespacePrefix;

	@Required
	@STProperty(description = "{" + MessageKeys.title$description + "}", displayName = "{" + MessageKeys.title$displayName + "}")
	public List<@LanguageTaggedString Literal> title;

	@Required
	@STProperty(description = "{" + MessageKeys.description$description + "}", displayName = "{" + MessageKeys.description$displayName + "}")
	public List<@LanguageTaggedString Literal> description;

	@Required
	@STProperty(description = "{" + MessageKeys.issued$description + "}", displayName = "{" + MessageKeys.issued$displayName + "}")
	public String issued;

	@Required
	@STProperty(description = "{" + MessageKeys.modified$description + "}", displayName = "{" + MessageKeys.modified$displayName + "}")
	public String modified;

	@Required
	@STProperty(description = "{" + MessageKeys.versionInfo$description + "}", displayName = "{" + MessageKeys.versionInfo$displayName + "}")
	public String versionInfo;

	@Required
	@STProperty(description = "{" + MessageKeys.creator$description + "}", displayName = "{" + MessageKeys.creator$displayName + "}")
	public List<IRI> creator;

	@STProperty(description = "{" + MessageKeys.contributor$description + "}", displayName = "{" + MessageKeys.contributor$displayName + "}")
	public List<IRI> contributor;

	@STProperty(description = "{" + MessageKeys.publisher$description + "}", displayName = "{" + MessageKeys.publisher$displayName + "}")
	public List<IRI> publisher;

	@STProperty(description = "{" + MessageKeys.rights$description + "}", displayName = "{" + MessageKeys.rights$displayName + "}")
	public String rights;

	@STProperty(description = "{" + MessageKeys.license$description + "}", displayName = "{" + MessageKeys.license$displayName + "}")
	@Enumeration(value={"<http://creativecommons.org/licenses/by/4.0/>", "<http://creativecommons.org/licenses/by-nd/4.0/>",
	"<http://creativecommons.org/licenses/by-sa/4.0/>", "<http://creativecommons.org/licenses/by-nc/4.0/>", "<http://creativecommons.org/licenses/by-nc-nd/4.0/>",
	"<http://creativecommons.org/licenses/by-nc-sa/4.0/>", "<https://joinup.ec.europa.eu/licence/european-union-public-licence-version-12-or-later-eupl>"}, open=true)
	public IRI license;

	@STProperty(description = "{" + MessageKeys.similarVocabulary$description + "}", displayName = "{" + MessageKeys.similarVocabulary$displayName + "}")
	public List<IRI> similarVocabulary;

	@Required
	@STProperty(description = "{" + MessageKeys.computeMetadata$description + "}", displayName = "{" + MessageKeys.computeMetadata$displayName + "}")
	public boolean computeMetadata = false;
}
