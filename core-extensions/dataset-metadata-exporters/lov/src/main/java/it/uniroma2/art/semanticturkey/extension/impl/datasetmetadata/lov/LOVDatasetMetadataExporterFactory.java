package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.lov;

import it.uniroma2.art.semanticturkey.extension.NonConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetmetadata.DatasetMetadataExporter;
import it.uniroma2.art.semanticturkey.extension.settings.ProjectSettingsManager;
import it.uniroma2.art.semanticturkey.i18n.STMessageSource;
import it.uniroma2.art.semanticturkey.mdr.bindings.STMetadataRegistryBackend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Factory for the instantiation of {@link LOVDatasetMetadataExporter}.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 */
@Component
public class LOVDatasetMetadataExporterFactory
		implements NonConfigurableExtensionFactory<LOVDatasetMetadataExporter>, ProjectSettingsManager<LOVDatasetMetadataExporterSettings>,
		DatasetMetadataExporter.ExtensionFactoryIntrospection {


	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.lov.LOVDatasetMetadataExporterFactory";
		private static final String name = keyBase + ".name";
		private static final String description = keyBase + ".description";
	}

	private final STMetadataRegistryBackend metadataRegistryBackend;

	@Autowired
	public LOVDatasetMetadataExporterFactory(STMetadataRegistryBackend metadataRegistryBackend) {
		this.metadataRegistryBackend = metadataRegistryBackend;
	}

	@Override
	public String getName() {
		return STMessageSource.getMessage(MessageKeys.name);
	}

	@Override
	public String getDescription() {
		return STMessageSource.getMessage(MessageKeys.description);
	}

	@Override
	public LOVDatasetMetadataExporter createInstance() {
		return new LOVDatasetMetadataExporter(this);
	}

	STMetadataRegistryBackend getMetadataRegistryBackend() {
		return metadataRegistryBackend;
	}
}
