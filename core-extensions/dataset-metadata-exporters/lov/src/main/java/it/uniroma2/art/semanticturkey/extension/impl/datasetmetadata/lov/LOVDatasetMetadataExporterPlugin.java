package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.lov;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class LOVDatasetMetadataExporterPlugin extends STPlugin {
    public LOVDatasetMetadataExporterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
