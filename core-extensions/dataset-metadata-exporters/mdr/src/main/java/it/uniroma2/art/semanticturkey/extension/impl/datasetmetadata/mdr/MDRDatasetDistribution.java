package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.mdr;

import it.uniroma2.art.semanticturkey.properties.Enumeration;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import org.eclipse.rdf4j.model.IRI;

import java.util.List;

public class MDRDatasetDistribution implements STProperties {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.mdr.MDRDatasetDistribution";

        public static final String shortName = keyBase + ".shortName";
        public static final String localName$description = keyBase + ".localName.description";
        public static final String localName$displayName = keyBase + ".localName.displayName";
        public static final String model$description = keyBase + ".model.description";
        public static final String model$displayName = keyBase + ".model.displayName";
        public static final String sparqlEndpoint$description = keyBase + ".sparqlEndpoint.description";
        public static final String sparqlEndpoint$displayName = keyBase + ".sparqlEndpoint.displayName";
        public static final String standardDereferenciation$description = keyBase + ".standardDereferenciation.description";
        public static final String standardDereferenciation$displayName = keyBase + ".standardDereferenciation.displayName";
        public static final String dataDump$description = keyBase + ".dataDump.description";
        public static final String dataDump$displayName = keyBase + ".dataDump.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.localName$description + "}", displayName = "{" + MessageKeys.localName$displayName + "}")
    @Required
    public String localName;

    @STProperty(description = "{" + MessageKeys.model$description + "}", displayName = "{" + MessageKeys.model$displayName + "}")
    @Required
    @Enumeration(value = {"<http://www.w3.org/2000/01/rdf-schema>",
            "<http://www.w3.org/2002/07/owl>", "<http://www.w3.org/2004/02/skos/core>", "<http://www.w3.org/ns/lemon/ontolex>"}, open = true)
    public IRI model;

    @STProperty(description = "{" + MessageKeys.sparqlEndpoint$description + "}", displayName = "{" + MessageKeys.sparqlEndpoint$displayName + "}")
    public List<MDRSPARQLEndpoint> sparqlEndpoint;

    @STProperty(description = "{" + MessageKeys.standardDereferenciation$description + "}", displayName = "{" + MessageKeys.standardDereferenciation$displayName + "}")
    public Boolean standardDereferenciation;

    @STProperty(description = "{" + MessageKeys.dataDump$description + "}", displayName = "{" + MessageKeys.dataDump$displayName + "}")
    public List<IRI> dataDump;

}
