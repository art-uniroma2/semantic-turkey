package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.mdr;

import it.uniroma2.art.semanticturkey.extension.NonConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.settings.ProjectSettingsManager;
import it.uniroma2.art.semanticturkey.i18n.STMessageSource;
import it.uniroma2.art.semanticturkey.mdr.bindings.STMetadataRegistryBackend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Factory for the instantiation of {@link MDRDatasetMetadataExporter}.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 */
@Component
public class MDRDatasetMetadataExporterFactory
		implements NonConfigurableExtensionFactory<MDRDatasetMetadataExporter>,
		ProjectSettingsManager<MDRDatasetMetadataExporterSettings> {


	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.mdr.MDRDatasetMetadataExporterFactory";
		private static final String name = keyBase + ".name";
		private static final String description = keyBase + ".description";
	}

	private final STMetadataRegistryBackend metadataRegistryBackend;

	@Autowired
	public MDRDatasetMetadataExporterFactory(STMetadataRegistryBackend metadataRegistryBackend) {
		this.metadataRegistryBackend = metadataRegistryBackend;
	}

	@Override
	public String getName() {
		return STMessageSource.getMessage(MessageKeys.name);
	}

	@Override
	public String getDescription() {
		return STMessageSource.getMessage(MessageKeys.description);
	}

	@Override
	public MDRDatasetMetadataExporter createInstance() {
		return new MDRDatasetMetadataExporter(this);
	}

	STMetadataRegistryBackend getMetadataRegistryBackend() {
		return metadataRegistryBackend;
	}
}
