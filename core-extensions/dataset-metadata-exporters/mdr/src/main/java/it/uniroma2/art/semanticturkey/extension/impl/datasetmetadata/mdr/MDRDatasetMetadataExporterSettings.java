package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.mdr;

import it.uniroma2.art.semanticturkey.constraints.LanguageTaggedString;
import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.properties.Enumeration;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

import java.util.List;

public class MDRDatasetMetadataExporterSettings implements Settings {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.mdr.MDRDatasetMetadataExporterSettings";

        public static final String shortName = keyBase + ".shortName";
        public static final String dataset_description_baseUri$description = keyBase + ".dataset_description_baseUri.description";
        public static final String dataset_description_baseUri$displayName = keyBase + ".dataset_description_baseUri.displayName";
        public static final String dataset_localName$description = keyBase + ".dataset_localName.description";
        public static final String dataset_localName$displayName = keyBase + ".dataset_localName.displayName";
        public static final String dataset_title$description = keyBase + ".dataset_title.description";
        public static final String dataset_title$displayName = keyBase + ".dataset_title.displayName";
        public static final String dataset_description$description = keyBase + ".dataset_description.description";
        public static final String dataset_description$displayName = keyBase + ".dataset_description.displayName";
        public static final String dataset_distribution$description = keyBase + ".dataset_distribution.description";
        public static final String dataset_distribution$displayName = keyBase + ".dataset_distribution.displayName";
        public static final String dataset_uriSpace$description = keyBase + ".dataset_uriSpace.description";
        public static final String dataset_uriSpace$displayName = keyBase + ".dataset_uriSpace.displayName";
    }

    @STProperty(description = "{" + MessageKeys.dataset_description_baseUri$description + "}", displayName = "{" + MessageKeys.dataset_description_baseUri$displayName + "}")
    @Required
    public IRI dataset_description_baseUri;

    @STProperty(description = "{" + MessageKeys.dataset_localName$description + "}", displayName = "{" + MessageKeys.dataset_localName$displayName + "}")
    @Required
    public String dataset_localName;

    @STProperty(description = "{" + MessageKeys.dataset_title$description + "}", displayName = "{" + MessageKeys.dataset_title$displayName + "}")
    @Required
    public List<@LanguageTaggedString Literal> dataset_title;

    @STProperty(description = "{" + MessageKeys.dataset_description$description + "}", displayName = "{" + MessageKeys.dataset_description$displayName + "}")
    @Required
    public List<@LanguageTaggedString Literal> dataset_description;

    @STProperty(description = "{" + MessageKeys.dataset_uriSpace$description + "}", displayName = "{" + MessageKeys.dataset_uriSpace$displayName + "}")
    @Required
    public String dataset_uriSpace;

    @STProperty(description = "{" + MessageKeys.dataset_distribution$description + "}", displayName = "{" + MessageKeys.dataset_distribution$displayName + "}")
    @Required
    public MDRDatasetDistribution dataset_distribution;

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

}
