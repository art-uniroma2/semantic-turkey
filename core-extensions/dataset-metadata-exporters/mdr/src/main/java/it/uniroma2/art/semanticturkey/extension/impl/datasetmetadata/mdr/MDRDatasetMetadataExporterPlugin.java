package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.mdr;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class MDRDatasetMetadataExporterPlugin extends STPlugin {
    public MDRDatasetMetadataExporterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
