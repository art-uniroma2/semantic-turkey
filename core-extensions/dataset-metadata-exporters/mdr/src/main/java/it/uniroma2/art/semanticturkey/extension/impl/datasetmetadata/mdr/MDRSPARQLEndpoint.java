package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.mdr;

import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import org.eclipse.rdf4j.model.IRI;

import java.util.Set;

public class MDRSPARQLEndpoint implements STProperties {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.mdr.MDRSPARQLEndpoint";

        public static final String shortName = keyBase + ".shortName";
        public static final String accessURL$description = keyBase + ".accessURL.description";
        public static final String accessURL$displayName = keyBase + ".accessURL.displayName";
        public static final String limitation$description = keyBase + ".limitation.description";
        public static final String limitation$displayName = keyBase + ".limitation.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.accessURL$description + "}", displayName = "{" + MessageKeys.accessURL$displayName + "}")
    @Required
    public IRI accessURL;

    @STProperty(description = "{" + MessageKeys.limitation$description + "}", displayName = "{" + MessageKeys.limitation$displayName + "}")
    public Set<IRI> limitation;

}