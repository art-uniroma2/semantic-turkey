package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.mdr;

import com.google.common.collect.Lists;
import it.uniroma2.art.lime.model.repo.LIMERepositoryConnectionWrapper;
import it.uniroma2.art.lime.model.repo.LIMERepositoryWrapper;
import it.uniroma2.art.lime.model.vocabulary.LIME;
import it.uniroma2.art.lime.profiler.LIMEProfiler;
import it.uniroma2.art.lime.profiler.ProfilerException;
import it.uniroma2.art.lime.profiler.ProfilerOptions;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetmetadata.DatasetMetadataExporter;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetmetadata.DatasetMetadataExporterException;
import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.mdr.bindings.STMetadataRegistryBackend;
import it.uniroma2.art.semanticturkey.mdr.core.DatasetMetadata;
import it.uniroma2.art.semanticturkey.mdr.core.MetadataRegistryStateException;
import it.uniroma2.art.semanticturkey.mdr.core.NoSuchDatasetMetadataException;
import it.uniroma2.art.semanticturkey.mdr.core.vocabulary.METADATAREGISTRY;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.resources.Scope;
import it.uniroma2.art.semanticturkey.utilities.ModelUtilities;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.DCAT;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.VOID;
import org.eclipse.rdf4j.model.vocabulary.XSD;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import jakarta.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A {@link DatasetMetadataExporter} for the
 * <a href="https://semanticturkey.uniroma2.it/doc/user/mdr/">Semantic Turkey Metadata Registry (MDR)</a> metadata profile
 * combining DCAT, VoID, FOAF, Dublin Core, etc.
 */
public class MDRDatasetMetadataExporter implements DatasetMetadataExporter {

	private final MDRDatasetMetadataExporterFactory factory;

	public MDRDatasetMetadataExporter(MDRDatasetMetadataExporterFactory factory) {
		this.factory = factory;
	}

	@Override
	public Model produceDatasetMetadata(Project project, RepositoryConnection conn, IRI dataGraph)
			throws DatasetMetadataExporterException, STPropertyAccessException {
		LIMERepositoryWrapper tempMetadataRepository = new LIMERepositoryWrapper(
				new SailRepository(new MemoryStore()));
		tempMetadataRepository.init();
		try {
			try (LIMERepositoryConnectionWrapper metadataConnection = tempMetadataRepository
					.getConnection()) {
				MDRDatasetMetadataExporterSettings pluginSettings = factory.getProjectSettings(project);

				IRI datasetDescriptionBaseURI = pluginSettings.dataset_description_baseUri;
				LIMEProfiler profiler = new LIMEProfiler(metadataConnection, datasetDescriptionBaseURI, conn,
						dataGraph);
				ProfilerOptions options = new ProfilerOptions();
				options.setMainDatasetName(pluginSettings.dataset_distribution.localName);

				try {
					profiler.profile(options);
				} catch (ProfilerException e) {
					throw new DatasetMetadataExporterException(e);

				}

				ValueFactory vf = metadataConnection.getValueFactory();
				IRI datasetIRI = vf.createIRI(ModelUtilities.createDefaultNamespaceFromBaseURI(pluginSettings.dataset_description_baseUri.stringValue()), pluginSettings.dataset_localName);

				IRI distributionIRI = (IRI) metadataConnection.getMainDataset(false).get();

				CollectionUtils.emptyIfNull(pluginSettings.dataset_title).forEach(title -> metadataConnection.add(datasetIRI, DCTERMS.TITLE, title));

				if (pluginSettings.dataset_uriSpace != null) {
					metadataConnection.add(datasetIRI, VOID.URI_SPACE, vf.createIRI(pluginSettings.dataset_uriSpace));
				}

				CollectionUtils.emptyIfNull(pluginSettings.dataset_description).forEach(description -> metadataConnection.add(datasetIRI, DCTERMS.DESCRIPTION, description));

				if (pluginSettings.dataset_distribution != null) {
					MDRDatasetDistribution datasetDistributionSettings = pluginSettings.dataset_distribution;

					if (datasetDistributionSettings.model != null) {
						metadataConnection.add(distributionIRI, DCTERMS.CONFORMS_TO, datasetDistributionSettings.model);
					}

					List<IRI> dataDumps = ObjectUtils.firstNonNull(datasetDistributionSettings.dataDump, Collections.emptyList());
					dataDumps.stream().forEach(dataDump ->
						metadataConnection.add(distributionIRI, VOID.DATA_DUMP, dataDump)
					);

					List<MDRSPARQLEndpoint> sparqlEndpoints = ObjectUtils.firstNonNull(datasetDistributionSettings.sparqlEndpoint, Collections.emptyList());
					sparqlEndpoints.forEach(sparqlEndpoint -> {
							IRI sparqlEndpointIRI = sparqlEndpoint.accessURL;
							metadataConnection.add(distributionIRI, VOID.SPARQL_ENDPOINT, sparqlEndpointIRI);
							metadataConnection.add(datasetIRI, VOID.SPARQL_ENDPOINT, sparqlEndpointIRI);
							metadataConnection.add(datasetIRI, VOID.SPARQL_ENDPOINT, sparqlEndpointIRI);

							ObjectUtils.firstNonNull(sparqlEndpoint.limitation, Collections.<IRI>emptyList())
									.forEach(limitation -> metadataConnection.add(sparqlEndpointIRI, METADATAREGISTRY.HAS_SPARQL_ENDPOINT_LIMITATION,limitation));
						}
					);
					if (!sparqlEndpoints.isEmpty()) {
						metadataConnection.add(distributionIRI, RDF.TYPE, METADATAREGISTRY.HTTP_SPARQL_PROVIDER);
					}

					if (datasetDistributionSettings.standardDereferenciation != null) {
						if (datasetDistributionSettings.standardDereferenciation) {
							metadataConnection.add(distributionIRI, METADATAREGISTRY.DEREFERENCIATION_SYSTEM, METADATAREGISTRY.STANDARD_DEREFERENCIATION);
						} else {
							metadataConnection.add(distributionIRI, METADATAREGISTRY.DEREFERENCIATION_SYSTEM, METADATAREGISTRY.NO_DEREFERENCIATION);
						}
					}

				}

				metadataConnection.add(datasetIRI, DCAT.HAS_DISTRIBUTION, distributionIRI);

				IRI datasetDescriptionIRI = pluginSettings.dataset_description_baseUri;
				metadataConnection.remove(datasetDescriptionIRI, FOAF.PRIMARY_TOPIC, null);
				metadataConnection.add(datasetDescriptionIRI, FOAF.PRIMARY_TOPIC, datasetIRI);


				Model metadataModel = new LinkedHashModel();
				metadataModel.setNamespace(RDFS.NS);
				metadataModel.setNamespace(XSD.NS);
				metadataModel.setNamespace(VOID.NS);
				metadataModel.setNamespace(LIME.NS);
				metadataModel.setNamespace(FOAF.NS);
				metadataModel.setNamespace(DCTERMS.NS);
				metadataModel.setNamespace(DCAT.NS);
				metadataModel.setNamespace(METADATAREGISTRY.NS);

				StatementCollector stmtCollector = new StatementCollector(metadataModel);

				metadataConnection.export(stmtCollector);
				return metadataModel;
			}
		} finally {
			tempMetadataRepository.shutDown();
		}
	}

	@Override
	public Settings importFromMetadataRegistry(Project project, Scope scope) throws NoSuchDatasetMetadataException, MetadataRegistryStateException {
		if (scope != Scope.PROJECT) return null;

		STMetadataRegistryBackend metadataRegistryBackend = factory.getMetadataRegistryBackend();

		@Nullable IRI mdrIRI = metadataRegistryBackend.findDatasetForProject(project);

		MDRDatasetMetadataExporterSettings settings = new MDRDatasetMetadataExporterSettings();
		settings.dataset_distribution = new MDRDatasetDistribution();

		if (mdrIRI != null) {
			DatasetMetadata datasetMetadata = metadataRegistryBackend.getDatasetMetadata(mdrIRI);

			datasetMetadata.getTitle().ifPresent(s -> settings.dataset_title = Lists.newArrayList(Values.literal(s, "en")));
			datasetMetadata.getDescription().ifPresent(s -> settings.dataset_description = Lists.newArrayList(Values.literal(s, "en")));
			datasetMetadata.getUriSpace().ifPresent(s -> settings.dataset_uriSpace = s);
			datasetMetadata.getSparqlEndpointMetadata().ifPresent(s -> {
				MDRSPARQLEndpoint endpointSettings = new MDRSPARQLEndpoint();
				endpointSettings.accessURL = s.getEndpoint();
				endpointSettings.limitation = s.getLimitations().stream().collect(Collectors.toSet());

				settings.dataset_distribution.sparqlEndpoint = Lists.newArrayList(endpointSettings);
			});
		}

		settings.dataset_distribution.model = project.getModel();

		return settings;
	}

}
