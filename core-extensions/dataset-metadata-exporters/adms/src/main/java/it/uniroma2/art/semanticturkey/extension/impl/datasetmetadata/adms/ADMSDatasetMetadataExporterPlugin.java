package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.adms;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class ADMSDatasetMetadataExporterPlugin extends STPlugin {
    public ADMSDatasetMetadataExporterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
