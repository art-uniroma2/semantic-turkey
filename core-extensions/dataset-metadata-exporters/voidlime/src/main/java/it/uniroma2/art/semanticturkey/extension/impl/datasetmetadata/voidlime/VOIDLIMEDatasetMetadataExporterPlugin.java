package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.voidlime;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class VOIDLIMEDatasetMetadataExporterPlugin extends STPlugin {
    public VOIDLIMEDatasetMetadataExporterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
