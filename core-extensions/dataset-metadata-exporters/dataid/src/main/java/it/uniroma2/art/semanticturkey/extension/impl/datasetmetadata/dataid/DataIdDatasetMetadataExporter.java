package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.dataid;

import it.uniroma2.art.lime.model.vocabulary.LIME;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetmetadata.DatasetMetadataExporter;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetmetadata.DatasetMetadataExporterException;
import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.mdr.bindings.STMetadataRegistryBackend;
import it.uniroma2.art.semanticturkey.mdr.core.DatasetMetadata;
import it.uniroma2.art.semanticturkey.mdr.core.MetadataRegistryStateException;
import it.uniroma2.art.semanticturkey.mdr.core.NoSuchDatasetMetadataException;
import it.uniroma2.art.semanticturkey.mdr.core.vocabulary.DCAT3FRAGMENT;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.resources.Scope;
import it.uniroma2.art.semanticturkey.utilities.ModelUtilities;
import org.apache.commons.collections4.CollectionUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.vocabulary.DCAT;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.VOID;
import org.eclipse.rdf4j.model.vocabulary.XSD;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import jakarta.annotation.Nullable;

/**
 * A {@link DatasetMetadataExporter} for the
 * <a href="https://semanticturkey.uniroma2.it/doc/user/mdr/">Semantic Turkey Metadata Registry (MDR)</a> metadata profile
 * combining DCAT, VoID, FOAF, Dublin Core, etc.
 */
public class DataIdDatasetMetadataExporter implements DatasetMetadataExporter {

	private final DataIdDatasetMetadataExporterFactory factory;

	public DataIdDatasetMetadataExporter(DataIdDatasetMetadataExporterFactory factory) {
		this.factory = factory;
	}

	@Override
	public Model produceDatasetMetadata(Project project, RepositoryConnection conn, IRI dataGraph)
			throws DatasetMetadataExporterException, STPropertyAccessException {
		Repository tempMetadataRepository = new SailRepository(new MemoryStore());
		tempMetadataRepository.init();
		try {
			try (RepositoryConnection metadataConnection = tempMetadataRepository
					.getConnection()) {

				ValueFactory vf = metadataConnection.getValueFactory();

				DataIdDatasetMetadataExporterSettings settings = factory.getProjectSettings(project);

				IRI baseURI = settings.dataIdBaseUri;
				String defaultNamespace = ModelUtilities.createDefaultNamespaceFromBaseURI(baseURI.stringValue());

				DataIdDataset dataset = settings.dataset;

				IRI datasetIRI = vf.createIRI(defaultNamespace, dataset.localName);

				metadataConnection.add(datasetIRI, RDF.TYPE, DATAID.DATASET);

				CollectionUtils.emptyIfNull(dataset.title).forEach(title -> metadataConnection.add(datasetIRI, DCTERMS.TITLE, title));
				CollectionUtils.emptyIfNull(dataset.description).forEach(description -> metadataConnection.add(datasetIRI, DCTERMS.DESCRIPTION, description));
				CollectionUtils.emptyIfNull(dataset.keyword).forEach(keyword -> metadataConnection.add(datasetIRI, DCAT3FRAGMENT.KEYWORD, keyword));

				metadataConnection.add(datasetIRI, DCTERMS.PUBLISHER, dataset.publisher);
				metadataConnection.add(datasetIRI, DCTERMS.MODIFIED, vf.createLiteral(dataset.modified, XSD.DATETIME));
				metadataConnection.add(datasetIRI, DCTERMS.ISSUED, vf.createLiteral(dataset.issued, XSD.DATETIME));

				CollectionUtils.emptyIfNull(dataset.license).forEach(license -> metadataConnection.add(datasetIRI, DCTERMS.LICENSE, license));
				CollectionUtils.emptyIfNull(dataset.landingPage).forEach(landingPage -> metadataConnection.add(datasetIRI, DCAT.LANDING_PAGE, landingPage));
				CollectionUtils.emptyIfNull(dataset.associatedAgent).forEach(associatedAgent -> metadataConnection.add(datasetIRI, DATAID.ASSOCIATED_AGENT, associatedAgent));

				CollectionUtils.emptyIfNull(dataset.previousVersion).forEach(previousVersion -> metadataConnection.add(datasetIRI, DATAID.PREVIOUS_VERSION, previousVersion));
				CollectionUtils.emptyIfNull(dataset.nextVersion).forEach(nextVersion -> metadataConnection.add(datasetIRI, DATAID.NEXT_VERSION, nextVersion));
				CollectionUtils.emptyIfNull(dataset.latestVersion).forEach(latestVersion -> metadataConnection.add(datasetIRI, DATAID.LATEST_VERSION, latestVersion));

				if (dataset.hasVersion != null) {
					metadataConnection.add(datasetIRI, DCTERMS.HAS_VERSION, vf.createLiteral(dataset.hasVersion));
				}
				
				CollectionUtils.emptyIfNull(dataset.similarData).forEach(similarData -> metadataConnection.add(datasetIRI, DATAID.SIMILAR_DATA, similarData));

				CollectionUtils.emptyIfNull(dataset.theme).forEach(theme -> metadataConnection.add(datasetIRI, DCAT.THEME, theme));
				CollectionUtils.emptyIfNull(dataset.rights).forEach(rights -> metadataConnection.add(datasetIRI, DCTERMS.RIGHTS, rights));

				for (DataIdDistribution distribution : CollectionUtils.emptyIfNull(dataset.distribution)) {
					IRI distributionIRI = vf.createIRI(defaultNamespace, distribution.localName);

					metadataConnection.add(datasetIRI, DCAT.HAS_DISTRIBUTION, distributionIRI);

					IRI distributionType = vf.createIRI(DATAID.NAMESPACE, distribution.type);

					metadataConnection.add(distributionIRI, RDF.TYPE, distributionType);

					CollectionUtils.emptyIfNull(distribution.accessURL).forEach(accessURL -> metadataConnection.add(distributionIRI, DCAT.ACCESS_URL, accessURL));
					CollectionUtils.emptyIfNull(distribution.downloadURL).forEach(downloadURL -> metadataConnection.add(distributionIRI, DCAT.DOWNLOAD_URL, downloadURL));

					metadataConnection.add(distributionIRI, DCTERMS.PUBLISHER, distribution.publisher);

					CollectionUtils.emptyIfNull(distribution.title).forEach(title -> metadataConnection.add(distributionIRI, DCTERMS.TITLE, title));
					CollectionUtils.emptyIfNull(distribution.description).forEach(description -> metadataConnection.add(distributionIRI, DCTERMS.DESCRIPTION, description));

					CollectionUtils.emptyIfNull(distribution.license).forEach(license -> metadataConnection.add(distributionIRI, DCTERMS.LICENSE, license));

					metadataConnection.add(distributionIRI, DCTERMS.MODIFIED, vf.createLiteral(distribution.modified, XSD.DATETIME));
					metadataConnection.add(distributionIRI, DCTERMS.ISSUED, vf.createLiteral(distribution.issued, XSD.DATETIME));

					if (distribution.mediaType != null) {
						metadataConnection.add(distributionIRI, DCAT.MEDIA_TYPE, distribution.mediaType);
					}

					CollectionUtils.emptyIfNull(distribution.previousVersion).forEach(previousVersion -> metadataConnection.add(distributionIRI, DATAID.PREVIOUS_VERSION, previousVersion));
					CollectionUtils.emptyIfNull(distribution.nextVersion).forEach(nextVersion -> metadataConnection.add(distributionIRI, DATAID.NEXT_VERSION, nextVersion));
					CollectionUtils.emptyIfNull(distribution.latestVersion).forEach(latestVersion -> metadataConnection.add(distributionIRI, DATAID.LATEST_VERSION, latestVersion));

					if (distribution.hasVersion != null) {
						metadataConnection.add(distributionIRI, DCTERMS.HAS_VERSION, vf.createLiteral(distribution.hasVersion));
					}


					CollectionUtils.emptyIfNull(distribution.rights).forEach(rights -> metadataConnection.add(distributionIRI, DCTERMS.RIGHTS, rights));

					if (distribution.byteSize != null) {
						metadataConnection.add(distributionIRI, DCAT.BYTE_SIZE, vf.createLiteral(distribution.byteSize));
					}
					if (distribution.sha256sum != null) {
						metadataConnection.add(distributionIRI, DATAID.SHA256SUM, vf.createLiteral(distribution.sha256sum));
					}
				}

				Model metadataModel = new LinkedHashModel();
				metadataModel.setNamespace(RDFS.NS);
				metadataModel.setNamespace(XSD.NS);
				metadataModel.setNamespace(VOID.NS);
				metadataModel.setNamespace(LIME.NS);
				metadataModel.setNamespace(FOAF.NS);
				metadataModel.setNamespace(DCTERMS.NS);
				metadataModel.setNamespace(DCAT.NS);
				metadataModel.setNamespace(DATAID.NS);

				StatementCollector stmtCollector = new StatementCollector(metadataModel);

				metadataConnection.export(stmtCollector);
				return metadataModel;
			}
		} finally {
			tempMetadataRepository.shutDown();
		}
	}

	@Override
	public Settings importFromMetadataRegistry(Project project, Scope scope) throws NoSuchDatasetMetadataException, MetadataRegistryStateException {
		if (scope != Scope.PROJECT) return null;

		STMetadataRegistryBackend metadataRegistryBackend = factory.getMetadataRegistryBackend();

		@Nullable IRI mdrIRI = metadataRegistryBackend.findDatasetForProject(project);

		DataIdDatasetMetadataExporterSettings settings = new DataIdDatasetMetadataExporterSettings();

		if (mdrIRI != null) {
			DatasetMetadata datasetMetadata = metadataRegistryBackend.getDatasetMetadata(mdrIRI);


		}

		return settings;
	}

}
