package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.dataid;

import it.uniroma2.art.semanticturkey.constraints.HasDatatype;
import it.uniroma2.art.semanticturkey.constraints.LanguageTaggedString;
import it.uniroma2.art.semanticturkey.constraints.RegExp;
import it.uniroma2.art.semanticturkey.properties.Enumeration;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

import java.util.List;

public class DataIdDistribution implements STProperties {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.dataid.DataIdDistribution";

        public static final String shortName = keyBase + ".shortName";
        public static final String localName$description = keyBase + ".localName.description";
        public static final String localName$displayName = keyBase + ".localName.displayName";
        public static final String type$description = keyBase + ".type.description";
        public static final String type$displayName = keyBase + ".type.displayName";
        public static final String accessURL$description = keyBase + ".accessURL.description";
        public static final String accessURL$displayName = keyBase + ".accessURL.displayName";
        public static final String downloadURL$description = keyBase + ".downloadURL.description";
        public static final String downloadURL$displayName = keyBase + ".downloadURL.displayName";
        public static final String publisher$description = keyBase + ".publisher.description";
        public static final String publisher$displayName = keyBase + ".publisher.displayName";
        public static final String title$description = keyBase + ".title.description";
        public static final String title$displayName = keyBase + ".title.displayName";
        public static final String description$description = keyBase + ".description.description";
        public static final String description$displayName = keyBase + ".description.displayName";
        public static final String license$description = keyBase + ".license.description";
        public static final String license$displayName = keyBase + ".license.displayName";
        public static final String modified$description = keyBase + ".modified.description";
        public static final String modified$displayName = keyBase + ".modified.displayName";
        public static final String issued$description = keyBase + ".issued.description";
        public static final String issued$displayName = keyBase + ".issued.displayName";
        public static final String mediaType$description = keyBase + ".mediaType.description";
        public static final String mediaType$displayName = keyBase + ".mediaType.displayName";
        public static final String previousVersion$description = keyBase + ".previousVersion.description";
        public static final String previousVersion$displayName = keyBase + ".previousVersion.displayName";
        public static final String latestVersion$description = keyBase + ".latestVersion.description";
        public static final String latestVersion$displayName = keyBase + ".latestVersion.displayName";
        public static final String nextVersion$description = keyBase + ".nextVersion.description";
        public static final String nextVersion$displayName = keyBase + ".nextVersion.displayName";
        public static final String hasVersion$description = keyBase + ".hasVersion.description";
        public static final String hasVersion$displayName = keyBase + ".hasVersion.displayName";
        public static final String rights$description = keyBase + ".rights.description";
        public static final String rights$displayName = keyBase + ".rights.displayName";
        public static final String byteSize$description = keyBase + ".byteSize.description";
        public static final String byteSize$displayName = keyBase + ".byteSize.displayName";
        public static final String sha256sum$description = keyBase + ".sha256sum.description";
        public static final String sha256sum$displayName = keyBase + ".sha256sum.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.localName$description + "}", displayName = "{" + MessageKeys.localName$displayName + "}")
    @Required
    public String localName;

    @STProperty(description = "{" + MessageKeys.type$description + "}", displayName = "{" + MessageKeys.type$displayName + "}")
    @Required
    @Enumeration(value = {"ServiceEndpoint", "SingleFile", "Directory", "FileCollection"})
    public String type;

    @STProperty(description = "{" + MessageKeys.accessURL$description + "}", displayName = "{" + MessageKeys.accessURL$displayName + "}")
    public List<IRI> accessURL;

    @STProperty(description = "{" + MessageKeys.downloadURL$description + "}", displayName = "{" + MessageKeys.downloadURL$displayName + "}")
    public List<IRI> downloadURL;

    @STProperty(description = "{" + MessageKeys.publisher$description + "}", displayName = "{" + MessageKeys.publisher$displayName + "}")
    @Required
    public IRI publisher;

    @STProperty(description = "{" + MessageKeys.title$description + "}", displayName = "{" + MessageKeys.title$displayName + "}")
    @Required
    public List<@LanguageTaggedString Literal> title;

    @STProperty(description = "{" + MessageKeys.description$description + "}", displayName = "{" + MessageKeys.description$displayName + "}")
    public List<@LanguageTaggedString Literal> description;

    @STProperty(description = "{" + MessageKeys.license$description + "}", displayName = "{" + MessageKeys.license$displayName + "}")
    public List<IRI> license;

    @STProperty(description = "{" + MessageKeys.modified$description + "}", displayName = "{" + MessageKeys.modified$displayName + "}")
    @Required
    @RegExp(regexp = "^[1-9][0-9]{3}-.+T[^.]+(Z|[+-].+)$")
    public String modified;

    @STProperty(description = "{" + MessageKeys.issued$description + "}", displayName = "{" + MessageKeys.issued$displayName + "}")
    @Required
    @RegExp(regexp = "^[1-9][0-9]{3}-.+T[^.]+(Z|[+-].+)$")
    public String issued;

    @STProperty(description = "{" + MessageKeys.mediaType$description + "}", displayName = "{" + MessageKeys.mediaType$displayName + "}")
    @Enumeration(value = {"<http://dataid.dbpedia.org/ns/mt#ApplicationNTriples>",
            "<http://dataid.dbpedia.org/ns/mt#TextTurtle>",
            "<http://dataid.dbpedia.org/ns/mt#TextTabSeparatedValues>",
            "<http://dataid.dbpedia.org/ns/mt#UNKNOWN>",
            "<http://dataid.dbpedia.org/ns/mt#TextPlain>",
            "<http://dataid.dbpedia.org/ns/mt#ApplicationRDFXML>",
            "<http://dataid.dbpedia.org/ns/mt#ApplicationJson>",
            "<http://dataid.dbpedia.org/ns/mt#TextCSV>",
            "<http://dataid.dbpedia.org/ns/mt#ApplicationXndjson>",
            "<http://dataid.dbpedia.org/ns/mt#ApplicationOctetStream>",
            "<http://dataid.dbpedia.org/ns/mt#ApplicationTrig>"}, open = true)
    public IRI mediaType;

    @STProperty(description = "{" + MessageKeys.previousVersion$description + "}", displayName = "{" + MessageKeys.previousVersion$displayName + "}")
    public List<IRI> previousVersion;

    @STProperty(description = "{" + MessageKeys.latestVersion$description + "}", displayName = "{" + MessageKeys.latestVersion$displayName + "}")
    public List<IRI> latestVersion;

    @STProperty(description = "{" + MessageKeys.nextVersion$description + "}", displayName = "{" + MessageKeys.nextVersion$displayName + "}")
    public List<IRI> nextVersion;

    @STProperty(description = "{" + MessageKeys.hasVersion$description + "}", displayName = "{" + MessageKeys.hasVersion$displayName + "}")
    public String hasVersion;

    @STProperty(description = "{" + MessageKeys.rights$description + "}", displayName = "{" + MessageKeys.rights$displayName + "}")
    public List<@LanguageTaggedString Literal> rights;

    @STProperty(description = "{" + MessageKeys.byteSize$description + "}", displayName = "{" + MessageKeys.byteSize$displayName + "}")
    public Long byteSize;

    @STProperty(description = "{" + MessageKeys.sha256sum$description + "}", displayName = "{" + MessageKeys.sha256sum$displayName + "}")
    @RegExp(regexp = "^[0-9a-f]{64}$")
    public String sha256sum;

}
