package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.dataid;

import it.uniroma2.art.semanticturkey.constraints.HasDatatype;
import it.uniroma2.art.semanticturkey.constraints.LanguageTaggedString;
import it.uniroma2.art.semanticturkey.constraints.RegExp;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

import java.util.List;

public class DataIdDataset implements STProperties {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.dataid.DataIdDataset";

        public static final String shortName = keyBase + ".shortName";
        public static final String localName$description = keyBase + ".localName.description";
        public static final String localName$displayName = keyBase + ".localName.displayName";
        public static final String title$description = keyBase + ".title.description";
        public static final String title$displayName = keyBase + ".title.displayName";
        public static final String description$description = keyBase + ".description.description";
        public static final String description$displayName = keyBase + ".description.displayName";
        public static final String keyword$description = keyBase + ".keyword.description";
        public static final String keyword$displayName = keyBase + ".keyword.displayName";
        public static final String publisher$description = keyBase + ".publisher.description";
        public static final String publisher$displayName = keyBase + ".publisher.displayName";
        public static final String modified$description = keyBase + ".modified.description";
        public static final String modified$displayName = keyBase + ".modified.displayName";
        public static final String issued$description = keyBase + ".issued.description";
        public static final String issued$displayName = keyBase + ".issued.displayName";
        public static final String license$description = keyBase + ".license.description";
        public static final String license$displayName = keyBase + ".license.displayName";
        public static final String landingPage$description = keyBase + ".landingPage.description";
        public static final String landingPage$displayName = keyBase + ".landingPage.displayName";
        public static final String associatedAgent$description = keyBase + ".associatedAgent.description";
        public static final String associatedAgent$displayName = keyBase + ".associatedAgent.displayName";
        public static final String previousVersion$description = keyBase + ".previousVersion.description";
        public static final String previousVersion$displayName = keyBase + ".previousVersion.displayName";
        public static final String latestVersion$description = keyBase + ".latestVersion.description";
        public static final String latestVersion$displayName = keyBase + ".latestVersion.displayName";
        public static final String nextVersion$description = keyBase + ".nextVersion.description";
        public static final String nextVersion$displayName = keyBase + ".nextVersion.displayName";
        public static final String hasVersion$description = keyBase + ".hasVersion.description";
        public static final String hasVersion$displayName = keyBase + ".hasVersion.displayName";
        public static final String similarData$description = keyBase + ".similarData.description";
        public static final String similarData$displayName = keyBase + ".similarData.displayName";
        public static final String theme$description = keyBase + ".theme.description";
        public static final String theme$displayName = keyBase + ".theme.displayName";
        public static final String rights$description = keyBase + ".rights.description";
        public static final String rights$displayName = keyBase + ".rights.displayName";
        public static final String distribution$description = keyBase + ".distribution.description";
        public static final String distribution$displayName = keyBase + ".distribution.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.localName$description + "}", displayName = "{" + MessageKeys.localName$displayName + "}")
    @Required
    public String localName;

    @STProperty(description = "{" + MessageKeys.title$description + "}", displayName = "{" + MessageKeys.title$displayName + "}")
    @Required
    public List<@LanguageTaggedString Literal> title;

    @STProperty(description = "{" + MessageKeys.description$description + "}", displayName = "{" + MessageKeys.description$displayName + "}")
    public List<@LanguageTaggedString Literal> description;

    @STProperty(description = "{" + MessageKeys.keyword$description + "}", displayName = "{" + MessageKeys.keyword$displayName + "}")
    public List<@LanguageTaggedString Literal> keyword;

    @STProperty(description = "{" + MessageKeys.publisher$description + "}", displayName = "{" + MessageKeys.publisher$displayName + "}")
    @Required
    public IRI publisher;

    @STProperty(description = "{" + MessageKeys.modified$description + "}", displayName = "{" + MessageKeys.modified$displayName + "}")
    @Required
    @RegExp(regexp = "^[1-9][0-9]{3}-.+T[^.]+(Z|[+-].+)$")
    public String modified;

    @STProperty(description = "{" + MessageKeys.issued$description + "}", displayName = "{" + MessageKeys.issued$displayName + "}")
    @Required
    @RegExp(regexp = "^[1-9][0-9]{3}-.+T[^.]+(Z|[+-].+)$")
    public String issued;

    @STProperty(description = "{" + MessageKeys.license$description + "}", displayName = "{" + MessageKeys.license$displayName + "}")
    @Required
    public List<IRI> license;

    @STProperty(description = "{" + MessageKeys.landingPage$description + "}", displayName = "{" + MessageKeys.landingPage$displayName + "}")
    public List<IRI> landingPage;

    @STProperty(description = "{" + MessageKeys.associatedAgent$description + "}", displayName = "{" + MessageKeys.associatedAgent$displayName + "}")
    @Required
    public List<IRI> associatedAgent;

    @STProperty(description = "{" + MessageKeys.previousVersion$description + "}", displayName = "{" + MessageKeys.previousVersion$displayName + "}")
    public List<IRI> previousVersion;

    @STProperty(description = "{" + MessageKeys.latestVersion$description + "}", displayName = "{" + MessageKeys.latestVersion$displayName + "}")
    public List<IRI> latestVersion;

    @STProperty(description = "{" + MessageKeys.nextVersion$description + "}", displayName = "{" + MessageKeys.nextVersion$displayName + "}")
    public List<IRI> nextVersion;

    @STProperty(description = "{" + MessageKeys.hasVersion$description + "}", displayName = "{" + MessageKeys.hasVersion$displayName + "}")
    public String hasVersion;

    @STProperty(description = "{" + MessageKeys.similarData$description + "}", displayName = "{" + MessageKeys.similarData$displayName + "}")
    public List<IRI> similarData;

    @STProperty(description = "{" + MessageKeys.theme$description + "}", displayName = "{" + MessageKeys.theme$displayName + "}")
    public List<IRI> theme;

    @STProperty(description = "{" + MessageKeys.rights$description + "}", displayName = "{" + MessageKeys.rights$displayName + "}")
    public List<@LanguageTaggedString Literal> rights;

    @STProperty(description = "{" + MessageKeys.distribution$description + "}", displayName = "{" + MessageKeys.distribution$displayName + "}")
    public List<DataIdDistribution> distribution;


}
