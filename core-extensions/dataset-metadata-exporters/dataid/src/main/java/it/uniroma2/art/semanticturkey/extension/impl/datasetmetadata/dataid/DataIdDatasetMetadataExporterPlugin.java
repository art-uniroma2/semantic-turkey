package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.dataid;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class DataIdDatasetMetadataExporterPlugin extends STPlugin {
    public DataIdDatasetMetadataExporterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
