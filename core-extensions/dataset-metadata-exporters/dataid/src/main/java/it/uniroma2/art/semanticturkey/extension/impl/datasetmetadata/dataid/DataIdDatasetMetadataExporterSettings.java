package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.dataid;

import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import org.eclipse.rdf4j.model.IRI;

public class DataIdDatasetMetadataExporterSettings implements Settings {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.dataid.DataIdDatasetMetadataExporterSettings";

        public static final String shortName = keyBase + ".shortName";
        public static final String dataIdBaseUri$description = keyBase + ".dataIdBaseUri.description";
        public static final String dataIdBaseUri$displayName = keyBase + ".dataIdBaseUri.displayName";
        public static final String dataset$description = keyBase + ".dataset.description";
        public static final String dataset$displayName = keyBase + ".dataset.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.dataIdBaseUri$description + "}", displayName = "{" + MessageKeys.dataIdBaseUri$displayName + "}")
    @Required
    public IRI dataIdBaseUri;

    @STProperty(description = "{" + MessageKeys.dataset$description + "}", displayName = "{" + MessageKeys.dataset$displayName + "}")
    @Required
    public DataIdDataset dataset;


}
