package it.uniroma2.art.semanticturkey.extension.impl.datasetmetadata.dataid;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public abstract class DATAID {

	/** The DataId namespace: http://dataid.dbpedia.org/ns/core# */
	public static final String NAMESPACE = "http://dataid.dbpedia.org/ns/core#";

	/**
	 * Recommended prefix for the DataId namespace: "dataid"
	 */
	public static final String PREFIX = "dataid";

	/**
	 * An immutable {@link Namespace} constant that represents the DataId namespace.
	 */
	public static final Namespace NS = new SimpleNamespace(PREFIX, NAMESPACE);

	/** dataid:Dataset */
	public static final IRI DATASET;

	/** dataid:associatedAgent */
	public static final IRI ASSOCIATED_AGENT;

	/** dataid:previousVersion */
	public static final IRI PREVIOUS_VERSION;

	/** dataid:nextVersion */
	public static final IRI NEXT_VERSION;

	/** dataid:latestVersion */
	public static final IRI LATEST_VERSION;

	/** dataid:similarData */
	public static final IRI SIMILAR_DATA;

	/** dataid:sha256sum */
	public static final IRI SHA256SUM;


	static {
		ValueFactory vf = SimpleValueFactory.getInstance();

		DATASET = vf.createIRI(NAMESPACE, "Dataset");
		ASSOCIATED_AGENT = vf.createIRI(NAMESPACE, "associatedAgent");
		PREVIOUS_VERSION = vf.createIRI(NAMESPACE, "previousVersion");
		NEXT_VERSION = vf.createIRI(NAMESPACE, "nextVersion");
		LATEST_VERSION = vf.createIRI(NAMESPACE, "latestVersion");
		SIMILAR_DATA = vf.createIRI(NAMESPACE, "similarData");
		SHA256SUM = vf.createIRI(NAMESPACE, "sha256sum");
	}
}
