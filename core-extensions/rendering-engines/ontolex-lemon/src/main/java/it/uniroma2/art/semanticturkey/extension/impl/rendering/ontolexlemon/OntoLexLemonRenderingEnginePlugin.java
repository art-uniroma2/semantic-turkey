package it.uniroma2.art.semanticturkey.extension.impl.rendering.ontolexlemon;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class OntoLexLemonRenderingEnginePlugin extends STPlugin {
    public OntoLexLemonRenderingEnginePlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
