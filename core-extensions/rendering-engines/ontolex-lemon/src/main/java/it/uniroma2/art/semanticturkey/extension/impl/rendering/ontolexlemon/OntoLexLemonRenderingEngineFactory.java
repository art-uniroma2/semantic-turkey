package it.uniroma2.art.semanticturkey.extension.impl.rendering.ontolexlemon;

import java.util.Arrays;
import java.util.Collection;

import it.uniroma2.art.semanticturkey.config.InvalidConfigurationException;
import it.uniroma2.art.semanticturkey.extension.ConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.NonConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.extpts.rendering.RenderingEngineExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.settings.PUSettingsManager;
import it.uniroma2.art.semanticturkey.i18n.STMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Factory for the instantiation of {@link OntoLexLemonRenderingEngine}.
 */
@Component
public class OntoLexLemonRenderingEngineFactory implements
		ConfigurableExtensionFactory<OntoLexLemonRenderingEngine, OntoLexLemonRenderingEngineConfiguration> {

	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.rendering.ontolexlemon.OntoLexLemonRenderingEngineFactory";
		private static final String name = keyBase + ".name";
		private static final String description = keyBase + ".description";
	}

	@Autowired
	private RenderingEngineExtensionPoint renderingEngineExtensionPoint;

	@Override
	public String getName() {
		return STMessageSource.getMessage(MessageKeys.name);
	}

	@Override
	public String getDescription() {
		return STMessageSource.getMessage(MessageKeys.description);
	}

	@Override
	public OntoLexLemonRenderingEngine createInstance(OntoLexLemonRenderingEngineConfiguration conf)
			throws InvalidConfigurationException {
		return new OntoLexLemonRenderingEngine(conf, renderingEngineExtensionPoint);
	}

	@Override
	public Collection<OntoLexLemonRenderingEngineConfiguration> getConfigurations() {
		return Arrays.asList(new OntoLexLemonRenderingEngineConfiguration());
	}
}
