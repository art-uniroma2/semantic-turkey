package it.uniroma2.art.semanticturkey.extension.impl.rendering.skos;

import java.util.Arrays;
import java.util.Collection;

import it.uniroma2.art.semanticturkey.config.InvalidConfigurationException;
import it.uniroma2.art.semanticturkey.extension.ConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.extpts.rendering.RenderingEngineExtensionPoint;
import it.uniroma2.art.semanticturkey.i18n.STMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Factory for the instantiation of {@link SKOSRenderingEngine}.
 */
@Component
public class SKOSRenderingEngineFactory
		implements ConfigurableExtensionFactory<SKOSRenderingEngine, SKOSRenderingEngineConfiguration> {

	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.rendering.skos.SKOSRenderingEngineFactory";
		private static final String name = keyBase + ".name";
		private static final String description = keyBase + ".description";
	}

	@Autowired
	private RenderingEngineExtensionPoint renderingEngineExtensionPoint;

	@Override
	public String getName() {
		return STMessageSource.getMessage(MessageKeys.name);
	}

	@Override
	public String getDescription() {
		return STMessageSource.getMessage(MessageKeys.description);
	}

	@Override
	public SKOSRenderingEngine createInstance(SKOSRenderingEngineConfiguration conf)
			throws InvalidConfigurationException {
		return new SKOSRenderingEngine(conf, renderingEngineExtensionPoint);
	}

	@Override
	public Collection<SKOSRenderingEngineConfiguration> getConfigurations() {
		return Arrays.asList(new SKOSRenderingEngineConfiguration());
	}

	
}
