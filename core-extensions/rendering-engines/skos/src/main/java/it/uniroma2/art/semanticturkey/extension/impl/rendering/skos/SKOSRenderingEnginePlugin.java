package it.uniroma2.art.semanticturkey.extension.impl.rendering.skos;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class SKOSRenderingEnginePlugin extends STPlugin {
    public SKOSRenderingEnginePlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
