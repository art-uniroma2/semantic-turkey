package it.uniroma2.art.semanticturkey.extension.impl.rendering.rdfs;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class RDFSRenderingEnginePlugin extends STPlugin {
    public RDFSRenderingEnginePlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
