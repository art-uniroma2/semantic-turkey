package it.uniroma2.art.semanticturkey.extension.impl.rendering.skosxl;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class SKOSXLRenderingEnginePlugin extends STPlugin {
    public SKOSXLRenderingEnginePlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
