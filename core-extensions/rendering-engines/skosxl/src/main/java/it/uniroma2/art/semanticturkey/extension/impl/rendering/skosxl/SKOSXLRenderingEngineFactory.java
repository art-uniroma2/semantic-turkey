package it.uniroma2.art.semanticturkey.extension.impl.rendering.skosxl;

import java.util.Arrays;
import java.util.Collection;

import it.uniroma2.art.semanticturkey.config.InvalidConfigurationException;
import it.uniroma2.art.semanticturkey.extension.ConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.extpts.rendering.RenderingEngineExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.extpts.rendering.RenderingEnginePUSettings;
import it.uniroma2.art.semanticturkey.i18n.STMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Factory for the instantiation of {@link SKOSXLRenderingEngine}.
 */
@Component
public class SKOSXLRenderingEngineFactory
		implements ConfigurableExtensionFactory<SKOSXLRenderingEngine, SKOSXLRenderingEngineConfiguration> {

	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.rendering.skosxl.SKOSXLRenderingEngineFactory";
		private static final String name = keyBase + ".name";
		private static final String description = keyBase + ".description";
	}

	@Autowired
	private RenderingEngineExtensionPoint renderingEngineExtensionPoint;

	@Override
	public String getName() {
		return STMessageSource.getMessage(MessageKeys.name);
	}

	@Override
	public String getDescription() {
		return STMessageSource.getMessage(MessageKeys.description);
	}

	@Override
	public SKOSXLRenderingEngine createInstance(SKOSXLRenderingEngineConfiguration conf)
			throws InvalidConfigurationException {
		return new SKOSXLRenderingEngine(conf, renderingEngineExtensionPoint);
	}

	@Override
	public Collection<SKOSXLRenderingEngineConfiguration> getConfigurations() {
		return Arrays.asList(new SKOSXLRenderingEngineConfiguration());
	}
}
