package it.uniroma2.art.semanticturkey.extension.impl.deployer.showvoc;

import it.uniroma2.art.semanticturkey.config.Configuration;
import it.uniroma2.art.semanticturkey.properties.FallbackSetting;
import it.uniroma2.art.semanticturkey.properties.OmitDefault;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import it.uniroma2.art.semanticturkey.resources.Scope;

/**
 * Configuration abstract base class for {@link ShowVocDeployer}.
 * 
 * <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 */
public abstract class ShowVocDeployerConfiguration implements Configuration {

	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.deployer.showvoc.ShowVocDeployerConfiguration";

		public static final String shortName = keyBase + ".shortName";
		public static final String htmlWarning = keyBase + ".htmlWarning";
		public static final String stHost$description = keyBase + ".stHost.description";
		public static final String stHost$displayName = keyBase + ".stHost.displayName";
		public static final String username$description = keyBase + ".username.description";
		public static final String username$displayName = keyBase + ".username.displayName";
		public static final String password$description = keyBase + ".password.description";
		public static final String password$displayName = keyBase + ".password.displayName";
		public static final String project$description = keyBase + ".project.description";
		public static final String project$displayName = keyBase + ".project.displayName";
	}

	@Override
	public String getShortName() {
		return "{" + MessageKeys.shortName + "}";
	}

	@Override
	public String getHTMLWarning() {
		return "{" + MessageKeys.htmlWarning + "}";
	}

	@STProperty(description = "{" + MessageKeys.stHost$description + "}", displayName = "{" + MessageKeys.stHost$displayName + "}")
	@Required
	@FallbackSetting(
			manager = "it.uniroma2.art.semanticturkey.settings.connections.showvoc.ConnectedShowVocSettingsManager",
			scope = Scope.SYSTEM,
			path = "apiBaseURL"
	)
	public String stHost;

	@STProperty(description = "{" + MessageKeys.username$description + "}", displayName = "{" + MessageKeys.username$displayName + "}")
	@Required
	@FallbackSetting(
			manager = "it.uniroma2.art.semanticturkey.settings.connections.showvoc.ConnectedShowVocSettingsManager",
			scope = Scope.SYSTEM,
			path = "username"
	)
	public String username;

	@STProperty(description = "{" + MessageKeys.password$description + "}", displayName = "{" + MessageKeys.password$displayName + "}")
	@Required
	@FallbackSetting(
			manager = "it.uniroma2.art.semanticturkey.settings.connections.showvoc.ConnectedShowVocSettingsManager",
			scope = Scope.SYSTEM,
			path = "password"
	)
	@OmitDefault()
	public String password;

	@STProperty(description = "{" + MessageKeys.project$description + "}", displayName = "{" + MessageKeys.project$displayName + "}")
	@Required
	public String project;
}
