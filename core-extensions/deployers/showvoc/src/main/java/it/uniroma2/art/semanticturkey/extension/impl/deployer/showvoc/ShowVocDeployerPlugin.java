package it.uniroma2.art.semanticturkey.extension.impl.deployer.showvoc;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class ShowVocDeployerPlugin extends STPlugin {
    public ShowVocDeployerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
