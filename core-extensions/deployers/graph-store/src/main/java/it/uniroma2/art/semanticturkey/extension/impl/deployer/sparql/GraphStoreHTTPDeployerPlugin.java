package it.uniroma2.art.semanticturkey.extension.impl.deployer.sparql;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class GraphStoreHTTPDeployerPlugin extends STPlugin {
    public GraphStoreHTTPDeployerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
