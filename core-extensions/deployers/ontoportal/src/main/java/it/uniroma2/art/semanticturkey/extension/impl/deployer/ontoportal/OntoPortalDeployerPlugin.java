package it.uniroma2.art.semanticturkey.extension.impl.deployer.ontoportal;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class OntoPortalDeployerPlugin extends STPlugin {
    public OntoPortalDeployerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
