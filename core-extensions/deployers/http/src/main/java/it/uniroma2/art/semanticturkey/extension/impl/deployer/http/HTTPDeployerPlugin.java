package it.uniroma2.art.semanticturkey.extension.impl.deployer.http;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class HTTPDeployerPlugin extends STPlugin {
    public HTTPDeployerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
