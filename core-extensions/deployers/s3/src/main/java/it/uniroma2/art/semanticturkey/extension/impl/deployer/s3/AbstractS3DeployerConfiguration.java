package it.uniroma2.art.semanticturkey.extension.impl.deployer.s3;

import it.uniroma2.art.semanticturkey.config.Configuration;

/**
 * Abstract base class of configuration classes for the {@link S3DeployerFactory}.
 *
 * @author <a href="mailto:tiziano.dicondina@devit.cloud">Tiziano Di Condina</a>
 */
public abstract class AbstractS3DeployerConfiguration implements Configuration {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.deployer.s3.AbstractS3DeployerConfiguration";

        public static final String shortName = keyBase + ".shortName";
        public static final String htmlWarning = keyBase + ".htmlWarning";
    }

    @Override
    public String getHTMLWarning() {
        return "{" + MessageKeys.htmlWarning + "}";
    }
}
