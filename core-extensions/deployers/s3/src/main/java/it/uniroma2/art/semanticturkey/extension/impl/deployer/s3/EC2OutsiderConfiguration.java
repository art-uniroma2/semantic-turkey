package it.uniroma2.art.semanticturkey.extension.impl.deployer.s3;

import it.uniroma2.art.semanticturkey.config.Configuration;
import it.uniroma2.art.semanticturkey.properties.FallbackSetting;
import it.uniroma2.art.semanticturkey.properties.OmitDefault;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import it.uniroma2.art.semanticturkey.resources.Scope;
import jakarta.validation.constraints.Pattern;

/**
 * Configuration class for {@link S3DeployerFactory}.
 *
 * @author <a href="mailto:tiziano.dicondina@devit.cloud">Tiziano Di Condina</a>
 */
public class EC2OutsiderConfiguration extends AbstractS3DeployerConfiguration implements Configuration {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.deployer.s3.EC2OutsiderConfiguration";

        public static final String shortName = keyBase + ".shortName";
        public static final String region$description = keyBase + ".region.description";
        public static final String region$displayName = keyBase + ".region.displayName";
        public static final String accessKey$description = keyBase + ".accessKey.description";
        public static final String accessKey$displayName = keyBase + ".accessKey.displayName";
        public static final String secretKey$description = keyBase + ".secretKey.description";
        public static final String secretKey$displayName = keyBase + ".secretKey.displayName";
        public static final String bucket$description = keyBase + ".bucket.description";
        public static final String bucket$displayName = keyBase + ".bucket.displayName";
        public static final String rootPath$description = keyBase + ".rootPath.description";
        public static final String rootPath$displayName = keyBase + ".rootPath.displayName";
        public static final String localPath$description = keyBase + ".localPath.description";
        public static final String localPath$displayName = keyBase + ".localPath.displayName";
        public static final String filename$description = keyBase + ".filename.description";
        public static final String filename$displayName = keyBase + ".filename.displayName";
        public static final String versionID$description = keyBase + ".versionID.description";
        public static final String versionID$displayName = keyBase + ".versionID.displayName";
        public static final String versioning$description = keyBase + ".versioning.description";
        public static final String versioning$displayName = keyBase + ".versioning.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.region$description + "}", displayName = "{" + MessageKeys.region$displayName + "}")
    @FallbackSetting(
            manager = "it.uniroma2.art.semanticturkey.settings.s3.S3Manager",
            scope = Scope.PROJECT,
            path = "region"
    )
    public String region;

    @STProperty(description = "{" + MessageKeys.accessKey$description + "}", displayName = "{" + MessageKeys.accessKey$displayName + "}")
    @OmitDefault()
    @FallbackSetting(
            manager = "it.uniroma2.art.semanticturkey.settings.s3.S3Manager",
            scope = Scope.PROJECT,
            path = "accessKey"
    )
    public String accessKey;

    @STProperty(description = "{" + MessageKeys.secretKey$description + "}", displayName = "{" + MessageKeys.secretKey$displayName + "}")
    @OmitDefault()
    @FallbackSetting(
            manager = "it.uniroma2.art.semanticturkey.settings.s3.S3Manager",
            scope = Scope.PROJECT,
            path = "secretKey"
    )
    public String secretKey;

    @STProperty(description = "{" + MessageKeys.bucket$description + "}", displayName = "{" + MessageKeys.bucket$displayName + "}")
    @FallbackSetting(
            manager = "it.uniroma2.art.semanticturkey.settings.s3.S3Manager",
            scope = Scope.PROJECT,
            path = "bucket"
    )
    public String bucket;

    @STProperty(description = "{" + MessageKeys.rootPath$description + "}", displayName = "{" + MessageKeys.rootPath$displayName + "}")
    @FallbackSetting(
            manager = "it.uniroma2.art.semanticturkey.settings.s3.S3Manager",
            scope = Scope.PROJECT,
            path = "rootPath"
    )
    public String rootPath;

    @STProperty(description = "{" + MessageKeys.localPath$description + "}", displayName = "{" + MessageKeys.localPath$displayName + "}")
    @FallbackSetting(
            manager = "it.uniroma2.art.semanticturkey.settings.s3.S3Manager",
            scope = Scope.PROJECT,
            path = "localPath"
    )
    public String localPath;

    @STProperty(description = "{" + MessageKeys.filename$description + "}", displayName = "{" + MessageKeys.filename$displayName + "}")
    @FallbackSetting(
            manager = "it.uniroma2.art.semanticturkey.settings.s3.S3Manager",
            scope = Scope.PROJECT,
            path = "filename"
    )
    public String filename;

    @Required
    @Pattern(regexp = "[0-9]+\\.[0-9]+(\\.[0-9]+)*")
    @STProperty(description = "{" + MessageKeys.versionID$description + "}", displayName = "{" + MessageKeys.versionID$displayName + "}")
    public String versionID;

    @STProperty(description = "{" + MessageKeys.versioning$description + "}", displayName = "{" + MessageKeys.versioning$displayName + "}")
    @FallbackSetting(
            manager = "it.uniroma2.art.semanticturkey.settings.s3.S3Manager",
            scope = Scope.PROJECT,
            path = "versioning"
    )
    public Boolean versioning;
}
