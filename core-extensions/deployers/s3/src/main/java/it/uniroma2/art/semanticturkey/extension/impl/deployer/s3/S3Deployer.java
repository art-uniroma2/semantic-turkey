package it.uniroma2.art.semanticturkey.extension.impl.deployer.s3;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import it.uniroma2.art.semanticturkey.exceptions.InvalidProjectNameException;
import it.uniroma2.art.semanticturkey.exceptions.ProjectAccessException;
import it.uniroma2.art.semanticturkey.exceptions.ProjectInexistentException;
import it.uniroma2.art.semanticturkey.extension.extpts.deployer.Deployer;
import it.uniroma2.art.semanticturkey.extension.extpts.deployer.FormattedResourceSource;
import it.uniroma2.art.semanticturkey.extension.extpts.deployer.StreamSourcedDeployer;
import it.uniroma2.art.semanticturkey.mvc.ResolvedRequestHolder;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.project.ProjectManager;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.settings.s3.S3Manager;
import it.uniroma2.art.semanticturkey.settings.s3.S3ProjectSettings;
import it.uniroma2.art.semanticturkey.user.STUser;
import it.uniroma2.art.semanticturkey.user.UsersManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;

/**
 * An implementation of the {@link Deployer} extension point that uses the Amazon S3 protocol to deploy
 * data into an S3 bucket. This class handles the deployment of resources
 * provided by a {@link FormattedResourceSource} by uploading the corresponding file to a specified bucket
 * and path within S3.
 *
 * @author <a href="mailto:tiziano.dicondina@devit.cloud">Tiziano Di Condina</a>
 */
public class S3Deployer implements StreamSourcedDeployer {

    protected static Logger logger = LoggerFactory.getLogger(S3Deployer.class);

    private final AbstractS3DeployerConfiguration conf;

    private final S3Manager s3Manager;

    private final AmazonS3 amazonS3;

    public S3Deployer(AbstractS3DeployerConfiguration conf, S3Manager s3Manager) {
        this.conf = conf;
        this.amazonS3 = getAmazonS3(conf);
        this.s3Manager = s3Manager;
    }

    @Override
    public void deploy(FormattedResourceSource source) throws IOException {

        // get the project from the HTTP request
        String ctxProject = ResolvedRequestHolder.get().getParameter("ctx_project");

        // get user
        STUser user = UsersManager.getLoggedUser();

        // init filename
        String filename = "dataset";

        // init localPath
        String localPath = ctxProject;

        // init rootPath
        String rootPath = null;

        // init bucket
        String bucket;

        // init versionID
        String versionID;

        // init versioning
        Boolean versioning;

        var fileExtension = source.getSourceFormattedResource().getDefaultFileExtension();

        try {

            // get the project to load the project settings
            Project project = ProjectManager.getProjectDescription(ctxProject);

            // get project settings
            S3ProjectSettings projectSettings = s3Manager.getProjectSettings(project);

            // get the class of the config
            Class<?> clazz = AopUtils.getTargetClass(conf);

            // create S3 object metadata
            var objectMetadata = new ObjectMetadata();

            // retrieve the backing file from the source's formatted resource. This file contains the data to be uploaded to S3
            File backingFile = source.getSourceFormattedResource().getBackingFile();

            // init the list of missing parameters
            var missingParameters = new ArrayList<String>();

            if (Objects.equals(clazz, EC2OutsiderConfiguration.class)) {

                // cast the config as EC2OutsiderConfiguration
                var castedConf = (EC2OutsiderConfiguration) conf;

                // get localPath from the config
                var confLocalPath = castedConf.localPath;

                // override ctxProject with localPath
                if (confLocalPath != null && !confLocalPath.isEmpty()) {
                    localPath = confLocalPath;
                }

                // get destination path from config
                String confRootPath = castedConf.rootPath;
                if (confRootPath != null && !confRootPath.isEmpty()) {
                    rootPath = confRootPath;
                }

                // get the bucket
                bucket = castedConf.bucket;

                // get the filename
                var configFilename = castedConf.filename;
                if (configFilename != null && !configFilename.isEmpty()) {
                    filename = configFilename;
                }

                // get versionID
                versionID = castedConf.versionID;

                // get versioning
                versioning = castedConf.versioning;

            } else if (Objects.equals(clazz, EC2InsiderConfiguration.class)) {

                // cast the config as EC2InsiderConfiguration
                var castedConf = (EC2InsiderConfiguration) conf;

                // get localPath from the config
                var confLocalPath = castedConf.localPath;

                // override ctxProject with localPath
                if (confLocalPath != null && !confLocalPath.isEmpty()) {
                    localPath = confLocalPath;
                }

                // get destination path from config
                String confRootPath = castedConf.rootPath;

                if (confRootPath != null && !confRootPath.isEmpty()) {
                    rootPath = confRootPath;
                }

                // get the bucket
                bucket = castedConf.bucket;

                // get the filename
                var configFilename = castedConf.filename;
                if (configFilename != null && !configFilename.isEmpty()) {
                    filename = configFilename;
                }

                // get versionID
                versionID = castedConf.versionID;

                // get versioning
                versioning = castedConf.versioning;

            } else {

                // cast the config as EC2InsiderCompactConfiguration
                var castedConf = (EC2InsiderCompactConfiguration) conf;

                // get destination path from config
                String settingsRootPath = projectSettings.rootPath;

                if (settingsRootPath != null && !settingsRootPath.isEmpty()) {
                    rootPath = settingsRootPath;
                }

                // get the bucket
                bucket = projectSettings.bucket;

                // get the filename
                var configFilename = castedConf.filename;
                if (configFilename != null && !configFilename.isEmpty()) {
                    filename = configFilename;
                }

                // get versionID
                versionID = castedConf.versionID;

                // get versioning
                versioning = projectSettings.versioning;
            }

            // add versioning as missing parameter if not present
            if (Objects.isNull(versioning)) missingParameters.add("versioning");

            // add bucket as missing parameter if not present
            if (Objects.isNull(bucket)) missingParameters.add("bucket");

            if (!missingParameters.isEmpty()) {
                throw new STPropertyAccessException(
                        String.format("Parameter(s) %s not found", String.join(", ", missingParameters))
                );
            }

            // create metadata
            objectMetadata.addUserMetadata("user", user.getIRI().stringValue());
            objectMetadata.addUserMetadata("semanticmodel", project.getModel().stringValue());
            objectMetadata.addUserMetadata("lexicalmodel", project.getLexicalizationModel().stringValue());
            objectMetadata.addUserMetadata("baseuri", project.getBaseURI());

            // if versioning is true set the version ID as metadata, else append it into the filename
            if (versioning) {
                objectMetadata.addUserMetadata("versionid", versionID);
            } else {
                filename = filename.concat("_").concat(versionID);
            }

            // add the extension to the filename
            filename = filename.concat(".").concat(fileExtension);

            // init file path
            var path = getPath(rootPath, localPath, filename);

            // init the object for S3 request
            var putObjectRequest = new PutObjectRequest(bucket, path, backingFile);

            // add metadata
            putObjectRequest.withMetadata(objectMetadata);

            try {

                // upload the retrieved file to the specified S3 bucket and destination path using the Amazon S3 client
                amazonS3.putObject(putObjectRequest);

                // write the content of the source's formatted resource to the file using a FileOutputStream. This ensures the data is persisted to the file after being uploaded to S3
                var fos = new FileOutputStream(backingFile);
                source.getSourceFormattedResource().writeTo(fos);

                // close the stream
                fos.close();
            } catch (AmazonServiceException e) {
                // if an Amazon S3 error occurs (e.g., bucket not found, access denied), log the error message
                logger.error("S3 error: {}", e.getErrorMessage());
                throw new IllegalArgumentException(e);
            }

        } catch (STPropertyAccessException | ProjectAccessException | ProjectInexistentException |
                 InvalidProjectNameException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Constructs a full path by concatenating the root path, local path, and filename.
     * <p>
     * This method handles null or empty values for the provided paths and ensures proper
     * concatenation with separators ('/').
     * </p>
     *
     * @param rootPath  the root directory path, may be null or empty
     * @param localPath the local directory path, must not be null or empty
     * @param filename  the name of the file to include in the path, must not be null or empty
     * @return the constructed full path as a {@code String}, ensuring the correct use of separators
     */
    private String getPath(String rootPath, String localPath, String filename) {
        var path = "";

        // add rootPath if not empty
        if (Objects.nonNull(rootPath) && !rootPath.isEmpty()) {
            path = rootPath.replaceAll("^/+", "").replaceAll("/+$", "");
        }

        // add localPath
        if (!path.isEmpty()) {
            path = path.concat("/");
        }
        path = path.concat(localPath);

        path = path.isEmpty() ? path.concat(filename) : path.concat("/").concat(filename);
        return path;
    }

    /**
     * Creates and returns an Amazon S3 client based on the provided S3 deployment configuration.
     * <p>
     * The method determines the specific S3 configuration type and initializes the client
     * with the appropriate credentials and region settings. For unrecognized configuration types,
     * an {@code IllegalArgumentException} is thrown.
     * </p>
     *
     * @param conf the S3 deployment configuration, must be an instance of a recognized configuration class
     *             such as {@code EC2OutsiderConfiguration}, {@code EC2InsiderConfiguration}, or
     *             {@code EC2InsiderCompactConfiguration}.
     * @return an {@code AmazonS3} client configured according to the specified deployment configuration
     * @throws IllegalArgumentException if the configuration type is not recognized
     */
    private AmazonS3 getAmazonS3(AbstractS3DeployerConfiguration conf) {
        Class<?> clazz = AopUtils.getTargetClass(conf);

        if (Objects.equals(clazz, EC2OutsiderConfiguration.class)) {
            EC2OutsiderConfiguration currentConf = (EC2OutsiderConfiguration) conf;
            return AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(currentConf.accessKey, currentConf.secretKey))).withRegion(currentConf.region).build();
        } else if (Objects.equals(clazz, EC2InsiderConfiguration.class) || Objects.equals(clazz, EC2InsiderCompactConfiguration.class)) {
            return AmazonS3ClientBuilder.defaultClient();
        } else {
            throw new IllegalArgumentException("Unrecognized configuration type");
        }
    }

}
