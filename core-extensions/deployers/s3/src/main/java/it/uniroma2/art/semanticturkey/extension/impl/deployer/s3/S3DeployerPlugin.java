package it.uniroma2.art.semanticturkey.extension.impl.deployer.s3;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class S3DeployerPlugin extends STPlugin {
    public S3DeployerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
