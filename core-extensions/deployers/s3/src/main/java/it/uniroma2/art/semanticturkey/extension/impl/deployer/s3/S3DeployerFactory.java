package it.uniroma2.art.semanticturkey.extension.impl.deployer.s3;

import java.util.Arrays;
import java.util.Collection;

import it.uniroma2.art.semanticturkey.extension.ConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.ExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.PUScopedConfigurableComponent;
import it.uniroma2.art.semanticturkey.i18n.STMessageSource;
import it.uniroma2.art.semanticturkey.settings.s3.S3Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The {@link ExtensionFactory} for the {@link S3Deployer}.
 * 
 * @author <a href="mailto:tiziano.dicondina@devit.cloud">Tiziano Di Condina</a>
 */
@Component
public class S3DeployerFactory implements ExtensionFactory<S3Deployer>,
		ConfigurableExtensionFactory<S3Deployer, AbstractS3DeployerConfiguration>,
		PUScopedConfigurableComponent<AbstractS3DeployerConfiguration> {

	private final S3Manager s3Manager;

	public S3DeployerFactory(S3Manager s3Manager) {
		this.s3Manager = s3Manager;
	}

	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.deployer.s3.S3DeployerFactory";
		private static final String name = keyBase + ".name";
		private static final String description = keyBase + ".description";
	}

	@Override
	public String getName() {
		return STMessageSource.getMessage(MessageKeys.name);
	}

	@Override
	public String getDescription() {
		return STMessageSource.getMessage(MessageKeys.description);
	}

	@Override
	public S3Deployer createInstance(AbstractS3DeployerConfiguration conf) {
		return new S3Deployer(conf, s3Manager);
	}

	@Override
	public Collection<AbstractS3DeployerConfiguration> getConfigurations() {
		return Arrays.asList(new EC2InsiderConfiguration(), new EC2OutsiderConfiguration(), new EC2InsiderCompactConfiguration());
	}

}
