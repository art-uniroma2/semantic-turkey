package it.uniroma2.art.semanticturkey.extension.impl.deployer.sftp;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class SFTPDeployerPlugin extends STPlugin {
    public SFTPDeployerPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
