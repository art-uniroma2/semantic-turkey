package it.uniroma2.art.semanticturkey.extension.impl.search.graphdb;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class GraphDBSearchStrategyPlugin extends STPlugin {
    public GraphDBSearchStrategyPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
