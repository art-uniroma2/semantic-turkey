package it.uniroma2.art.semanticturkey.extension.impl.search.regex;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class RegexSearchStrategyPlugin extends STPlugin {
    public RegexSearchStrategyPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
