package it.uniroma2.art.semanticturkey.extension.impl.loader.sparql;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class GraphStoreHTTPLoaderPlugin extends STPlugin {
    public GraphStoreHTTPLoaderPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
