package it.uniroma2.art.semanticturkey.extension.impl.loader.s3;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import it.uniroma2.art.semanticturkey.exceptions.InvalidProjectNameException;
import it.uniroma2.art.semanticturkey.exceptions.ProjectAccessException;
import it.uniroma2.art.semanticturkey.exceptions.ProjectInexistentException;
import it.uniroma2.art.semanticturkey.extension.extpts.reformattingexporter.ClosableFormattedResource;
import it.uniroma2.art.semanticturkey.mvc.ResolvedRequestHolder;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.project.ProjectManager;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.settings.s3.S3Manager;
import it.uniroma2.art.semanticturkey.settings.s3.S3ProjectSettings;
import jakarta.annotation.Nullable;

import it.uniroma2.art.semanticturkey.extension.extpts.loader.FormattedResourceTarget;
import it.uniroma2.art.semanticturkey.extension.extpts.loader.Loader;
import it.uniroma2.art.semanticturkey.extension.extpts.loader.StreamTargetingLoader;
import it.uniroma2.art.semanticturkey.resources.DataFormat;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.aop.support.AopUtils;

/**
 * An implementation of the {@link Loader} extension point that leverages the Amazon S3 protocol
 * to load data from an S3 bucket into a {@link FormattedResourceTarget}. This class retrieves
 * the content from an S3 object, stores it temporarily on the local file system, and then
 * passes it to the target resource for further processing.
 *
 * @author <a href="mailto:tiziano.dicondina@devit.cloud">Tiziano Di Condina</a>
 */
public class S3Loader implements StreamTargetingLoader {

    private final AbstractS3LoaderConfiguration conf;

    private final S3Manager s3Manager;

    private final AmazonS3 amazonS3;

    public S3Loader(AbstractS3LoaderConfiguration conf, S3Manager s3Manager) {
        this.conf = conf;
        this.amazonS3 = getAmazonS3(conf);
        this.s3Manager = s3Manager;
    }

    @Override
    public void load(FormattedResourceTarget target, @Nullable DataFormat acceptedFormat) throws IOException {

        // get the project from the HTTP request
        String ctxProject = ResolvedRequestHolder.get().getParameter("ctx_project");

        // init filename
        String filename = "dataset";

        // init localPath
        String localPath = ctxProject;

        // init rootPath
        String rootPath = null;

        // init bucket
        String bucket;

        // init versionID
        String versionID;

        // init versioning
        Boolean versioning;

        try {

            // get the project to load the project settings
            Project project = ProjectManager.getProjectDescription(ctxProject);

            // get project settings
            S3ProjectSettings projectSettings = s3Manager.getProjectSettings(project);

            // get the class of the config
            Class<?> clazz = AopUtils.getTargetClass(conf);

            // init the list of missing parameters
            var missingParameters = new ArrayList<String>();

            if (Objects.equals(clazz, EC2OutsiderConfiguration.class)) {

                // cast the config as EC2OutsiderConfiguration
                var castedConf = (EC2OutsiderConfiguration) conf;

                // get localPath from the config
                var confLocalPath = castedConf.localPath;

                // override ctxProject with localPath
                if (confLocalPath != null && !confLocalPath.isEmpty()) {
                    localPath = confLocalPath;
                }

                // get destination path from config
                String confRootPath = castedConf.rootPath;
                if (confRootPath != null && !confRootPath.isEmpty()) {
                    rootPath = confRootPath;
                }

                // get the bucket
                bucket = castedConf.bucket;

                // get the filename
                var configFilename = castedConf.filename;
                if (configFilename != null && !configFilename.isEmpty()) {
                    filename = configFilename;
                }

                // get versionID
                versionID = castedConf.versionID;

                // get versioning
                versioning = castedConf.versioning;

            } else if (Objects.equals(clazz, EC2InsiderConfiguration.class)) {

                // cast the config as EC2InsiderConfiguration
                var castedConf = (EC2InsiderConfiguration) conf;

                // get localPath from the config
                var confLocalPath = castedConf.localPath;

                // override ctxProject with localPath
                if (confLocalPath != null && !confLocalPath.isEmpty()) {
                    localPath = confLocalPath;
                }

                // get destination path from config
                String confRootPath = castedConf.rootPath;

                if (confRootPath != null && !confRootPath.isEmpty()) {
                    rootPath = confRootPath;
                }

                // get the bucket
                bucket = castedConf.bucket;

                // get the filename
                var configFilename = castedConf.filename;
                if (configFilename != null && !configFilename.isEmpty()) {
                    filename = configFilename;
                }

                // get versionID
                versionID = castedConf.versionID;

                // get versioning
                versioning = castedConf.versioning;

            } else {

                // cast the config as EC2InsiderCompactConfiguration
                var castedConf = (EC2InsiderCompactConfiguration) conf;

                // get destination path from config
                String settingsRootPath = projectSettings.rootPath;

                if (settingsRootPath != null && !settingsRootPath.isEmpty()) {
                    rootPath = settingsRootPath;
                }

                // get the bucket
                bucket = projectSettings.bucket;

                // get the filename
                var configFilename = castedConf.filename;
                if (configFilename != null && !configFilename.isEmpty()) {
                    filename = configFilename;
                }

                // get versionID
                versionID = castedConf.versionID;

                // get versioning
                versioning = projectSettings.versioning;
            }

            // add versioning as missing parameter if not present
            if (Objects.isNull(versioning)) missingParameters.add("versioning");

            // add bucket as missing parameter if not present
            if (Objects.isNull(bucket)) missingParameters.add("bucket");

            if (!missingParameters.isEmpty()) {
                throw new STPropertyAccessException(
                        String.format("Parameter(s) %s not found", String.join(", ", missingParameters))
                );
            }

            // init file path
            var path = getPath(rootPath, localPath, filename);

            // init the S3 input stream
            S3ObjectInputStream is;

            // get the S3 Object
            S3Object object = versioning
                    ? getMostRecentObjectByVersionID(bucket, path, versionID)
                    : amazonS3.getObject(bucket, path);

            // get the input stream from the retrieved S3 object to read its content
            is = object.getObjectContent();

            // create a temporary file on the local file system, with a prefix "loadRDF" and a random suffix
            File backingFile = File.createTempFile("loadRDF", null);

            try (FileOutputStream os = new FileOutputStream(backingFile)) {
                // Copy the content of the S3 input stream into the local temporary file
                IOUtils.copy(is, os);
            }

            // extract the original filename from the provided S3 filename
            @Nullable
            String originalFilename = FilenameUtils.getName(filename);

            // set the target resource with a custom formatted resource object passing the temporary file and the original filename
            target.setTargetFormattedResource(new ClosableFormattedResource(backingFile, null, null, null, originalFilename));

            // close the input stream from the S3 object to release resources
            is.close();

        } catch (STPropertyAccessException | ProjectAccessException | ProjectInexistentException |
                 InvalidProjectNameException | IllegalStateException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * Retrieves the most recent S3 object from a bucket and path based on a specific version ID.
     * <p>
     * This method first fetches all version summaries for the specified bucket and path.
     * It then filters the objects to include only those whose metadata matches the given version ID.
     * Finally, it identifies the most recent object from the filtered list.
     * </p>
     *
     * @param bucket    the name of the S3 bucket
     * @param path      the path (key prefix) within the bucket to search for objects
     * @param versionID the version ID to match against the object metadata
     * @return the most recent {@code S3Object} matching the version ID
     * @throws IllegalStateException if no object is found matching the version ID
     */
    private S3Object getMostRecentObjectByVersionID(String bucket, String path, String versionID) {

        // fetch version summaries and filter based on the given version ID
        List<S3Object> matchingObjects = amazonS3.listVersions(bucket, path)
                .getVersionSummaries()
                .stream()
                .map(version -> amazonS3.getObject(new GetObjectRequest(bucket, version.getKey(), version.getVersionId())))
                .filter(object -> matchesVersionID(object, versionID))
                .toList();

        // find and return the most recent object
        return getMostRecent(matchingObjects)
                .orElseThrow(() -> new IllegalStateException("S3 object not found"));
    }

    /**
     * Checks if the version ID of an S3Object matches the specified version ID.
     *
     * @param object    the {@code S3Object} to check
     * @param versionID the version ID to match
     * @return {@code true} if the object's version ID matches, {@code false} otherwise
     */
    private boolean matchesVersionID(S3Object object, String versionID) {
        return versionID != null &&
               versionID.equalsIgnoreCase(object.getObjectMetadata().getUserMetadata().get("versionid"));
    }

    /**
     * Finds the most recent S3 object from a list of S3 objects based on their last modified date.
     * <p>
     * The method uses Java Stream API to iterate over the list and identifies the object with the
     * latest `LastModified` timestamp. The result is wrapped in an {@code Optional}, allowing for
     * safe handling of empty lists.
     * </p>
     *
     * @param s3Objects the list of {@code S3Object} to search through
     * @return an {@code Optional<S3Object>} containing the most recent object, or empty if the list is empty
     */
    private Optional<S3Object> getMostRecent(List<S3Object> s3Objects) {
        return s3Objects.stream()
                .max(Comparator.comparing(s3Object -> s3Object.getObjectMetadata().getLastModified()));
    }

    /**
     * Constructs a full path by concatenating the root path, local path, and filename.
     * <p>
     * This method handles null or empty values for the provided paths and ensures proper
     * concatenation with separators ('/').
     * </p>
     *
     * @param rootPath  the root directory path, may be null or empty
     * @param localPath the local directory path, must not be null or empty
     * @param filename  the name of the file to include in the path, must not be null or empty
     * @return the constructed full path as a {@code String}, ensuring the correct use of separators
     */
    private String getPath(String rootPath, String localPath, String filename) {
        var path = "";

        // add rootPath if not empty
        if (Objects.nonNull(rootPath) && !rootPath.isEmpty()) {
            path = rootPath.replaceAll("^/+", "").replaceAll("/+$", "");
        }

        // add localPath
        if (!path.isEmpty()) {
            path = path.concat("/");
        }
        path = path.concat(localPath);

        path = path.isEmpty() ? path.concat(filename) : path.concat("/").concat(filename);
        return path;
    }

    /**
     * Creates and returns an Amazon S3 client based on the provided S3 deployment configuration.
     * <p>
     * The method determines the specific S3 configuration type and initializes the client
     * with the appropriate credentials and region settings. For unrecognized configuration types,
     * an {@code IllegalArgumentException} is thrown.
     * </p>
     *
     * @param conf the S3 deployment configuration, must be an instance of a recognized configuration class
     *             such as {@code EC2OutsiderConfiguration}, {@code EC2InsiderConfiguration}, or
     *             {@code EC2InsiderCompactConfiguration}.
     * @return an {@code AmazonS3} client configured according to the specified deployment configuration
     * @throws IllegalArgumentException if the configuration type is not recognized
     */
    private AmazonS3 getAmazonS3(AbstractS3LoaderConfiguration conf) {
        Class<?> clazz = AopUtils.getTargetClass(conf);

        if (Objects.equals(clazz, EC2OutsiderConfiguration.class)) {
            EC2OutsiderConfiguration currentConf = (EC2OutsiderConfiguration) conf;
            return AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(currentConf.accessKey, currentConf.secretKey))).withRegion(currentConf.region).build();
        } else if (Objects.equals(clazz, EC2InsiderConfiguration.class) || Objects.equals(clazz, EC2InsiderCompactConfiguration.class)) {
            return AmazonS3ClientBuilder.defaultClient();
        } else {
            throw new IllegalArgumentException("Unrecognized configuration type");
        }
    }

}
