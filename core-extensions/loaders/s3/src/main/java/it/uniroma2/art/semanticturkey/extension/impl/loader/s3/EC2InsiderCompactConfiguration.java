package it.uniroma2.art.semanticturkey.extension.impl.loader.s3;

import it.uniroma2.art.semanticturkey.config.Configuration;
import it.uniroma2.art.semanticturkey.properties.FallbackSetting;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import it.uniroma2.art.semanticturkey.resources.Scope;
import jakarta.validation.constraints.Pattern;

/**
 * Configuration class for {@link S3LoaderFactory}.
 *
 * @author <a href="mailto:tiziano.dicondina@devit.cloud">Tiziano Di Condina</a>
 */
public class EC2InsiderCompactConfiguration extends AbstractS3LoaderConfiguration implements Configuration {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.extension.impl.loader.s3.EC2InsiderCompactConfiguration";

        public static final String shortName = keyBase + ".shortName";
        public static final String filename$description = keyBase + ".filename.description";
        public static final String filename$displayName = keyBase + ".filename.displayName";
        public static final String versionID$description = keyBase + ".versionID.description";
        public static final String versionID$displayName = keyBase + ".versionID.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.filename$description + "}", displayName = "{" + MessageKeys.filename$displayName + "}")
    @FallbackSetting(
            manager = "it.uniroma2.art.semanticturkey.settings.s3.S3Manager",
            scope = Scope.PROJECT,
            path = "filename"
    )
    public String filename;

    @Required
    @Pattern(regexp = "[0-9]+\\.[0-9]+(\\.[0-9]+)*")
    @STProperty(description = "{" + MessageKeys.versionID$description + "}", displayName = "{" + MessageKeys.versionID$displayName + "}")
    public String versionID;

}
