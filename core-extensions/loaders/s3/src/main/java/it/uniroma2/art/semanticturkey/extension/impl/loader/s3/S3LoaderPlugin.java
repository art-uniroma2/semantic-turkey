package it.uniroma2.art.semanticturkey.extension.impl.loader.s3;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class S3LoaderPlugin extends STPlugin {
    public S3LoaderPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
