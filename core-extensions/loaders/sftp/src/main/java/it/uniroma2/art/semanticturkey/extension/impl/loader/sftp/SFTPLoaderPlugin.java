package it.uniroma2.art.semanticturkey.extension.impl.loader.sftp;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class SFTPLoaderPlugin extends STPlugin {
    public SFTPLoaderPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
