package it.uniroma2.art.semanticturkey.extension.impl.loader.http;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class HTTPLoaderPlugin extends STPlugin {
    public HTTPLoaderPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
