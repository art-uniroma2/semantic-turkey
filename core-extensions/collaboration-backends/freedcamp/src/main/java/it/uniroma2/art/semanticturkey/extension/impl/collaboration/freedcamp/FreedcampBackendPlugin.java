package it.uniroma2.art.semanticturkey.extension.impl.collaboration.freedcamp;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class FreedcampBackendPlugin extends STPlugin {
    public FreedcampBackendPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
