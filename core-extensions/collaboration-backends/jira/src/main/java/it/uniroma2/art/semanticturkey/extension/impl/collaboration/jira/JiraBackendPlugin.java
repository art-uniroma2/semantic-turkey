package it.uniroma2.art.semanticturkey.extension.impl.collaboration.jira;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class JiraBackendPlugin extends STPlugin {
    public JiraBackendPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
