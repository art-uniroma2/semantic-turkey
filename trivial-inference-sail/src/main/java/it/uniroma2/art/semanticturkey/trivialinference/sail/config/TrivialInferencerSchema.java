package it.uniroma2.art.semanticturkey.trivialinference.sail.config;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.util.Values;

public abstract class TrivialInferencerSchema {
    public static final String NAMESPACE = "http://semanticturkey.uniroma2.it/config/sail/trivialinferencer#";

    public static final IRI INFERENCE_ENABLED;

    static  {
        INFERENCE_ENABLED = Values.iri(NAMESPACE, "inferenceEnabled");
    }
}
