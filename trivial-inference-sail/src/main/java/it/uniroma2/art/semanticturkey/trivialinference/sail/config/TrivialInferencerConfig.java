package it.uniroma2.art.semanticturkey.trivialinference.sail.config;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.impl.BooleanLiteral;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.sail.config.AbstractDelegatingSailImplConfig;
import org.eclipse.rdf4j.sail.config.SailConfigException;
import org.eclipse.rdf4j.sail.config.SailImplConfig;

/**
 * A configuration class for the {@link TrivialInferencerTest} sail.
 * 
 * @author <a href="fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 */
public class TrivialInferencerConfig extends AbstractDelegatingSailImplConfig {

	private static final boolean INFERENCE_ENABLED_DEFAULT = true;
	private boolean inferenceEnabled = INFERENCE_ENABLED_DEFAULT;

	public TrivialInferencerConfig() {
		this(null);
	}

	public TrivialInferencerConfig(SailImplConfig delegate) {
		super(TrivialInferencerFactory.SAIL_TYPE, delegate);
	}

	public void setInferenceEnabled(boolean inferenceEnabled) {
		this.inferenceEnabled = inferenceEnabled;
	}

	public boolean isInferenceEnabled() {
		return inferenceEnabled;
	}

	@Override
	public void parse(Model m, Resource implNode) throws SailConfigException {
		super.parse(m, implNode);
		Models.getPropertyLiteral(m, implNode, TrivialInferencerSchema.INFERENCE_ENABLED).ifPresent(l -> setInferenceEnabled(l.booleanValue()));
	}

	@Override
	public Resource export(Model m) {
		Resource implNode = super.export(m);
		Models.setProperty(m, implNode, TrivialInferencerSchema.INFERENCE_ENABLED, BooleanLiteral.valueOf(isInferenceEnabled()));
		return implNode;
	}
}
