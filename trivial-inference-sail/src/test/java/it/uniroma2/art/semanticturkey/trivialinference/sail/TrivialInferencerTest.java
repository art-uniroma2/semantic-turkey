package it.uniroma2.art.semanticturkey.trivialinference.sail;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertThat;

/**
 * Test class for {@link TrivialInferencer}.
 *
 * @author <a href="fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 */
public abstract class TrivialInferencerTest {
    public static final IRI g = SimpleValueFactory.getInstance().createIRI("http://example.org/g");
    public static final IRI g2 = SimpleValueFactory.getInstance().createIRI("http://example.org/g2");
    protected Repository repo;

    @Before
    public abstract void setup();

    @After
    public void teardown() {
        if (repo != null) {
            repo.shutDown();
        }
    }

    protected void add(String turtleSerializedRDF, Resource... contexts) throws IOException {
        turtleSerializedRDF = "@prefix ex: <http://example.org/> . \n@prefix rdf: <" + RDF.NAMESPACE + "> .\n@prefix rdfs: <" + RDFS.NAMESPACE + "> .\n@prefix owl: <" + OWL.NAMESPACE + "> .\n@prefix foaf: <" + FOAF.NAMESPACE + "> .\n" +
                turtleSerializedRDF;
        Model statementsToAdd = Rio.parse(new StringReader(turtleSerializedRDF), RDFFormat.TURTLE);
        try (RepositoryConnection conn = repo.getConnection()) {
            conn.begin();
            conn.add(statementsToAdd, contexts);
            conn.commit();
        }
    }

    protected void remove(String turtleSerializedRDF, Resource... contexts) throws IOException {
        turtleSerializedRDF = "@prefix ex: <http://example.org/> . \n@prefix rdf: <" + RDF.NAMESPACE + "> .\n@prefix rdfs: <" + RDFS.NAMESPACE + "> .\n@prefix owl: <" + OWL.NAMESPACE + "> .\n@prefix foaf: <" + FOAF.NAMESPACE + "> .\n" +
                turtleSerializedRDF;
        Model statementsToRemove = Rio.parse(new StringReader(turtleSerializedRDF), RDFFormat.TURTLE);
        try (RepositoryConnection conn = repo.getConnection()) {
            conn.begin();
            conn.remove(statementsToRemove, contexts);
            conn.commit();
        }
    }

    protected void assertDataset(String trigSerializedRDF) throws IOException {
        trigSerializedRDF = "@prefix ex: <http://example.org/> . \n@prefix rdf: <" + RDF.NAMESPACE + "> .\n@prefix rdfs: <" + RDFS.NAMESPACE + "> .\n@prefix owl: <" + OWL.NAMESPACE + "> .\n@prefix foaf: <" + FOAF.NAMESPACE + "> .\n" +
                trigSerializedRDF;
        Model expectedStatements = Rio.parse(new StringReader(trigSerializedRDF), RDFFormat.TRIG);
        Model actualStatements = new LinkedHashModel();
        Repositories.consume(repo, conn -> conn.export(new StatementCollector(actualStatements)));
        assertThat(actualStatements, CoreMatchers.equalTo(expectedStatements));
    }


    @Test
    public void testAdditionOfSymmetricPropertyAfterDefinition() throws IOException {
        add("ex:friend a owl:SymmetricProperty .", g);
        add("ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "ex:john ex:friend ex:philipp . ", g);
        assertDataset("graph <" + g + "> {\n" +
                "  # Asserted T-BOX\n" +
                "  ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "  #Asserted A-BOX\n" +
                "  " +
                "  ex:john a foaf:Person .\n" +
                "  ex:philipp a foaf:Person .\n" +
                "  ex:john ex:friend ex:philipp . \n" +
                "\n" +
                "  # Inferred Statement\n" +
                "  ex:philipp ex:friend ex:john .\n" +
                "}");
    }

    @Test
    public void testAdditionOfSymmetricPropertyTogetherWithDefinition() throws IOException {
        add("ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "ex:john ex:friend ex:philipp . ", g);
        assertDataset("graph <" + g + "> {\n" +
                "  ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "  ex:john a foaf:Person .\n" +
                "  ex:philipp a foaf:Person .\n" +
                "  ex:john ex:friend ex:philipp . \n" +
                "}\n" +
                "\n" +
                "# No inference because the symmetric property is defined in the same transaction it is used\n");
    }

    @Test
    public void testAdditionOfInversePropertyAfterDefinition() throws IOException {
        add("ex:parent owl:inverseOf ex:child .", g);
        add("ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "ex:john ex:parent ex:philipp . ", g);
        assertDataset("graph <" + g + "> {\n" +
                "  # Asserted T-BOX\n" +
                "  ex:parent owl:inverseOf ex:child .\n" +
                "\n" +
                "  #Asserted A-BOX\n" +
                "  " +
                "  ex:john a foaf:Person .\n" +
                "  ex:philipp a foaf:Person .\n" +
                "  ex:john ex:parent ex:philipp . \n" +
                "\n" +
                "  # Inferred Statement\n" +
                "  ex:philipp ex:child ex:john .\n" +
                "}");
    }

    @Test
    public void testAdditionOfInversePropertyAfterDefinition2() throws IOException {
        add("ex:parent owl:inverseOf ex:child .", g);
        add("ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "ex:john ex:parent ex:philipp . ", g);
        assertDataset("graph <" + g + "> {\n" +
                "  # Asserted T-BOX\n" +
                "  ex:parent owl:inverseOf ex:child .\n" +
                "\n" +
                "  #Asserted A-BOX\n" +
                "  " +
                "  ex:john a foaf:Person .\n" +
                "  ex:philipp a foaf:Person .\n" +
                "  ex:philipp ex:child ex:john . \n" +
                "\n" +
                "  # Inferred Statements\n" +
                "  ex:john ex:parent ex:philipp .\n" +
                "}");
    }

    @Test
    public void testAdditionOfInversePropertyOnTBOX() throws IOException {
        add("""
                ex:parent a owl:ObjectProperty ;
                  owl:inverseOf ex:child .
                
                ex:child a owl:ObjectProperty .
                """, g);
        assertDataset("graph <" + g + "> {\n" +
                """
                # Asserted T-BOX
                ex:parent a owl:ObjectProperty ;
                  owl:inverseOf ex:child .
                
                ex:child a owl:ObjectProperty .
                
                # Inferred T-BOX (note that requires ex:child to be defined!
                ex:child owl:inverseOf ex:parent .
                """ +
                "}");
    }

    @Test
    public void testAdditionOfInversePropertyTogetherWithDefinition() throws IOException {
        add("ex:parent owl:inverseOf ex:child . \n" +
                "\n" +
                "ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "ex:john ex:parent ex:philipp . ", g);
        assertDataset("graph <" + g + "> {\n" +
                "  # Asserted T-BOX\n" +
                "  ex:parent owl:inverseOf ex:child .\n" +
                "\n" +
                "  #Asserted A-BOX\n" +
                "  " +
                "  ex:john a foaf:Person .\n" +
                "  ex:philipp a foaf:Person .\n" +
                "  ex:john ex:parent ex:philipp . \n" +
                "}\n" +
                "  # No inference because the inverse property is defined in the same transaction it is used\n");
    }

    @Test
    public void testRemovalOfSymmetricPropertyTogetherAfterDefinition() throws IOException {
        add("ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "ex:john ex:friend ex:philipp . \n" +
                "ex:philipp ex:friend ex:john .", g);

        remove("ex:john ex:friend ex:philipp .");


        assertDataset("graph <" + g + "> {\n" +
                "  ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "ex:john a foaf:Person .\n" +
                "  ex:philipp a foaf:Person .\n" +
                "}\n" +
                "\n" +
                "# removed the triples in both directions");
    }

    @Test
    public void testRemovalOfInversePropertyTogetherAfterDefinition() throws IOException {
        add("ex:parent owl:inverseOf ex:child .\n" +
                "\n" +
                "ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "ex:john ex:parent ex:philipp . \n" +
                "ex:philipp ex:child ex:john .", g);

        remove("ex:john ex:parent ex:philipp .");

        assertDataset("graph <" + g + "> {\n" +
                "  ex:parent owl:inverseOf ex:child .\n" +
                "\n" +
                "ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "}\n" +
                "\n" +
                "# removed the triples in both directions");
    }

    @Test
    public void testRemovalOfInversePropertyOnTBOX() throws IOException {
        add("""
                ex:parent a owl:ObjectProperty ;
                  owl:inverseOf ex:child .
                
                ex:child a owl:ObjectProperty ;
                  owl:inverseOf ex:parent .
                """, g);
        remove("ex:child owl:inverseOf ex:parent .");
        assertDataset("graph <" + g + "> {\n" +
                """
                # Asserted T-BOX
                ex:parent a owl:ObjectProperty .
                
                ex:child a owl:ObjectProperty .
                
                # Removed the inverseOf axiom in both directions .
                """ +
                "}");
    }

    @Test
    public void testAdditionOfSymmetricPropertyForAlreadyExistingResource() throws IOException {
        add("ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n", g);

        add("ex:john ex:friend ex:philipp .\n", g);

        assertDataset("graph <" + g + "> {\n" +
                "  # Asserted T-BOX" +
                "  # Asserted TBOX\n" +
                "  ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "  # Asserted A-BOX\n" +
                "  ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "  \n" +
                " \n" +
                "  ex:john ex:friend ex:philipp " +
                ".\n" +
                "\n" +
                "  # Inferred Statement\n" +
                "  ex:philipp ex:friend ex:john. " +
                " " +
                " \n" +
                "}\n");
    }

    @Test
    public void testAdditionOfSymmetricPropertyForResourcesDefinedInSameTransaction() throws IOException {
        add("ex:friend a owl:SymmetricProperty .\n", g);
        add("ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "ex:john ex:friend ex:philipp . \n", g);

        assertDataset("graph <" + g + "> {\n" +
                "  # Asserted TBOX\n" +
                "  ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "  # Asserted A-BOX\n" +
                "  ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "  \n" +
                " \n" +
                "  ex:john ex:friend ex:philipp " +
                ".\n" +
                "\n" +
                "  # Inferred Statement\n" +
                "  ex:philipp ex:friend ex:john. " +
                " " +
                " \n" +
                "}\n");
    }

    @Test
    public void testAdditionOfSymmetricPropertyForResourcesDefinedlaterInSameTransaction() throws IOException {
        add("ex:friend a owl:SymmetricProperty .\n", g);
        add("ex:john ex:friend ex:philipp . \n" +
                "ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .", g);

        assertDataset("graph <" + g + "> {\n" +
                "  # Asserted TBOX\n" +
                "  ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "  # Asserted A-BOX\n" +
                "  ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "  \n" +
                " \n" +
                "  ex:john ex:friend ex:philipp " +
                ".\n" +
                "\n" +
                "  # Inferred Statement\n" +
                "  ex:philipp ex:friend ex:john. " +
                " " +
                " \n" +
                "}\n");
    }

    @Test
    public void testSymmetricPropertyForResourcesLaterDefinedInOtherTransaction() throws IOException {
        add("ex:friend a owl:SymmetricProperty .\n", g);

        add("ex:john ex:friend ex:philipp .\n", g);

        assertDataset("graph <" + g + "> {\n" +
                "  # Asserted TBOX\n" +
                "  ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "  # Asserted A-BOX\n" +
                " " +
                " ex:john ex:friend ex:philipp " +
                ".\n" +
                "\n" +
                "  # The symmetric statement has not been inferred because ex:philipp is not defined \n" +
                "}\n");

        add("ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person .\n" +
                "\n", g);

        assertDataset("graph <" + g + "> {\n" +
                "  # Asserted TBOX\n" +
                "  ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "  # Asserted A-BOX\n" +
                " " +
                " ex:john ex:friend ex:philipp " +
                ".\n" +
                "  ex:john a foaf:Person .\n" +
                "  ex:philipp a foaf:Person .\n" +
                "  # The symmetric statement has not been inferreed because ex:philipp has been defined in a subsequent transaction after\n" +
                "  # the assertion of the symmetric property \n" +
                "}\n");

    }

    @Test
    public void testNonGroundDeleteWithSymmetricProperty() throws IOException {
        ValueFactory vf = SimpleValueFactory.getInstance();
        IRI philipp = vf.createIRI("http://example.org/philipp");

        add("ex:friend a owl:SymmetricProperty .\n" +
                "ex:john a foaf:Person ;\n" +
                "  ex:friend ex:philipp .\n" +
                "  \n" +
                "ex:philipp a foaf:Person ;\n" +
                "  ex:friend ex:john .", g);

        try (RepositoryConnection conn = repo.getConnection()) {
            conn.begin();
            conn.remove(philipp, null, null, g2);
            conn.commit();
        }

        assertDataset("graph <" + g + "> {\n" +
                "  # Asserted TBOX\n" +
                "  ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "  # Asserted A-BOX\n" +
                "  ex:john a foaf:Person ;\n" +
                "    ex:friend ex:philipp .\n" +
                "    \n" +
                "  ex:philipp a foaf:Person ;\n" +
                "    ex:friend ex:john .\n" +
                "\n" +
                "  # The statements using the ex:friend property have not been removed because the delete mentioned a different graph  \n" +
                "}\n");

        try (RepositoryConnection conn = repo.getConnection()) {
            conn.begin();
            conn.remove(philipp, null, null, g);
            conn.commit();
        }

        assertDataset("graph <" + g + "> {\n" +
                "  # Asserted TBOX\n" +
                "  ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "  # Asserted A-BOX\n" +
                "  ex:john a foaf:Person ." +
                "\n" +
                "  # The statements using the ex:friend property have been removed in both directions  \n" +
                "}\n");

    }

    @Test
    public void testNonGroundDeleteWithEmptyCtxWithSymmetricProperty() throws IOException {
        ValueFactory vf = SimpleValueFactory.getInstance();
        IRI philipp = vf.createIRI("http://example.org/philipp");

        add("ex:friend a owl:SymmetricProperty .\n" +
                "ex:john a foaf:Person ;\n" +
                "  ex:friend ex:philipp .\n" +
                "  \n" +
                "ex:philipp a foaf:Person ;\n" +
                "  ex:friend ex:john .", g);

        try (RepositoryConnection conn = repo.getConnection()) {
            conn.begin();
            conn.remove(philipp, null, null);
            conn.commit();
        }

        assertDataset("graph <" + g + "> {\n" +
                "  # Asserted TBOX\n" +
                "  ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "  # Asserted A-BOX\n" +
                "  ex:john a foaf:Person ." +
                "\n" +
                "  # The statements using the ex:friend property have been removed in both directions  \n" +
                "}\n");
    }

    @Test
    public void testAddThenRemoveInSameTransaction() throws IOException {
        ValueFactory vf = SimpleValueFactory.getInstance();
        IRI friend = vf.createIRI("http://example.org/friend");
        IRI john = vf.createIRI("http://example.org/john");
        IRI philipp = vf.createIRI("http://example.org/philipp");

        add("ex:friend a owl:SymmetricProperty .\n" +
                "ex:john a foaf:Person .\n" +
                "ex:philipp a foaf:Person ;\n" +
                "  ex:friend ex:john .");

        try (RepositoryConnection conn = repo.getConnection()) {
            conn.begin();
            conn.add(john, friend, philipp);
            conn.remove(john, friend, philipp);
            conn.commit();
        }

        assertDataset(
                "  # Asserted TBOX\n" +
                "  ex:friend a owl:SymmetricProperty .\n" +
                "\n" +
                "  # Asserted A-BOX\n" +
                "  ex:john a foaf:Person .\n" +
                "  ex:philipp a foaf:Person .");

    }

}
