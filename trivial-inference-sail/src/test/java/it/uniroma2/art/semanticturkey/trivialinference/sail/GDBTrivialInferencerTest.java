package it.uniroma2.art.semanticturkey.trivialinference.sail;

import com.ontotext.trree.graphdb.GraphDBSailConfig;
import it.uniroma2.art.semanticturkey.trivialinference.sail.config.TrivialInferencerConfig;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryFactory;

import java.io.File;
import java.io.IOException;

public class GDBTrivialInferencerTest extends TrivialInferencerTest {

	@Override
	public void setup() {
		try {
			FileUtils.deleteDirectory(new File("storage"));
		} catch (IOException e) {
			ExceptionUtils.rethrow(e);
		}
		GraphDBSailConfig owlimSailConfig = new GraphDBSailConfig();
		repo = new SailRepositoryFactory()
				.getRepository(new SailRepositoryConfig(new TrivialInferencerConfig(owlimSailConfig)));

		repo.init();
	}

}
