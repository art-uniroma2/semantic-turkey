package it.uniroma2.art.semanticturkey.trivialinference.sail;

import com.ontotext.trree.graphdb.GraphDBSailConfig;
import it.uniroma2.art.semanticturkey.trivialinference.sail.config.TrivialInferencerConfig;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryFactory;
import org.eclipse.rdf4j.sail.memory.config.MemoryStoreConfig;
import org.eclipse.rdf4j.sail.shacl.config.ShaclSailConfig;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.eclipse.rdf4j.model.util.Values.iri;

public class SHACLCompatibilityTest {
    
    /**
     * This test has been introduced to check against issues emerging from the use of the SHACL and other sails that
     * invoke the prepare() method. As an early implementation of the Trivial Inferencer materialized the triples at the
     * commit(), and thus after the invocation of the prepare() method (by the implementation of the commit() by outer
     * sails), we received the following exception.
     *
     * <code>
     * java.lang.IllegalStateException: Detected changes after prepare() has been called.
     *
     * 	at org.eclipse.rdf4j.sail.shacl.ShaclSailConnection.statementAdded(ShaclSailConnection.java:978)
     * 	at org.eclipse.rdf4j.sail.helpers.AbstractNotifyingSailConnection.notifyStatementAdded(AbstractNotifyingSailConnection.java:53)
     * 	at org.eclipse.rdf4j.sail.base.SailSourceConnection.add(SailSourceConnection.java:765)
     * 	at org.eclipse.rdf4j.sail.base.SailSourceConnection.addStatement(SailSourceConnection.java:597)
     * 	at org.eclipse.rdf4j.sail.helpers.AbstractSailConnection.addStatement(AbstractSailConnection.java:565)
     * 	at org.eclipse.rdf4j.sail.helpers.SailConnectionWrapper.addStatement(SailConnectionWrapper.java:158)
     * 	at it.uniroma2.art.semanticturkey.trivialinference.sail.TrivialInferencerConnection.access$001(TrivialInferencerConnection.java:37)
     * 	at it.uniroma2.art.semanticturkey.trivialinference.sail.TrivialInferencerConnection$GroundQuadruplePatternAddition.applyInverse(TrivialInferencerConnection.java:91)
     * 	at it.uniroma2.art.semanticturkey.trivialinference.sail.TrivialInferencerConnection.doTrivialInference(TrivialInferencerConnection.java:265)
     * 	at it.uniroma2.art.semanticturkey.trivialinference.sail.TrivialInferencerConnection.lambda$commit$0(TrivialInferencerConnection.java:223)
     * 	at java.base/java.lang.Iterable.forEach(Iterable.java:75)
     * 	at it.uniroma2.art.semanticturkey.trivialinference.sail.TrivialInferencerConnection.commit(TrivialInferencerConnection.java:223)
     * 	at org.eclipse.rdf4j.sail.helpers.SailConnectionWrapper.commit(SailConnectionWrapper.java:148)
     * 	at org.eclipse.rdf4j.sail.shacl.ShaclSailConnection.commit(ShaclSailConnection.java:284)
     * 	at org.eclipse.rdf4j.repository.sail.SailRepositoryConnection.commit(SailRepositoryConnection.java:226)
     * 	at it.uniroma2.art.semanticturkey.trivialinference.sail.RDF4JTrivialInferencerTest.testSHACLCompatibility(RDF4JTrivialInferencerTest.java:50)
     * 	at java.base/jdk.internal.reflect.DirectMethodHandleAccessor.invoke(DirectMethodHandleAccessor.java:103)
     * 	at java.base/java.lang.reflect.Method.invoke(Method.java:580)
     * 	at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:59)
     * 	at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)
     * 	at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:56)
     * 	at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)
     * 	at org.junit.internal.runners.statements.RunBefores.evaluate(RunBefores.java:26)
     * 	at org.junit.internal.runners.statements.RunAfters.evaluate(RunAfters.java:27)
     * 	at org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)
     * 	at org.junit.runners.BlockJUnit4ClassRunner$1.evaluate(BlockJUnit4ClassRunner.java:100)
     * 	at org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:366)
     * 	at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:103)
     * 	at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:63)
     * 	at org.junit.runners.ParentRunner$4.run(ParentRunner.java:331)
     * 	at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:79)
     * 	at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:329)
     * 	at org.junit.runners.ParentRunner.access$100(ParentRunner.java:66)
     * 	at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:293)
     * 	at org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)
     * 	at org.junit.runners.ParentRunner.run(ParentRunner.java:413)
     * 	at org.junit.runner.JUnitCore.run(JUnitCore.java:137)
     * 	at com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:69)
     * 	at com.intellij.rt.junit.IdeaTestRunner$Repeater$1.execute(IdeaTestRunner.java:38)
     * 	at com.intellij.rt.execution.junit.TestsRepeater.repeat(TestsRepeater.java:11)
     * 	at com.intellij.rt.junit.IdeaTestRunner$Repeater.startRunnerWithArgs(IdeaTestRunner.java:35)
     * 	at com.intellij.rt.junit.JUnitStarter.prepareStreamsAndStart(JUnitStarter.java:232)
     * 	at com.intellij.rt.junit.JUnitStarter.main(JUnitStarter.java:55)
     * </code>
     */
    @Test
    public void testRDF4J() {
        ShaclSailConfig sailImplConfig = new ShaclSailConfig(new TrivialInferencerConfig(new MemoryStoreConfig()));
        sailImplConfig.setValidationEnabled(true);
        var tempRepo = new SailRepositoryFactory().getRepository(
                new SailRepositoryConfig(sailImplConfig));
        try {
            tempRepo.init();
            try (var con = tempRepo.getConnection()) {
                con.begin();
                con.add(FOAF.KNOWS, RDF.TYPE, OWL.SYMMETRICPROPERTY, iri("http://example.org/"));
                con.commit();

                con.begin();
                con.add(iri("http://example.org/mario"), RDF.TYPE, FOAF.PERSON, iri("http://example.org/"));
                con.add(iri("http://example.org/luigi"), RDF.TYPE, FOAF.PERSON, iri("http://example.org/"));
                con.commit();

                con.begin();
                con.add(iri("http://example.org/mario"), FOAF.KNOWS, iri("http://example.org/luigi"), iri("http://example.org/"));
                con.commit();
            }
        } finally {
            tempRepo.shutDown();
        }

    }


    @Test
    public void testGDB() {
        try {
            FileUtils.deleteDirectory(new File("storage"));
        } catch (IOException e) {
            ExceptionUtils.rethrow(e);
        }

        ShaclSailConfig sailImplConfig = new ShaclSailConfig(new TrivialInferencerConfig(new GraphDBSailConfig()));
        sailImplConfig.setValidationEnabled(true);
        var tempRepo = new SailRepositoryFactory().getRepository(
                new SailRepositoryConfig(sailImplConfig));
        try {
            tempRepo.init();
            try (var con = tempRepo.getConnection()) {
                con.begin();
                con.add(FOAF.KNOWS, RDF.TYPE, OWL.SYMMETRICPROPERTY, iri("http://example.org/"));
                con.commit();

                con.begin();
                con.add(iri("http://example.org/mario"), RDF.TYPE, FOAF.PERSON, iri("http://example.org/"));
                con.add(iri("http://example.org/luigi"), RDF.TYPE, FOAF.PERSON, iri("http://example.org/"));
                con.commit();

                con.begin();
                con.add(iri("http://example.org/mario"), FOAF.KNOWS, iri("http://example.org/luigi"), iri("http://example.org/"));
                con.commit();
            }
        } finally {
            tempRepo.shutDown();
        }

    }
}
