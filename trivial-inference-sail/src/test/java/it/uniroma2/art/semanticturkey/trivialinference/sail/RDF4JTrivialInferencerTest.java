package it.uniroma2.art.semanticturkey.trivialinference.sail;

import com.ontotext.trree.graphdb.GraphDBSailConfig;
import org.eclipse.rdf4j.common.transaction.IsolationLevels;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryFactory;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.config.MemoryStoreConfig;

import it.uniroma2.art.semanticturkey.trivialinference.sail.config.TrivialInferencerConfig;
import org.eclipse.rdf4j.sail.shacl.config.ShaclSailConfig;
import org.junit.Test;

import static org.eclipse.rdf4j.model.util.Values.iri;

public class RDF4JTrivialInferencerTest extends TrivialInferencerTest {

	@Override
	public void setup() {
		repo = new SailRepositoryFactory().getRepository(
				new SailRepositoryConfig(new TrivialInferencerConfig(new MemoryStoreConfig())));
		repo.init();
	}

}
