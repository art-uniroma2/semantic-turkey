package it.uniroma2.art.semanticturkey.services.core.ontolexlemon;

import it.uniroma2.art.semanticturkey.extension.extpts.rendering.RenderingEngineExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.impl.rendering.AbstractLabelBasedRenderingEngineConfiguration;
import it.uniroma2.art.semanticturkey.extension.impl.rendering.BaseRenderingEngine;

public class DecompComponentRenderer extends BaseRenderingEngine {

    private static AbstractLabelBasedRenderingEngineConfiguration conf;

    static {
        conf = new AbstractLabelBasedRenderingEngineConfiguration() {

            @Override
            public String getShortName() {
                return "foo";
            }

        };
        conf.languages = null;
    }

    public DecompComponentRenderer(RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
        super(conf, true, renderingEngineExtensionPoint);
    }

    public DecompComponentRenderer(boolean fallbackToTerm, RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
        super(conf, fallbackToTerm, renderingEngineExtensionPoint);
    }

    @Override
    public void getGraphPatternInternal(StringBuilder gp) {
        gp.append(
                // @formatter:off
                "?resource <http://www.w3.org/ns/lemon/decomp#correspondsTo> [ \n" +
                        "  <http://www.w3.org/ns/lemon/ontolex#canonicalForm> [ \n" +
                        "    <http://www.w3.org/ns/lemon/ontolex#writtenRep> ?labelInternal \n" +
                        "  ] \n" +
                        "] \n"
                // @formatter:on
        );
    }

}
