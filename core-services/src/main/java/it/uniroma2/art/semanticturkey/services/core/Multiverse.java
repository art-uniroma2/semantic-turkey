package it.uniroma2.art.semanticturkey.services.core;


import it.uniroma2.art.semanticturkey.exceptions.WorldAlreadyExistingException;
import it.uniroma2.art.semanticturkey.multiverse.MultiverseManager;
import it.uniroma2.art.semanticturkey.multiverse.World;
import it.uniroma2.art.semanticturkey.multiverse.WorldInfo;
import it.uniroma2.art.semanticturkey.services.STServiceAdapter;
import it.uniroma2.art.semanticturkey.services.annotations.RequestMethod;
import it.uniroma2.art.semanticturkey.services.annotations.STService;
import it.uniroma2.art.semanticturkey.services.annotations.STServiceOperation;
import jakarta.validation.constraints.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import java.io.IOException;
import java.util.Collection;

/**
 * Services related to the management of multiple worlds.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 * @see MultiverseManager
 */
@STService
public class Multiverse extends STServiceAdapter {

	protected static Logger logger = LoggerFactory.getLogger(Multiverse.class);

	@Autowired
	private MultiverseManager multiverseManager;

	/**
	 * Creates a new world.
	 *
	 * @param name name of the world to create
	 */
	@STServiceOperation(method = RequestMethod.POST)
	@PreAuthorize("@auth.isAdmin()")
	public void createWorld(@Pattern(regexp = World.NAME_REGEX) String name) throws IOException, WorldAlreadyExistingException {
		multiverseManager.createWorld(name);
	}

	/**
	 * Returns the list of worlds.
	 */
	@STServiceOperation
	@PreAuthorize("@auth.isAdmin()")
	public Collection<String> listWorlds() {
		return multiverseManager.listWorldNames();
	}

	/**
	 * Returns information about alternative worlds.
	 */
	@STServiceOperation
	@PreAuthorize("@auth.isAdmin()")
	public Collection<WorldInfo> listAlternativeWorldInfos() {
		return multiverseManager.listAlternativeWorldInfos();
	}

	/**
	 * Destroys a world.
	 *
	 * @param name name of the world to destroy.
	 *
	 */
	@STServiceOperation(method = RequestMethod.POST)
	@PreAuthorize("@auth.isAdmin()")
	public void destroyWorld(String name) {
		multiverseManager.destroyWorld(name);
	}


}
