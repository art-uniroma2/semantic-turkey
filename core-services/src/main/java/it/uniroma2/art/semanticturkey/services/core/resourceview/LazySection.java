package it.uniroma2.art.semanticturkey.services.core.resourceview;

/**
 * A {@link ResourceViewSection} that is populates lazily with the result of a service invocation.
 */
public class LazySection implements ResourceViewSection {

    public LazySection() {
    }

    public boolean isLazy() {
        return true;
    }
}
