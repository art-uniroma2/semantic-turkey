package it.uniroma2.art.semanticturkey.services.core;

import java.io.IOException;
import java.util.Collection;

import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.properties.STPropertiesManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.uniroma2.art.semanticturkey.config.Configuration;
import it.uniroma2.art.semanticturkey.config.ConfigurationManager;
import it.uniroma2.art.semanticturkey.config.ConfigurationNotFoundException;
import it.uniroma2.art.semanticturkey.extension.ExtensionPointManager;
import it.uniroma2.art.semanticturkey.extension.NoSuchConfigurationManager;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.properties.STPropertyUpdateException;
import it.uniroma2.art.semanticturkey.properties.WrongPropertiesException;
import it.uniroma2.art.semanticturkey.resources.Reference;
import it.uniroma2.art.semanticturkey.services.STServiceAdapter;
import it.uniroma2.art.semanticturkey.services.annotations.RequestMethod;
import it.uniroma2.art.semanticturkey.services.annotations.STService;
import it.uniroma2.art.semanticturkey.services.annotations.STServiceOperation;
import it.uniroma2.art.semanticturkey.user.UsersManager;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 * This class provides services for handling configurations.
 */
@STService
public class Configurations extends STServiceAdapter {

	private static final Logger logger = LoggerFactory.getLogger(Configurations.class);

	@Autowired
	private ExtensionPointManager exptManager;

	/**
	 * Returns the available configuration managers
	 * 
	 * @return
	 */
	@STServiceOperation
	public Collection<ConfigurationManager<?>> getConfigurationManagers() {
		return exptManager.getConfigurationManagers();
	}

	/**
	 * Returns a specific configuration manager
	 * 
	 * @param componentID
	 * @return
	 * @throws NoSuchConfigurationManager
	 */
	@STServiceOperation
	public ConfigurationManager<?> getConfigurationManager(String componentID)
			throws NoSuchConfigurationManager {
		return exptManager.getConfigurationManager(componentID);
	}

	/**
	 * Returns the stored configurations associated with the given component
	 * 
	 * @param componentID
	 * @return
	 * @throws NoSuchConfigurationManager
	 */
	@STServiceOperation
	public Collection<Reference> getConfigurationReferences(String componentID)
			throws NoSuchConfigurationManager {
		Project project = null;
		if (stServiceContext.hasContextParameter("project")) {
			project = getProject();
		}
		return exptManager.getConfigurationReferences(project, UsersManager.getLoggedUser(),
				componentID);
	}

	/**
	 * Returns a stored configuration given its relative reference
	 * 
	 * @param componentID
	 * @param relativeReference
	 * @return
	 * @throws NoSuchConfigurationManager
	 * @throws WrongPropertiesException
	 * @throws ConfigurationNotFoundException
	 * @throws IOException
	 * @throws STPropertyAccessException
	 */
	@STServiceOperation
	@PreAuthorize("@auth.isConfigurationActionAuthorized(#relativeReference, 'R')")
	public Configuration getConfiguration(String componentID, String relativeReference)
			throws NoSuchConfigurationManager, IOException, ConfigurationNotFoundException,
			WrongPropertiesException, STPropertyAccessException {
		return exptManager.getConfiguration(componentID, parseReference(relativeReference));
	}

	/**
	 * Stores a configurations
	 * 
	 * @param componentID
	 * @param relativeReference
	 * @return
	 * @throws NoSuchConfigurationManager
	 * @throws WrongPropertiesException
	 * @throws ConfigurationNotFoundException
	 * @throws IOException
	 * @throws STPropertyUpdateException
	 * @throws STPropertyAccessException
	 */
	@STServiceOperation(method = RequestMethod.POST)
	@PreAuthorize("@auth.isConfigurationActionAuthorized(#relativeReference, 'U')")
	public void storeConfiguration(String componentID, String relativeReference, ObjectNode configuration)
			throws NoSuchConfigurationManager, IOException, WrongPropertiesException,
			STPropertyUpdateException, STPropertyAccessException {
		exptManager.storeConfiguration(componentID, parseReference(relativeReference), configuration);
	}

	/**
	 * Deletes a previously stored configuration
	 * 
	 * @param componentID
	 * @param relativeReference
	 * @throws NoSuchConfigurationManager
	 * @throws ConfigurationNotFoundException
	 */
	@STServiceOperation(method = RequestMethod.POST)
	@PreAuthorize("@auth.isConfigurationActionAuthorized(#relativeReference, 'D')")
	public void deleteConfiguration(String componentID, String relativeReference)
			throws NoSuchConfigurationManager, ConfigurationNotFoundException {
		exptManager.deleteConfiguraton(componentID, parseReference(relativeReference));
	}

	@STServiceOperation(method = RequestMethod.POST)
	@PreAuthorize("@auth.isConfigurationActionAuthorized(#oldRelativeReference, 'U')")
	public void renameConfiguration(String componentID, String oldRelativeReference, String newRelativeReference)
			throws NoSuchConfigurationManager, ConfigurationNotFoundException, WrongPropertiesException, IOException, STPropertyAccessException, STPropertyUpdateException {
		Reference oldRef = parseReference(oldRelativeReference);
		Reference newRef = parseReference(newRelativeReference);

		Configuration config = exptManager.getConfiguration(componentID, oldRef);
		ObjectNode objectNode = STPropertiesManager.storeSTPropertiesToObjectNode(config, true);
		exptManager.storeConfiguration(componentID, newRef, objectNode);
		exptManager.deleteConfiguraton(componentID, oldRef);
	}

}