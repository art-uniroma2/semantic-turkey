package it.uniroma2.art.semanticturkey.services.core;

import it.uniroma2.art.semanticturkey.customservice.CustomServiceHandlerMapping;
import it.uniroma2.art.semanticturkey.extension.extpts.rendering.RenderingEngineExtensionPoint;
import it.uniroma2.art.semanticturkey.mdr.core.MetadataRegistryBackend;
import it.uniroma2.art.semanticturkey.services.core.converters.StringToFilteringPipelineConverter;
import it.uniroma2.art.semanticturkey.services.core.converters.StringToFilteringStepConverter;
import it.uniroma2.art.semanticturkey.services.core.ontolexlemon.DecompComponentRenderer;
import it.uniroma2.art.semanticturkey.services.core.ontolexlemon.FormRenderer;
import it.uniroma2.art.semanticturkey.services.core.ontolexlemon.LexicalEntryRenderer;
import it.uniroma2.art.semanticturkey.services.core.ontolexlemon.LexiconRenderer;
import it.uniroma2.art.semanticturkey.services.core.projects.ProjectStarter;
import it.uniroma2.art.semanticturkey.spring.STBaseServiceConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan
@Import(STBaseServiceConfiguration.class)
public class STCoreServicesConfig implements WebMvcConfigurer {
    @Autowired
    private MetadataRegistryBackend mdr;

    @Bean
    public CustomServiceHandlerMapping customServiceHandlerMapping(STBaseServiceConfiguration.HandlerInterceptors handlerInterceptors) {
        var mapping = new CustomServiceHandlerMapping();
        mapping.setOrder(- 1); // custom handler mappings should be evaluated before RequestMappingHandlerMapping, which has order 0
        mapping.setInterceptors(handlerInterceptors.get());
        return mapping;
    }

    @Bean
    public ProjectStarter projectStarter() {
        return new ProjectStarter();
    }

    @Bean
    public FormRenderer formRenderer(RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
        return new FormRenderer(false, renderingEngineExtensionPoint);
    }

    @Bean
    @Qualifier("withFallbackToTerm")
    public LexicalEntryRenderer lexicalEntryRendererWithFallbackToTerm(RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
        return new LexicalEntryRenderer(true, renderingEngineExtensionPoint);
    }

    @Bean
    @Qualifier("withoutFallbackToTerm")
    public LexicalEntryRenderer lexicalEntryRendererWithoutFallbackToTerm(RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
        return new LexicalEntryRenderer(false, renderingEngineExtensionPoint);
    }

    @Bean
    @Qualifier("withFallbackToTerm")
    public DecompComponentRenderer decompComponentRendererWithFallbackToTerm(RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
        return new DecompComponentRenderer(true, renderingEngineExtensionPoint);
    }

    @Bean
    @Qualifier("withoutFallbackToTerm")
    public DecompComponentRenderer decompComponentRendererWithoutFallbackToTerm(RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
        return new DecompComponentRenderer(false, renderingEngineExtensionPoint);
    }
    @Bean
    @Qualifier("withFallbackToTerm")
    public LexiconRenderer lexiconRendererWithFallbackToTerm(RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
        return new LexiconRenderer(true, renderingEngineExtensionPoint);
    }

    @Bean
    @Qualifier("withoutFallbackToTerm")
    public LexiconRenderer lexiconRendererWithoutFallbackToTerm(RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
        return new LexiconRenderer(false, renderingEngineExtensionPoint);
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToFilteringStepConverter());
        registry.addConverter(new StringToFilteringPipelineConverter());
    }
}
