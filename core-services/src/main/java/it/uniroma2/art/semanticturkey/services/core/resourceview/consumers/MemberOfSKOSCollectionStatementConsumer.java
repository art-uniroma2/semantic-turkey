package it.uniroma2.art.semanticturkey.services.core.resourceview.consumers;

import it.uniroma2.art.semanticturkey.customform.CustomFormManager;
import it.uniroma2.art.semanticturkey.customviews.ProjectCustomViewsManager;
import it.uniroma2.art.semanticturkey.services.core.resourceview.LazySectionStatementConsumer;

public class MemberOfSKOSCollectionStatementConsumer extends LazySectionStatementConsumer {

	public MemberOfSKOSCollectionStatementConsumer(CustomFormManager cfManager, ProjectCustomViewsManager projCvManager) {
		super("collections");
	}

}
