package it.uniroma2.art.semanticturkey.services.core;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.io.Closer;
import it.uniroma2.art.lime.model.repo.LIMERepositoryConnectionWrapper;
import it.uniroma2.art.lime.profiler.LIMEProfiler;
import it.uniroma2.art.lime.profiler.ProfilerException;
import it.uniroma2.art.maple.orchestration.AssessmentException;
import it.uniroma2.art.maple.orchestration.MediationFramework;
import it.uniroma2.art.semanticturkey.changetracking.sail.config.ChangeTrackerFactory;
import it.uniroma2.art.semanticturkey.changetracking.sail.config.ChangeTrackerSchema;
import it.uniroma2.art.semanticturkey.changetracking.vocabulary.CHANGELOG;
import it.uniroma2.art.semanticturkey.config.Configuration;
import it.uniroma2.art.semanticturkey.config.ConfigurationManager;
import it.uniroma2.art.semanticturkey.config.InvalidConfigurationException;
import it.uniroma2.art.semanticturkey.data.role.RDFResourceRole;
import it.uniroma2.art.semanticturkey.email.EmailApplicationContext;
import it.uniroma2.art.semanticturkey.email.EmailService;
import it.uniroma2.art.semanticturkey.email.EmailServiceFactory;
import it.uniroma2.art.semanticturkey.exceptions.DuplicatedResourceException;
import it.uniroma2.art.semanticturkey.exceptions.ExceptionDAO;
import it.uniroma2.art.semanticturkey.exceptions.InvalidProjectNameException;
import it.uniroma2.art.semanticturkey.exceptions.ProjectAccessException;
import it.uniroma2.art.semanticturkey.exceptions.ProjectCreationException;
import it.uniroma2.art.semanticturkey.exceptions.ProjectDeletionException;
import it.uniroma2.art.semanticturkey.exceptions.ProjectInconsistentException;
import it.uniroma2.art.semanticturkey.exceptions.ProjectInexistentException;
import it.uniroma2.art.semanticturkey.exceptions.ProjectUpdateException;
import it.uniroma2.art.semanticturkey.exceptions.ReservedPropertyUpdateException;
import it.uniroma2.art.semanticturkey.exceptions.UnsupportedLexicalizationModelException;
import it.uniroma2.art.semanticturkey.exceptions.UnsupportedModelException;
import it.uniroma2.art.semanticturkey.extension.ExtensionPointManagerHolder;
import it.uniroma2.art.semanticturkey.extension.NoSuchConfigurationManager;
import it.uniroma2.art.semanticturkey.extension.NoSuchSettingsManager;
import it.uniroma2.art.semanticturkey.extension.NonConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetcatalog.DatasetCatalogConnector;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetcatalog.DatasetDescription;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetcatalog.DownloadDescription;
import it.uniroma2.art.semanticturkey.extension.impl.rendering.BaseRenderingEngine;
import it.uniroma2.art.semanticturkey.ontology.TransitiveImportMethodAllowance;
import it.uniroma2.art.semanticturkey.plugin.PluginSpecification;
import it.uniroma2.art.semanticturkey.project.AbstractProject;
import it.uniroma2.art.semanticturkey.project.CorruptedProject;
import it.uniroma2.art.semanticturkey.project.ForbiddenProjectAccessException;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.project.ProjectACL;
import it.uniroma2.art.semanticturkey.project.ProjectACL.AccessLevel;
import it.uniroma2.art.semanticturkey.project.ProjectACL.LockLevel;
import it.uniroma2.art.semanticturkey.project.ProjectConsumer;
import it.uniroma2.art.semanticturkey.project.ProjectInfo;
import it.uniroma2.art.semanticturkey.project.ProjectManager;
import it.uniroma2.art.semanticturkey.project.ProjectManager.AccessResponse;
import it.uniroma2.art.semanticturkey.project.ProjectStatus;
import it.uniroma2.art.semanticturkey.project.ProjectStatus.Status;
import it.uniroma2.art.semanticturkey.project.ProjectVisibility;
import it.uniroma2.art.semanticturkey.project.ProjectsInfoAndFacetsCount;
import it.uniroma2.art.semanticturkey.project.RepositoryAccess;
import it.uniroma2.art.semanticturkey.project.RepositoryLocation;
import it.uniroma2.art.semanticturkey.project.RepositorySummary;
import it.uniroma2.art.semanticturkey.project.SHACLSettings;
import it.uniroma2.art.semanticturkey.project.STLocalRepositoryManager;
import it.uniroma2.art.semanticturkey.project.STRepositoryInfo;
import it.uniroma2.art.semanticturkey.properties.DataSize;
import it.uniroma2.art.semanticturkey.properties.Pair;
import it.uniroma2.art.semanticturkey.properties.PropertyNotFoundException;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STPropertiesManager;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.properties.STPropertyUpdateException;
import it.uniroma2.art.semanticturkey.properties.WrongPropertiesException;
import it.uniroma2.art.semanticturkey.properties.dynamic.STPropertiesSchema;
import it.uniroma2.art.semanticturkey.rbac.RBACException;
import it.uniroma2.art.semanticturkey.rbac.RBACManager;
import it.uniroma2.art.semanticturkey.resources.Scope;
import it.uniroma2.art.semanticturkey.services.STServiceAdapter;
import it.uniroma2.art.semanticturkey.services.annotations.JsonSerialized;
import it.uniroma2.art.semanticturkey.services.annotations.Optional;
import it.uniroma2.art.semanticturkey.services.annotations.RequestMethod;
import it.uniroma2.art.semanticturkey.services.annotations.STService;
import it.uniroma2.art.semanticturkey.services.annotations.STServiceOperation;
import it.uniroma2.art.semanticturkey.services.core.projects.PreloadedDataStore;
import it.uniroma2.art.semanticturkey.services.core.projects.PreloadedDataSummary;
import it.uniroma2.art.semanticturkey.services.core.projects.ProjectPropertyInfo;
import it.uniroma2.art.semanticturkey.services.support.STServiceContextUtils;
import it.uniroma2.art.semanticturkey.settings.core.CoreSystemSettings;
import it.uniroma2.art.semanticturkey.settings.core.PreloadProfilerSettings;
import it.uniroma2.art.semanticturkey.settings.core.PreloadSettings;
import it.uniroma2.art.semanticturkey.settings.core.ProjectsSearchGroup;
import it.uniroma2.art.semanticturkey.settings.core.SemanticTurkeyCoreSettingsManager;
import it.uniroma2.art.semanticturkey.settings.facets.CorruptedProjectFacets;
import it.uniroma2.art.semanticturkey.settings.facets.CustomProjectFacetsSchemaStore;
import it.uniroma2.art.semanticturkey.settings.facets.ProjectFacetsIndexLuceneUtils;
import it.uniroma2.art.semanticturkey.settings.facets.ProjectFacets;
import it.uniroma2.art.semanticturkey.settings.facets.ProjectFacetsStore;
import it.uniroma2.art.semanticturkey.settings.facets.struct.FacetsResultStruct;
import it.uniroma2.art.semanticturkey.settings.facets.struct.ProjectsAndFacetsResult;
import it.uniroma2.art.semanticturkey.settings.facets.struct.ProjectsResult;
import it.uniroma2.art.semanticturkey.trivialinference.sail.config.TrivialInferencerFactory;
import it.uniroma2.art.semanticturkey.trivialinference.sail.config.TrivialInferencerSchema;
import it.uniroma2.art.semanticturkey.user.ProjectBindingException;
import it.uniroma2.art.semanticturkey.user.ProjectUserBindingsManager;
import it.uniroma2.art.semanticturkey.user.STUser;
import it.uniroma2.art.semanticturkey.user.UserException;
import it.uniroma2.art.semanticturkey.user.UsersGroup;
import it.uniroma2.art.semanticturkey.user.UsersManager;
import it.uniroma2.art.semanticturkey.utilities.ReflectionUtilities;
import it.uniroma2.art.semanticturkey.utilities.Utilities;
import it.uniroma2.art.semanticturkey.vocabulary.GRAPHDB_CONFIG;
import it.uniroma2.art.semanticturkey.vocabulary.SUPPORT;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.rdf4j.model.*;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.Literals;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.RDFParserRegistry;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.sail.config.SailConfigSchema;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sail.shacl.config.ShaclSailConfig;
import org.eclipse.rdf4j.sail.shacl.config.ShaclSailFactory;
import org.eclipse.rdf4j.sail.shacl.config.ShaclSailSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import jakarta.annotation.Nullable;
import jakarta.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@STService
public class Projects extends STServiceAdapter {

    private final String NO_VALUE_FOR_FACET = "";

    private static final Logger logger = LoggerFactory.getLogger(Projects.class);

    @Autowired
    private MediationFramework mediationFramework;

    @Autowired
    private PreloadedDataStore preloadedDataStore;

    @Autowired
    private SemanticTurkeyCoreSettingsManager coreSettingsManager;

    @Autowired
    private GlobalSearch globalSearchServices;

    @Autowired
    private MAPLE mapleServices;

    /**
     * Returns the backend type of the context repository
     *
     * @return
     */
    @STServiceOperation
    public String getContextRepositoryBackend() {
        String repId = STServiceContextUtils.getRepositoryId(stServiceContext);
        java.util.Optional<STRepositoryInfo> repInfo = getProject().getRepositoryManager().getSTRepositoryInfo(repId);
        return repInfo.map(STRepositoryInfo::getBackendType).orElse(null);
    }

    // TODO understand how to specify remote repository / different sail configurations
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isSuperUser(false)")
    public void createProject(ProjectConsumer consumer, String projectName, IRI model,
            IRI lexicalizationModel, String baseURI, @Optional String defaultNamespace,
            boolean historyEnabled, boolean validationEnabled,
            @Optional(defaultValue = "false") boolean blacklistingEnabled, RepositoryAccess repositoryAccess,
            String coreRepoID,
            @Optional(defaultValue = "{\"factoryId\" : \"it.uniroma2.art.semanticturkey.extension.impl.repositoryimplconfigurer.predefined.PredefinedRepositoryConfigurer\", \"configuration\" : {\"@type\" : \"it.uniroma2.art.semanticturkey.extension.impl.repositoryimplconfigurer.predefined.RDF4JNativeSailConfiguration\"}}") PluginSpecification coreRepoSailConfigurerSpecification,
            @Optional String coreBackendType, String supportRepoID,
            @Optional(defaultValue = "{\"factoryId\" : \"it.uniroma2.art.semanticturkey.extension.impl.repositoryimplconfigurer.predefined.PredefinedRepositoryConfigurer\", \"configuration\" : {\"@type\" : \"it.uniroma2.art.semanticturkey.extension.impl.repositoryimplconfigurer.predefined.RDF4JNativeSailConfiguration\"}}") PluginSpecification supportRepoSailConfigurerSpecification,
            @Optional String supportBackendType,
            @Optional(defaultValue = "{\"factoryId\" : \"it.uniroma2.art.semanticturkey.extension.impl.urigen.template.NativeTemplateBasedURIGenerator\", \"configuration\" : {\"@type\" : \"it.uniroma2.art.semanticturkey.extension.impl.urigen.template.NativeTemplateBasedURIGeneratorConfiguration\"}}") PluginSpecification uriGeneratorSpecification,
            @Optional PluginSpecification renderingEngineSpecification,
            @Optional @JsonSerialized List<Pair<RDFResourceRole, String>> resourceMetadataAssociations,
            @Optional String preloadedDataFileName, @Optional RDFFormat preloadedDataFormat,
            @Optional TransitiveImportMethodAllowance transitiveImportAllowance, @Optional String leftDataset,
            @Optional String rightDataset, @Optional boolean shaclEnabled,
            @Optional @JsonSerialized SHACLSettings shaclSettings, @Optional boolean trivialInferenceEnabled,
            @Optional(defaultValue = "false") boolean openAtStartup,
            @Optional AccessLevel universalAccess,
            @Optional ProjectVisibility visibility,
            @Optional Literal label, @Optional(defaultValue = "false") boolean undoEnabled,
            @Optional EmailApplicationContext appCtx)
            throws ProjectInconsistentException, InvalidProjectNameException, ProjectInexistentException,
            ProjectAccessException, ForbiddenProjectAccessException, DuplicatedResourceException,
            ProjectCreationException, ClassNotFoundException, WrongPropertiesException, RBACException,
            UnsupportedModelException, UnsupportedLexicalizationModelException, InvalidConfigurationException,
            STPropertyAccessException, IOException, ReservedPropertyUpdateException, ProjectUpdateException,
            STPropertyUpdateException, NoSuchConfigurationManager, ProjectBindingException {

        List<Object> preloadRelatedArgs = Arrays.asList(preloadedDataFileName, preloadedDataFormat,
                transitiveImportAllowance);
        if (!preloadRelatedArgs.stream().allMatch(java.util.Objects::nonNull)
                && !preloadRelatedArgs.stream().noneMatch(java.util.Objects::nonNull)) {
            throw new IllegalArgumentException(
                    "All preload-related arguments must be specified together, or none of them can be specified");
        }

        // If no rendering engine has been configured, guess the best one based on the model type
        if (renderingEngineSpecification == null) {
            renderingEngineSpecification = BaseRenderingEngine
                    .getRenderingEngineSpecificationForLexicalModel(lexicalizationModel)
                    .orElseThrow(() -> new IllegalArgumentException(
                            "Unsupported lexicalization model: " + lexicalizationModel));
        }

        if (shaclSettings != null) {
            if (!shaclEnabled) {
                throw new IllegalArgumentException(
                        "It is not allowed to specify SHACL-related settings if SHACL is not enabled");
            }

            if (!repositoryAccess.isCreation()) {
                throw new IllegalArgumentException(
                        "It is not allowed to sprecify SHACL-related settings if accessing an existing repository");
            }
        }

        Set<IRI> failedImports = new HashSet<>();


        boolean deletePreloadedDataFile = false;
        ReentrantLock lock = null;
        try {
            File preloadedDataFile = null;

            if (preloadedDataFileName != null) {
                lock = preloadedDataStore.getReentrantLock(preloadedDataFileName);
                lock.lock();
                preloadedDataFile = preloadedDataStore.startConsumingPreloadedData(preloadedDataFileName);
            }
            ProjectManager.createProject(consumer, projectName, label, model, lexicalizationModel,
                    baseURI.trim(), (defaultNamespace == null ? null : defaultNamespace.trim()),
                    historyEnabled, validationEnabled, blacklistingEnabled, repositoryAccess, coreRepoID,
                    coreRepoSailConfigurerSpecification, coreBackendType, supportRepoID,
                    supportRepoSailConfigurerSpecification, supportBackendType, uriGeneratorSpecification,
                    renderingEngineSpecification, resourceMetadataAssociations, preloadedDataFile,
                    preloadedDataFormat, transitiveImportAllowance, failedImports, leftDataset, rightDataset,
                    shaclEnabled, shaclSettings, trivialInferenceEnabled, openAtStartup, universalAccess,
                    visibility, undoEnabled);
            deletePreloadedDataFile = true;
        } finally {
            try {
                if (preloadedDataFileName != null) {
                    preloadedDataStore.finishConsumingPreloadedData(preloadedDataFileName,
                            deletePreloadedDataFile, lock);
                }
            } finally {
                if (lock != null) {
                    // release the lock
                    lock.unlock();
                }
            }
        }
        // create the index about the facets of this project
        Project project = ProjectManager.getProject(projectName);
        ProjectInfo projectInfo = getProjectInfoHelper(ProjectConsumer.SYSTEM, ProjectACL.AccessLevel.R,
                ProjectACL.LockLevel.NO, false, false, project);
        if (projectInfo == null) {
            throw new ProjectAccessException(projectName);
        }
        //ProjectFacetsIndexUtils.recreateFacetIndexForProjectAPI(projectName, projectInfo); // OLD
        ProjectFacetsIndexLuceneUtils.createFacetIndexAPI(projectInfo);

        STUser loggedUser = UsersManager.getLoggedUser();
        if (loggedUser.isSuperUser(true)) {
            //set the superuser as PM of the created project
            ProjectUserBindingsManager.addRolesToPUBinding(loggedUser, project, List.of(RBACManager.DefaultRole.PROJECTMANAGER));
            //send notification to administrators
            try {
                EmailService emailService = EmailServiceFactory.getService(appCtx, coreSettingsManager);
                emailService.sendProjCreationMailToAdmin(loggedUser, project);
            } catch (
                    Exception e) { //catch generic Exception in order to avoid annoying exception raised to the client when the configuration is invalid
                logger.error(Utilities.printFullStackTrace(e));
            }
        }
    }

    /**
     * Returns an empty form for SHACL settings upon project creation.
     *
     * @return
     */
    @STServiceOperation
    @PreAuthorize("@auth.isSuperUser(false)")
    public SHACLSettings createEmptySHACLSettingsForm() {
        return new SHACLSettings();
    }

    @STServiceOperation()
    public Boolean projectExists(String projectName) throws InvalidProjectNameException {
        return ProjectManager.projectExists(projectName);
    }

    /**
     * @param consumer
     * @param requestedAccessLevel
     * @param requestedLockLevel
     * @param onlyOpen if true, return only the open projects
     * @param visibilityFilter if provided, return only projects with the given visibility
     * @param roleFilter if provided, return only projects where the logged user has assigned the given role
     * @return
     * @throws ProjectAccessException
     */
    @STServiceOperation
    public List<ProjectInfo> listProjects(@Optional(defaultValue = "SYSTEM") ProjectConsumer consumer,
            @Optional(defaultValue = "R") ProjectACL.AccessLevel requestedAccessLevel,
            @Optional(defaultValue = "NO") ProjectACL.LockLevel requestedLockLevel,
            @Optional(defaultValue = "false") boolean onlyOpen,
            @Optional(defaultValue = "") List<ProjectVisibility> visibilityFilter,
            @Optional String roleFilter
    ) throws ProjectAccessException, IOException, InvalidProjectNameException {

        logger.debug("listProjects, asked by consumer: " + consumer);

        List<ProjectInfo> listProjInfo = new ArrayList<>();

        STUser user = UsersManager.getLoggedUser();

        Collection<AbstractProject> projects = ProjectManager.listProjects(consumer);

        for (AbstractProject absProj : projects) {
            if (absProj instanceof Project proj) {
                ProjectInfo projInfo = getProjectInfoHelper(consumer, requestedAccessLevel, requestedLockLevel, true, onlyOpen, proj);
                if (projInfo != null) {
                    if (!visibilityFilter.isEmpty() && !visibilityFilter.contains(projInfo.getVisibility())) {
                        continue; //skip project if the required visibility filter is not satisfied
                    }
                    if (roleFilter != null && ProjectUserBindingsManager.getPUBindingRoles(user, proj, false)
                            .stream().noneMatch(r -> r.getName().equals(roleFilter))) {
                        continue; //skip project if the required role is not assigned to current user
                    }
                    listProjInfo.add(projInfo);
                }
            }
        }

        // check if the lucene dir (for the facets) exists, if not, create the indexes
        createFacetIndexIfNeeded();

        return listProjInfo;
    }


    protected void createFacetIndexIfNeeded() throws ProjectAccessException, IOException, InvalidProjectNameException {
        // check if the lucene dir (for the facets) exists, if not, create the indexes
        if (!ProjectFacetsIndexLuceneUtils.areLuceneDirsPresent()) {
            // iterate over the existing projects
            Collection<AbstractProject> abstractProjectCollection = ProjectManager
                    .listProjects(ProjectConsumer.SYSTEM);
            List<ProjectInfo> projInfoList = new ArrayList<>();
            for (AbstractProject abstractProject : abstractProjectCollection) {
                ProjectInfo projInfo = getProjectInfoHelper(ProjectConsumer.SYSTEM, ProjectACL.AccessLevel.R,
                        ProjectACL.LockLevel.NO, false, false, abstractProject);
                if (projInfo != null) {
                    projInfoList.add(projInfo);
                }
            }
            // create the indexes
            //ProjectFacetsIndexUtils.createFacetIndexAPI(projInfoList); // OLD
            ProjectFacetsIndexLuceneUtils.createFacetIndexAPI(projInfoList);
        }
    }


    /**
     * Returns the projects where there is at least a user with the given role
     *
     * @param consumer
     * @param role
     * @param requestedAccessLevel
     * @param requestedLockLevel
     * @param onlyOpen
     * @return
     * @throws ProjectAccessException
     */
    /*
    // NOT USED ANYMORE
    @STServiceOperation
    public List<ProjectInfo> listProjectsPerRole(@Optional(defaultValue = "SYSTEM") ProjectConsumer consumer,
            String role, @Optional(defaultValue = "R") ProjectACL.AccessLevel requestedAccessLevel,
            @Optional(defaultValue = "NO") ProjectACL.LockLevel requestedLockLevel,
            @Optional(defaultValue = "false") boolean userDependent,
            @Optional(defaultValue = "false") boolean onlyOpen) throws ProjectAccessException {
        List<ProjectInfo> listProjInfo = new ArrayList<>();

        for (AbstractProject absProj : ProjectManager.listProjects(consumer)) {
            ProjectInfo projInfo = getProjectInfoHelper(consumer, requestedAccessLevel, requestedLockLevel,
                    userDependent, onlyOpen, absProj);
            if (projInfo != null) {
                Collection<ProjectUserBinding> puBindings = ProjectUserBindingsManager
                        .listPUBindingsOfProject(absProj);
                for (ProjectUserBinding pub : puBindings) { // looks into the bindings if there is at least
                    // one with the given role
                    if (pub.getRoles().stream().anyMatch(r -> r.getName().equals(role))) {
                        // the PU binding has the given role
                        listProjInfo.add(projInfo);
                        break; // project added, no need to look for other PUBindings
                    }
                }
            }
        }
        return listProjInfo;
    }
     */

    @STServiceOperation
    public Boolean isProjectExisting(String projectName, @Optional(defaultValue = "true") boolean includeClosed) {
        try {
            Project proj = ProjectManager.getProject(projectName, includeClosed);
            return proj != null; //no exception raised => project exists
        } catch (ProjectAccessException | ProjectInexistentException | InvalidProjectNameException e) {
            return false; //exception raised => project doesn't exists
        }
    }

    /**
     * Returns information
     *
     * @param consumer
     * @param requestedAccessLevel
     * @param requestedLockLevel
     * @param projectName
     * @return
     * @throws ProjectInexistentException
     * @throws InvalidProjectNameException
     * @throws ProjectAccessException
     */
    @STServiceOperation
    public ProjectInfo getProjectInfo(@Optional(defaultValue = "SYSTEM") ProjectConsumer consumer,
            @Optional(defaultValue = "R") ProjectACL.AccessLevel requestedAccessLevel,
            @Optional(defaultValue = "NO") ProjectACL.LockLevel requestedLockLevel, String projectName)
            throws IllegalStateException, ProjectAccessException, InvalidProjectNameException,
            ProjectInexistentException {

        try {
            Project proj = ProjectManager.getProject(projectName, true);
            return getProjectInfoHelper(consumer, requestedAccessLevel, requestedLockLevel, false, false, proj);
        } catch (ProjectInexistentException projectInexistentException) {
            // in case of the exception ProjectInexistentException (so asking for a project not existing),
            // set the setStackTrace of such exception with an empty Array
            projectInexistentException.setStackTrace(new StackTraceElement[0]);
            throw projectInexistentException;
        }

    }

    /**
     * Returns metadata about a project. If either <code>userDependent</code> or <code>onlyOpen</code> is
     * <code>true</code>, then this operation might return <code>null</code>.
     *
     * @param consumer
     * @param requestedAccessLevel
     * @param requestedLockLevel
     * @param userDependent        if true, returns only the projects accessible by the logged user (the user has a role
     *                             assigned in it)
     * @param onlyOpen             if true, return only the open projects
     * @param absProj
     * @return
     * @throws ProjectAccessException
     */
    public ProjectInfo getProjectInfoHelper(ProjectConsumer consumer,
            ProjectACL.AccessLevel requestedAccessLevel, ProjectACL.LockLevel requestedLockLevel,
            boolean userDependent, boolean onlyOpen, AbstractProject absProj) {
        String name = absProj.getName();
        String baseURI = null;
        String defaultNamespace = null;
        String model = null;
        String lexicalizationModel = null;
        boolean historyEnabled = false;
        boolean validationEnabled = false;
        boolean blacklistingEnabled = false;
        boolean shaclEnabled = false;
        boolean undoEnabled = false;
        boolean trivialInferenceEnabled = false;
        boolean readOnly = false;
        boolean open = false;
        AccessResponse access = null;
        RepositoryLocation repoLocation = new RepositoryLocation(null);
        ProjectStatus status = new ProjectStatus(Status.ok);
        ProjectVisibility visibility = ProjectVisibility.AUTHORIZED;
        Map<String, String> labels = null;
        String description = null;
        ProjectFacets facets = null;
        String createdAt = null;
        boolean openAtStartup = false;

        if (absProj instanceof Project proj) {

            baseURI = proj.getBaseURI();
            defaultNamespace = proj.getDefaultNamespace();
            model = proj.getModel().stringValue();
            lexicalizationModel = proj.getLexicalizationModel().stringValue();
            historyEnabled = proj.isHistoryEnabled();
            validationEnabled = proj.isValidationEnabled();
            blacklistingEnabled = proj.isBlacklistingEnabled();
            shaclEnabled = proj.isSHACLEnabled();
            undoEnabled = proj.isUndoEnabled();
            trivialInferenceEnabled = proj.isTrivialInferencerEnabled();
            readOnly = proj.isReadonly();
            visibility = proj.getVisibility();
            open = ProjectManager.isOpen(proj);
            access = ProjectManager.checkAccessibility(consumer, proj, requestedAccessLevel,
                    requestedLockLevel);
            repoLocation = proj.getDefaultRepositoryLocation();
            labels = proj.getLabels();
            description = proj.getDescription();
            createdAt = proj.getCreatedAt();
            openAtStartup = proj.isOpenAtStartupEnabled();
            if (onlyOpen && !open) {
                return null;
            }
            if (userDependent && !ProjectUserBindingsManager.hasUserAccessToProject(UsersManager.getLoggedUser(), proj)) {
                return null;
            }
            try {
                facets = (ProjectFacets) exptManager.getSettings(proj, null, null,
                        ProjectFacetsStore.class.getName(), Scope.PROJECT);
            } catch (IllegalStateException | STPropertyAccessException | NoSuchSettingsManager e) {
                facets = new CorruptedProjectFacets(e);
            }

        } else { // absProj instanceof CorruptedProject
            CorruptedProject proj = (CorruptedProject) absProj;
            status = new ProjectStatus(Status.corrupted, proj.getCauseOfCorruption().getMessage());
        }
        return new ProjectInfo(name, open, baseURI, defaultNamespace, model, lexicalizationModel,
                historyEnabled, validationEnabled, blacklistingEnabled, shaclEnabled, undoEnabled,
                trivialInferenceEnabled, readOnly, visibility, access, repoLocation, status, labels, description,
                createdAt, openAtStartup, facets);
    }

    /**
     * Returns the access statuses for every project-consumer combination. Returns a response with a set of
     * <code>project</code> elements containing <code>consumer</code> elements and a <code>lock</code>
     * element. Each <code>project</code> element has a single attribute: its <code>name</code>. The
     * <code>consumer</code> elements have the following attributes:
     * <ul>
     * <li><code>name</code>: consumer's name</li>
     * <li><code>availableACLLevel</code>: ACL given from the project to the consumer</li>
     * <li><code>acquiredACLLevel</code>: The access level with which the consumer accesses the project (only
     * specified if the project is accessed by the consumer)</li>
     * </ul>
     * The <code>lock</code> element has the following attributes:
     * <ul>
     * <li><code>availableLockLevel</code>: lock level exposed by the project</li>
     * <li><code>lockingConsumer</code></li>: name of the consumer that locks the project. Specified only if
     * there is a consumer locking the current project.
     * <li><code>acquiredLockLevel</code>: lock level which with a consumer is locking the project (optional
     * as the previous</li>
     * </ul>
     *
     * @return
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     * @throws ProjectAccessException
     * @throws IOException
     */
    @STServiceOperation
    @PreAuthorize("@auth.isAuthorized('pm(project)', 'R')")
    public JsonNode getAccessStatusMap()
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException {
        JsonNodeFactory jsonFactory = JsonNodeFactory.instance;
        ArrayNode responseNode = jsonFactory.arrayNode();

        // list and sort alphabetically
        List<AbstractProject> projects = new ArrayList<>(ProjectManager.listProjects());
        projects.sort(Comparator.comparing(AbstractProject::getName));

        for (AbstractProject absProj : projects) {
            if (absProj instanceof Project project) {
                JsonNode projectNode = createProjectAclNode(project);
                responseNode.add(projectNode);
            }
        }
        return responseNode;
    }

    @STServiceOperation
    @PreAuthorize("@auth.isAuthorized('pm(project)', 'R')")
    public JsonNode getAccessStatus(String projectName)
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException {
        Project project = ProjectManager.getProjectDescription(projectName);
        return createProjectAclNode(project);
    }

    private JsonNode createProjectAclNode(Project project)
            throws ProjectAccessException, InvalidProjectNameException, ProjectInexistentException {
        JsonNodeFactory jsonFactory = JsonNodeFactory.instance;

        ObjectNode projectNode = jsonFactory.objectNode();
        projectNode.set("name", jsonFactory.textNode(project.getName()));

        ArrayNode consumerArrayNode = jsonFactory.arrayNode();

        List<AbstractProject> consumers = new ArrayList<>(ProjectManager.listProjects());
        consumers.sort(Comparator.comparing(AbstractProject::getName));

        consumers.removeIf(c -> c.getName().equals(project.getName())); // remove itself from its possible
        // consumers

        ProjectACL projectAcl = project.getACL();

        // status for SYSTEM
        ProjectConsumer consumer = ProjectConsumer.SYSTEM;
        JsonNode consumerAclNode = createConsumerAclNode(project, consumer);
        consumerArrayNode.add(consumerAclNode);
        // ACL for other ProjectConsumer
        for (AbstractProject absCons : consumers) {
            if (absCons instanceof Project) {
                consumer = absCons;
                consumerAclNode = createConsumerAclNode(project, consumer);
                consumerArrayNode.add(consumerAclNode);
            }
        }

        projectNode.set("consumers", consumerArrayNode);

        AccessLevel univAclLevel = projectAcl.getUniversalAccessLevel();
        String universalAclStr = univAclLevel != null ? univAclLevel.name() : null;
        projectNode.set("universalACLLevel", jsonFactory.textNode(universalAclStr));

        // LOCK for the project
        ObjectNode lockNode = jsonFactory.objectNode();
        lockNode.set("availableLockLevel", jsonFactory.textNode(projectAcl.getLockLevel().name()));
        ProjectConsumer lockingConsumer = ProjectManager.getLockingConsumer(project.getName());
        String lockingConsumerName = null;
        String acquiredLockLevel = null;
        if (lockingConsumer != null) { // the project could be not locked by any consumer
            lockingConsumerName = lockingConsumer.getName();
            acquiredLockLevel = ProjectManager.getLockingLevel(project.getName(), lockingConsumer).name();
        }
        lockNode.set("lockingConsumer", jsonFactory.textNode(lockingConsumerName));
        lockNode.set("acquiredLockLevel", jsonFactory.textNode(acquiredLockLevel));
        projectNode.set("lock", lockNode);

        return projectNode;
    }

    private JsonNode createConsumerAclNode(Project project, ProjectConsumer consumer)
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException {
        JsonNodeFactory jsonFactory = JsonNodeFactory.instance;

        ObjectNode consumerNode = jsonFactory.objectNode();
        consumerNode.set("name", jsonFactory.textNode(consumer.getName()));

        ProjectACL projectAcl = project.getACL();

        String availableAclLevel = null;
        AccessLevel aclForConsumer = projectAcl.getAccessLevelForConsumer(consumer);
        if (aclForConsumer != null) {
            availableAclLevel = aclForConsumer.name();
        }
        consumerNode.set("availableACLLevel", jsonFactory.textNode(availableAclLevel));

        String acquiredAclLevel = null;
        AccessLevel accessedLevel = ProjectManager.getAccessedLevel(project.getName(), consumer);
        if (accessedLevel != null) {
            acquiredAclLevel = accessedLevel.name();
        }
        consumerNode.set("acquiredACLLevel", jsonFactory.textNode(acquiredAclLevel));

        return consumerNode;
    }

    /**
     * Update the AccessLevel of the current project
     *
     * @param consumerName
     * @param accessLevel  if not provided revoke any access level assigned from the project to the consumer
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     * @throws ProjectAccessException
     * @throws ProjectUpdateException
     * @throws ReservedPropertyUpdateException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorized('pm(project)', 'U')")
    public void updateAccessLevel(String consumerName, @Optional AccessLevel accessLevel)
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException,
            ProjectUpdateException, ReservedPropertyUpdateException {
        Project project = getProject();
        if (accessLevel != null) {
            project.getACL().grantAccess(ProjectManager.getProjectDescription(consumerName), accessLevel);
        } else {
            project.getACL().revokeAccess(ProjectManager.getProjectDescription(consumerName));
        }
    }

    /**
     * @param projectName
     * @param consumerName
     * @param accessLevel  if not provided revoke any access level assigned from the project to the consumer
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     * @throws ProjectAccessException
     * @throws ProjectUpdateException
     * @throws ReservedPropertyUpdateException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void updateProjectAccessLevel(String projectName, String consumerName,
            @Optional AccessLevel accessLevel) throws InvalidProjectNameException, ProjectInexistentException,
            ProjectAccessException, ProjectUpdateException, ReservedPropertyUpdateException {
        Project project = ProjectManager.getProject(projectName, true);
        if (accessLevel != null) {
            project.getACL().grantAccess(ProjectManager.getProjectDescription(consumerName), accessLevel);
        } else {
            project.getACL().revokeAccess(ProjectManager.getProjectDescription(consumerName));
        }
    }

    /**
     * Update the universal (for every consumer) AccessLevel of the current project
     *
     * @param accessLevel if not provided revoke any universal access level assigned from the project
     * @throws ProjectUpdateException
     * @throws ReservedPropertyUpdateException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorized('pm(project)', 'U')")
    public void updateUniversalAccessLevel(@Optional AccessLevel accessLevel)
            throws ProjectUpdateException, ReservedPropertyUpdateException {
        Project project = getProject();
        if (accessLevel != null) {
            project.getACL().grantUniversalAccess(accessLevel);
        } else {
            project.getACL().revokeUniversalAccess();
        }
    }

    /**
     * Update the universal (for every consumer) AccessLevel of the given project
     *
     * @param projectName
     * @param accessLevel if not provided revoke any universal access level assigned from the project
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     * @throws ProjectAccessException
     * @throws ProjectUpdateException
     * @throws ReservedPropertyUpdateException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void updateUniversalProjectAccessLevel(String projectName, @Optional AccessLevel accessLevel)
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException,
            ProjectUpdateException, ReservedPropertyUpdateException {
        Project project = ProjectManager.getProject(projectName, true);
        if (accessLevel != null) {
            project.getACL().grantUniversalAccess(accessLevel);
        } else {
            project.getACL().revokeUniversalAccess();
        }
    }

    /**
     * Updates the lock level of the accessed project
     *
     * @param lockLevel
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     * @throws ProjectAccessException
     * @throws ProjectUpdateException
     * @throws ReservedPropertyUpdateException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorized('pm(project)', 'U')")
    public void updateLockLevel(LockLevel lockLevel)
            throws ProjectUpdateException, ReservedPropertyUpdateException {
        Project project = getProject();
        project.getACL().setLockableWithLevel(lockLevel);
    }

    /**
     * Updates the lock level of the project with the given <code>projectName</code>
     *
     * @param projectName
     * @param lockLevel
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     * @throws ProjectAccessException
     * @throws ProjectUpdateException
     * @throws ReservedPropertyUpdateException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void updateProjectLockLevel(String projectName, LockLevel lockLevel)
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException,
            ProjectUpdateException, ReservedPropertyUpdateException {
        Project project = ProjectManager.getProject(projectName, true);
        project.getACL().setLockableWithLevel(lockLevel);
    }

    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorizedInProject('pm(project)', 'U', #projectName)")
    public void setProjectLabels(String projectName, Map<String, String> labels)
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException,
            ProjectUpdateException {
        Project project = ProjectManager.getProject(projectName, true);
        project.setLabels(labels);
    }

    /**
     * returns the last connection info (user, timestamp, sessionId) of the given project.
     * This service requires admin capabilities since it is a cross-project service.
     * Moreover, it wouldn't make sense for a PM to retrieve this info because,
     * given that a user has a PM role only after he accesses the project,
     * it would always return the same PM-user as last connected
     *
     * @param projectName
     * @return
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     * @throws ProjectAccessException
     */
    @STServiceOperation
    @PreAuthorize("@auth.isAdmin()")
    public JsonNode getLastConnectionInfo(String projectName)
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException {
        Project project = ProjectManager.getProjectDescription(projectName);
        Project.LastConnectionInfo connInfo = project.getLastConnectionInfo();
        if (connInfo != null) { //if user has ever accessed the project
            JsonNodeFactory jsonFactory = JsonNodeFactory.instance;
            ObjectNode respNode = jsonFactory.objectNode();
            respNode.set("timestamp", jsonFactory.numberNode(connInfo.getTimestamp()));
            respNode.set("userIri", jsonFactory.textNode(connInfo.getUserIri().stringValue())); //in case of user not found, gives at least a useful info
            try {
                STUser user = UsersManager.getUser(connInfo.getUserIri());
                respNode.set("user", user.getAsJsonObject());
            } catch (UserException e) {
                respNode.set("user", null);
            }
            return respNode;
        } else {
            return null;
        }
    }

    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void deleteProject(String projectName) throws ProjectDeletionException,
            ProjectAccessException, ProjectUpdateException, ReservedPropertyUpdateException,
            InvalidProjectNameException, ProjectInexistentException, IOException {
        ProjectManager.deleteProject(projectName);
        // delete the project from the Lucene index as well
        //ProjectFacetsIndexUtils.deleteProjectFromFacetIndex(projectName); // OLD
        ProjectFacetsIndexLuceneUtils.removeProjectFromIndex(projectName);
    }


    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void deleteAllFacetsIndexes() {
        ProjectFacetsIndexLuceneUtils.deleteFacetIndexesFolders();
    }


    /**
     * see
     * {@link ProjectManager#accessProject(ProjectConsumer, String, it.uniroma2.art.semanticturkey.project.ProjectACL.AccessLevel, it.uniroma2.art.semanticturkey.project.ProjectACL.LockLevel)}
     *
     * @param consumer
     * @param projectName
     * @param requestedAccessLevel
     * @param requestedLockLevel
     * @return
     * @throws ForbiddenProjectAccessException
     * @throws ProjectAccessException
     * @throws ProjectInexistentException
     * @throws InvalidProjectNameException
     * @throws IOException
     * @throws ProjectBindingException
     * @throws RBACException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void accessProject(ProjectConsumer consumer, String projectName,
            ProjectACL.AccessLevel requestedAccessLevel, ProjectACL.LockLevel requestedLockLevel)
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException,
            ForbiddenProjectAccessException {
        ProjectManager.accessProject(consumer, projectName, requestedAccessLevel, requestedLockLevel);
    }

    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public Map<String, ExceptionDAO> accessAllProjects(
            @Optional(defaultValue = "SYSTEM") ProjectConsumer consumer,
            @Optional(defaultValue = "RW") ProjectACL.AccessLevel requestedAccessLevel,
            @Optional(defaultValue = "R") ProjectACL.LockLevel requestedLockLevel,
            @Optional(defaultValue = "false") boolean onlyProjectsAtStartup) throws ProjectAccessException {

        Map<String, ExceptionDAO> projectExceptionMap = new HashMap<>();

        // iterate over the existing projects
        Collection<AbstractProject> abstractProjectCollection = ProjectManager
                .listProjects(ProjectConsumer.SYSTEM);
        for (AbstractProject abstractProject : abstractProjectCollection) {
            ProjectInfo projInfo = getProjectInfoHelper(ProjectConsumer.SYSTEM, ProjectACL.AccessLevel.R,
                    ProjectACL.LockLevel.NO, false, false, abstractProject);
            if (!projInfo.isOpen()) {
                // if the project is closed, open it, if requested
                try {
                    if (onlyProjectsAtStartup) {
                        // check if this is one of the project that should be open at startup, is so, open in
                        if (projInfo.isOpenAtStartup()) {
                            ProjectManager.accessProject(consumer, projInfo.getName(), requestedAccessLevel,
                                    requestedLockLevel);
                        }
                    } else {
                        ProjectManager.accessProject(consumer, projInfo.getName(), requestedAccessLevel,
                                requestedLockLevel);
                    }
                } catch (InvalidProjectNameException | ProjectInexistentException | ProjectAccessException
                         | ForbiddenProjectAccessException e) {
                    // take note of the problematic project
                    projectExceptionMap.put(projInfo.getName(), ExceptionDAO.valueOf(e));
                }
            }
        }
        return projectExceptionMap;
    }

    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void disconnectFromAllProjects(@Optional(defaultValue = "SYSTEM") ProjectConsumer consumer)
            throws ProjectAccessException {
        // iterate over the existing projects
        Collection<AbstractProject> abstractProjectCollection = ProjectManager
                .listProjects(ProjectConsumer.SYSTEM);
        for (AbstractProject abstractProject : abstractProjectCollection) {
            ProjectInfo projInfo = getProjectInfoHelper(ProjectConsumer.SYSTEM, ProjectACL.AccessLevel.R,
                    ProjectACL.LockLevel.NO, false, false, abstractProject);
            if (projInfo.isOpen()) {
                // if the project is opened, close it
                String projectName = projInfo.getName();
                ProjectManager.disconnectFromProject(consumer, projectName);
            }
        }
    }

    /**
     * see {@link ProjectManager#disconnectFromProject(ProjectConsumer, String)}
     *
     * @param consumer
     * @param projectName
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void disconnectFromProject(ProjectConsumer consumer, String projectName) {

        ProjectManager.disconnectFromProject(consumer, projectName);
    }

    /*
     * this one has being temporarily not imported from the old project service, as it requires to close and
     * reopen a project. Not clear if we should allow a project to be deactivated/activated. Surely,
     * considering the fact that now more clients may be accessing the project, it would be dangerous to close
     * it and reopen it
     *
     * public Response saveProjectAs(Project<?> project, String newProjectName) throws
     * InvalidProjectNameException,
     */

    /**
     * saves project <code>projectName</code> to <code>newProject</code>
     *
     * @param projectName
     * @return
     * @throws ProjectInexistentException
     * @throws IOException
     * @throws DuplicatedResourceException
     * @throws InvalidProjectNameException
     * @throws ProjectAccessException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void cloneProject(String projectName, String newProjectName) throws InvalidProjectNameException,
            DuplicatedResourceException, IOException, ProjectInexistentException, ProjectAccessException {

        logger.debug("requested to export current project");

        ProjectManager.cloneProjectToNewProject(projectName, newProjectName);
    }

    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void exportProject(HttpServletResponse oRes,
            @RequestParam(value = "projectName") String projectName)
            throws IOException, ProjectAccessException {
        File tempServerFile = File.createTempFile("export", ".zip");
        logger.debug("requested to export current project");
        ProjectManager.exportProject(projectName, tempServerFile);
        oRes.setHeader("Content-Disposition", "attachment; filename=export.zip");
        FileInputStream is = new FileInputStream(tempServerFile);
        IOUtils.copy(is, oRes.getOutputStream());
        oRes.setContentType("application/zip");
        oRes.flushBuffer();
        is.close();
    }

    /**
     * @param importPackage
     * @param newProjectName
     * @throws InvalidProjectNameException
     * @throws ProjectUpdateException
     * @throws ProjectInconsistentException
     * @throws DuplicatedResourceException
     * @throws ProjectCreationException
     * @throws IOException
     * @throws ProjectBindingException
     * @throws ProjectAccessException
     * @throws ProjectInexistentException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void importProject(MultipartFile importPackage, String newProjectName)
            throws IOException, ProjectCreationException, DuplicatedResourceException, ProjectUpdateException,
            InvalidProjectNameException {

        logger.debug("requested to import project from file: " + importPackage);

        File projectFile = File.createTempFile("prefix", "suffix");
        importPackage.transferTo(projectFile);
        ProjectManager.importProject(projectFile, newProjectName);
    }

    /**
     * this service returns a list name-value for all the property of a given project. Returns a response with
     * elements called {@code propertyTag} with attributes {@code propNameAttr} for property name and
     *
     * @param projectName (optional)the project queried for properties
     * @return
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     * @throws ProjectAccessException
     * @throws IOException
     */
    @STServiceOperation
    @PreAuthorize("@auth.isAuthorized('pm(project)', 'R')")
    public Collection<ProjectPropertyInfo> getProjectPropertyMap(String projectName)
            throws InvalidProjectNameException, ProjectInexistentException, IOException {

        return ProjectManager.getProjectPropertyMap(projectName).entrySet().stream()
                .map(entry -> new ProjectPropertyInfo(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    /**
     * this service returns a list name-value for all the property of a given project. Returns a response with
     * elements called {@code propertyTag} with attributes {@code propNameAttr} for property name and
     *
     * @param projectName (optional)the project queried for properties
     * @return
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     * @throws ProjectAccessException
     * @throws IOException
     */
    @STServiceOperation
    @PreAuthorize("@auth.isAdmin()")
    public String getProjectPropertyFileContent(String projectName)
            throws InvalidProjectNameException, ProjectInexistentException, IOException {
        return ProjectManager.getProjectPropertyFileContent(projectName);
    }

    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void saveProjectPropertyFileContent(String projectName, String content)
            throws InvalidProjectNameException, ProjectInexistentException, IOException {
        ProjectManager.saveProjectPropertyFileContent(projectName, content);
    }

    /**
     * This service sets the value of a property of the current project.
     *
     * @param propName
     * @param propValue
     * @return
     * @throws ProjectAccessException
     * @throws ProjectInexistentException
     * @throws InvalidProjectNameException
     * @throws ReservedPropertyUpdateException
     * @throws ProjectUpdateException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorizedInProject('pm(project)', 'U', #projectName)")
    public void setProjectProperty(String projectName, String propName, @Optional String propValue)
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException,
            ProjectUpdateException, ReservedPropertyUpdateException {
        Project project = ProjectManager.getProject(projectName, true);
        if (propValue != null) {
            project.setProperty(propName, propValue);
        } else {
            project.removeProperty(propName);
        }
    }

    /**
     * Sets the facets of a project
     *
     * @param projectName
     * @param facets
     * @throws STPropertyAccessException
     * @throws WrongPropertiesException
     * @throws STPropertyUpdateException
     * @throws NoSuchSettingsManager
     * @throws IllegalStateException
     * @throws ProjectInexistentException
     * @throws InvalidProjectNameException
     * @throws ProjectAccessException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorizedInProject('pm(project)', 'U', #projectName)")
    public void setProjectFacets(String projectName, ObjectNode facets)
            throws IllegalStateException, NoSuchSettingsManager, STPropertyUpdateException,
            WrongPropertiesException, STPropertyAccessException, ProjectAccessException,
            InvalidProjectNameException, ProjectInexistentException, IOException {
        Project project = ProjectManager.getProject(projectName, true);
        exptManager.storeSettings(ProjectFacetsStore.class.getName(), project, UsersManager.getLoggedUser(), null,
                Scope.PROJECT, facets);
        // update the index about the facets of this project
        ProjectInfo projectInfo = getProjectInfoHelper(ProjectConsumer.SYSTEM, ProjectACL.AccessLevel.R,
                ProjectACL.LockLevel.NO, false, false, project);
        if (projectInfo == null) {
            throw new ProjectAccessException(projectName);
        }

        //ProjectFacetsIndexUtils.recreateFacetIndexForProjectAPI(projectName, projectInfo); // OLD
        ProjectFacetsIndexLuceneUtils.createFacetIndexAPI(projectInfo);
    }

    /**
     * Returns the facets of a project
     *
     * @param projectName
     * @throws NoSuchSettingsManager
     * @throws STPropertyAccessException
     * @throws IllegalStateException
     * @throws ProjectInexistentException
     * @throws InvalidProjectNameException
     * @throws ProjectAccessException
     */
    @STServiceOperation
    @PreAuthorize("@auth.isAdmin()")
    public ProjectFacets getProjectFacets(String projectName)
            throws IllegalStateException, STPropertyAccessException, NoSuchSettingsManager,
            ProjectAccessException, InvalidProjectNameException, ProjectInexistentException {
        Project project = ProjectManager.getProject(projectName, true);
        STUser user = UsersManager.getLoggedUser();
        UsersGroup group = ProjectUserBindingsManager.getUserGroup(user, project);
        return (ProjectFacets) exptManager.getSettings(project, user, group,
                ProjectFacetsStore.class.getName(), Scope.PROJECT);
    }

    /**
     * Returns an uninitialized form for project facets. Differently from {@link #getProjectFacets(String)},
     * this operation doesn't accept a project name as argument nor does it look at the current project
     *
     * @return
     * @throws IllegalStateException
     * @throws STPropertyAccessException
     * @throws NoSuchSettingsManager
     * @throws ProjectAccessException
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     */
    @STServiceOperation
    public ProjectFacets getProjectFacetsForm() throws IllegalStateException, STPropertyAccessException {
        return STPropertiesManager.loadSTPropertiesFromObjectNodes(ProjectFacets.class, false,
                STPropertiesManager.createObjectMapper(exptManager), JsonNodeFactory.instance.objectNode());
    }

    /**
     * Returns the schema of custom project facets
     *
     * @throws NoSuchSettingsManager
     * @throws STPropertyAccessException
     * @throws IllegalStateException
     */
    @STServiceOperation
    public STPropertiesSchema getCustomProjectFacetsSchema()
            throws IllegalStateException, STPropertyAccessException, NoSuchSettingsManager {
        STUser user = UsersManager.getLoggedUser();
        return (STPropertiesSchema) exptManager.getSettings(null, user, null,
                CustomProjectFacetsSchemaStore.class.getName(), Scope.SYSTEM);
    }

    /**
     * Sets the schema of custom project facets
     *
     * @param facetsSchema
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void setCustomProjectFacetsSchema(ObjectNode facetsSchema)
            throws IllegalStateException, NoSuchSettingsManager, STPropertyUpdateException,
            WrongPropertiesException, STPropertyAccessException {
        exptManager.storeSettings(CustomProjectFacetsSchemaStore.class.getName(), null,
                UsersManager.getLoggedUser(), null, Scope.SYSTEM, facetsSchema);
    }

    /**
     * Returns the repositories associated with a (closed) project. Optionally, it is possible to skip local
     * repositories.
     *
     * @param projectName
     * @param excludeLocal
     * @throws ProjectAccessException
     * @throws ProjectInexistentException
     * @throws InvalidProjectNameException
     */
    @STServiceOperation
    @PreAuthorize("@auth.isAdmin()")
    public Collection<RepositorySummary> getRepositories(String projectName,
            @Optional(defaultValue = "false") boolean excludeLocal)
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException {

        Collection<RepositorySummary> rv = new ArrayList<>();

        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager repoManager = new STLocalRepositoryManager(
                    project.getProjectDirectory());
            repoManager.init();
            try {
                Collection<RepositorySummary> summaries = Project.getRepositorySummaries(repoManager,
                        excludeLocal);
                rv.addAll(summaries);
            } finally {
                repoManager.shutDown();
            }
        });

        return rv;
    }

    /**
     * Modifies the access credentials of a repository associated with a given (closed) project. The new
     * username and password are optional: if they are not given, they are considered <code>null</code>, thus
     * indicating an unprotected repository.
     *
     * @param projectName
     * @param repositoryID
     * @param newUsername
     * @param newPassword
     * @throws ProjectAccessException
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void modifyRepositoryAccessCredentials(String projectName, String repositoryID,
            @Optional String newUsername, @Optional String newPassword)
            throws ProjectAccessException, InvalidProjectNameException, ProjectInexistentException {
        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager repoManager = new STLocalRepositoryManager(
                    project.getProjectDirectory());
            repoManager.init();
            try {
                repoManager.modifyAccessCredentials(repositoryID, newUsername, newPassword);
            } finally {
                repoManager.shutDown();
            }
        });
    }

    /**
     * Modifies the access credentials of (possibly) many repositories at once. The repositories shall match
     * the provided <code>serverURL</code> and <code>currentUsername</code> (only if
     * <code>matchUsername</code> is <code>true</code>). When username matching is active, a <code>null</code>
     * value for <code>currentUsername</code> indicates repositories with no associated username.
     *
     * @param projectName
     * @param serverURL
     * @param matchUsername
     * @param currentUsername
     * @param newUsername
     * @param newPassword
     * @throws ProjectAccessException
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void batchModifyRepostoryAccessCredentials(String projectName, String serverURL,
            @Optional(defaultValue = "false") boolean matchUsername, @Optional String currentUsername,
            @Optional String newUsername, @Optional String newPassword)
            throws ProjectAccessException, InvalidProjectNameException, ProjectInexistentException {
        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager repoManager = new STLocalRepositoryManager(
                    project.getProjectDirectory());
            repoManager.init();
            try {
                repoManager.batchModifyAccessCredentials(serverURL, matchUsername, currentUsername,
                        newUsername, newPassword);
            } finally {
                repoManager.shutDown();
            }
        });

    }

    /**
     * Preloads data contained provided in the request body.
     *
     * @param preloadedData
     * @param preloadedDataFormat
     * @return
     * @throws IOException
     * @throws ProfilerException
     * @throws RepositoryException
     * @throws RDFParseException
     * @throws AssessmentException
     * @throws STPropertyAccessException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isSuperUser(false)")
    public PreloadedDataSummary preloadDataFromFile(MultipartFile preloadedData,
            RDFFormat preloadedDataFormat) throws IOException, RDFParseException, RepositoryException,
            ProfilerException, AssessmentException, STPropertyAccessException {
        File preloadedDataFile = preloadedDataStore.preloadData(preloadedData::transferTo);

        String baseURI = null;
        IRI model = null;
        IRI lexicalizationModel = null;

        return preloadDataInternal(baseURI, model, lexicalizationModel, preloadedDataFile,
                preloadedDataFormat);
    }

    /**
     * Preloads data from URL.
     *
     * @param preloadedDataURL
     * @param preloadedDataFormat
     * @return
     * @throws IOException
     * @throws FileNotFoundException
     * @throws ProfilerException
     * @throws RepositoryException
     * @throws RDFParseException
     * @throws AssessmentException
     * @throws STPropertyAccessException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isSuperUser(false)")
    public PreloadedDataSummary preloadDataFromURL(URL preloadedDataURL,
            @Optional RDFFormat preloadedDataFormat) throws IOException, RDFParseException,
            RepositoryException, ProfilerException, AssessmentException, STPropertyAccessException {

        logger.debug("Preload data from URL = {} (format = {})", preloadedDataURL, preloadedDataFormat);

        File preloadedDataFile;

        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        try (CloseableHttpClient httpClient = httpClientBuilder.useSystemProperties().build()) {
            HttpGet request = new HttpGet(preloadedDataURL.toExternalForm());
            Set<RDFFormat> rdfFormats = preloadedDataFormat != null
                    ? Collections.singleton(preloadedDataFormat)
                    : RDFParserRegistry.getInstance().getKeys();
            List<String> acceptParams = RDFFormat.getAcceptParams(rdfFormats, false, null);
            acceptParams.forEach(acceptParam -> request.addHeader("Accept", acceptParam));
            request.addHeader("Accept", "application/zip;q=0.5");
            request.addHeader("Accept", "application/gzip;q=0.5");
            request.addHeader("Accept", "*/*;q=0.1");

            try (CloseableHttpResponse httpResponse = httpClient.execute(request)) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (preloadedDataFormat == null) {
                    Header contentTypeHeader = httpEntity.getContentType();
                    if (contentTypeHeader != null) {
                        ContentType contentType = ContentType.parse(contentTypeHeader.getValue());
                        String mime = contentType.getMimeType();
                        // only process non-archive mime types
                        if (!Arrays.asList("application/zip", "application/gzip").contains(mime)) {
                            preloadedDataFormat = Rio.getParserFormatForMIMEType(mime)
                                    .orElseThrow(Rio.unsupportedFormat(mime));
                        }
                    }

                    if (preloadedDataFormat == null) { // not provided, nor obtained through MIME type
                        // this should also handle filenames decorated by archive formats e.g. .nt.gz
                        preloadedDataFormat = Rio.getParserFormatForFileName(preloadedDataURL.getPath())
                                .orElse(null);
                    }
                }

                preloadedDataFile = preloadedDataStore.preloadData(f -> {
                    try (OutputStream out = new FileOutputStream(f)) {
                        IOUtils.copy(httpEntity.getContent(), out);
                    }
                });
            }
        }

        String baseURI = null;
        IRI model = null;
        IRI lexicalizationModel = null;

        return preloadDataInternal(baseURI, model, lexicalizationModel, preloadedDataFile,
                preloadedDataFormat);
    }

    /**
     * Preloads data from a catalog.
     *
     * @param connectorId
     * @param datasetId
     * @return
     * @throws IOException
     * @throws ProfilerException
     * @throws RepositoryException
     * @throws RDFParseException
     * @throws AssessmentException
     * @throws STPropertyAccessException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isSuperUser(false)")
    public PreloadedDataSummary preloadDataFromCatalog(String connectorId, String datasetId)
            throws IOException, RDFParseException, RepositoryException, ProfilerException,
            AssessmentException, STPropertyAccessException {
        DatasetCatalogConnector datasetCatalogConnector = (DatasetCatalogConnector) ((NonConfigurableExtensionFactory<?>) exptManager
                .getExtension(connectorId)).createInstance();

        DatasetDescription datasetDescrition = datasetCatalogConnector.describeDataset(datasetId);
        URL dataDump = datasetDescrition.getDataDumps().stream().map(DownloadDescription::getAccessURL)
                .findAny().orElse(null);
        if (dataDump == null) {
            IRI ontologyIRI = datasetDescrition.getOntologyIRI();
            if (ontologyIRI == null) {
                throw new IOException("Missing data dump for preloaded dataset");
            } else {
                dataDump = new URL(ontologyIRI.toString());
            }
        }

        return preloadDataFromURL(dataDump, null);
    }

    private PreloadedDataSummary preloadDataInternal(@Nullable String baseURI, @Nullable IRI model,
            @Nullable IRI lexicalizationModel, File preloadedDataFile, RDFFormat preloadedDataFormat)
            throws RDFParseException, RepositoryException, IOException, ProfilerException,
            AssessmentException, STPropertyAccessException {

        if (!preloadedDataFile.exists()) {
            throw new FileNotFoundException(preloadedDataFile.getPath() + ": not existing");
        }

        if (!preloadedDataFile.isFile()) {
            throw new FileNotFoundException(preloadedDataFile.getPath() + ": not a normal file");
        }

        DataSize dataSize = new DataSize(preloadedDataFile.length(), DataSize.DataUnit.B);

        List<PreloadedDataSummary.PreloadWarning> preloadWarnings = new ArrayList<>();

        if (baseURI == null || model == null || lexicalizationModel == null) {
            CoreSystemSettings coreSystemSettings;
            try {
                coreSystemSettings = (CoreSystemSettings) exptManager.getSettings(null, UsersManager.getLoggedUser(), null, SemanticTurkeyCoreSettingsManager.class.getName(), Scope.SYSTEM);
            } catch (NoSuchSettingsManager e) {
                throw new RuntimeException(e); // this should never happen
            }

            PreloadSettings preloadSettings = java.util.Optional.ofNullable(coreSystemSettings.preload).orElseGet(PreloadSettings::new);
            PreloadProfilerSettings preloadProfilerSettings = java.util.Optional.ofNullable(preloadSettings.profiler).orElseGet(PreloadProfilerSettings::new);

            DataSize profilerDataSizeThreshold = java.util.Optional.ofNullable(preloadProfilerSettings.threshold).orElseGet(() -> new DataSize(1, DataSize.DataUnit.MiB));

            if (dataSize.compareTo(profilerDataSizeThreshold) > 0) { // preloaded data too big to profile
                preloadWarnings = new ArrayList<>(1);
                preloadWarnings
                        .add(new PreloadedDataSummary.ProfilerSizeTresholdExceeded(profilerDataSizeThreshold));
            } else { // profile the preloaded data to obtain the necessary information
                preloadWarnings = new ArrayList<>();

                try (Closer closer = Closer.create()) {
                    // metadata repository
                    SailRepository metadataRepo = new SailRepository(new MemoryStore());
                    metadataRepo.init();
                    closer.register(metadataRepo::shutDown);

                    // data repository
                    SailRepository dataRepo = new SailRepository(new MemoryStore());
                    dataRepo.init();
                    closer.register(dataRepo::shutDown);

                    try (LIMERepositoryConnectionWrapper metadataConn = new LIMERepositoryConnectionWrapper(
                            metadataRepo, metadataRepo.getConnection());
                         RepositoryConnection dataConn = dataRepo.getConnection()) {
                        ValueFactory vf = dataConn.getValueFactory();

                        IRI metadataBaseURI = vf.createIRI(
                                "http://example.org/" + UUID.randomUUID() + "/void.ttl");
                        IRI dataGraph = vf.createIRI("urn:uuid:" + UUID.randomUUID());

                        // load preloaded data to the data repository
                        dataConn.add(preloadedDataFile, null, preloadedDataFormat, dataGraph);

                        // profile the preloaded data
                        LIMEProfiler profiler = new LIMEProfiler(metadataConn, metadataBaseURI, dataConn,
                                dataGraph);
                        profiler.profile();

                        // export the profile as a Model
                        Model profile = new LinkedHashModel();
                        StatementCollector collector = new StatementCollector(profile);
                        metadataConn.export(collector);

                        // Extract information from the profile
                        IRI mainDataset = metadataConn.getMainDataset(false).filter(IRI.class::isInstance)
                                .map(IRI.class::cast).orElse(null);

                        if (lexicalizationModel == null) {
                            lexicalizationModel = mediationFramework
                                    .assessLexicalizationModel(mainDataset, profile).orElse(null);
                        }

                        logger.debug("main dataset = {}", mainDataset);
                        logger.debug("profile = {}", new Object() {
                            @Override
                            public String toString() {
                                StringWriter writer = new StringWriter();
                                Rio.write(profile, Rio.createWriter(RDFFormat.TURTLE, writer));
                                return writer.toString();
                            }
                        });

                        if (model == null) {
                            model = Models.objectIRI(QueryResults.asModel(
                                            metadataConn.getStatements(mainDataset, DCTERMS.CONFORMS_TO, null)))
                                    .orElse(null);
                        }

                        // Extract the baseURI as the ontology IRI
                        java.util.Optional<IRI> baseURIHolder;
                        try (RepositoryResult<Statement> queryResults = dataConn.getStatements(null, RDF.TYPE, OWL.ONTOLOGY);
                             Stream<Statement> stream = QueryResults.stream(queryResults)) {
                            baseURIHolder = stream
                                    .filter(s -> s.getSubject() instanceof IRI).map(s -> (IRI) s.getSubject())
                                    .findAny();
                        }

                        if (baseURIHolder.isPresent()) { // gets the base URI from the ontology object
                            baseURI = baseURIHolder.get().stringValue();
                        } else { // otherwise, determine the base URI from the data
                            TupleQuery nsQuery = dataConn.prepareTupleQuery(
                                    // @formatter:off
                                    "SELECT ?ns (COUNT(*) as ?count)  WHERE {\n" +
                                            "    GRAPH ?dataGraph {\n" +
                                            "    	?s ?p ?o .\n" +
                                            "    }\n" +
                                            "}\n" +
                                            "GROUP BY (REPLACE(STR(?s), \"^([^#]*(#|\\\\/))(.*)$\", \"$1\") as ?ns)\n" +
                                            "ORDER BY DESC(?count)\n" +
                                            "LIMIT 1"
                                    // @formatter:on
                            );
                            nsQuery.setBinding("dataGraph", dataGraph);
                            BindingSet bs = QueryResults.singleResult(nsQuery.evaluate());
                            if (bs != null && bs.hasBinding("ns")) {
                                baseURI = bs.getValue("ns").stringValue(); // possible trailing # stripped
                                // later
                            }

                        }

                        if (baseURI != null && baseURI.endsWith("#")) {
                            baseURI = baseURI.substring(0, baseURI.length() - 1);
                        }
                    }
                }
            }
        }

        return new PreloadedDataSummary(baseURI, model, lexicalizationModel, preloadedDataFile,
                preloadedDataFormat, preloadWarnings);
    }

    // ** ALL SERVICES AND APIS DEALING WITH THE LUCENE INDEX FOR THE FACETS**//

    @STServiceOperation
    public Map<String, List<String>> getFacetsAndValue(@Optional(defaultValue = "false") boolean onlyOpen,
            @Optional(defaultValue = "") List<ProjectVisibility> visibilityFilter)
            throws IOException, ProjectAccessException, InvalidProjectNameException {
        Map<String, List<String>> facetValueListMap = new HashMap<>();


        //create the indexes, if needed
        createFacetIndexIfNeeded();

        //get all the candidate projects
        List<String> candidateProjectNameList = preFilterProject(true, onlyOpen, visibilityFilter);

        Map<String, List<String>> facetToValueListMap = new HashMap<>();
        facetToValueListMap.put(ProjectFacetsIndexLuceneUtils.PROJECT_NAME, candidateProjectNameList);

        // do the search, using the Lucene index
        ProjectsAndFacetsResult projectsAndFacetsResult = ProjectFacetsIndexLuceneUtils.search(facetToValueListMap);

        //from projectsAndFacetsResult get the facetResult
        FacetsResultStruct facetsResultStruct = projectsAndFacetsResult.getFacetsResult();
        Map<String, Map<String, Integer>> facetToValueToCountMapMap = facetsResultStruct.getFacetToValueToCountMapMap(true);

        for (String facetName : facetToValueToCountMapMap.keySet()) {
            Map<String, Integer> facetValueToCountMap = facetToValueToCountMapMap.get(facetName);
            for (String facetValue : facetValueToCountMap.keySet()) {
                if (!facetValueListMap.containsKey(facetName)) {
                    facetValueListMap.put(facetName, new ArrayList<>());
                }
                if (!facetValueListMap.get(facetName).contains(facetValue)) {
                    facetValueListMap.get(facetName).add(facetValue);
                }
            }
        }
        return facetValueListMap;

        /*
         // old implementation, this implementation returns ALL facets without doing any restriction on the projects metadata
        // (e.g. if they are open, dependent on the current user, or other metadata)
        Map<String, List<String>> facetValueListMap = new HashMap<>();
        FacetsResultStruct facetsResultStruct = ProjectFacetsIndexLuceneUtils.getAllFacets(true);
        Map<String, Map<String, Integer>> facetToValueToCountMapMap = facetsResultStruct.getFacetToValueToCountMapMap(true);
        for (String facetName : facetToValueToCountMapMap.keySet()) {
            Map<String, Integer> facetValueToCountMap = facetToValueToCountMapMap.get(facetName);
            for (String facetValue : facetValueToCountMap.keySet()) {
                if (!facetValueListMap.containsKey(facetName)) {
                    facetValueListMap.put(facetName, new ArrayList<>());
                }
                if (!facetValueListMap.get(facetName).contains(facetValue)) {
                    facetValueListMap.get(facetName).add(facetValue);
                }
            }
        }
        return facetValueListMap;

         */
    }

    /*
    // NOT USED ANYMORE
    @STServiceOperation
    public Map<String, Map<String, Integer>> getFacetsAndValueWithCount(@Optional(defaultValue = "false") boolean onlyOpen,
                                                        @Optional(defaultValue = "") List<ProjectVisibility> visibilityFilter)
            throws IOException, ProjectAccessException, InvalidProjectNameException {

        //create the indexes, if needed
        createFacetIndexIfNeeded();

        //get all the candidate projects
        List<String> candidateProjectNameList = preFilterProject(true, onlyOpen, visibilityFilter);

        Map<String, List<String>> facetToValueListMap = new HashMap<>();
        facetToValueListMap.put(ProjectFacetsIndexLuceneUtils.PROJECT_NAME, candidateProjectNameList);

        // do the search, using the Lucene index
        ProjectsAndFacetsResult projectsAndFacetsResult = ProjectFacetsIndexLuceneUtils.search(facetToValueListMap);

        //from projectsAndFacetsResult get the facetResult
        FacetsResultStruct facetsResultStruct = projectsAndFacetsResult.getFacetsResult();
        return facetsResultStruct.getFacetToValueToCountMapMap(true);
    }
     */


    /**
     * Create the Lucene index for the facets in ALL projects
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void createFacetIndex() throws InvalidProjectNameException,
            ProjectAccessException, IOException {

        // iterate over the existing projects
        Collection<AbstractProject> abstractProjectCollection = ProjectManager
                .listProjects(ProjectConsumer.SYSTEM);
        List<ProjectInfo> projInfoList = new ArrayList<>();
        for (AbstractProject abstractProject : abstractProjectCollection) {
            ProjectInfo projInfo = getProjectInfoHelper(ProjectConsumer.SYSTEM, ProjectACL.AccessLevel.R,
                    ProjectACL.LockLevel.NO, false, false, abstractProject);
            if (projInfo != null) {
                projInfoList.add(projInfo);
            }
        }
        // create the indexes
        //ProjectFacetsIndexUtils.createFacetIndexAPI(projInfoList); // OLD
        ProjectFacetsIndexLuceneUtils.createFacetIndexAPI(projInfoList);
    }

    /**
     * Create the Lucene index for the facets in ALL projects
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void recreateFacetIndexForProject(String projectName) throws
            InvalidProjectNameException, ProjectAccessException, IOException {
        Project project = ProjectManager.getProject(projectName);
        ProjectInfo projectInfo = getProjectInfoHelper(ProjectConsumer.SYSTEM, ProjectACL.AccessLevel.R,
                ProjectACL.LockLevel.NO, false, false, project);
        if (projectInfo == null) {
            throw new ProjectAccessException(projectName);
        }
        //ProjectFacetsIndexUtils.recreateFacetIndexForProjectAPI(projectName, projectInfo); // OLD
        ProjectFacetsIndexLuceneUtils.createFacetIndexAPI(projectInfo);
    }

    /**
     * retrive the list of the projects, grouped by the value of a specific facet
     *
     * @param bagOf    the name of the facet that will be used to group the returning projects
     * @param onlyOpen if true, return only the open projects
     * @param visibilityFilter if provided, return only projects with the given visibility
     * @param roleFilter if provided, return only projects where the logged user has assigned the given role
     * @return
     * @throws IOException
     * @throws InvalidProjectNameException
     * @throws ProjectAccessException
     * @throws PropertyNotFoundException
     */
    @STServiceOperation(method = RequestMethod.POST)
    public Map<String, List<ProjectInfo>> retrieveProjects(String bagOf,
            @Optional(defaultValue = "false") boolean onlyOpen,
            @Optional(defaultValue = "") List<ProjectVisibility> visibilityFilter,
            @Optional String roleFilter
    ) throws IOException, InvalidProjectNameException, ProjectAccessException {
        Map<String, List<ProjectInfo>> facetToProjetInfoListMap = new HashMap<>();

        STUser user = UsersManager.getLoggedUser();

        // bagOf and query cannot be both empty/null
        if ((bagOf == null || bagOf.isEmpty())) {
            throw new IllegalArgumentException("bagOf cannot be null/empty");
        }

        //create the indexes, if needed
        createFacetIndexIfNeeded();

        //get all the candidate projects
        List<String> candidateProjectNameList = preFilterProject(true, onlyOpen, visibilityFilter);

        //get the list of all the projects (just their name) from the lucene index
        ProjectsResult projectListFromIndex = ProjectFacetsIndexLuceneUtils.getProjectListFromIndex();
        List<String> notExistingProjectList = new ArrayList<>();
        for (String projectName : projectListFromIndex.getProjectNameList()) {
            ProjectInfo projectInfo = null;
            try {
                //from the projectName get the Project and then the ProjectInfo (if possible)
                Project project = ProjectManager.getProjectDescription(projectName);
                projectInfo = getProjectInfoHelper(ProjectConsumer.SYSTEM, AccessLevel.R,
                        LockLevel.NO, true, onlyOpen, project);

                if (projectInfo != null) {
                    //this project exist, check if this project is in the candidate list
                    if (!candidateProjectNameList.contains(projectName)) {
                        // skip this project since is not in the candidate list
                        continue;
                    }
                    if (roleFilter != null && ProjectUserBindingsManager.getPUBindingRoles(user, project, false)
                            .stream().noneMatch(r -> r.getName().equals(roleFilter))) {
                        continue; //skip project if the required role is not assigned to current user
                    }
                }

            } catch (ProjectInexistentException e) {
                // the project does not exist, so it will be removed from the index
                notExistingProjectList.add(projectName);
            }
            if (projectInfo == null) {
                continue;
            }


            //get the value of the desired facet (if it exists in the current projectInfo) to put the projectInfo
            // in the right group (if it does not exist, use the "" as the name of the facet)
            List<String> facetValueList = ProjectFacetsIndexLuceneUtils.getFacetValueFromProjectInfoForBagResults(projectInfo,
                    bagOf);

            if (!facetValueList.isEmpty()) {
                for (String facetValue : facetValueList) {
                    if (!facetToProjetInfoListMap.containsKey(facetValue)) {
                        facetToProjetInfoListMap.put(facetValue, new ArrayList<>());
                    }
                    facetToProjetInfoListMap.get(facetValue).add(projectInfo);
                }
            } else {
                //since facetValueList is empty, then use NO_VALUE_FOR_FACET as the value
                if (!facetToProjetInfoListMap.containsKey(NO_VALUE_FOR_FACET)) {
                    facetToProjetInfoListMap.put(NO_VALUE_FOR_FACET, new ArrayList<>());
                }
                facetToProjetInfoListMap.get(NO_VALUE_FOR_FACET).add(projectInfo);
            }
        }
        // remove all the not existing projects from the index
        for (String projectName : notExistingProjectList) {
            //ProjectFacetsIndexUtils.deleteProjectFromFacetIndex(projectName); // old
            ProjectFacetsIndexLuceneUtils.removeProjectFromIndex(projectName);
        }

        return facetToProjetInfoListMap;
    }


    /*
    // NOT USED ANYMORE
    @STServiceOperation(method = RequestMethod.POST)
    public Map<String, List<ProjectInfo>> searchAndRetriveProjects(@Optional String bagOf,
            Map<String, List<String>> facetToValueListMap,
            @Optional(defaultValue = "false") boolean onlyOpen,
            @Optional(defaultValue = "") List<ProjectVisibility> visibilityFilter) throws ProjectAccessException,
            InvalidProjectNameException, IOException {

        Map<String, List<ProjectInfo>> facetValueToProjetInfoListMap = new HashMap<>();

        // do the search, using the Lucene index
        ProjectsAndFacetsResult projectsAndFacetsResult = doSearchWithFacets(facetToValueListMap, true, onlyOpen,
                visibilityFilter);

        //from the projectsAndFacetsResult consider only the retrieved projects
        ProjectsResult projectListFromIndex = projectsAndFacetsResult.getProjectsResult();
        List<String> notExistingProjectList = new ArrayList<>();

        for (String projectName : projectListFromIndex.getProjectNameList()) {
            ProjectInfo projectInfo = null;

            try {
                //from the projectName get the Project and then the ProjectInfo (if possible)
                Project project = ProjectManager.getProjectDescription(projectName);
                projectInfo = getProjectInfoHelper(ProjectConsumer.SYSTEM, AccessLevel.R,
                        LockLevel.NO, true, onlyOpen, project);
            } catch (ProjectInexistentException e) {
                // the project does not exist, so it will be removed from the index
                notExistingProjectList.add(projectName);
            }
            if (projectInfo == null) {
                continue;
            }

            if (bagOf != null && !bagOf.isEmpty()) {
                //get the value of the desired facet (if it exists in the current projectInfo) to put the projectInfo
                // in the right group (if it does not exist, use the "" as the name of the facet)
                List<String> facetValueList = ProjectFacetsIndexLuceneUtils.getFacetValueFromProjectInfoForBagResults(projectInfo,
                        bagOf);

                if (!facetValueList.isEmpty()) {
                    for (String facetValue : facetValueList) {
                        if (!facetValueToProjetInfoListMap.containsKey(facetValue)) {
                            facetValueToProjetInfoListMap.put(facetValue, new ArrayList<>());
                        }
                        facetValueToProjetInfoListMap.get(facetValue).add(projectInfo);
                    }
                } else {
                    //since facetValueList is empty, then use NO_VALUE_FOR_FACET as the value
                    if (!facetValueToProjetInfoListMap.containsKey(NO_VALUE_FOR_FACET)) {
                        facetValueToProjetInfoListMap.put(NO_VALUE_FOR_FACET, new ArrayList<>());
                    }
                    facetValueToProjetInfoListMap.get(NO_VALUE_FOR_FACET).add(projectInfo);
                }

            } else {
                //bagOf is null or empty, so no need to group the projects
                if (!facetValueToProjetInfoListMap.containsKey("")) {
                    facetValueToProjetInfoListMap.put("", new ArrayList<>());
                }
                facetValueToProjetInfoListMap.get("").add(projectInfo);
            }
        }
        // remove all the not existing projects from the index
        for (String projectName : notExistingProjectList) {
            //ProjectFacetsIndexUtils.deleteProjectFromFacetIndex(projectName); // old
            ProjectFacetsIndexLuceneUtils.removeProjectFromIndex(projectName);
        }
        return facetValueToProjetInfoListMap;
    }
    */

    /*
    // NOT USED ANYMORE
    @STServiceOperation(method = RequestMethod.POST)
    public Map<String, Map<String, Integer>> searchAndRetriveFacets(Map<String, List<String>> facetToValueListMap,
            @Optional(defaultValue = "false") boolean onlyOpen,
            @Optional(defaultValue = "") List<ProjectVisibility> visibilityFilter) throws ProjectAccessException,
            InvalidProjectNameException, IOException {


        Map<String, Map<String, Integer>> facetNameToFacetValueToCountMapMap;

        // do the search, using the Lucene index
        ProjectsAndFacetsResult projectsAndFacetsResult = doSearchWithFacets(facetToValueListMap, true, onlyOpen,
                visibilityFilter);

        //from projectsAndFacetsResult get the facetResult
        FacetsResultStruct facetsResultStruct = projectsAndFacetsResult.getFacetsResult();
        facetNameToFacetValueToCountMapMap = facetsResultStruct.getFacetToValueToCountMapMap(true);

        return facetNameToFacetValueToCountMapMap;
    }
     */

    @STServiceOperation(method = RequestMethod.POST)
    public ProjectsInfoAndFacetsCount searchAndRetrieveProjectsAndFacets(@Optional String bagOf,
            Map<String, List<String>> facetToValueListMap,
            @Optional(defaultValue = "false") boolean onlyOpen,
            @Optional(defaultValue = "") List<ProjectVisibility> visibilityFilter) throws ProjectAccessException,
            InvalidProjectNameException, IOException {

        ProjectsInfoAndFacetsCount projectsInfoAndFacetsCount = new ProjectsInfoAndFacetsCount();

        // this list is used to check if, in the results there are any project that no longer exists is ST and so
        // they should be removed from the lucene indexes (in this case, the search should be done again since its results
        // about the facets are wrong)
        List<String> notExistingProjectList = new ArrayList<>();
        boolean doSearchAgain;
        int countIteration = 0; // this is to avoid infinite loop

        do {
            ++countIteration;

            // do the search, using the Lucene index
            ProjectsAndFacetsResult projectsAndFacetsResult = doSearchWithFacets(facetToValueListMap, true, onlyOpen,
                    visibilityFilter);


            //from the projectsAndFacetsResult consider only the retrieved projects
            ProjectsResult projectListFromIndex = projectsAndFacetsResult.getProjectsResult();


            for (String projectName : projectListFromIndex.getProjectNameList()) {
                ProjectInfo projectInfo = null;

                try {
                    //from the projectName get the Project and then the ProjectInfo (if possible)
                    Project project = ProjectManager.getProjectDescription(projectName);
                    projectInfo = getProjectInfoHelper(ProjectConsumer.SYSTEM, AccessLevel.R,
                            LockLevel.NO, true, onlyOpen, project);
                } catch (ProjectInexistentException e) {
                    // the project does not exist, so it will be removed from the index
                    notExistingProjectList.add(projectName);
                }
                if (projectInfo == null) {
                    continue;
                }

                if (bagOf != null && !bagOf.isEmpty()) {
                    //get the value of the desired facet (if it exists in the current projectInfo) to put the projectInfo
                    // in the right group (if it does not exist, use the "" as the name of the facet)
                    List<String> facetValueList = ProjectFacetsIndexLuceneUtils.getFacetValueFromProjectInfoForBagResults(projectInfo,
                            bagOf);
                    if (!facetValueList.isEmpty()) {
                        for (String facetValue : facetValueList) {
                            projectsInfoAndFacetsCount.addProjetInfoWithFacetValue(facetValue, projectInfo);
                        }
                    } else {
                        //since facetValueList is empty, then use NO_VALUE_FOR_FACET as the value
                        projectsInfoAndFacetsCount.addProjetInfoWithFacetValue(NO_VALUE_FOR_FACET, projectInfo);
                    }
                } else {
                    //bagOf is null or empty, so no need to group the projects
                    projectsInfoAndFacetsCount.addProjetInfoWithFacetValue("", projectInfo);
                }
            }

            //check if notExistingProjectList is empty or not
            if (!notExistingProjectList.isEmpty() && countIteration < 4) {
                // remove all the not existing projects from the index
                for (String projectName : notExistingProjectList) {
                    ProjectFacetsIndexLuceneUtils.removeProjectFromIndex(projectName);
                }
                doSearchAgain = true;
                notExistingProjectList.clear();
            } else {
                // since all the returned projects exists, get from projectsAndFacetsResult get the facetResult
                FacetsResultStruct facetsResultStruct = projectsAndFacetsResult.getFacetsResult();
                Map<String, Map<String, Integer>> facetNameToFacetValueToCountMapMap = facetsResultStruct.getFacetToValueToCountMapMap(true);
                projectsInfoAndFacetsCount.addFacetNameFacetValueAndCount(facetNameToFacetValueToCountMapMap);
                doSearchAgain = false;
            }
        } while (doSearchAgain);

        return projectsInfoAndFacetsCount;

    }


    private ProjectsAndFacetsResult doSearchWithFacets(Map<String, List<String>> facetToValueListMap,
            boolean userDependent, boolean onlyOpen,
            List<ProjectVisibility> visibilityFilter)
            throws ProjectAccessException, InvalidProjectNameException, IOException {
        //create the indexes, if needed
        createFacetIndexIfNeeded();

        //get all the candidate projects
        List<String> candidateProjectNameList = preFilterProject(userDependent, onlyOpen, visibilityFilter);

        //pass the candidateProjectNameList to the search so only these candidate project will be returned
        if (!facetToValueListMap.containsKey(ProjectFacetsIndexLuceneUtils.PROJECT_NAME)) {
            facetToValueListMap.put(ProjectFacetsIndexLuceneUtils.PROJECT_NAME, candidateProjectNameList);
        } else {
            facetToValueListMap.get(ProjectFacetsIndexLuceneUtils.PROJECT_NAME).addAll(candidateProjectNameList);
        }

        // do the search, using the Lucene index
        ProjectsAndFacetsResult projectsAndFacetsResult = ProjectFacetsIndexLuceneUtils.search(facetToValueListMap);

        return projectsAndFacetsResult;
    }


    // get the list of projects, using the input filter (get this list WITHOUT using the Lucene index)
    private List<String> preFilterProject(boolean userDependent, boolean onlyOpen,
            List<ProjectVisibility> visibilityFilter) throws ProjectAccessException {
        List<String> projectNameList = new ArrayList<>();

        Collection<AbstractProject> projects = ProjectManager.listProjects();

        for (AbstractProject absProj : projects) {
            ProjectInfo projInfo = getProjectInfoHelper(ProjectConsumer.SYSTEM, AccessLevel.R, LockLevel.NO,
                    userDependent, onlyOpen, absProj);
            if (projInfo != null) {
                if (!visibilityFilter.isEmpty() && !visibilityFilter.contains(projInfo.getVisibility())) {
                    continue; //skip project
                }
                //add this project (its name) to the list that will be returned
                projectNameList.add(projInfo.getName());
            }
        }

        return projectNameList;
    }

    /**
     * Enables/disables blacklisting in a <em>closed</em> project with <em>validation</em> already enabled
     *
     * @param projectName
     * @param blacklistingEnabled
     * @throws ProjectInexistentException
     * @throws InvalidProjectNameException
     * @throws ProjectAccessException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void setBlacklistingEnabled(String projectName, boolean blacklistingEnabled)
            throws ProjectAccessException, InvalidProjectNameException, ProjectInexistentException {
        ProjectManager.handleProjectExclusively(projectName, project -> {
            // checks that validation is already enabled
            if (!project.isValidationEnabled()) {
                throw new IllegalArgumentException(
                        "Cannot enable blacklisting on a project without validation: " + projectName);
            }
            if (project.isBlacklistingEnabled() == blacklistingEnabled)
                return; // nothing to do

            STLocalRepositoryManager prjRepMgr = new STLocalRepositoryManager(project.getProjectDirectory());
            prjRepMgr.init();
            try {
                prjRepMgr.operateOnUnfoldedManager(Project.CORE_REPOSITORY, (repMgr, repId) -> {
                    // updates the repository configuration

                    ValueFactory vf = SimpleValueFactory.getInstance();
                    Model configModel = repMgr.getRepositoryConfig(repId);
                    Resource changeTrackerSailHolder = Models
                            .subject(configModel.filter(null, SailConfigSchema.SAILTYPE,
                                    vf.createLiteral(ChangeTrackerFactory.SAIL_TYPE)))
                            .orElseThrow(() -> new IllegalArgumentException(
                                    "Unable to find the ChangeTracker sail in the repository configuration"));
                    Models.setProperty(configModel, changeTrackerSailHolder,
                            ChangeTrackerSchema.BLACKLISTING_ENABLED, vf.createLiteral(blacklistingEnabled));
                    if (blacklistingEnabled) {
                        if (CHANGELOG
                                .isNull(Models
                                        .getPropertyIRI(configModel, changeTrackerSailHolder,
                                                ChangeTrackerSchema.BLACKLIST_GRAPH)
                                        .orElse(CHANGELOG.NULL))) {
                            Models.setProperty(configModel, changeTrackerSailHolder,
                                    ChangeTrackerSchema.BLACKLIST_GRAPH, SUPPORT.BLACKLIST);
                        }
                    }

                    // writes the updated configuration
                    repMgr.addRepositoryConfig(configModel);
                });

                // udpates the project property
                project.setReservedProperty(Project.BLACKLISTING_ENABLED_PROP,
                        Boolean.toString(blacklistingEnabled));
            } catch (ProjectUpdateException e) {
                throw new IllegalStateException(
                        "Unable to update the project properties for setting blacklisting to "
                                + blacklistingEnabled);
            } finally {
                prjRepMgr.shutDown();
            }

        });
    }

    /**
     * Sets whether SHACL validation on commit is enabled in a <em>closed</em> project
     *
     * @param projectName
     * @param shaclValidationEnabled
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void setSHACLValidationEnabled(String projectName, boolean shaclValidationEnabled) throws ProjectAccessException, ProjectInexistentException, InvalidProjectNameException {
        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager prjRepMgr = new STLocalRepositoryManager(project.getProjectDirectory());
            prjRepMgr.init();
            try {
                prjRepMgr.operateOnUnfoldedManager(Project.CORE_REPOSITORY, (modelBasedRepositoryManager, repId) -> {
                    Model repConfig = modelBasedRepositoryManager.getRepositoryConfig(repId);
                    Resource shaclSail = Models.subject(repConfig.filter(null, SailConfigSchema.SAILTYPE, Values.literal(ShaclSailFactory.SAIL_TYPE))).orElseThrow(() -> new IllegalArgumentException("the SHACL Sail is not configured for the repository"));
                    Models.setProperty(repConfig, shaclSail, ShaclSailSchema.VALIDATION_ENABLED, Values.literal(shaclValidationEnabled));
                    modelBasedRepositoryManager.addRepositoryConfig(repConfig);
                });
            } finally {
                prjRepMgr.shutDown();
            }
        });
    }

    /**
     * Tells whether SHACL validation on commit is enabled in a <em>closed</em> project.
     *
     * @param projectName
     * @return
     */
    @STServiceOperation
    @PreAuthorize("@auth.isAdmin()")
    public Boolean isSHACLValidationEnabled(String projectName) throws ProjectAccessException, ProjectInexistentException, InvalidProjectNameException {
        MutableBoolean validationEnabled = new MutableBoolean(ShaclSailConfig.VALIDATION_ENABLED_DEFAULT);
        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager prjRepMgr = new STLocalRepositoryManager(project.getProjectDirectory());
            prjRepMgr.init();
            try {
                prjRepMgr.operateOnUnfoldedManager(Project.CORE_REPOSITORY, (modelBasedRepositoryManager, repId) -> {
                    Model repConfig = modelBasedRepositoryManager.getRepositoryConfig(repId);
                    Resource shaclSail = Models.subject(repConfig.filter(null, SailConfigSchema.SAILTYPE, Values.literal(ShaclSailFactory.SAIL_TYPE))).orElseThrow(() -> new IllegalArgumentException("the SHACL Sail is not configured for the repository"));
                    boolean v = Models.getPropertyLiteral(repConfig, shaclSail, ShaclSailSchema.VALIDATION_ENABLED).map(l -> Literals.getBooleanValue(l, ShaclSailConfig.VALIDATION_ENABLED_DEFAULT)).orElse(ShaclSailConfig.VALIDATION_ENABLED_DEFAULT);
                    validationEnabled.setValue(v);
                });
            } finally {
                prjRepMgr.shutDown();
            }
        });
        return validationEnabled.toBoolean();
    }

    /**
     * Sets the query timeout. If the passed value is greater than zero, also the "Throw exception on query timeout" is
     * set to true (false is the passed value is 0 or less)
     *
     * @param projectName      the name of the project
     * @param timeoutInSeconds the number of second to set for the timeout (a value of 0 or less it means that the is no timeout)
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void setQueryTimeout(String projectName, int timeoutInSeconds) throws ProjectAccessException, ProjectInexistentException, InvalidProjectNameException {
        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager prjRepMgr = new STLocalRepositoryManager(project.getProjectDirectory());
            prjRepMgr.init();
            try {
                prjRepMgr.operateOnUnfoldedManager(Project.CORE_REPOSITORY, (modelBasedRepositoryManager, repId) -> {
                    Model repConfig = modelBasedRepositoryManager.getRepositoryConfig(repId);

                    Resource currentSail = Models.subject(repConfig.filter(null, GRAPHDB_CONFIG.QUERY_TIMEOUT, null)).orElseThrow(() -> new IllegalArgumentException("the Query Timeout is not configured for the repository"));
                    //Resource currentSail2 = Models.subject(repConfig.filter(null, GRAPHDB_CONFIG.THROW_QUERY_EVALUATION_EXCEPTION_ON_TIMEOUT, null)).orElseThrow(() -> new IllegalArgumentException("the Query Timeout is not configured for the repository"));

                    int timeoutInSecToSet = Math.max(timeoutInSeconds, 0);
                    boolean throwExceptionBool = timeoutInSecToSet > 0;
                    Models.setProperty(repConfig, currentSail, GRAPHDB_CONFIG.QUERY_TIMEOUT, Values.literal(timeoutInSecToSet));
                    Models.setProperty(repConfig, currentSail, GRAPHDB_CONFIG.THROW_QUERY_EVALUATION_EXCEPTION_ON_TIMEOUT, Values.literal(throwExceptionBool));

                    modelBasedRepositoryManager.addRepositoryConfig(repConfig);
                });
            } finally {
                prjRepMgr.shutDown();
            }
        });
    }

    /**
     * Tells the current value of the query timeout, in seconds.
     *
     * @param projectName the name of the project
     * @return the current value of the query timeout, in seconds
     */
    @STServiceOperation
    @PreAuthorize("@auth.isAdmin()")
    public Integer getQueryTimeoutValue(String projectName) throws ProjectAccessException, ProjectInexistentException, InvalidProjectNameException {
        MutableInt timeoutValueMutable = new MutableInt();
        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager prjRepMgr = new STLocalRepositoryManager(project.getProjectDirectory());
            prjRepMgr.init();
            try {
                prjRepMgr.operateOnUnfoldedManager(Project.CORE_REPOSITORY, (modelBasedRepositoryManager, repId) -> {
                    Model repConfig = modelBasedRepositoryManager.getRepositoryConfig(repId);

                    int timeoutValue = Literals.getIntValue(Models.object(repConfig.filter(null, GRAPHDB_CONFIG.QUERY_TIMEOUT, null))
                            .orElseThrow(() -> new IllegalArgumentException("the Query Timeout property, " + GRAPHDB_CONFIG.QUERY_TIMEOUT +
                                    " is not configured for the repository")), 0);
                    timeoutValueMutable.setValue(timeoutValue);

                });
            } finally {
                prjRepMgr.shutDown();
            }
        });
        return timeoutValueMutable.intValue();
    }

    /**
     * Tells whether <em>http://www.ontotext.com/config/graphdb#throw-QueryEvaluationException-on-timeout</em> is true or false
     *
     * @param projectName the name of the project
     * @return the value of the property <em>http://www.ontotext.com/config/graphdb#throw-QueryEvaluationException-on-timeout</em>
     */
    @STServiceOperation
    @PreAuthorize("@auth.isAdmin()")
    public Boolean isQueryEvaluationExceptionForTimeoutEnabled(String projectName) throws ProjectAccessException, ProjectInexistentException, InvalidProjectNameException {
        MutableBoolean EvaluationExceptionEnabled = new MutableBoolean();
        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager prjRepMgr = new STLocalRepositoryManager(project.getProjectDirectory());
            prjRepMgr.init();
            try {
                prjRepMgr.operateOnUnfoldedManager(Project.CORE_REPOSITORY, (modelBasedRepositoryManager, repId) -> {
                    Model repConfig = modelBasedRepositoryManager.getRepositoryConfig(repId);

                    boolean booleanValue = Literals.getBooleanValue(Models.object(repConfig.filter(null, GRAPHDB_CONFIG.THROW_QUERY_EVALUATION_EXCEPTION_ON_TIMEOUT, null))
                            .orElseThrow(() -> new IllegalArgumentException("the Query Evaluation Exception property "
                                    + GRAPHDB_CONFIG.THROW_QUERY_EVALUATION_EXCEPTION_ON_TIMEOUT
                                    + " is not configured for the repository")), false);

                    EvaluationExceptionEnabled.setValue(booleanValue);
                });
            } finally {
                prjRepMgr.shutDown();
            }
        });
        return EvaluationExceptionEnabled.toBoolean();
    }

    /**
     * Sets whether undo is enabled in a <em>closed</em> project. Undo can be enabled on projects with history or validation
     * and in any project in which the change tracker happens to be set up (see {@link #isChangeTrackerSetUp(String)}
     *
     * @param projectName
     * @param undoEnabled
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void setUndoEnabled(String projectName, boolean undoEnabled) throws ProjectAccessException, ProjectInexistentException, InvalidProjectNameException {
        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager prjRepMgr = new STLocalRepositoryManager(project.getProjectDirectory());
            prjRepMgr.init();
            boolean undoEnabledOrig = project.isUndoEnabled();
            try {
                project.setReservedProperty(Project.UNDO_ENABLED_PROP, String.valueOf(undoEnabled));
            } catch (ProjectUpdateException e) {
                ExceptionUtils.rethrow(e);
            }

            try {
                prjRepMgr.operateOnUnfoldedManager(Project.CORE_REPOSITORY, (modelBasedRepositoryManager, repId) -> {
                    Model repConfig = modelBasedRepositoryManager.getRepositoryConfig(repId);
                    Resource changeTrackerSail = Models.subject(repConfig.filter(null, SailConfigSchema.SAILTYPE, Values.literal(ChangeTrackerFactory.SAIL_TYPE))).orElseThrow(() -> new IllegalArgumentException("the ChangeTracker Sail is not configured for the repository"));
                    Models.setProperty(repConfig, changeTrackerSail, ChangeTrackerSchema.UNDO_ENABLED, Values.literal(undoEnabled));
                    try {
                        modelBasedRepositoryManager.addRepositoryConfig(repConfig);
                    } catch (Exception e) {
                        try {
                            project.setReservedProperty(Project.UNDO_ENABLED_PROP, String.valueOf(undoEnabledOrig));
                        } catch (ProjectUpdateException e2) {
                            e.addSuppressed(e2);
                            ExceptionUtils.rethrow(e);
                        }
                    }
                });
            } finally {
                prjRepMgr.shutDown();
            }
        });
    }

    /**
     * Tells whether undo is enabled in a <em>closed</em> project.
     *
     * @param projectName
     * @return
     */
    @STServiceOperation
    @PreAuthorize("@auth.isAdmin()")
    public Boolean isUndoEnabled(String projectName) throws ProjectAccessException, ProjectInexistentException, InvalidProjectNameException {
        MutableBoolean undoEnabled = new MutableBoolean(false);
        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager prjRepMgr = new STLocalRepositoryManager(project.getProjectDirectory());
            prjRepMgr.init();
            try {
                prjRepMgr.operateOnUnfoldedManager(Project.CORE_REPOSITORY, (modelBasedRepositoryManager, repId) -> {
                    Model repConfig = modelBasedRepositoryManager.getRepositoryConfig(repId);
                    Resource changeTrackerSail = Models.subject(repConfig.filter(null, SailConfigSchema.SAILTYPE, Values.literal(ChangeTrackerFactory.SAIL_TYPE))).orElseThrow(() -> new IllegalArgumentException("the ChangeTracker Sail is not configured for the repository"));
                    boolean v = Models.getPropertyLiteral(repConfig, changeTrackerSail, ChangeTrackerSchema.UNDO_ENABLED).map(Literal::booleanValue).orElse(false);
                    undoEnabled.setValue(v);
                });
            } finally {
                prjRepMgr.shutDown();
            }
        });
        return undoEnabled.toBoolean();
    }

    /**
     * Sets whether Trivial Inference is enabled in a <em>closed</em> project
     *
     * @param projectName
     * @param trivialInferenceEnabled
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void setTrivialInferenceEnabled(String projectName, boolean trivialInferenceEnabled) throws ProjectAccessException, ProjectInexistentException, InvalidProjectNameException {
        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager prjRepMgr = new STLocalRepositoryManager(project.getProjectDirectory());
            prjRepMgr.init();
            try {
                prjRepMgr.operateOnUnfoldedManager(Project.CORE_REPOSITORY, (modelBasedRepositoryManager, repId) -> {
                    Model repConfig = modelBasedRepositoryManager.getRepositoryConfig(repId);
                    Resource trivialInferencerSail = Models.subject(repConfig.filter(null, SailConfigSchema.SAILTYPE, Values.literal(TrivialInferencerFactory.SAIL_TYPE))).orElseThrow(() -> new IllegalArgumentException("the TrivialInferencer Sail is not configured for the repository"));
                    Models.setProperty(repConfig, trivialInferencerSail, TrivialInferencerSchema.INFERENCE_ENABLED, Values.literal(trivialInferenceEnabled));
                    modelBasedRepositoryManager.addRepositoryConfig(repConfig);
                });
            } finally {
                prjRepMgr.shutDown();
            }
        });
    }

    /**
     * Tells whether trivial inference is enabled in a <em>closed</em> project.
     *
     * @param projectName
     * @return
     */
    @STServiceOperation
    @PreAuthorize("@auth.isAdmin()")
    public Boolean isTrivialInferenceEnabled(String projectName) throws ProjectAccessException, ProjectInexistentException, InvalidProjectNameException {
        MutableBoolean trivialInferenceEnabled = new MutableBoolean(false);
        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager prjRepMgr = new STLocalRepositoryManager(project.getProjectDirectory());
            prjRepMgr.init();
            try {
                prjRepMgr.operateOnUnfoldedManager(Project.CORE_REPOSITORY, (modelBasedRepositoryManager, repId) -> {
                    Model repConfig = modelBasedRepositoryManager.getRepositoryConfig(repId);
                    Resource trivialInferencerSail = Models.subject(repConfig.filter(null, SailConfigSchema.SAILTYPE, Values.literal(TrivialInferencerFactory.SAIL_TYPE))).orElseThrow(() -> new IllegalArgumentException("the TrivialInferencer Sail is not configured for the repository"));
                    boolean v = Models.getPropertyLiteral(repConfig, trivialInferencerSail, TrivialInferencerSchema.INFERENCE_ENABLED).map(Literal::booleanValue).orElse(false);
                    trivialInferenceEnabled.setValue(v);
                });
            } finally {
                prjRepMgr.shutDown();
            }
        });
        return trivialInferenceEnabled.toBoolean();
    }

    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void setReadOnly(String projectName, boolean readOnly) throws ProjectAccessException, ProjectInexistentException, InvalidProjectNameException, ProjectUpdateException {
        Project project = ProjectManager.getProject(projectName, true);
        project.setReservedProperty(Project.READONLY_PROP, String.valueOf(readOnly));
    }

    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void setVisibility(String projectName, ProjectVisibility visibility) throws ProjectAccessException, ProjectInexistentException, InvalidProjectNameException, ProjectUpdateException {
        Project project = ProjectManager.getProject(projectName, true);
        project.setReservedProperty(Project.VISIBILITY_PROP, String.valueOf(visibility));
    }

    /**
     * ShowVoc dedicated service to change project visibility from staging to public.
     * Additionally, it changes the universal access level to R,
     * creates the index and generate the metadata (if not yet available)
     *
     * @throws Exception
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void makePublic() throws Exception {
        Project project = getProject();
        project.setReservedProperty(Project.VISIBILITY_PROP, ProjectVisibility.PUBLIC.toString());
        //public projects in SV need R universal access to be read from other projects (e.g. aligned resources have to be resolvable)
        project.getACL().grantUniversalAccess(AccessLevel.R);
        globalSearchServices.createIndex();
        if (!mapleServices.checkProjectMetadataAvailability()) {
            mapleServices.profileProject();
        }
    }

    /**
     * ShowVoc dedicated service to change project visibility from public to staging.
     * Additionally, it removes the universal access and (optional) clears the index
     *
     * @param deleteIndex
     * @throws Exception
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void makeStaging(@Optional(defaultValue = "true") boolean deleteIndex) throws Exception {
        Project project = getProject();
        project.setReservedProperty(Project.VISIBILITY_PROP, ProjectVisibility.AUTHORIZED.toString());
        project.getACL().revokeUniversalAccess();
        if (deleteIndex) {
            globalSearchServices.clearSpecificIndex(project.getName());
        }
    }

    /**
     * Tells whether the change tracker is set up for a <em>closed</em> project.
     *
     * @param projectName
     * @return
     */
    @STServiceOperation
    @PreAuthorize("@auth.isAdmin()")
    public Boolean isChangeTrackerSetUp(String projectName) throws ProjectAccessException, ProjectInexistentException, InvalidProjectNameException {
        MutableBoolean changeTrackerSetup = new MutableBoolean(false);
        ProjectManager.handleProjectExclusively(projectName, project -> {
            STLocalRepositoryManager prjRepMgr = new STLocalRepositoryManager(project.getProjectDirectory());
            prjRepMgr.init();
            try {
                prjRepMgr.operateOnUnfoldedManager(Project.CORE_REPOSITORY, (modelBasedRepositoryManager, repId) -> {
                    Model repConfig = modelBasedRepositoryManager.getRepositoryConfig(repId);
                    changeTrackerSetup.setValue(Models.subject(repConfig.filter(null, SailConfigSchema.SAILTYPE, Values.literal(ChangeTrackerFactory.SAIL_TYPE))).isPresent());
                });
            } finally {
                prjRepMgr.shutDown();
            }
        });
        return changeTrackerSetup.toBoolean();
    }


    /**
     * Enables/Disables the possibility to automatically open a project when SemanticTurkey is executed
     *
     * @param projectName
     * @param openAtStartup
     * @throws InvalidProjectNameException
     * @throws ProjectInexistentException
     * @throws ProjectAccessException
     * @throws ProjectUpdateException
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAdmin()")
    public void setOpenAtStartup(String projectName, boolean openAtStartup)
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException,
            ProjectUpdateException {
        Project project = ProjectManager.getProject(projectName, true);
        project.setReservedProperty(Project.OPEN_AT_STARTUP_PROP, String.valueOf(openAtStartup));
    }

    @STServiceOperation
    @PreAuthorize("@auth.isAdmin()")
    public Boolean getOpenAtStartup(String projectName)
            throws InvalidProjectNameException, ProjectInexistentException, ProjectAccessException {
        Project project = ProjectManager.getProject(projectName, true);
        return project.isOpenAtStartupEnabled();
    }

    /**
     * Returns the rendering engine associated with a project together with its (optional) configuration
     *
     * @param projectName
     * @return
     * @throws ProjectInexistentException
     * @throws InvalidProjectNameException
     * @throws ProjectAccessException
     */
    @PreAuthorize("@auth.isAdmin()")
    @STServiceOperation
    public org.apache.commons.lang3.tuple.Pair<String, STProperties> getRenderingEngineConfiguration(
            String projectName)
            throws ProjectAccessException, InvalidProjectNameException, ProjectInexistentException {
        MutableObject<org.apache.commons.lang3.tuple.Pair<String, STProperties>> rv = new MutableObject<>();
        ProjectManager.handleProjectExclusively(projectName, project -> {
            org.apache.commons.lang3.tuple.Pair<String, STProperties> pair = getBoundComponentConfiguration(
                    project, Project.RENDERING_ENGINE_FACTORY_ID_PROP,
                    Project.RENDERING_ENGINE_CONFIG_FILENAME);

            rv.setValue(pair);

        });
        return rv.getValue();
    }

    /**
     * Updates the configuration of the rendering engine associated with a project
     *
     * @param projectName
     * @param renderingEngineSpecification
     * @throws ProjectInexistentException
     * @throws InvalidProjectNameException
     * @throws ProjectAccessException
     */
    @PreAuthorize("@auth.isAdmin()")
    @STServiceOperation(method = RequestMethod.POST)
    public void updateRenderingEngineConfiguration(String projectName,
            PluginSpecification renderingEngineSpecification)
            throws ProjectAccessException, InvalidProjectNameException, ProjectInexistentException {
        ProjectManager.handleProjectExclusively(projectName, project -> {
            try {
                updateBoundComponentConfiguration(project, Project.RENDERING_ENGINE_FACTORY_ID_PROP,
                        Project.RENDERING_ENGINE_CONFIGURATION_TYPE_PROP,
                        Project.RENDERING_ENGINE_CONFIG_FILENAME, renderingEngineSpecification);
            } catch (IOException | ProjectUpdateException e) {
                ExceptionUtils.rethrow(e);
            }
        });
    }

    /**
     * Returns the uri generator associated with a project together with its (optional) configuration
     *
     * @param projectName
     * @return
     * @throws ProjectInexistentException
     * @throws InvalidProjectNameException
     * @throws ProjectAccessException
     */
    @PreAuthorize("@auth.isAdmin()")
    @STServiceOperation
    public org.apache.commons.lang3.tuple.Pair<String, STProperties> getURIGeneratorConfiguration(
            String projectName)
            throws ProjectAccessException, InvalidProjectNameException, ProjectInexistentException {
        MutableObject<org.apache.commons.lang3.tuple.Pair<String, STProperties>> rv = new MutableObject<>();
        ProjectManager.handleProjectExclusively(projectName, project -> {
            org.apache.commons.lang3.tuple.Pair<String, STProperties> pair = getBoundComponentConfiguration(
                    project, Project.URI_GENERATOR_FACTORY_ID_PROP, Project.URI_GENERATOR_CONFIG_FILENAME);

            rv.setValue(pair);

        });
        return rv.getValue();
    }

    /**
     * Updates the configuration of the uri generator associated with a project
     *
     * @param projectName
     * @param uriGeneratorSpecification
     * @throws ProjectInexistentException
     * @throws InvalidProjectNameException
     * @throws ProjectAccessException
     */
    @PreAuthorize("@auth.isAdmin()")
    @STServiceOperation(method = RequestMethod.POST)
    public void updateURIGeneratorConfiguration(String projectName,
            PluginSpecification uriGeneratorSpecification)
            throws ProjectAccessException, InvalidProjectNameException, ProjectInexistentException {
        ProjectManager.handleProjectExclusively(projectName, project -> {
            try {
                updateBoundComponentConfiguration(project, Project.URI_GENERATOR_FACTORY_ID_PROP,
                        Project.URI_GENERATOR_CONFIGURATION_TYPE_PROP, Project.URI_GENERATOR_CONFIG_FILENAME,
                        uriGeneratorSpecification);
            } catch (IOException | ProjectUpdateException e) {
                ExceptionUtils.rethrow(e);
            }
        });
    }

    protected org.apache.commons.lang3.tuple.Pair<String, STProperties> getBoundComponentConfiguration(
            Project project, String factoryIdProp, String configFilename) throws RuntimeException {
        try {
            String componentID = project.getProperty(factoryIdProp);

            @Nullable
            STProperties config;

            File configFile = new File(project.getProjectDirectory(), configFilename);

            if (configFile.exists()) {
                ConfigurationManager<?> cm = exptManager.getConfigurationManager(componentID);

                Class<? extends Configuration> configBaseClass = ReflectionUtilities
                        .getInterfaceArgumentTypeAsClass(cm.getClass(), ConfigurationManager.class, 0);
                config = STPropertiesManager.loadSTPropertiesFromYAMLFiles(configBaseClass, true, configFile);
            } else {
                config = null;
            }

            return ImmutablePair.of(componentID, config);
        } catch (NoSuchConfigurationManager | STPropertyAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private void updateBoundComponentConfiguration(Project project, String factoryIdProp,
            String configTypeProp, String configFilename, PluginSpecification componentSpec)
            throws IOException, ProjectUpdateException {
        project.setReservedProperty(factoryIdProp, componentSpec.getFactoryId());
        File componentConfigurationFile = new File(project.getProjectDirectory(), configFilename);
        if (componentSpec.getConfiguration() != null) {
            try (FileWriter fw = new FileWriter(componentConfigurationFile)) {
                ObjectNode configuration = componentSpec.getConfiguration();
                if (StringUtils.isNoneBlank(componentSpec.getConfigType())) {
                    configuration = configuration.deepCopy();
                    configuration.put(STPropertiesManager.SETTINGS_TYPE_PROPERTY, componentSpec.getConfigType());
                }
                STPropertiesManager.storeObjectNodeInYAML(configuration,
                        componentConfigurationFile);
            }
        } else {
            if (!componentConfigurationFile.delete()) {
                logger.warn("Failed to delete file " + componentConfigurationFile.getPath());
            }
        }
    }

    @STServiceOperation()
    public List<ProjectsSearchGroup> listSearchProjectsGroups() throws InvalidProjectNameException,
            STPropertyAccessException, NoSuchSettingsManager, STPropertyUpdateException {

        SemanticTurkeyCoreSettingsManager systemSettingsManager = (SemanticTurkeyCoreSettingsManager)
                ExtensionPointManagerHolder.getExtensionPointManager().getSettingsManager(SemanticTurkeyCoreSettingsManager.class.getName());
        CoreSystemSettings systemSettings = systemSettingsManager.getSystemSettings();
        List<ProjectsSearchGroup> projectsSearchGroups = systemSettings.projectsSearchGroups;

        if (projectsSearchGroups != null) {
            /*
            groups cleaning: removes no more existing projects from groups
             */
            //true if a group has been cleaned, thus setting needs to be stored again
            boolean settingUpdated = false;
            for (ProjectsSearchGroup group : projectsSearchGroups) {
                Iterator<String> iterator = group.projectNames.iterator();
                while (iterator.hasNext()) {
                    String projName = iterator.next();
                    if (!ProjectManager.existsProject(projName)) {
                        iterator.remove();
                        settingUpdated = true;
                    }
                }
            }
            if (settingUpdated) {
                systemSettingsManager.storeSystemSettings(systemSettings);
            }
            return projectsSearchGroups;
        } else {
            return new ArrayList<>();
        }
    }

}