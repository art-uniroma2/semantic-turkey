package it.uniroma2.art.semanticturkey.services.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import it.uniroma2.art.semanticturkey.extension.NoSuchSettingsManager;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.properties.STPropertyUpdateException;
import it.uniroma2.art.semanticturkey.properties.WrongPropertiesException;
import it.uniroma2.art.semanticturkey.resources.Reference;
import it.uniroma2.art.semanticturkey.resources.Scope;
import it.uniroma2.art.semanticturkey.services.STServiceAdapter;
import it.uniroma2.art.semanticturkey.services.annotations.Optional;
import it.uniroma2.art.semanticturkey.services.annotations.Read;
import it.uniroma2.art.semanticturkey.services.annotations.RequestMethod;
import it.uniroma2.art.semanticturkey.services.annotations.STService;
import it.uniroma2.art.semanticturkey.services.annotations.STServiceOperation;
import it.uniroma2.art.semanticturkey.settings.download.DownloadProjectSettings;
import it.uniroma2.art.semanticturkey.settings.download.DownloadSettingsManager;
import it.uniroma2.art.semanticturkey.settings.download.DownloadType;
import it.uniroma2.art.semanticturkey.settings.download.SingleDownload;
import it.uniroma2.art.semanticturkey.storage.DirectoryEntryInfo;
import it.uniroma2.art.semanticturkey.storage.StorageManager;
import it.uniroma2.art.semanticturkey.utilities.Utilities;
import jakarta.servlet.http.HttpServletResponse;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandler;
import org.eclipse.rdf4j.rio.Rio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * @author <a href="mailto:turbati@info.uniroma2.it">Andrea Turbati</a>
 */

@STService
public class Download extends STServiceAdapter {

    private final String DOWNLOAD_DIR_NAME = "download";
    private final String PROJ = "proj:";

    private static Logger logger = LoggerFactory.getLogger(Download.class);


    /**
     * Create a downloadable distribution for the current dataset
     * @param fileName
     * @param localizedLabel
     * @param format
     * @param zipFile
     * @param overwrite
     * @throws IOException
     * @throws NoSuchSettingsManager
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorized('pm(project, downloads)', 'C')")
    @Read
    public void createDownload(String fileName, Literal localizedLabel, RDFFormat format,
            @Optional(defaultValue = "true") boolean zipFile,
            @Optional(defaultValue = "false") boolean overwrite) throws IOException, NoSuchSettingsManager {
        // check if the DOWNLOAD_DIR_NAME exist in the project folder, if not, create it
        checkAndInCaseCreateFolder();

        File tempFile = File.createTempFile(fileName, ".temp");
        try (OutputStream out = new FileOutputStream(tempFile)) {
            RDFHandler rdfHandler = Rio.createWriter(format, out);
            getManagedConnection().export(rdfHandler, getWorkingGraph());
        }

        addDistribution(tempFile, fileName, format, localizedLabel, zipFile, overwrite);

        // delete the old file (so, only the zip version remain)
        if (!tempFile.delete()) {
            logger.warn("Failed to delete file " + tempFile.getPath());
        }
    }

    /**
     * Generate a downloadable distribution containing the alignments of the current dataset
     * @param targetUriPrefix
     * @param mappingProperties
     * @param fileName
     * @param localizedLabel
     * @param format
     * @param zipFile
     * @param overwrite
     * @throws IOException
     * @throws NoSuchSettingsManager
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorized('pm(project, downloads)', 'C')")
    @Read
    public void createAlignmentDownload(@Optional String targetUriPrefix,
            @Optional(defaultValue = "") List<IRI> mappingProperties,
            String fileName, Literal localizedLabel,
            @Optional(defaultValue = "TURTLE") RDFFormat format,
            @Optional(defaultValue = "true") boolean zipFile,
            @Optional(defaultValue = "false") boolean overwrite) throws IOException, NoSuchSettingsManager {
        // check if the DOWNLOAD_DIR_NAME exist in the project folder, if not, create it
        checkAndInCaseCreateFolder();

        File tempFile = File.createTempFile(fileName, ".temp");

        GraphQuery gq = Alignment.prepareMappingsQuery(getManagedConnection(), getProject(), mappingProperties, targetUriPrefix);

        try (OutputStream out = new FileOutputStream(tempFile)) {
            RDFHandler rdfHandler = Rio.createWriter(format, out);
            gq.evaluate(rdfHandler);
        }

        addDistribution(tempFile, fileName, format, localizedLabel, zipFile, overwrite);

        // delete the old file (so, only the zip version remain)
        if (!tempFile.delete()) {
            logger.warn("Failed to delete file " + tempFile.getPath());
        }
    }

    /**
     * Add an external link download
     * @param externalLink
     * @param localizedLabel
     * @param distribution tells if the file represents a dataset distribution or simple file related to the dataset
     * @throws NoSuchSettingsManager
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorized('pm(project, downloads)', 'C')")
    public void createExternalDownload(IRI externalLink, Literal localizedLabel,
            @Optional(defaultValue = "true") boolean distribution,
            @Optional(defaultValue = "false") boolean overwrite)
            throws NoSuchSettingsManager, STPropertyAccessException, FileAlreadyExistsException {
        checkAndInCaseCreateFolder();

        DownloadProjectSettings downloadProjectSettings = getDownloadProjectSettings();
        if (downloadProjectSettings.fileNameToSingleDownloadMap.containsKey(externalLink.stringValue()) && !overwrite) {
            throw new FileAlreadyExistsException(externalLink.stringValue());
        }

        addSingleDownloadToStorage(externalLink.stringValue(), localizedLabel, null, DownloadType.external, distribution);
    }

    /**
     * Add a file to the downloadable files (not distribution)
     * @param file
     * @param localizedLabel
     * @param overwrite
     * @throws IOException
     * @throws NoSuchSettingsManager
     */
    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorized('pm(project, downloads)', 'C')")
    public void uploadFile(MultipartFile file, Literal localizedLabel,
            @Optional(defaultValue = "false") boolean distribution,
            @Optional(defaultValue = "false") boolean overwrite) throws IOException, NoSuchSettingsManager {
        checkAndInCaseCreateFolder();

        String fileName = file.getOriginalFilename();

        Reference distRef = parseReference(PROJ + "/" + DOWNLOAD_DIR_NAME + "/" + fileName);

        File distFile = StorageManager.getFile(distRef);
        if (distFile.exists() && !overwrite) {
            throw new FileAlreadyExistsException(distFile.getName());
        }
        //simply copy the input file to the distribution
        try (FileOutputStream fos = new FileOutputStream(distFile); BufferedOutputStream stream = new BufferedOutputStream(fos)) {
            byte[] bytes = file.getBytes();
            stream.write(bytes);
        }
        addSingleDownloadToStorage(fileName, localizedLabel, null, DownloadType.local, distribution);
    }

    @STServiceOperation
    public List<RDFFormat> getAvailableFormats() {
        List<RDFFormat> availableFormatsList = new ArrayList<>();

        // add the supported formats
        availableFormatsList.add(RDFFormat.RDFXML);
        availableFormatsList.add(RDFFormat.NTRIPLES);
        availableFormatsList.add(RDFFormat.N3);
        availableFormatsList.add(RDFFormat.NQUADS);

        return availableFormatsList;
    }

    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorized('pm(project, downloads)', 'D')")
    public void removeDownload(String fileName) throws IOException, NoSuchSettingsManager {

        // remove the file fileName
        String filePath = PROJ + "/" + DOWNLOAD_DIR_NAME + "/" + fileName;
        //Reference ref = parseReference(filePath);
        //StorageManager.deleteFile(ref);
        deleteFile(filePath);

        // update the YAML file
        removeSettingsEntries(Collections.singletonList(fileName));
    }


    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorized('pm(project, downloads)', 'D')")
    public void removeExternalDistribution(String distRef) throws NoSuchSettingsManager {
        removeSettingsEntries(Collections.singletonList(distRef));
    }


    @STServiceOperation
    public DownloadProjectSettings getDownloadInfoList() throws NoSuchSettingsManager {
        checkAndInCaseCreateFolder();

        //cleanup: delete entry in settings if the related file is not in download dir
        try {
            // get the list of the download
            List<String> fileList = new ArrayList<>();
            Reference ref = parseReference(PROJ + "/" + DOWNLOAD_DIR_NAME);
            Collection<DirectoryEntryInfo> directoryEntryInfoCollection = StorageManager.list(ref);
            for (DirectoryEntryInfo directoryEntryInfo : directoryEntryInfoCollection) {
                String fileName = directoryEntryInfo.getName();
                fileList.add(fileName);
            }

            DownloadProjectSettings downloadProjectSettings = getDownloadProjectSettings();
            List<String> filesToRemove = new ArrayList<>();
            for (Map.Entry<String, SingleDownload> entry: downloadProjectSettings.fileNameToSingleDownloadMap.entrySet()) {
                String fileName = entry.getKey();
                if (entry.getValue().type.equals(DownloadType.local) && !fileList.contains(fileName)) {
                    filesToRemove.add(fileName);
                }
            }
            if (!filesToRemove.isEmpty()) {
                removeSettingsEntries(filesToRemove);
            }
        } catch (STPropertyAccessException e) {
            throw new RuntimeException(e); // this should not happen
        }

        try {
            DownloadProjectSettings downloadProjectSettings = getDownloadProjectSettings();
            return downloadProjectSettings;
        } catch (STPropertyAccessException e) {
            throw new RuntimeException(e); // this should not happen
        }

    }

    /**
     * Downloads a file
     *
     * @param oRes     the response object to which the file will be written to
     * @param fileName the name of the file to download from this project
     * @return
     */
    @STServiceOperation
    public void getFile(HttpServletResponse oRes, String fileName) throws IOException {
        String filePath = PROJ + "/" + DOWNLOAD_DIR_NAME + "/" + fileName;
        Reference ref = parseReference(filePath);
        StorageManager.getFileContent(oRes.getOutputStream(), ref, oRes::setContentLength);
    }


    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorized('pm(project, downloads)', 'U')")
    public void updateLocalized(String fileName, String localized, String lang) throws NoSuchSettingsManager {
        Map<String, String> localizedMap = new HashMap<>();
        localizedMap.put(lang, localized);
        this.updateLocalizedMapInternal(fileName, localizedMap);
    }

    @STServiceOperation(method = RequestMethod.POST)
    @PreAuthorize("@auth.isAuthorized('pm(project, downloads)', 'U')")
    public void updateLocalizedMap(String fileName, Map<String, String> localizedMap) throws NoSuchSettingsManager {
        this.updateLocalizedMapInternal(fileName, localizedMap);
    }

    public void updateLocalizedMapInternal(String fileName, Map<String, String> localizedMap) throws NoSuchSettingsManager {
        try {
            DownloadProjectSettings downloadProjectSettings = getDownloadProjectSettings();
            if (!downloadProjectSettings.fileNameToSingleDownloadMap.containsKey(fileName)) {
                // the desired fileName does not exist in the config file, so return;
                return;
            }
            SingleDownload singleDownload = downloadProjectSettings.fileNameToSingleDownloadMap.get(fileName);
            singleDownload.langToLocalizedMap = localizedMap;

            ObjectMapper mapper = new ObjectMapper();
            ObjectNode objectNode = mapper.valueToTree(downloadProjectSettings);

            exptManager.storeSettings(DownloadSettingsManager.class.getName(), getProject(), null, null,
                    Scope.PROJECT, objectNode);
        } catch (STPropertyAccessException | STPropertyUpdateException | WrongPropertiesException e) {
            throw new RuntimeException(e); // this should not happen
        }
    }

    // this function check if there is the DOWNLOAD_DIR_NAME in the project folder, if, not, such directory is created
    private boolean checkAndInCaseCreateFolder() {
        Reference refDir = parseReference(PROJ + "/" + DOWNLOAD_DIR_NAME);
        return StorageManager.createDirectoryIfNotExisting(refDir);
    }

    private void deleteFile(String filePath) throws IOException {
        Reference ref = parseReference(filePath);
        StorageManager.deleteFile(ref);
    }

private void removeSettingsEntries(List<String> fileNames) throws NoSuchSettingsManager {
    try {
        DownloadProjectSettings downloadProjectSettings = getDownloadProjectSettings();
        for (String fileName: fileNames) {
            downloadProjectSettings.fileNameToSingleDownloadMap.remove(fileName);
        }
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode objectNode = mapper.valueToTree(downloadProjectSettings);

        exptManager.storeSettings(DownloadSettingsManager.class.getName(), getProject(), null, null,
                Scope.PROJECT, objectNode);
    } catch (STPropertyAccessException | STPropertyUpdateException | WrongPropertiesException e) {
        throw new RuntimeException(e); // this should not happen
    }
}



    /**
     *
     * @param fileToStore file to store into the distributions folder
     * @param fileName the name of the file to store
     * @param format the format of the distribution
     * @param localizedLabel localized label of the dist
     * @param zipFile tells if the distribution file needs to be stored as compressed zip
     * @param overwrite tells if the file can be overwritten in case already existing
     * @throws IOException
     * @throws NoSuchSettingsManager
     */
    private void addDistribution(File fileToStore, String fileName, RDFFormat format, Literal localizedLabel,
            boolean zipFile, boolean overwrite) throws IOException, NoSuchSettingsManager {

        String ext = zipFile ? ".zip" : "." + format.getDefaultFileExtension();
        String distFileName = fileName.endsWith(ext) ? fileName : fileName + ext;
        Reference distRef = parseReference(PROJ + "/" + DOWNLOAD_DIR_NAME + "/" + distFileName);

        File distFile = StorageManager.getFile(distRef);
        if (distFile.exists() && !overwrite) {
            throw new FileAlreadyExistsException(distFile.getName());
        }

        if (zipFile) {
            // zip the file
            try (OutputStream out = new FileOutputStream(distFile)) {
                try (ZipOutputStream zout = new ZipOutputStream(out)) {
                    //the entry in the zip will have the same file name, but with extension determined by the rdf format
                    ZipEntry ze = new ZipEntry(fileName + "." + format.getDefaultFileExtension());
                    zout.putNextEntry(ze);
                    try (FileInputStream fin = new FileInputStream(fileToStore)) {
                        byte[] buffer = new byte[4096];
                        for (int n; (n = fin.read(buffer)) > 0; ) {
                            zout.write(buffer, 0, n);
                        }
                    }
                }
            }
        } else {
            //simply copy the input file to the distribution
            Utilities.copy(fileToStore, distFile);
        }

        addSingleDownloadToStorage(distFile.getName(), localizedLabel, format.getName(), DownloadType.local, true);
    }

    /**
     * Create and store the SingleDownload entry to the storage DownloadSettingsManager
     * @param localizedLabel
     * @param fileName
     * @param format
     * @param distribution
     * @throws NoSuchSettingsManager
     */
    private void addSingleDownloadToStorage(String fileName, Literal localizedLabel, String format, DownloadType type, boolean distribution)
            throws NoSuchSettingsManager {
        try {
            DownloadProjectSettings downloadProjectSettings = getDownloadProjectSettings();

            Map<String, String> langToLocalizedMap = new HashMap<>();
            if (localizedLabel.getLabel().isEmpty() && localizedLabel.getLanguage().isEmpty()) {
                throw new IllegalArgumentException("Invalid localized label for distribution: " + localizedLabel.stringValue());
            }
            langToLocalizedMap.put(localizedLabel.getLanguage().get(), localizedLabel.getLabel());
            SingleDownload singleDownload = new SingleDownload();
            singleDownload.fileName = fileName;
            singleDownload.timestamp = new Date().getTime();
            singleDownload.langToLocalizedMap = langToLocalizedMap;
            singleDownload.format = format;
            singleDownload.type = type;
            singleDownload.distribution = distribution;

            downloadProjectSettings.fileNameToSingleDownloadMap.put(fileName, singleDownload);

            ObjectMapper mapper = new ObjectMapper();
            ObjectNode objectNode = mapper.valueToTree(downloadProjectSettings);

            exptManager.storeSettings(DownloadSettingsManager.class.getName(), getProject(), null, null,
                    Scope.PROJECT, objectNode);

        } catch (STPropertyAccessException | STPropertyUpdateException | WrongPropertiesException e) {
            throw new RuntimeException(e); // this should not happen
        }
    }


    /**
     * Retrieves and returns the DownloadProjectSettings. Ensures also that fileNameToSingleDownloadMap is initialized
     * (it could not be in case it has never been created)
     * @return
     * @throws NoSuchSettingsManager
     * @throws STPropertyAccessException
     */
    private DownloadProjectSettings getDownloadProjectSettings() throws NoSuchSettingsManager, STPropertyAccessException {
        DownloadProjectSettings downloadProjectSettings = (DownloadProjectSettings) exptManager.getSettings(getProject(),
                null, null, DownloadSettingsManager.class.getName(), Scope.PROJECT);
        if (downloadProjectSettings.fileNameToSingleDownloadMap == null) {
            downloadProjectSettings.fileNameToSingleDownloadMap = new HashMap<>();
        }
        return downloadProjectSettings;
    }

}
