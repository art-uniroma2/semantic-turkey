package it.uniroma2.art.semanticturkey.services.core;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class STCoreServicesPlugin extends STPlugin {
    public STCoreServicesPlugin(PluginWrapper wrapper) {
        super(wrapper, "it.uniroma2.art.semanticturkey/st-core-services", false);
    }

    @Override
    protected Class<?> getConfigurationClass() {
        return STCoreServicesConfig.class;
    }
}
