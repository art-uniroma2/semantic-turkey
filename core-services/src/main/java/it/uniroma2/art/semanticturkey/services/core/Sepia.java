package it.uniroma2.art.semanticturkey.services.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import it.uniroma2.art.semanticturkey.extension.ExtensionPointManager;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.properties.STPropertyUpdateException;
import it.uniroma2.art.semanticturkey.resources.Scope;
import it.uniroma2.art.semanticturkey.security.CspWhitelistService;
import it.uniroma2.art.semanticturkey.services.ExceptionFacet;
import it.uniroma2.art.semanticturkey.services.STServiceAdapter;
import it.uniroma2.art.semanticturkey.services.annotations.JsonSerialized;
import it.uniroma2.art.semanticturkey.services.annotations.Optional;
import it.uniroma2.art.semanticturkey.services.annotations.RequestMethod;
import it.uniroma2.art.semanticturkey.services.annotations.STService;
import it.uniroma2.art.semanticturkey.services.annotations.STServiceOperation;
import it.uniroma2.art.semanticturkey.settings.core.CoreSystemSettings;
import it.uniroma2.art.semanticturkey.settings.core.SemanticTurkeyCoreSettingsManager;
import it.uniroma2.art.semanticturkey.settings.core.SepiaSettings;
import it.uniroma2.art.semanticturkey.user.RoleCreationException;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@STService
public class Sepia extends STServiceAdapter {

    @Autowired
    private CspWhitelistService cspWhitelistService;

    @Autowired
    private ExtensionPointManager exptManager;

    @Autowired
    private SemanticTurkeyCoreSettingsManager coreSettingsManager;

    private static final Logger logger = LoggerFactory.getLogger(Sepia.class);

    @STServiceOperation
    public String getEndpointUrl() throws STPropertyAccessException {
        SepiaSettings settings = coreSettingsManager.getSystemSettings().sepiaSettings;
        if (settings != null) {
            return settings.endpointURL;
        } else {
            return null;
        }
    }

    @PreAuthorize("@auth.isAdmin()")
    @STServiceOperation
    public SepiaSettings getSettings() throws IOException, RoleCreationException, STPropertyAccessException {
        return coreSettingsManager.getSystemSettings().sepiaSettings;
    }

    @PreAuthorize("@auth.isAdmin()")
    @STServiceOperation(method = RequestMethod.POST)
    public void storeSettings(String endpointURL) throws IOException, RoleCreationException, STPropertyAccessException, STPropertyUpdateException {
        CoreSystemSettings sysSettings = coreSettingsManager.getSystemSettings();
        SepiaSettings settings = sysSettings.sepiaSettings;
        if (settings == null) {
            settings = new SepiaSettings();
        }
        settings.endpointURL = endpointURL;
        sysSettings.sepiaSettings = settings;
        coreSettingsManager.storeSettings(null, null, null, Scope.SYSTEM, sysSettings);
        cspWhitelistService.refreshWhitelist();
    }


    /**
     * Here below there are the APIs that execute requests to Sepia backend.
     * There is a mix of usage of RestTemplate and Apache CloseableHttpClient for executing GET, POST, PATCH and DELETE
     * In the future everything could be aligned to use CloseableHttpClient as it is done in
     * RemoteAlignmentServices.ClientTemplate
     */

    private final RestTemplate restTemplate;

    public Sepia(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @STServiceOperation
    public JsonNode listConfigurations(@Optional String extensionId, @Optional String extensionPointClass) throws STPropertyAccessException, JsonProcessingException {
        String url = getSepiaEndpointUrl() + "configurations";
        UriComponentsBuilder uriBuidler = UriComponentsBuilder.fromHttpUrl(url);
        if (extensionId != null) {
            uriBuidler = uriBuidler.queryParam("extensionId", extensionId);
        }
        if (extensionPointClass != null) {
            uriBuidler = uriBuidler.queryParam("extensionPointClass", extensionPointClass);
        }
        String urlWithParams = uriBuidler.toUriString();

        ResponseEntity<String> response = restTemplate.getForEntity(urlWithParams, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(response.getBody());
    }

    @STServiceOperation
    public JsonNode listDocsets() throws STPropertyAccessException, JsonProcessingException {
        String url = getSepiaEndpointUrl() + "docsets";
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(response.getBody());
    }

    @STServiceOperation
    public JsonNode findDocset(String id) throws STPropertyAccessException, JsonProcessingException {
        String url = getSepiaEndpointUrl() + "docsets/" + id;
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(response.getBody());
    }


    @STServiceOperation(method = RequestMethod.POST)
    public void createDocset(String name, String extensionId, String configurationId, List<String> contentIds) throws STPropertyAccessException, JsonProcessingException {
        String url = getSepiaEndpointUrl() + "docsets";
        // Create a custom map for serialization
        Map<String, Object> body = new HashMap<>();
        body.put("name", name);
        body.put("extensionId", extensionId);
        body.put("configurationId", configurationId);
        body.put("contentIds", new ArrayList<>(contentIds));

        // Convert the Map to a JSON string
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonBody = objectMapper.writeValueAsString(body);
        // Set headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        // Create the HTTP entity with the headers and body
        HttpEntity<String> requestEntity = new HttpEntity<>(jsonBody, headers);

        restTemplate.postForEntity(url, requestEntity, String.class);
    }

    @STServiceOperation(method = RequestMethod.POST)
    public void deleteDocset(String id) throws STPropertyAccessException {
        String url = getSepiaEndpointUrl() + "docsets/" + id;
        restTemplate.exchange(url, HttpMethod.DELETE, null, Void.class);
    }

    @STServiceOperation(method = RequestMethod.POST)
    public JsonNode updateDocset(@JsonSerialized ObjectNode docset) throws STPropertyAccessException, IOException, SepiaBackendException {
        String url = getSepiaEndpointUrl() + "docsets";

        HttpPatch httpPatch = new HttpPatch(url);
        httpPatch.setHeader("Content-Type", "application/json");
        httpPatch.setEntity(new StringEntity(docset.toString()));

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().useSystemProperties().build()) {
            HttpResponse response = httpClient.execute(httpPatch);
            String responseAsString = EntityUtils.toString(response.getEntity());

            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() / 100 != 2) {
                throw new SepiaBackendException(new ObjectMapper().readTree(responseAsString));
            } else {
                return new ObjectMapper().readTree(responseAsString);
            }
        }
    }

    @STServiceOperation
    public JsonNode hasPathFinderCapability() throws STPropertyAccessException, JsonProcessingException {
        String url = getSepiaEndpointUrl() + "docsets/hasPathFinderCapability";
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(response.getBody());
    }

    @STServiceOperation
    public JsonNode listFiles(String extensionId, String configurationId, @Optional String dir) throws STPropertyAccessException, JsonProcessingException {
        String url = getSepiaEndpointUrl() + "file-manager/files";
        UriComponentsBuilder uriBuidler = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("extensionId", extensionId)
                .queryParam("configurationId", configurationId);
        if (dir != null) {
            uriBuidler = uriBuidler.queryParam("dir", dir);
        }
        String urlWithParams = uriBuidler.toUriString();

        ResponseEntity<String> response = restTemplate.getForEntity(urlWithParams, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(response.getBody());
    }

    @STServiceOperation(method = RequestMethod.POST)
    public void createDir(String extensionId, String configurationId, String dir) throws STPropertyAccessException {
        String url = getSepiaEndpointUrl() + "file-manager/files/createDirectory";

        String urlWithParams = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("extensionId", extensionId)
                .queryParam("configurationId", configurationId)
                .queryParam("dir", dir)
                .toUriString();
        // Set headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        // Create the HTTP entity with the headers and body
        HttpEntity<String> requestEntity = new HttpEntity<>(headers);

        restTemplate.postForEntity(urlWithParams, requestEntity, String.class);
    }

    @STServiceOperation(method = RequestMethod.POST)
    public void deleteFileOrDirectory(String extensionId, String configurationId, String id) throws STPropertyAccessException {
        String url = getSepiaEndpointUrl() + "file-manager/files/" + id;
        String urlWithParams = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("extensionId", extensionId)
                .queryParam("configurationId", configurationId)
                .toUriString();
        restTemplate.exchange(urlWithParams, HttpMethod.DELETE, null, Void.class);
    }

    @STServiceOperation(method = RequestMethod.POST)
    public JsonNode createFile(String extensionId, String configurationId, String path, MultipartFile file, @Optional boolean overwrite) throws STPropertyAccessException, IOException, URISyntaxException, SepiaBackendException {
        String url = getSepiaEndpointUrl() + "file-manager/files";

        URIBuilder uriBuilder = new URIBuilder(url);
        HttpPost httpPost = new HttpPost(uriBuilder.build());

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        File tempFile = File.createTempFile("tempfile", ".tmp");
        file.transferTo(tempFile);
        FileBody fileBody = new FileBody(tempFile, ContentType.DEFAULT_BINARY);
        builder.addPart("file", fileBody);
        builder.addPart("extensionId", new StringBody(extensionId, ContentType.MULTIPART_FORM_DATA));
        builder.addPart("configurationId", new StringBody(configurationId, ContentType.MULTIPART_FORM_DATA));
        builder.addPart("path", new StringBody(path, ContentType.MULTIPART_FORM_DATA));
        if (overwrite) {
            builder.addPart("overwrite", new StringBody(String.valueOf(overwrite), ContentType.MULTIPART_FORM_DATA));
        }

        org.apache.http.HttpEntity entity = builder.build();
        httpPost.setEntity(entity);
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().useSystemProperties().build()) {
            HttpResponse response = httpClient.execute(httpPost);
            String responseAsString = EntityUtils.toString(response.getEntity());

            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() / 100 != 2) {
                throw new SepiaBackendException(new ObjectMapper().readTree(responseAsString));
            } else {
                return new ObjectMapper().readTree(responseAsString);
            }
        }
    }

    @STServiceOperation
    public JsonNode search(String text, String extensionId, String configurationId, String dir) throws STPropertyAccessException, JsonProcessingException {
        String url = getSepiaEndpointUrl() + "file-manager/files/search";
        UriComponentsBuilder uriBuidler = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("text", text)
                .queryParam("extensionId", extensionId)
                .queryParam("configurationId", configurationId)
                .queryParam("dir", dir);
        String urlWithParams = uriBuidler.toUriString();
        ResponseEntity<String> response = restTemplate.getForEntity(urlWithParams, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(response.getBody());
    }

    @STServiceOperation
    public JsonNode findPath(String id, String extensionId, String configurationId) throws STPropertyAccessException {
        String url = getSepiaEndpointUrl() + "file-manager/files/" + id + "/path";
        UriComponentsBuilder uriBuidler = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("extensionId", extensionId)
                .queryParam("configurationId", configurationId);
        String urlWithParams = uriBuidler.toUriString();
        ResponseEntity<String> response = restTemplate.getForEntity(urlWithParams, String.class);
        return JsonNodeFactory.instance.textNode(response.getBody());
    }

    private String getSepiaEndpointUrl() throws STPropertyAccessException {
        //no check on endpointURL defined, it is assumed that client calls the API only if endpoint is configured
        String url = coreSettingsManager.getSystemSettings().sepiaSettings.endpointURL;
        if (!url.endsWith("/")) {
            url += "/";
        }
        return url;
    }

    public static class SepiaBackendException extends Exception {
        private final JsonNode sepiaError;

        public SepiaBackendException(JsonNode sepiaError) {
            super("Sepia Backend error");
            this.sepiaError = sepiaError;
        }

        @ExceptionFacet("sepiaError")
        public JsonNode getSepiaError() {
            return sepiaError;
        }
    }

}
