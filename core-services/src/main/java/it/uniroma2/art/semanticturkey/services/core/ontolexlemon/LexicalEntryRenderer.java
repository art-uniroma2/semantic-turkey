package it.uniroma2.art.semanticturkey.services.core.ontolexlemon;

import it.uniroma2.art.semanticturkey.extension.extpts.rendering.RenderingEngineExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.impl.rendering.BaseRenderingEngine;
import it.uniroma2.art.semanticturkey.extension.impl.rendering.AbstractLabelBasedRenderingEngineConfiguration;

public class LexicalEntryRenderer extends BaseRenderingEngine {

	private static AbstractLabelBasedRenderingEngineConfiguration conf;

	static {
		conf = new AbstractLabelBasedRenderingEngineConfiguration() {

			@Override
			public String getShortName() {
				return "foo";
			}

		};
		conf.languages = null;
	}

	public LexicalEntryRenderer(RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
		super(conf, true, renderingEngineExtensionPoint);
	}

	public LexicalEntryRenderer(boolean fallbackToTerm, RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
		super(conf, fallbackToTerm, renderingEngineExtensionPoint);
	}

	@Override
	public void getGraphPatternInternal(StringBuilder gp) {
		gp.append(
				"?resource <http://www.w3.org/ns/lemon/ontolex#canonicalForm> [<http://www.w3.org/ns/lemon/ontolex#writtenRep> ?labelInternal ] .\n");
	}

}