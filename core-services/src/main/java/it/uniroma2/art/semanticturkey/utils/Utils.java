package it.uniroma2.art.semanticturkey.utils;

import it.uniroma2.art.semanticturkey.data.role.RDFResourceRole;
import it.uniroma2.art.semanticturkey.services.AnnotatedValue;
import it.uniroma2.art.semanticturkey.services.STServiceContext;
import it.uniroma2.art.semanticturkey.services.support.QueryBuilder;
import it.uniroma2.art.semanticturkey.tx.RDF4JRepositoryUtils;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import java.util.Collection;

public class Utils {

    public static RDFResourceRole typeof(STServiceContext stServiceContext,  Resource resource) {
        Repository repo = stServiceContext.getProject().getRepository();
        RepositoryConnection repoConn = RDF4JRepositoryUtils.getConnection(repo);
        try {
            QueryBuilder qb;
            StringBuilder sb = new StringBuilder();
            sb.append(
                    // @formatter:off
                    " SELECT ?resource WHERE {							\n" +
                            " BIND (?temp as ?resource)							\n" +
                            " } 												\n" +
                            " GROUP BY ?resource								\n"
                    // @formatter:on
            );
            qb = new QueryBuilder(stServiceContext, sb.toString());
            qb.setBinding("temp", resource);
            qb.processRole();
            Collection<AnnotatedValue<Resource>> res = qb.runQuery();
            Value role = res.iterator().next().getAttributes().get("role");
            return RDFResourceRole.valueOf(role.stringValue());
        } finally {
            RDF4JRepositoryUtils.releaseConnection(repoConn, repo);
        }
    }
}
