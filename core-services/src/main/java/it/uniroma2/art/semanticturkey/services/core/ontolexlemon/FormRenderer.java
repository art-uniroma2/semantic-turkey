package it.uniroma2.art.semanticturkey.services.core.ontolexlemon;

import it.uniroma2.art.semanticturkey.extension.extpts.rendering.RenderingEngineExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.impl.rendering.BaseRenderingEngine;
import it.uniroma2.art.semanticturkey.extension.impl.rendering.AbstractLabelBasedRenderingEngineConfiguration;
import jakarta.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

public class FormRenderer extends BaseRenderingEngine {

	private static AbstractLabelBasedRenderingEngineConfiguration conf;

	static {
		conf = new AbstractLabelBasedRenderingEngineConfiguration() {

			@Override
			public String getShortName() {
				return "foo";
			}

		};
		conf.languages = null;
	}

	public FormRenderer(boolean fallbackToTerm, RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
		super(conf, fallbackToTerm, renderingEngineExtensionPoint);
	}

	@Override
	public void getGraphPatternInternal(StringBuilder gp) {
		gp.append("?resource <http://www.w3.org/ns/lemon/ontolex#writtenRep> ?labelInternal .\n");
	}

}