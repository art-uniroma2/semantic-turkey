package it.uniroma2.art.semanticturkey.services.core.ontolexlemon;

import it.uniroma2.art.semanticturkey.extension.extpts.rendering.RenderingEngineExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.impl.rendering.BaseRenderingEngine;
import it.uniroma2.art.semanticturkey.extension.impl.rendering.AbstractLabelBasedRenderingEngineConfiguration;

public class LexiconRenderer extends BaseRenderingEngine {

	private static AbstractLabelBasedRenderingEngineConfiguration conf;

	static {
		conf = new AbstractLabelBasedRenderingEngineConfiguration() {

			@Override
			public String getShortName() {
				return "foo";
			}

		};
	}

	public LexiconRenderer(RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
		super(conf, true, renderingEngineExtensionPoint);
	}

	public LexiconRenderer(boolean fallbackToTerm, RenderingEngineExtensionPoint renderingEngineExtensionPoint) {
		super(conf, fallbackToTerm, renderingEngineExtensionPoint);
	}

	@Override
	public void getGraphPatternInternal(StringBuilder gp) {
		gp.append("?resource <http://purl.org/dc/terms/title> ?labelInternal .\n");
	}

}