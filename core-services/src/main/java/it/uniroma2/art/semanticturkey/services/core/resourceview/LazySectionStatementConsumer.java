package it.uniroma2.art.semanticturkey.services.core.resourceview;

import com.google.common.collect.ImmutableMap;
import it.uniroma2.art.semanticturkey.data.access.ResourcePosition;
import it.uniroma2.art.semanticturkey.project.Project;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import java.util.Map;
import java.util.Set;

/**
 * A {@link StatementConsumer} returning a {@link LazySection} that will be populated later using the result of a
 * service invocation
 */
public class LazySectionStatementConsumer implements StatementConsumer {
    private final String sectionName;

    public LazySectionStatementConsumer(String sectionName)  {
        this.sectionName = sectionName;
    }

    public String getSectionName() {
        return sectionName;
    }

    @Override
    public Map<String, ResourceViewSection> consumeStatements(Project project, RepositoryConnection repoConn, ResourcePosition resourcePosition, Resource resource, Model statements, Set<Statement> processedStatements, Resource workingGraph, Map<Resource, Map<String, Value>> resource2attributes, Map<IRI, Map<Resource, Literal>> predicate2resourceCreShow, Model propertyModel) {
        return ImmutableMap.of(sectionName, new LazySection());
    }
}
