package it.uniroma2.art.semanticturkey.services.core;

import it.uniroma2.art.lime.model.vocabulary.ONTOLEX;
import it.uniroma2.art.semanticturkey.constraints.LocallyDefined;
import it.uniroma2.art.semanticturkey.exceptions.SearchStatusException;
import it.uniroma2.art.semanticturkey.extension.extpts.search.SearchStrategy;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.search.SearchMode;
import it.uniroma2.art.semanticturkey.search.ServiceForSearches;
import it.uniroma2.art.semanticturkey.services.AnnotatedValue;
import it.uniroma2.art.semanticturkey.services.STServiceAdapter;
import it.uniroma2.art.semanticturkey.services.annotations.Optional;
import it.uniroma2.art.semanticturkey.services.annotations.Read;
import it.uniroma2.art.semanticturkey.services.annotations.STService;
import it.uniroma2.art.semanticturkey.services.annotations.STServiceOperation;
import it.uniroma2.art.semanticturkey.services.support.QueryBuilder;
import it.uniroma2.art.semanticturkey.vocabulary.XKOSFragment;
import jakarta.servlet.http.HttpServletResponse;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.model.vocabulary.SKOSXL;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.queryrender.RenderUtils;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@STService
public class XKOS extends STServiceAdapter {

    protected static Logger logger = LoggerFactory.getLogger(XKOS.class);


    /**
     * Returns the list of Correspondences (e.g. instances of the class xkos:Correspondence) and, optionally,
     * the number of elements that are associated to each Correspondence with the property xkos:madeOf
     * @param numAss true to get the number of elements in each Correspondence (using the property xkos:madeOf)
     * @return
     */
    @STServiceOperation
    @Read
    @PreAuthorize("@auth.isAuthorized('rdf(resource, alignment)', 'R')")
    public Collection<AnnotatedValue<Resource>> getCorrespondences(@Optional(defaultValue = "true") boolean numAss ) {

        // @formatter:off
        String query =
                " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>						\n" +
                        " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>					\n" +
                        " PREFIX owl: <http://www.w3.org/2002/07/owl#>							\n" +
                        " PREFIX skos: <http://www.w3.org/2004/02/skos/core#>					\n" +
                        " PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>					\n" +
                        " PREFIX xkos: <http://rdf-vocabulary.ddialliance.org/xkos#>            \n" +
                        " SELECT ?resource  ?attr_comparesA ?attr_comparesB ";
        if (numAss) {
            query += "(COUNT (?madeOfRes) AS ?attr_numAss) ";
        }
        query += generateNatureSPARQLSelectPart() + "									        \n" +
                        " WHERE {																\n "+
                "?resource a "+NTriplesUtil.toNTriplesString(XKOSFragment.CORRESPONDENCE)+" . \n";

        // manage the (optional) part about the number of xkos:madeOf
        if (numAss) {
            query += " OPTIONAL {?resource "+NTriplesUtil.toNTriplesString(XKOSFragment.MADEOF)+" ?madeOfRes . }    \n";
        }

        //manage the (maximum) two xkos:compares (so manage the case in which there are two or only one)
        query +="OPTIONAL {                                                                    \n" +
                "   ?resource  "+NTriplesUtil.toNTriplesString(XKOSFragment.COMPARES)+" ?attr_comparesA .   \n" +
                "   ?resource  "+NTriplesUtil.toNTriplesString(XKOSFragment.COMPARES)+" ?attr_comparesB .   \n" +
                "   FILTER( STR(?attr_comparesA) > STR(?attr_comparesB) ).                     \n" +
                "}                                                                             \n" +
                "OPTIONAL {                                                                    \n" +
                "   ?resource  "+NTriplesUtil.toNTriplesString(XKOSFragment.COMPARES)+" ?attr_comparesA .   \n" +
                "   FILTER NOT EXISTS {                                                        \n" +
                "       ?resource  "+NTriplesUtil.toNTriplesString(XKOSFragment.COMPARES)+" ?attr_comparesB .   \n" +
                "       FILTER( STR(?attr_comparesA) != STR(?attr_comparesB) )                 \n" +
                "   }                                                                          \n" +
                "}";



                query +=generateNatureSPARQLWherePart("?resource") +
                " }																				\n " +
                " GROUP BY ?resource ?attr_comparesA ?attr_comparesB							\n ";
        // @formatter:on

        QueryBuilder qb = createQueryBuilder(query);
        qb.processRendering();
        qb.processQName();

        //return qb.runQuery();

        Collection<AnnotatedValue<Resource>> resultValueCollection = qb.runQuery();
        //do some post-processing to see if the values of the two field "comparesA" and "comparesB" need to switched
        // or not (in "comparesA" there should be the local scheme, so a scheme starting with the current namespace )
        String namespace = getProject().getDefaultNamespace();
        String specialCaseScheme = namespace.endsWith("/") ? namespace.substring(0, namespace.length()-1) : namespace;

        for (AnnotatedValue<Resource> annotatedValue : resultValueCollection) {
            Map<String, Value> attributeMap = annotatedValue.getAttributes();
            Value comparesA = attributeMap.get("comparesA");
            Value comparesB = attributeMap.get("comparesB");
            //check if any of the two is null, in that case do nothing
            if (comparesA==null || comparesB==null) {
                //do nothing
                continue;
            }
            //check if "comparesA" and "comparesB" should be swapped
            if(isSwapIsNeeded(annotatedValue.getValue(), comparesB, XKOSFragment.TARGETCONCEPT)) {
                // do the swap
                attributeMap.put("comparesA", comparesB);
                attributeMap.put("comparesB", comparesA);
            }

            /*
            //OLD IMPLEMENTATION
            if (comparesA.stringValue().startsWith(namespace) || comparesA.stringValue().equals(specialCaseScheme)) {
                //the value in "comparesA" already seems to belong to the current project (it starts with the same
                // namespace, so do nothing)
                continue;
            } else if (comparesB.stringValue().startsWith(namespace) || comparesB.stringValue().equals(specialCaseScheme)) {
                // "comparesA" seems not to belong to the current project, while "comparesB" seems to belong to it,
                // so do the switch
                attributeMap.put("comparesA", comparesB);
                attributeMap.put("comparesB", comparesA);
            }
             */

        }

        return resultValueCollection;
    }

    private boolean isSwapIsNeeded(Resource correspondence, Value scheme, IRI prop) {
        if (!correspondence.isIRI() || !scheme.isIRI()) {
            //since either "correspondence" or "comparesB" are not IRI, just return false
            return false;
        }
        //do a SPARQL query to check if there is at least one "xkos:targetConcept" which is not in the scheme "comparesB".
        // If this is the case, then the swap between comparesA and comparesB should not be done, so return false
        String query = "SELECT ?concept\n" +
                "WHERE {\n"+
                NTriplesUtil.toNTriplesString(correspondence)+" " +NTriplesUtil.toNTriplesString(XKOSFragment.MADEOF)+" ?madeOf .\n" +
                "?madeOf " +NTriplesUtil.toNTriplesString(prop)+" ?concept .\n" +
                "FILTER NOT EXISTS {\n" +
                "?concept "+NTriplesUtil.toNTriplesString(SKOS.IN_SCHEME)+" "+NTriplesUtil.toNTriplesString(scheme)+" .\n"+
                "}\n"+
                "}\n" +
                "LIMIT 1";
        RepositoryConnection conn = getManagedConnection();
        TupleQuery tupleQuery = conn.prepareTupleQuery(query);
        try (TupleQueryResult tupleQueryResult = tupleQuery.evaluate()) {
            while (tupleQueryResult.hasNext()) {
                BindingSet bindingSet = tupleQueryResult.next();
                String concept = bindingSet.getValue("concept").stringValue();
                //since a concept was found that is the "prop" of the correspondence but is not defined as being
                // in the scheme comparesB, then it means that the xkos:targetConcept of the correspondence do not belong to the
                //scheme and so comparesB should be instead placed in comparesA (and viceversa)
                return true;
            }
        }
        //the SPARQL query did not return any value, so do the swap, return fasel
        return false;
    }


    /**
     * This returns the associations for a given correspondence. As associations, on average, hold just one concept per
     * side, the assConcepts parameter (defaulted to true) adds the concepts as annotations for the annotated value. As
     * an annotation could even hold hundreds of concepts (yet not usually), a certain syntax is used to
     * represent all URIs in the source/targetConcept properties. The syntax is:
     * <http:...> single URI
     * [<http:...>, <http:...>, <http:...>] for multiple URIs
     * {<http:...>, <http:...>, <http:...>} for multiple URIs where the number exceeds assConceptsLimit and thus it is clear
     *                                      that the list is incomplete (it contains only a number of URIs = assConceptsLimit)
     * so from the first bracket it is clear if there are multiple URIs.
     * @param correspondence
     * @param assConcepts
     * @param assConcepsLimit
     * @return
     */
    @STServiceOperation
    @Read
    @PreAuthorize("@auth.isAuthorized('rdf(resource, alignment)', 'R')")
    public Collection<AnnotatedValue<Resource>> getAssociations(@LocallyDefined IRI correspondence,
                              @Optional(defaultValue = "true") boolean assConcepts,
                              @Optional(defaultValue = "5") int assConcepsLimit) {
        // @formatter:off
        // first do a query to get all the  xkos:ConceptAssociation (resource in the query),
        // associated via the property xkos:madeOf to the input correspondence
        String query =
                " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>				\n" +
                " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>					\n" +
                " PREFIX owl: <http://www.w3.org/2002/07/owl#>							\n" +
                " PREFIX skos: <http://www.w3.org/2004/02/skos/core#>					\n" +
                " PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>					\n" +
                " PREFIX xkos: <http://rdf-vocabulary.ddialliance.org/xkos#>            \n" +
                " SELECT ?resource                                                      \n" +
                generateNatureSPARQLSelectPart() +
                " WHERE {                                                               \n" +
                " " + NTriplesUtil.toNTriplesString(correspondence) + " "+NTriplesUtil.toNTriplesString(XKOSFragment.MADEOF)+"  ?resource . \n" +
                " ?resource a "+NTriplesUtil.toNTriplesString(XKOSFragment.CONCEPTASSOCIATION)+" . \n" +
                " } GROUP BY ?resource ";
        // @formatter:on

        QueryBuilder qb = createQueryBuilder(query);
        qb.processRendering();
        qb.processQName();

        Collection<AnnotatedValue<Resource>> annotatedValueCollection = qb.runQuery();

        if (assConcepts) {
            //do another query, to get the various xkos:sourceConcept and xkos:targetConcept associated to the
            // retrieved ?resource (of the previous query) and add this information to the previous
            // annotatedValueCollection and then return the modified annotatedValueCollection

            addSourceAndTargetToConceptAssociation(correspondence, annotatedValueCollection, assConcepsLimit);
        }

        return annotatedValueCollection;

    }

    @STServiceOperation
    @Read
    @PreAuthorize("@auth.isAuthorized('rdf(resource, alignment)', 'R')")
    public Collection<AnnotatedValue<Resource>> getSingleAssociation(@LocallyDefined IRI association) {
        // @formatter:off
        // first do a query to get all the  xkos:ConceptAssociation (resource in the query),
        // associated via the property xkos:madeOf to the input correspondence
        String query =
                " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>						\n" +
                        " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>					\n" +
                        " PREFIX owl: <http://www.w3.org/2002/07/owl#>							\n" +
                        " PREFIX skos: <http://www.w3.org/2004/02/skos/core#>					\n" +
                        " PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>					\n" +
                        " PREFIX xkos: <http://rdf-vocabulary.ddialliance.org/xkos#>            \n" +
                        " SELECT ?resource                                                      \n" +
                        generateNatureSPARQLSelectPart() +
                        " WHERE {                                                               \n" +
                        " ?resource a "+NTriplesUtil.toNTriplesString(XKOSFragment.CONCEPTASSOCIATION)+" . \n" +
                        " FILTER(?resource = "+NTriplesUtil.toNTriplesString(association)+")    \n" +
                        " } GROUP BY ?resource ";
        // @formatter:on

        QueryBuilder qb = createQueryBuilder(query);
        qb.processRendering();
        qb.processQName();

        Collection<AnnotatedValue<Resource>> annotatedValueCollection = qb.runQuery();

        //do another query, to get the various xkos:sourceConcept and xkos:targetConcept associated to the
        // retrieved ?resource (of the previous email) and add this information to the previous
        // annotatedValueCollection and then return the modified annotatedValueCollection

        // @formatter:off
        query = " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>				\n" +
                " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>					\n" +
                " PREFIX owl: <http://www.w3.org/2002/07/owl#>							\n" +
                " PREFIX skos: <http://www.w3.org/2004/02/skos/core#>					\n" +
                " PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>					\n" +
                " PREFIX xkos: <http://rdf-vocabulary.ddialliance.org/xkos#>            \n" +
                " SELECT ?pred ?obj                                           \n" +
                " WHERE {                                                               \n" +
                " "+NTriplesUtil.toNTriplesString(association)+" a "+NTriplesUtil.toNTriplesString(XKOSFragment.CONCEPTASSOCIATION)+" . \n" +
                " "+NTriplesUtil.toNTriplesString(association)+" ?pred ?obj .                                                \n" +
                " }";
        // @formatter:on

        HashSet<IRI> sourceSet = new HashSet<>();
        HashSet<IRI> targetSet = new HashSet<>();

        RepositoryConnection conn = getManagedConnection();
        TupleQuery tupleQuery = conn.prepareTupleQuery(query);
        try (TupleQueryResult tupleQueryResult = tupleQuery.evaluate()) {
            while (tupleQueryResult.hasNext()) {
                BindingSet bindingSet = tupleQueryResult.next();
                Value predValue = bindingSet.getValue("pred");
                Value objValue = bindingSet.getValue("obj");
                if (!(predValue instanceof IRI) || !(objValue instanceof IRI)) {
                    //skip this result, since at least one of the three element is not an IRI
                    continue;
                }
                IRI predIRI = (IRI) predValue;
                IRI obj = (IRI) objValue;

                //check if the predIRI is either a xkos:source and xkos:targetConcept
                if (predIRI.equals(XKOSFragment.SOURCECONCEPT)) {
                    sourceSet.add(obj);
                } else if (predIRI.equals(XKOSFragment.TARGETCONCEPT)) {
                    targetSet.add(obj);
                }
            }
        }

        //iterate over the annotatedValueCollection to add the source/target values (in theory, only annotatedValue
        // should be present)

        for (AnnotatedValue<Resource> annotatedValue : annotatedValueCollection) {
            Value subjectValue = annotatedValue.getValue();
            if (!subjectValue.isIRI()) {
                // skip this value, since it is not an IRI
            }

            //get the set from the resourceToSourceSetMap
            if (!sourceSet.isEmpty()) {
                StringBuilder sourceStringList = new StringBuilder();
                if (sourceSet.size() == 1) {
                    sourceStringList = new StringBuilder(NTriplesUtil.toNTriplesString(sourceSet.iterator().next()));
                } else {
                    boolean first = true;
                    sourceStringList = new StringBuilder("[");
                    for (IRI sourceIri : sourceSet) {
                        if (!first) {
                            sourceStringList.append(", ");
                        }
                        first = false;
                        sourceStringList.append(NTriplesUtil.toNTriplesString(sourceIri));
                    }
                    sourceStringList.append("]");
                }
                String sourceConceptQName = XKOSFragment.PREFIX+":"+XKOSFragment.SOURCECONCEPT.getLocalName();
                annotatedValue.setAttribute(sourceConceptQName, sourceStringList.toString());
            }

            //get the set from the resourceToTargetSetMap
            if (!targetSet.isEmpty()) {
                StringBuilder targetStringList = new StringBuilder();
                if (targetSet.size() == 1) {
                    targetStringList = new StringBuilder(NTriplesUtil.toNTriplesString(targetSet.iterator().next()));
                } else {
                    boolean first = true;
                    targetStringList = new StringBuilder("[");
                    for (IRI sourceIri : targetSet) {
                        if (!first) {
                            targetStringList.append(", ");
                        }
                        first = false;
                        targetStringList.append(NTriplesUtil.toNTriplesString(sourceIri));
                    }
                    targetStringList.append("]");
                }
                String targetConceptQName = XKOSFragment.PREFIX+":"+XKOSFragment.TARGETCONCEPT.getLocalName();
                annotatedValue.setAttribute(targetConceptQName, targetStringList.toString());
            }
        }

        return annotatedValueCollection;
    }

    @STServiceOperation
    @Read
    @PreAuthorize("@auth.isAuthorized('rdf(resource, alignment)', 'R')")
    public void exportAssociations(HttpServletResponse oRes,
            @Optional IRI correspondence,
            @Optional(defaultValue = "TURTLE") RDFFormat format) throws IOException {
        String query = """
                PREFIX xkos: <http://rdf-vocabulary.ddialliance.org/xkos#>
                CONSTRUCT WHERE {
                   ?c a xkos:Correspondence .
                   ?c xkos:compares ?conceptScheme1 .
                   ?c xkos:compares ?conceptScheme2 .
                   ?c xkos:madeOf ?assoc .
                   ?assoc a xkos:ConceptAssociation .
                   ?assoc ?p ?o .
                   ?assoc xkos:sourceConcept ?source .
                   ?assoc xkos:targetConcept ?target .
                }
                """;
        GraphQuery gq = getManagedConnection().prepareGraphQuery(query);
        if (correspondence != null) {
            gq.setBinding("c", correspondence);
        }
        oRes.setContentType(format.getDefaultMIMEType());
        gq.evaluate(Rio.createWriter(format, oRes.getOutputStream()));

    }

    /**
     * This returns the associations for a given correspondence by filtering them according to the matching concept in
     * the xkos:sourceConcept
     * @param searchString
     * @param useLexicalizations
     * @param useLocalName
     * @param useURI
     * @param searchMode
     * @param useNotes
     * @param langs
     * @param includeLocales
     * @param searchInRDFSLabel
     * @param searchInSKOSLabel
     * @param searchInSKOSXLLabel
     * @param searchInOntolex
     * @return
     */
    @STServiceOperation
    @Read
    @PreAuthorize("@auth.isAuthorized('rdf(resource, alignment)', 'R')")
    public Collection<AnnotatedValue<Resource>> filterAssociations(
                                    @LocallyDefined IRI correspondence,
                                    @Optional(defaultValue = "true") boolean assConcepts,
                                    @Optional(defaultValue = "5") int assConcepsLimit,
                                    @Optional String searchString,
                                    @Optional(defaultValue="true") boolean useLexicalizations,
                                    @Optional(defaultValue="false") boolean useLocalName,
                                    @Optional(defaultValue="false") boolean useURI,
                                    SearchMode searchMode,
                                    @Optional(defaultValue="false") boolean useNotes,
                                    @Optional List<String> langs,
                                    //@Optional String sortByLang,
                                    @Optional(defaultValue="false") boolean includeLocales,
                                    @Optional(defaultValue="false") boolean searchInRDFSLabel,
                                    @Optional(defaultValue="false") boolean searchInSKOSLabel,
                                    @Optional(defaultValue="false") boolean searchInSKOSXLLabel,
                                    @Optional(defaultValue="false") boolean searchInOntolex) throws SearchStatusException, STPropertyAccessException {


        // first do a query to get all the  xkos:ConceptAssociation (resource in the query),
        // associated via the property xkos:madeOf to the input correspondence
        String query =
                " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>				\n" +
                        " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>					\n" +
                        " PREFIX owl: <http://www.w3.org/2002/07/owl#>							\n" +
                        " PREFIX skos: <http://www.w3.org/2004/02/skos/core#>					\n" +
                        " PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>					\n" +
                        " PREFIX xkos: <http://rdf-vocabulary.ddialliance.org/xkos#>            \n" +
                        " SELECT ?resource                                                      \n" +
                        generateNatureSPARQLSelectPart() +
                        " WHERE {                                                               \n" +
                        " " + NTriplesUtil.toNTriplesString(correspondence) + " "+NTriplesUtil.toNTriplesString(XKOSFragment.MADEOF)+"  ?resource . \n" +
                        " ?resource a "+NTriplesUtil.toNTriplesString(XKOSFragment.CONCEPTASSOCIATION)+" . \n" +
                        " } GROUP BY ?resource ";
        // @formatter:on

        QueryBuilder qb = createQueryBuilder(query);
        qb.processRendering();
        qb.processQName();

        Collection<AnnotatedValue<Resource>> associationsAnnotatedValueCollection = qb.runQuery();


        //do another SPARQL query to get the list of concepts that are xkos:sourceConcept of the associations of the
        // input correspondence and in this query use the various input to do the desired search
        Map<String, Set<String>> conceptToConceptassociationSetMap = new HashMap<>();
        Map<String, Set<String>> conceptassociationToConcepSetMap = new HashMap<>();

        IRI lexModel = getProject().getLexicalizationModel();

        StringBuilder customQueryPart = new StringBuilder();
        customQueryPart.append("\n").append(NTriplesUtil.toNTriplesString(correspondence)).append(" ")
                .append(NTriplesUtil.toNTriplesString(XKOSFragment.MADEOF)).append(" ?conceptAssociation .");
        customQueryPart.append("\n?conceptAssociation a ").append(NTriplesUtil.toNTriplesString(XKOSFragment.CONCEPTASSOCIATION)).append(" . ");
        customQueryPart.append("\n?conceptAssociation ").append(NTriplesUtil.toNTriplesString(XKOSFragment.SOURCECONCEPT)).append(" ?resource . ");
        customQueryPart.append("\nFILTER(isIRI(?resource))");

        //prepare the list of variable that needs to be added to the INNER SELECT, and GROUP BY
        List<String> varToAddToSelAndGroupList = new ArrayList<>();
        varToAddToSelAndGroupList.add("?conceptAssociation");

        //prepare the namespace map
        Map<String, String> prefixToNamespaceMap = getProject().getOntologyManager().getNSPrefixMappings(false);

        query = ServiceForSearches.getPrefixes() +
                "\nSELECT DISTINCT ?resource ?conceptAssociation" +
                "\nWHERE{" +
                "\n{";

        //use the searchInstancesOfClass to construct the first part of the query (the subquery)
        query += instantiateSearchStrategy().searchInstancesOfClass(stServiceContext, null, searchString, useLexicalizations,
                useLocalName, useURI, useNotes, searchMode, langs, includeLocales, true, true, lexModel,
                searchInRDFSLabel, searchInSKOSLabel, searchInSKOSXLLabel, searchInOntolex, null,
                SearchStrategy.StatusFilter.ANYTHING, null, null,
                customQueryPart.toString(), varToAddToSelAndGroupList, null, instantiateSearchStrategy(),
                getProject().getBaseURI(), prefixToNamespaceMap, false, false);

        /*
        if (sortByLang != null) {
            query += getComputeRenderingPattern("?resource", "?show") + "FILTER(LCASE(LANG(?show)) = '" + sortByLang + "') \n";
        }
         */

        query+="\n}";
        query+= "\n}";
        //query+= sortByLang != null ? "ORDER BY ASC(LCASE(?show)) \n" : "ORDER BY ASC(?s) \n";

        logger.debug("query = " + query);

        TupleQuery tq = getManagedConnection().prepareTupleQuery(query);
        tq.setIncludeInferred(false);
        try (TupleQueryResult tupleQueryResult = tq.evaluate()) {
            //iterate over the result to create a map of concept-conceptAssociation (a concept may be associated to multiple
            // conceptAssociations) and a map of conceptAssociation-concept (a conceptAssociation can have multiple concepts)

            while (tupleQueryResult.hasNext()) {
                BindingSet bindingSet = tupleQueryResult.next();
                String conceptAnnoation = bindingSet.getValue("conceptAssociation").stringValue();
                String concept = bindingSet.getValue("resource").stringValue();

                if (!conceptToConceptassociationSetMap.containsKey(concept)) {
                    conceptToConceptassociationSetMap.put(concept, new HashSet<>());
                }
                conceptToConceptassociationSetMap.get(concept).add(conceptAnnoation);

                if (!conceptassociationToConcepSetMap.containsKey(conceptAnnoation)) {
                    conceptassociationToConcepSetMap.put(conceptAnnoation, new HashSet<>());
                }
                conceptassociationToConcepSetMap.get(conceptAnnoation).add(concept);
            }
        }

        // iterate over the returned list xkos:ConceptAssociation (returned in the first query) and see if any of the
        // returned concepts (from the second query) are xkos:sourceConcept of these ConceptAssociation
        // (remove the ConceptAssociation that do not have any of the rerurned concepts)
        Collection<AnnotatedValue<Resource>> annotatedValueToRemove = new ArrayList<>();
        for (AnnotatedValue<Resource> annotatedValue : associationsAnnotatedValueCollection) {
            String conceptAssociation = annotatedValue.getValue().stringValue();
            if (!conceptassociationToConcepSetMap.containsKey(conceptAssociation)) {
                //this conceptAssociation was not returned in the second query, so t should be removed from associationsAnnotatedValueCollection
                annotatedValueToRemove.add(annotatedValue);
            }
        }
        //remove from associationsAnnotatedValueCollection the elements in annotatedValueToRemove
        associationsAnnotatedValueCollection.removeAll(annotatedValueToRemove);

        // do a final query, if assConcepts is true, to get the various xkos:sourceConcept and xkos:targetConcept associated to the
        // retrieved ?resource (of the first query) and add this information to the previous
        // associationsAnnotatedValueCollection and then return the modified associationsAnnotatedValueCollection
        if (assConcepts) {
            addSourceAndTargetToConceptAssociation(correspondence, associationsAnnotatedValueCollection, assConcepsLimit);
        }

        //return the list of Associations, after they have been filtered using the xkos:sourceConcept that matched
        // the input search
        return associationsAnnotatedValueCollection;
    }

    private void addSourceAndTargetToConceptAssociation(IRI correspondence,
                                                        Collection<AnnotatedValue<Resource>> annotatedValueCollection,
                                                        int assConcepsLimit) {
        // @formatter:off
        String query = " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>				\n" +
                " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>					\n" +
                " PREFIX owl: <http://www.w3.org/2002/07/owl#>							\n" +
                " PREFIX skos: <http://www.w3.org/2004/02/skos/core#>					\n" +
                " PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>					\n" +
                " PREFIX xkos: <http://rdf-vocabulary.ddialliance.org/xkos#>            \n" +
                " SELECT ?resource ?pred ?obj                                           \n" +
                " WHERE {                                                               \n" +
                " " + NTriplesUtil.toNTriplesString(correspondence) + " "+NTriplesUtil.toNTriplesString(XKOSFragment.MADEOF)+" ?resource . \n" +
                " ?resource a "+NTriplesUtil.toNTriplesString(XKOSFragment.CONCEPTASSOCIATION)+" . \n" +
                " ?resource ?pred ?obj .                                                \n" +
                " }";
        // @formatter:on

        HashMap<IRI, HashSet<IRI>> resourceToSourceSetMap = new HashMap<>();
        HashMap<IRI, HashSet<IRI>> resourceToTargetSetMap = new HashMap<>();

        RepositoryConnection conn = getManagedConnection();
        TupleQuery tupleQuery = conn.prepareTupleQuery(query);
        tupleQuery.setBinding("correspondence", correspondence);
        try (TupleQueryResult tupleQueryResult = tupleQuery.evaluate()) {
            while (tupleQueryResult.hasNext()) {
                BindingSet bindingSet = tupleQueryResult.next();
                Value resourceValue = bindingSet.getValue("resource");
                Value predValue = bindingSet.getValue("pred");
                Value objValue = bindingSet.getValue("obj");
                if (!(resourceValue instanceof IRI) || !(predValue instanceof IRI) ||
                        !(objValue instanceof IRI)) {
                    //skip this result, since at least one of the three element is not an IRI
                    continue;
                }
                IRI resourceIRI = (IRI) resourceValue;
                IRI predIRI = (IRI) predValue;
                IRI obj = (IRI) objValue;

                //check if the predIRI is either a xkos:source and xkos:targetConcept
                if (predIRI.equals(XKOSFragment.SOURCECONCEPT)) {
                    if (!resourceToSourceSetMap.containsKey(resourceIRI)) {
                        resourceToSourceSetMap.put(resourceIRI, new HashSet<>());
                    }
                    resourceToSourceSetMap.get(resourceIRI).add(obj);
                } else if (predIRI.equals(XKOSFragment.TARGETCONCEPT)) {
                    if (!resourceToTargetSetMap.containsKey(resourceIRI)) {
                        resourceToTargetSetMap.put(resourceIRI, new HashSet<>());
                    }
                    resourceToTargetSetMap.get(resourceIRI).add(obj);
                }
            }
        }

        //iterate over the annotatedValueCollection to add the source/target values

        for (AnnotatedValue<Resource> annotatedValue : annotatedValueCollection) {
            Value subjectValue = annotatedValue.getValue();
            if (!subjectValue.isIRI()) {
                // skip this value, since it is not an IRI
            }
            IRI subjectIRI = (IRI) subjectValue;

            //get the set from the resourceToSourceSetMap
            HashSet<IRI> sourceSet = resourceToSourceSetMap.get(subjectIRI);
            if (sourceSet != null) {
                StringBuilder sourceStringList = new StringBuilder();
                if (sourceSet.size() == 1) {
                    sourceStringList = new StringBuilder(NTriplesUtil.toNTriplesString(sourceSet.iterator().next()));
                } else if (sourceSet.size() > 1 && sourceSet.size() <= assConcepsLimit) {
                    boolean first = true;
                    sourceStringList = new StringBuilder("[");
                    for (IRI sourceIri : sourceSet) {
                        if (!first) {
                            sourceStringList.append(", ");
                        }
                        first = false;
                        sourceStringList.append(NTriplesUtil.toNTriplesString(sourceIri));
                    }
                    sourceStringList.append("]");
                } else if (sourceSet.size() > assConcepsLimit) {
                    int count = 0;
                    boolean first = true;
                    sourceStringList = new StringBuilder("{");
                    for (IRI sourceIri : sourceSet) {
                        if (++count > assConcepsLimit) {
                            break;
                        }
                        if (!first) {
                            sourceStringList.append(", ");
                        }
                        first = false;
                        sourceStringList.append(NTriplesUtil.toNTriplesString(sourceIri));
                    }
                    sourceStringList.append("}");
                }
                String sourceConceptQName = XKOSFragment.PREFIX+":"+XKOSFragment.SOURCECONCEPT.getLocalName();
                annotatedValue.setAttribute(sourceConceptQName, sourceStringList.toString());
            }

            //get the set from the resourceToTargetSetMap
            HashSet<IRI> targetSet = resourceToTargetSetMap.get(subjectIRI);
            if (targetSet != null) {
                StringBuilder targetStringList = new StringBuilder();
                if (targetSet.size() == 1) {
                    targetStringList = new StringBuilder(NTriplesUtil.toNTriplesString(targetSet.iterator().next()));
                } else if (targetSet.size() > 1 && targetSet.size() <= assConcepsLimit) {
                    boolean first = true;
                    targetStringList = new StringBuilder("[");
                    for (IRI sourceIri : targetSet) {
                        if (!first) {
                            targetStringList.append(", ");
                        }
                        first = false;
                        targetStringList.append(NTriplesUtil.toNTriplesString(sourceIri));
                    }
                    targetStringList.append("]");
                } else if (targetSet.size() > assConcepsLimit) {
                    int count = 0;
                    boolean first = true;
                    targetStringList = new StringBuilder("{");
                    for (IRI targetIri : targetSet) {
                        if (++count > assConcepsLimit) {
                            break;
                        }
                        if (!first) {
                            targetStringList.append(", ");
                        }
                        first = false;
                        targetStringList.append(NTriplesUtil.toNTriplesString(targetIri));
                    }
                    targetStringList.append("}");
                }
                String targetConceptQName = XKOSFragment.PREFIX+":"+XKOSFragment.TARGETCONCEPT.getLocalName();
                annotatedValue.setAttribute(targetConceptQName, targetStringList.toString());
            }
        }
    }


    /**
     * Returns the SPARQL pattern for computing the rendering of a resource
     * @param subjBinding binding of the subject resource to compute the redering (with leading ?, e.g. ?subject)
     * @param showBinding binding of the rendering literal value (with leading ?, e.g. ?show)
     * @return
     */
    //copied from the method, with the same name, in the service Alignment
    private String getComputeRenderingPattern(String subjBinding, String showBinding) {
        //patterns copied from several implementations of BaseRenderingEngine
        String pattern;
        IRI lexModel = getProject().getLexicalizationModel();
        if (lexModel.equals(Project.SKOS_LEXICALIZATION_MODEL)) {
            pattern = subjBinding + " " + RenderUtils.toSPARQL(SKOS.PREF_LABEL) + " " + showBinding + " . \n";
        } else if (lexModel.equals(Project.SKOSXL_LEXICALIZATION_MODEL)) {
            pattern = subjBinding + " (" + RenderUtils.toSPARQL(org.eclipse.rdf4j.model.vocabulary.SKOSXL.PREF_LABEL) + "/" +
                    RenderUtils.toSPARQL(org.eclipse.rdf4j.model.vocabulary.SKOSXL.LITERAL_FORM) + ") | " +
                    RenderUtils.toSPARQL(SKOSXL.LITERAL_FORM)  + showBinding + " . \n";
        } else if (lexModel.equals(Project.RDFS_LEXICALIZATION_MODEL)) {
            pattern = subjBinding + " " + RenderUtils.toSPARQL(RDFS.LABEL) + " " + showBinding + " . \n";
        } else { // if (lexModel.equals(Project.ONTOLEXLEMON_LEXICALIZATION_MODEL)) {
            pattern = "{ \n"+
                    "	{ " + subjBinding + " " + RenderUtils.toSPARQL(ONTOLEX.IS_DENOTED_BY) + " | ^" + RenderUtils.toSPARQL(ONTOLEX.DENOTES)+ " | " + RenderUtils.toSPARQL(ONTOLEX.IS_EVOKED_BY) + " | ^" + RenderUtils.toSPARQL(ONTOLEX.EVOKES) + " ?entry. } \n" +
                    " 	UNION \n" +
                    "	{ \n" +
                    "		?sense " + RenderUtils.toSPARQL(ONTOLEX.REFERENCE) + " | ^"+RenderUtils.toSPARQL(ONTOLEX.IS_REFERENCE_OF) + " | " +  RenderUtils.toSPARQL(ONTOLEX.IS_LEXICALIZED_SENSE_OF) + " | ^" + RenderUtils.toSPARQL(ONTOLEX.LEXICALIZED_SENSE) + " ?resource . \n" +
                    "		?sense " + RenderUtils.toSPARQL(ONTOLEX.IS_SENSE_OF) + " | ^"+RenderUtils.toSPARQL(ONTOLEX.SENSE) + "> ?entry . \n" +
                    "	} \n" +
                    "	?entry " + RenderUtils.toSPARQL(ONTOLEX.CANONICAL_FORM) + " [ "+RenderUtils.toSPARQL(ONTOLEX.WRITTEN_REP) + " ?labelInternal ] . \n" +
                    "} \n" +
                    "UNION \n" +
                    "{ "+ subjBinding + " " + RenderUtils.toSPARQL(DCTERMS.TITLE)+" " + showBinding + " . } \n" +
                    "UNION \n" +
                    "{ "+ subjBinding + " " + RenderUtils.toSPARQL(RDFS.LABEL) + " " + showBinding + " . } \n";
        }
        return pattern;
    }

}
