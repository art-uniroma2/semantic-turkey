package it.uniroma2.art.semanticturkey.properties;

import it.uniroma2.art.semanticturkey.resources.Scope;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation marks a property to tell that its default value should not be included in the form returned to the client
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface OmitDefault {
}
