package it.uniroma2.art.semanticturkey.pf4j;

import it.uniroma2.art.semanticturkey.event.Event;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebApplicationContext;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ResolvableType;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.function.Predicate.not;

public class STPluginWebApplicationContext extends AnnotationConfigServletWebApplicationContext {
    protected Set<Event> alreadySeenEvents = ConcurrentHashMap.newKeySet();

    @Override
    protected void publishEvent(Object event, ResolvableType eventType) {
        if (event instanceof Event stEvent) {
            boolean firstTimeSeen = false;
            try {
                firstTimeSeen = alreadySeenEvents.add(stEvent);
                if (firstTimeSeen) {  // ignores ST Events that have already been processed
                    super.publishEvent(event, eventType);
                }
            } finally {
                if (firstTimeSeen) {
                    alreadySeenEvents.remove(stEvent);
                }
            }
        } else {
            super.publishEvent(event, eventType);
        }
    }
}
