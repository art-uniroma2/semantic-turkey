package it.uniroma2.art.semanticturkey.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public abstract class GRAPHDB_CONFIG {

	/** The GraphDB Config namespace: http://www.ontotext.com/config/graphdb# */
	public static final String NAMESPACE = "http://www.ontotext.com/config/graphdb#";

	/**
	 * Recommended prefix for the GraphDB Config namespace: "graphdbconfig"
	 */
	public static final String PREFIX = "gdbcfg";

	/**
	 * An immutable {@link Namespace} constant that represents the GraphDB Config namespace.
	 */
	public static final Namespace NS = new SimpleNamespace(PREFIX, NAMESPACE);

	/** gdbcfg:base-URL */
	public static final IRI BASE_URL;

	/** gdbcfg:defaultNS */
	public static final IRI DEFAULT_NS;

	/** gdbcfg:entity-index-size */
	public static final IRI ENTITY_INDEX_SIZE;

	/** gdbcfg:entity-id-size */
	public static final IRI ENTITY_ID_SIZE;

	/** gdbcfg:imports */
	public static final IRI IMPORTS;

	/** gdbcfg:repository-type */
	public static final IRI REPOSITORY_TYPE;

	/** gdbcfg:ruleset */
	public static final IRI RULESET;

	/** gdbcfg:storage-folder */
	public static final IRI STORAGE_FOLDER;

	/** gdbcfg:enable-context-index */
	public static final IRI ENABLE_CONTEXT_INDEX;

	/** gdbcfg:enablePredicateList */
	public static final IRI ENABLE_PREDICATE_LIST;

	/** gdbcfg:in-memory-literal-properties */
	public static final IRI IN_MEMORY_LITERAL_PROPERTIES;

	/** gdbcfg:enable-literal-index */
	public static final IRI ENABLE_LITERAL_INDEX;

	/** gdbcfg:check-for-inconsistencies */
	public static final IRI CHECK_FOR_INCONSISTENCIES;

	/** gdbcfg:disable-sameAs */
	public static final IRI DISABLE_SAME_AS;

	/** gdbcfg:query-timeout */
	public static final IRI QUERY_TIMEOUT;

	/** gdbcfg:query-limit-results */
	public static final IRI QUERY_LIMIT_RESULTS;

	/** gdbcfg:throw-QueryEvaluationException-on-timeout */
	public static final IRI THROW_QUERY_EVALUATION_EXCEPTION_ON_TIMEOUT;

	/** gdbcfg:enable-fts-index */
	public static final IRI ENABLE_FTS_INDEX;

	/** gdbcfg:fts-indexes */
	public static final IRI FTS_INDEXES;

	/** gdbcfg:fts-iris-index */
	public static final IRI FTS_IRIS_INDEX;

	/** gdbcfg:fts-string-literals-index */
	public static final IRI FTS_STRING_LITERALS_INDEX;


	/** gdbcfg:read-only */
	public static final IRI READ_ONLY;

	static {
		ValueFactory vf = SimpleValueFactory.getInstance();

		BASE_URL = vf.createIRI(NAMESPACE, "base-URL");
		DEFAULT_NS = vf.createIRI(NAMESPACE, "defaultNS");
		ENTITY_INDEX_SIZE = vf.createIRI(NAMESPACE, "entity-index-size");
		ENTITY_ID_SIZE = vf.createIRI(NAMESPACE, "entity-id-size");
		IMPORTS = vf.createIRI(NAMESPACE, "imports");
		REPOSITORY_TYPE = vf.createIRI(NAMESPACE, "repository-type");
		RULESET = vf.createIRI(NAMESPACE, "ruleset");
		STORAGE_FOLDER = vf.createIRI(NAMESPACE, "storage-folder");
		ENABLE_CONTEXT_INDEX = vf.createIRI(NAMESPACE, "enable-context-index");
		ENABLE_PREDICATE_LIST = vf.createIRI(NAMESPACE, "enablePredicateList");
		IN_MEMORY_LITERAL_PROPERTIES = vf.createIRI(NAMESPACE, "in-memory-literal-properties");
		ENABLE_LITERAL_INDEX = vf.createIRI(NAMESPACE, "enable-literal-index");
		CHECK_FOR_INCONSISTENCIES = vf.createIRI(NAMESPACE, "check-for-inconsistencies");
		DISABLE_SAME_AS = vf.createIRI(NAMESPACE, "disable-sameAs");
		QUERY_TIMEOUT = vf.createIRI(NAMESPACE, "query-timeout");
		QUERY_LIMIT_RESULTS = vf.createIRI(NAMESPACE, "query-limit-results");
		THROW_QUERY_EVALUATION_EXCEPTION_ON_TIMEOUT = vf.createIRI(NAMESPACE,
				"throw-QueryEvaluationException-on-timeout");
		READ_ONLY = vf.createIRI(NAMESPACE, "read-only");
		ENABLE_FTS_INDEX = vf.createIRI(NAMESPACE, "enable-fts-index");
		FTS_INDEXES = vf.createIRI(NAMESPACE, "fts-indexes");
		FTS_IRIS_INDEX = vf.createIRI(NAMESPACE, "fts-iris-index");
		FTS_STRING_LITERALS_INDEX = vf.createIRI(NAMESPACE, "fts-string-literals-index");
	}
}
