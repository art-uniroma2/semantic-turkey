package it.uniroma2.art.semanticturkey.settings.facets;

import it.uniroma2.art.semanticturkey.exceptions.InvalidProjectNameException;
import it.uniroma2.art.semanticturkey.exceptions.ProjectAccessException;
import it.uniroma2.art.semanticturkey.project.ProjectInfo;
import it.uniroma2.art.semanticturkey.properties.PropertyNotFoundException;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.dynamic.DynamicSTProperties;
import it.uniroma2.art.semanticturkey.resources.Resources;
import it.uniroma2.art.semanticturkey.settings.facets.struct.FacetsResultStruct;
import it.uniroma2.art.semanticturkey.settings.facets.struct.ProjectWithFacetsForIndex;
import it.uniroma2.art.semanticturkey.settings.facets.struct.ProjectsAndFacetsResult;
import it.uniroma2.art.semanticturkey.settings.facets.struct.ProjectsResult;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.facet.DrillDownQuery;
import org.apache.lucene.facet.FacetField;
import org.apache.lucene.facet.FacetResult;
import org.apache.lucene.facet.Facets;
import org.apache.lucene.facet.FacetsCollector;
import org.apache.lucene.facet.FacetsCollectorManager;
import org.apache.lucene.facet.FacetsConfig;
import org.apache.lucene.facet.LabelAndValue;
import org.apache.lucene.facet.taxonomy.FastTaxonomyFacetCounts;
import org.apache.lucene.facet.taxonomy.TaxonomyReader;
import org.apache.lucene.facet.taxonomy.TaxonomyWriter;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyReader;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyWriter;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexUpgrader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.SegmentReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.MultiCollector;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.TopScoreDocCollectorManager;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ProjectFacetsIndexLuceneUtils {

    public static final int MAX_RESULT_QUERY_FACETS = 1000;
    public static final int MAX_RESULT_QUERY_PROJECTS = 3000;


    public static final String indexMainDir = "index";

    public static final String lucIndexDirName = "indexIndex";
    public static final String lucTaxoDirName = "taxoIndex";

    private static final String oldLucDirName = "facetsIndex"; // used only for the deletion of the folder

    private static final String lucGlobalContentIndexDirName = "globalContentIndex";
    private static final String notificationPrefIndexName = "notificationPrefIndex";
    private static final String userNotificationIndexName = "userNotificationIndex";



    //special project properties
    public static final String PROJECT_NAME = "prjName";
    public static final String PROJECT_MODEL = "prjModel";
    public static final String PROJECT_LEX_MODEL = "prjLexModel";
    public static final String PROJECT_HISTORY = "prjHistoryEnabled";
    public static final String PROJECT_VALIDATION = "prjValidationEnabled";
    public static final String PROJECT_DESCRIPTION = "prjDescription";


    public static boolean areLuceneDirsPresent() {
        File mainIndexDir = getMainIndexDir();
        File luceneIndexDir = new File(mainIndexDir, lucIndexDirName);
        if (!luceneIndexDir.exists()) {
            return false;
        }
        File luceneTaxoDir = new File(mainIndexDir, lucTaxoDirName);
        if (!luceneTaxoDir.exists()) {
            return false;
        }
        return true;
    }

    public static boolean isGlobalContentLuceneDirPresent() {
        File mainIndexDir = getMainIndexDir();
        File luceneIndexDir = new File(mainIndexDir, lucGlobalContentIndexDirName);
        if (!luceneIndexDir.exists()) {
            return false;
        }
        return true;
    }

    public static boolean isNotificationPrefIndexDirPresent() {
        File mainIndexDir = getMainIndexDir();
        File luceneIndexDir = new File(mainIndexDir, notificationPrefIndexName);
        if (!luceneIndexDir.exists()) {
            return false;
        }
        return true;
    }

    public static boolean isUserNotificationIndexPresent() {
        File mainIndexDir = getMainIndexDir();
        File luceneIndexDir = new File(mainIndexDir, userNotificationIndexName);
        if (!luceneIndexDir.exists()) {
            return false;
        }
        return true;
    }


    //method to create the indexes (2 distinct indexes, one for the search and one for the facets related functionalities)
    // for all the input projects
    public static void createFacetIndexAPI(List<ProjectInfo> projectInfoList) throws IOException, ProjectAccessException,
            InvalidProjectNameException {
        try (TaxonomyWriter taxonomyWriter = getTaxoWriter();
             IndexWriter indexWriter = getIndexWriter(); ) {
            for (ProjectInfo projectInfo : projectInfoList) {
                addProjectToIndex(projectInfo, true, indexWriter, taxonomyWriter);
            }
        }
    }

    public static void createFacetIndexAPI(ProjectInfo projectInfo) throws IOException, ProjectAccessException,
            InvalidProjectNameException {
        List<ProjectInfo> projectInfoList = new ArrayList<>();
        projectInfoList.add(projectInfo);
        createFacetIndexAPI(projectInfoList);

    }

    //method to add a NEW project or an existing one to the indexes (before it clears the index for the existing project)
    private static void addProjectToIndex(ProjectInfo projectInfo, boolean removePrevIndex,
                                          IndexWriter indexWriter, TaxonomyWriter taxonomyWriter) throws InvalidProjectNameException,
            ProjectAccessException, IOException {
        // remove the previous entry, if needed
        if (removePrevIndex) {
            removeProjectFromIndex(projectInfo.getName(), indexWriter);
        }
        //add the facets from the projectInfo (and the special value as well as facets)
        ProjectWithFacetsForIndex projectWithFacetsForIndex = new ProjectWithFacetsForIndex();

        projectWithFacetsForIndex.addFacetAndValue(PROJECT_NAME, projectInfo.getName());
        projectWithFacetsForIndex.addFacetAndValue(PROJECT_MODEL, projectInfo.getModel());
        projectWithFacetsForIndex.addFacetAndValue(PROJECT_LEX_MODEL, projectInfo.getLexicalizationModel());
        projectWithFacetsForIndex.addFacetAndValue(PROJECT_HISTORY, Boolean.toString(projectInfo.isHistoryEnabled()));
        projectWithFacetsForIndex.addFacetAndValue(PROJECT_VALIDATION, Boolean.toString(projectInfo.isValidationEnabled()));
        projectWithFacetsForIndex.addFacetAndValue(PROJECT_DESCRIPTION, projectInfo.getDescription());
        //iterate over the (real) facets of the project
        ProjectFacets projectFacets = projectInfo.getFacets();
        for (String facetName : projectFacets.getProperties()) {
            try {
                Object value = projectFacets.getPropertyValue(facetName);
                if (value instanceof DynamicSTProperties) {
                    //this is to manage the case for the non default facets (i.e. the ones created by the users/administrators),
                    // so the customFacets
                    getValuesFromSTProperties((STProperties) value, projectWithFacetsForIndex);
                } else if (value != null) {
                    List<String> facetValueList = normalizeFacetValue(value);
                    projectWithFacetsForIndex.addFacetAndValueList(facetName, facetValueList);
                }
            } catch (PropertyNotFoundException e) {
                // the facet was not found, so skip it and pass to the next one
            }
        }

        // add projectWithFacetsForIndex in the indexes
        addProjectToIndex(projectWithFacetsForIndex, indexWriter, taxonomyWriter);

    }

    public static void removeProjectFromIndex(String projectName) throws IOException {
        try (IndexWriter indexWriter = getIndexWriter() ) {
            //remove from the index the document having as PROJECT_NAME the name of the project
            indexWriter.deleteDocuments(new Term(PROJECT_NAME, projectName));
        }
    }

    private static void removeProjectFromIndex(String projectName, IndexWriter indexWriter) throws IOException {
        //remove from the index the document having as PROJECT_NAME the name of the project
        indexWriter.deleteDocuments(new Term(PROJECT_NAME, projectName));
    }

    public static void deleteFacetIndexesFolders(){
        //delete both indexed folders (useful when the version of Lucene has changed so the old indexes are no longer
        // valid)
        File luceneIndexDirFile = getLuceneFacetIndexDir();
        if(luceneIndexDirFile.exists()) {
            deleteDirOrFile(luceneIndexDirFile);
        }
        File taxoIndexDirFile = getLuceneTaxoDir();
        if(taxoIndexDirFile.exists()) {
            deleteDirOrFile(taxoIndexDirFile);
        }
        File oldLucDirFile = getOldLucDir();
        if(oldLucDirFile.exists()) {
            deleteDirOrFile(oldLucDirFile);
        }
    }

    public static void deleteGlobalSearchIndexFolder() {
        //delete the index folder regarding the Global Search
        File luceneIndexDirFile = getLuceneGlobalAccessDir();
        if(luceneIndexDirFile.exists()) {
            deleteDirOrFile(luceneIndexDirFile);
        }
    }

    /**
     * check if the Notification Indexes can be upgraded to the current version of Lucene
     * @return true if the upgrade was possible, false if it was not possible and so they were just deleted
     */
    public static boolean upgradeOrDeleteNotificationIndexes() {
        // check if both indexed are readable. If they are readable, then do the upgrade. If they are not readable,
        // due to an IndexFormatTooOldException, delete both lucene folders
        boolean indexesReadable = true;

        if (isNotificationPrefIndexDirPresent()) {
            //since the folder is present, check if they are readable
            indexesReadable = isNotificationPrefIndexAccessible();
        }
        if (isUserNotificationIndexPresent()) {
            //since the folder is present, check if the index is readable
            indexesReadable =  indexesReadable && isUserNotificationIndexAccessible();
        }

        if ((indexesReadable)) {
            //both indexes are readable, so do the update
            if (!upgradeNotificationIndexesFolders()) {
                //there was a problem, so delete the folders
                deleteNotificationIndexesFolders();
            }
            return true;
        } else {
            //at least one of the due indexes is not readable, so delete the folders
            deleteNotificationIndexesFolders();
            return false;
        }
    }

    private static boolean isNotificationPrefIndexAccessible() {
        try {
            try (Directory directory = FSDirectory.open(getNotificationPrefIndexDir().toPath());
                DirectoryReader ignored = DirectoryReader.open(directory)) {
                // do nothing, this was done just to check if the index is accessible
            }
        } catch (IOException exception) {
            //there was a problem, so return false;
            return false;
        }
        return true;
    }

    private static boolean isUserNotificationIndexAccessible() {
        try {
            try (Directory directory = FSDirectory.open(getUserNotificationIndexDir().toPath());
                DirectoryReader ignored = DirectoryReader.open(directory)) {
                // do nothing, this was done just to check if the index is accessible
            }
        } catch (IOException exception) {
            //there was a problem, so return false;
            return false;
        }
        return true;
    }

    private static boolean upgradeNotificationIndexesFolders() {

        try {
            //do the update for the NotificationPrefIndex

            // Open the directory where the index is stored
            try (Directory directory = FSDirectory.open(getNotificationPrefIndexDir().toPath())) {
                // Create an IndexUpgrader instance
                IndexUpgrader indexUpgrader = new IndexUpgrader(directory);
                // Perform the upgrade
                indexUpgrader.upgrade();
            }
        } catch (IOException e) {
            return false;
        }

        try {
            //do the update for the NotificationPrefIndex

            // Open the directory where the index is stored
            try(Directory directory = FSDirectory.open((getUserNotificationIndexDir().toPath()))) {
                // Create an IndexUpgrader instance
                IndexUpgrader indexUpgrader = new IndexUpgrader(directory);
                // Perform the upgrade
                indexUpgrader.upgrade();
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    private static void deleteNotificationIndexesFolders() {
        //delete the indexes folders regarding the Notification
        File dirToDelete = getNotificationPrefIndexDir();
        if (dirToDelete.exists()) {
            deleteDirOrFile(dirToDelete);
        }
        dirToDelete = getUserNotificationIndexDir();
        if (dirToDelete.exists()) {
            deleteDirOrFile(dirToDelete);
        }
    }

    public static String getCurrentLuceneVersionNotificationIndex() throws IOException {
        String version = "";
        if (isNotificationPrefIndexDirPresent()) {
            try (IndexReader indexReader = createNotificationPrefIndexReader()) {
                for (LeafReaderContext leaf : indexReader.leaves()) {
                    SegmentReader segmentReader = (SegmentReader) leaf.reader();
                    version = segmentReader.getSegmentInfo().info.getVersion().toString();
                    break;
                }
            }
        } else if (isUserNotificationIndexPresent()) {
            try (IndexReader indexReader = createUserNotificationIndexReader()) {
                for (LeafReaderContext leaf : indexReader.leaves()) {
                    SegmentReader segmentReader = (SegmentReader) leaf.reader();
                    version = segmentReader.getSegmentInfo().info.getVersion().toString();
                    break;
                }
            }
        }
        return version;
    }

    private static void addProjectToIndex(ProjectWithFacetsForIndex projectWithFacetsForIndex, IndexWriter indexWriter,
                                          TaxonomyWriter taxoWriter) throws IOException {
        FacetsConfig facetsConfig = new FacetsConfig();
        //add the facets to both indexes (lucIndexDirName and taxoIndex)
        Document document = new Document();
        Map<String, List<String>> facetNameToFaceValueListMap = projectWithFacetsForIndex.getFacetNameToFaceValueListMap();
        for (String facetName : facetNameToFaceValueListMap.keySet()) {
            facetsConfig.setMultiValued(facetName, true); // To have the possibility that that facet is multi value
            for (String facetValue : facetNameToFaceValueListMap.get(facetName)) {
                if (facetValue!=null && !facetValue.isEmpty()) {
                    document.add(new FacetField(facetName, facetValue));
                    document.add(new StringField(facetName, facetValue, Field.Store.YES));
                }
            }
        }
        indexWriter.addDocument(facetsConfig.build(taxoWriter, document));
     }

    public static FacetsResultStruct getAllFacets(boolean excludeProjNameAndProjDesc) throws IOException {
        FacetsResultStruct facetsResultStruct = new FacetsResultStruct();
        try (IndexReader indexReader = createIndexReader();
             TaxonomyReader taxonomyReader = createTaxoReader()) {

            IndexSearcher searcher = new IndexSearcher(indexReader);
            // MatchAllDocsQuery is for "browsing" (counts facets for all non-deleted docs in the index); normally
            // you'd use a "normal" query:
            FacetsCollector fc = searcher.search(new MatchAllDocsQuery(), new FacetsCollectorManager());

            FacetsConfig facetsConfig = new FacetsConfig(); // CHECK
            Facets facets = new FastTaxonomyFacetCounts(taxonomyReader, facetsConfig, fc);

            List<FacetResult> facetResultsList = facets.getAllDims(MAX_RESULT_QUERY_FACETS);
            for (FacetResult facetResult : facetResultsList) {
                for (LabelAndValue labelAndValue : facetResult.labelValues) {
                    if (!excludeProjNameAndProjDesc || (!facetResult.dim.equals(PROJECT_NAME) && !facetResult.dim.equals(PROJECT_DESCRIPTION))) {
                        //the excludeProjNameAndProjDesc is false (so ALL dim will be considered) or excludeProjNameAndProjDesc is true
                        // so dim should not be either PROJECT_NAME or PROJECT_DESCRIPTION
                        facetsResultStruct.addFacetValueCount(facetResult.dim, labelAndValue.label, labelAndValue.value.intValue());
                    }
                }
            }
        }
        return facetsResultStruct;
    }

    public static ProjectsResult getProjectListFromIndex() throws IOException {
        ProjectsResult projectsResult = new ProjectsResult();
        try (IndexReader indexReader = createIndexReader();
             TaxonomyReader taxonomyReader = createTaxoReader()) {

            IndexSearcher searcher = new IndexSearcher(indexReader);
            TopDocs topDocs = searcher.search(new MatchAllDocsQuery(), MAX_RESULT_QUERY_PROJECTS);

            ScoreDoc[] scoreDocs = topDocs.scoreDocs;
            for (int i=0; i<scoreDocs.length; ++i) {
                Document document = searcher.doc(scoreDocs[i].doc);
                String name = document.get(PROJECT_NAME);
                projectsResult.addProjectName(name);
            }
        }
        return projectsResult;
    }

    public static ProjectsAndFacetsResult search(Map<String, List<String>> facetToValueListMap) throws IOException {
        ProjectsAndFacetsResult projectWithFacetsForIndex;
        try (IndexReader indexReader = createIndexReader();
             TaxonomyReader taxonomyReader = createTaxoReader()) {
            FacetsConfig facetsConfig = new FacetsConfig(); // CHECK
            DrillDownQuery drillDownQuery = new DrillDownQuery(facetsConfig);
            //the search is done by using the info of the input map. Each key represent a facetName and the value of the
            // map are the value of the facet that are placed in OR, for the same facet, and in AND for the other facet
            for (String facetName : facetToValueListMap.keySet()) {
                List<String> facetValueList = facetToValueListMap.get(facetName);
                for (String facetValue : facetValueList) {
                    //add the various OR part for the current facetName
                    drillDownQuery.add(facetName, facetValue);
                }
            }
            //now execute the query and then collect the results and the "remaning" facets

            // Create a facets collector
            FacetsCollector facetsCollector = new FacetsCollector();

            // Create a top score doc collector
            TopScoreDocCollectorManager topScoreDocCollectorManager =
                    new TopScoreDocCollectorManager(MAX_RESULT_QUERY_PROJECTS, MAX_RESULT_QUERY_PROJECTS);
            TopScoreDocCollector topScoreDocCollector = topScoreDocCollectorManager.newCollector();

            // Create a multi collector (for the index topScoreDocCollector and the facetsCollector for the facets)
            Collector collector = MultiCollector.wrap(topScoreDocCollector, facetsCollector);

            // Search for the query and collect facets
            IndexSearcher searcher = new IndexSearcher(indexReader);
            searcher.search(drillDownQuery, collector);


            // Get the search results
            TopDocs topDocs = topScoreDocCollector.topDocs();
            //iterate over the topDocs to get the results (the projects name)
            ProjectsResult projectsResult = new ProjectsResult();
            ScoreDoc[] scoreDocs = topDocs.scoreDocs;
            for (int i=0; i<scoreDocs.length; ++i) {
                Document document = searcher.doc(scoreDocs[i].doc);
                String name = document.get(PROJECT_NAME);
                projectsResult.addProjectName(name);
            }

            // Get the facets results
            FacetsResultStruct facetsResultStruct = new FacetsResultStruct();
            Facets facets = new FastTaxonomyFacetCounts(taxonomyReader, facetsConfig, facetsCollector);
            List<FacetResult> facetResultsList = facets.getAllDims(MAX_RESULT_QUERY_FACETS);
            for (FacetResult facetResult : facetResultsList) {
                for (LabelAndValue labelAndValue : facetResult.labelValues) {
                    facetsResultStruct.addFacetValueCount(facetResult.dim, labelAndValue.label, labelAndValue.value.intValue());
                }
            }

            //add projectsResult and  to projectsResult
            projectWithFacetsForIndex = new ProjectsAndFacetsResult(facetsResultStruct, projectsResult);

        }
        return projectWithFacetsForIndex;
    }


    public static Optional<Document> getDocumentForProject(String name) throws IOException {
        ClassLoader oldCtxClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(IndexWriter.class.getClassLoader());
        try {

            BooleanQuery.Builder builderBoolean = new BooleanQuery.Builder();
            builderBoolean.add(new TermQuery(new Term(PROJECT_NAME, name)), BooleanClause.Occur.MUST);

            try (IndexReader indexReader = createIndexReader()) {
                IndexSearcher searcher = new IndexSearcher(indexReader);
                TopDocs topDocs = searcher.search(builderBoolean.build(), 1);
                ScoreDoc[] scoreDocs = topDocs.scoreDocs;

                if (scoreDocs == null || scoreDocs.length == 0) {
                    return Optional.empty();
                } else {
                    return Optional.of(searcher.doc(scoreDocs[0].doc));
                }
            }
        } finally {
            Thread.currentThread().setContextClassLoader(oldCtxClassLoader);
        }
    }


    /*UTILITIES methods*/
    public static List<String> normalizeFacetValue(Object value) {
        ArrayList<String> valueList = new ArrayList<>();
        //this service method returns a list of String since a facet can have multiple values
        if (value == null) {
            return valueList;
        }
        if (value instanceof Collection<?>) {
            //it has multiple values, so normalize each single value
            for (Object singleValue : (Collection) value) {
                valueList.add(normalizeFacetSingleValue(singleValue));
            }
        } else {
            valueList.add(normalizeFacetSingleValue(value));
        }
        return valueList;
    }

    public static String normalizeFacetSingleValue(Object value) {
        if (value instanceof IRI) {
            return NTriplesUtil.toNTriplesString((IRI) value);
        } else if (value instanceof Literal) {
            return NTriplesUtil.toNTriplesString((Literal) value);
        } else {
            return value.toString();
        }
    }


    private static File getLuceneFacetIndexDir() {
        File mainIndexDir = getMainIndexDir();
        File luceneIndexDir = new File(mainIndexDir, lucIndexDirName);
        if (!luceneIndexDir.exists()) {
            luceneIndexDir.mkdir();
        }

        return luceneIndexDir;
    }

    private static File getLuceneTaxoDir() {
        File mainIndexDir = getMainIndexDir();
        File luceneIndexDir = new File(mainIndexDir, lucTaxoDirName);
        if (!luceneIndexDir.exists()) {
            luceneIndexDir.mkdir();
        }

        return luceneIndexDir;
    }

    private static File getOldLucDir() {
        // this method should be called only in deleteFacetIndexesFolders
        File mainIndexDir = getMainIndexDir();
        File luceneIndexDir = new File(mainIndexDir, oldLucDirName);
        if (!luceneIndexDir.exists()) {
            luceneIndexDir.mkdir();
        }

        return luceneIndexDir;
    }

    // this method is also used by the services GlobalSearch and this is why it is public
    public static File getLuceneGlobalAccessDir() {
        File mainIndexDir = getMainIndexDir();
        File luceneIndexDir = new File(mainIndexDir, lucGlobalContentIndexDirName);
        if(!luceneIndexDir.exists()) {
            luceneIndexDir.mkdir();
        }
        return luceneIndexDir;
    }


    // this method is also used by the services NotificationPreferencesAPI and this is why it is public
    public static File getNotificationPrefIndexDir() {
        File mainIndexDir = getMainIndexDir();
        File luceneIndexDir = new File(mainIndexDir, notificationPrefIndexName);
        if(!luceneIndexDir.exists()) {
            luceneIndexDir.mkdir();
        }
        return luceneIndexDir;
    }

    // this method is also used by the services UserNotificationsAPI and this is why it is public
    public static File getUserNotificationIndexDir() {
        File mainIndexDir = getMainIndexDir();
        File luceneIndexDir = new File(mainIndexDir, userNotificationIndexName);
        if(!luceneIndexDir.exists()) {
            luceneIndexDir.mkdir();
        }
        return luceneIndexDir;
    }

    private static IndexReader createNotificationPrefIndexReader() throws IOException {
        Directory directory = FSDirectory.open(getNotificationPrefIndexDir().toPath());
        return DirectoryReader.open(directory);
    }

    private static IndexReader createUserNotificationIndexReader() throws IOException {
        Directory directory = FSDirectory.open(getUserNotificationIndexDir().toPath());
        return DirectoryReader.open(directory);
    }

    private static  File getMainIndexDir() {
        String mainIndexPath = Resources.getSemTurkeyDataDir() + File.separator + indexMainDir;
        File mainIndexDir = new File(mainIndexPath);
        if (!mainIndexDir.exists()) {
            mainIndexDir.mkdir();
        }
        return mainIndexDir;
    }

    private static IndexWriter getIndexWriter() throws IOException {
        Directory directory = FSDirectory.open(getLuceneFacetIndexDir().toPath());
        SimpleAnalyzer simpleAnalyzer = new SimpleAnalyzer();
        IndexWriterConfig config = new IndexWriterConfig(simpleAnalyzer);
        return new IndexWriter(directory, config);
    }

    private static TaxonomyWriter getTaxoWriter() throws IOException {
        Directory directory = FSDirectory.open(getLuceneTaxoDir().toPath());
        SimpleAnalyzer simpleAnalyzer = new SimpleAnalyzer();
        //IndexWriterConfig config = new IndexWriterConfig(simpleAnalyzer);
        return  new DirectoryTaxonomyWriter(directory);
    }

    private static IndexReader createIndexReader() throws IOException {
        Directory directory = FSDirectory.open(getLuceneFacetIndexDir().toPath());
        return DirectoryReader.open(directory);
    }

    public static IndexReader createIndexReaderPublic() throws IOException {
        Directory directory = FSDirectory.open(getLuceneFacetIndexDir().toPath());
        return DirectoryReader.open(directory);
    }

    private static TaxonomyReader createTaxoReader() throws IOException {
        Directory directory = FSDirectory.open(getLuceneTaxoDir().toPath());
        return new DirectoryTaxonomyReader(directory);
    }

    private static void getValuesFromSTProperties(STProperties stProperties, ProjectWithFacetsForIndex projectWithFacetsForIndex)
            throws PropertyNotFoundException {
        Collection<String> propertiesList = stProperties.getProperties();
        for (String propName : propertiesList) {
            if (stProperties.getPropertyValue(propName) == null) {
                // no value, so skip it
                continue;
            }
            List<String> valueStringList = normalizeFacetValue(stProperties.getPropertyValue(propName));
            projectWithFacetsForIndex.addFacetAndValueList(propName, valueStringList);
        }
    }

    public static List<String> getFacetValueFromProjectInfoForBagResults(ProjectInfo projectInfo, String facetName )  {
        ProjectFacets projectFacets = projectInfo.getFacets();
        List<String> facetValueList = new ArrayList<>();
        if (projectFacets == null) {
            return facetValueList;
        }

        // this method will retrive the facetValue from the projectInfo given an input facetName and it is used to group
        // the project according to the facetName.
        // The complexity is that some facetName are not really facets of the projectInfo, but are specific attribute
        // (e.g. the projectName or the projectModel), so, for these case, behave accordingly
        if (facetName.equalsIgnoreCase(PROJECT_NAME)) {
            facetValueList.add(projectInfo.getName());
            return facetValueList;
        } else if (facetName.equalsIgnoreCase(PROJECT_MODEL)) {
            facetValueList.add(projectInfo.getModel());
            return facetValueList;
        } else if (facetName.equalsIgnoreCase(PROJECT_LEX_MODEL)) {
            facetValueList.add(projectInfo.getLexicalizationModel());
            return facetValueList;
        } else if (facetName.equalsIgnoreCase(PROJECT_HISTORY)) {
            facetValueList.add(Boolean.toString(projectInfo.isHistoryEnabled()));
            return facetValueList;
        } else if (facetName.equalsIgnoreCase(PROJECT_VALIDATION)) {
            facetValueList.add(Boolean.toString(projectInfo.isValidationEnabled()));
            return facetValueList;
        } else if (facetName.equalsIgnoreCase(PROJECT_DESCRIPTION)) {
            facetValueList.add(projectInfo.getDescription());
            return facetValueList;
        }

        //the input facetName is not a special value, so just get the facet
        Object facetValueObject = null;
        try {
            facetValueObject = projectFacets.getPropertyValue(facetName);
            if (facetValueObject == null) {
                //in this case do nothing, so facetValue is still an empty list
            } else {
                facetValueList = ProjectFacetsIndexLuceneUtils.normalizeFacetValue(facetValueObject);
            }
        } catch (PropertyNotFoundException e) {
            //this case can happen if the facetName is, in reality, inside the "customFacets", so, in this case,
            // to avoid passing the value "customFacets" (which could change and this could be a problem in the future)
            // iterate over ALL ProjectFacets
            facetValueList = getFacetValueFromProjectInfoForBagResults(projectFacets, facetName);

        }

        return facetValueList;
    }

    private static List<String> getFacetValueFromProjectInfoForBagResults(ProjectFacets projectFacets, String inputFacetName) {
        for (String facetName : projectFacets.getProperties()) {
            try {
                Object value = projectFacets.getPropertyValue(facetName);
                if (value instanceof DynamicSTProperties) {
                    STProperties stProperties = (STProperties) value;
                    Collection<String> propertiesList = stProperties.getProperties();
                    for (String propName : propertiesList) {
                        if (stProperties.getPropertyValue(propName) == null) {
                            // no value, so skip it
                            continue;
                        }
                        if (propName.equals(inputFacetName)) {
                            return normalizeFacetValue(stProperties.getPropertyValue(propName));
                        }
                    }
                } else if (value != null) {
                    if (facetName.equals(inputFacetName)) {
                        return normalizeFacetValue(value);
                    }
                }
            } catch (PropertyNotFoundException e) {
                // the facet was not found, so skip it and pass to the next one
            }
        }
        return new ArrayList<>();
    }

    private static void deleteDirOrFile(File fileOrDirFile) {
        if(fileOrDirFile.isDirectory()){
            for(File file : fileOrDirFile.listFiles()) {
                deleteDirOrFile(file);
            }
            //now that all files and dire have been deleted, delete the directory
            fileOrDirFile.delete();
        } else {
            //it is a file, so just delete it
            fileOrDirFile.delete();
        }
    }

}
