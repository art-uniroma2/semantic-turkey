package it.uniroma2.art.semanticturkey.validators;

import it.uniroma2.art.semanticturkey.constraints.SpringCron;
import org.springframework.scheduling.support.CronExpression;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Validates {@link it.uniroma2.art.semanticturkey.constraints.SpringCron} constraint.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 */
public class SpringCronValidator implements ConstraintValidator<SpringCron, String> {

	@Override
	public void initialize(SpringCron constraintAnnotation) {
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		try {
			CronExpression.isValidExpression(value);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}

}
