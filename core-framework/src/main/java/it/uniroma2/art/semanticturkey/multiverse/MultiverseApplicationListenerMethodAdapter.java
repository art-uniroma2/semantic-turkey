package it.uniroma2.art.semanticturkey.multiverse;

import org.springframework.context.event.ApplicationListenerMethodAdapter;

import java.lang.reflect.Method;

/**
 * {@link org.springframework.context.ApplicationListener} implementation invoking the same method in each world.
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class MultiverseApplicationListenerMethodAdapter extends ApplicationListenerMethodAdapter {

    private final MultiverseManager multiverseManager;

    /**
     * Construct a new ApplicationListenerMethodAdapter.
     *
     * @param beanName          the name of the bean to invoke the listener method on
     * @param targetClass       the target class that the method is declared on
     * @param method            the listener method to invoke
     * @param multiverseManager the multiverse manager
     */
    public MultiverseApplicationListenerMethodAdapter(String beanName, Class<?> targetClass, Method method, MultiverseManager multiverseManager) {
        super(beanName, targetClass, method);
        this.multiverseManager = multiverseManager;
    }

    @Override
    protected Object doInvoke(Object... args) {
        try {
            multiverseManager.executeInAllWorlds(() -> super.doInvoke(args));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
        return null;
    }
}
