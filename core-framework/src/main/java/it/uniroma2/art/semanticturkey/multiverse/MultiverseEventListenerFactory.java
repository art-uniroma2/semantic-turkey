package it.uniroma2.art.semanticturkey.multiverse;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListenerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.transaction.event.TransactionalApplicationListenerMethodAdapter;

import java.lang.reflect.Method;

/**
 * {@link EventListenerFactory} implementation that handles {@link MultiverseEventListener}
 * annotated methods.
 */
public class MultiverseEventListenerFactory implements EventListenerFactory, Ordered {

    private final MultiverseManager multiverseManager;
    private int order = 40;

    public MultiverseEventListenerFactory(MultiverseManager multiverseManager) {
        this.multiverseManager = multiverseManager;
    }


    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public int getOrder() {
        return this.order;
    }


    @Override
    public boolean supportsMethod(Method method) {
        return AnnotatedElementUtils.hasAnnotation(method, MultiverseEventListener.class);
    }

    @Override
    public ApplicationListener<?> createApplicationListener(String beanName, Class<?> type, Method method) {
        return new MultiverseApplicationListenerMethodAdapter(beanName, type, method, multiverseManager);
    }

}
