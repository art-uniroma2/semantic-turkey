package it.uniroma2.art.semanticturkey.settings.core;

import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;

public class SepiaSettings implements STProperties {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.core.SepiaSettings";

        public static final String shortName = keyBase + ".shortName";

        public static final String endpointURL$description = keyBase + ".endpointURL.description";
        public static final String endpointURL$displayName = keyBase + ".endpointURL.displayName";

    }

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.endpointURL$description + "}",
            displayName = "{" + MessageKeys.endpointURL$displayName + "}")
    @Required
    public String endpointURL;

}
