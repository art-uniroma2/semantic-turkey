package it.uniroma2.art.semanticturkey.security;

import it.uniroma2.art.semanticturkey.user.STUser;

import java.util.ArrayList;
import java.util.List;

public class ActiveUserStore {

    private List<STUser> users;

    public ActiveUserStore() {
        users = new ArrayList<>();
    }

    public List<STUser> getUsers() {
        return users;
    }

    public void setUsers(List<STUser> users) {
        this.users = users;
    }
}