package it.uniroma2.art.semanticturkey.spring;

import it.uniroma2.art.semanticturkey.extension.ExtensionPointManager;
import it.uniroma2.art.semanticturkey.history.HistoryMetadataPostProcessor;
import it.uniroma2.art.semanticturkey.i18n.I18NConstants;
import it.uniroma2.art.semanticturkey.multiverse.MultiverseEventListenerFactory;
import it.uniroma2.art.semanticturkey.multiverse.MultiverseHandlerInterceptor;
import it.uniroma2.art.semanticturkey.multiverse.MultiverseManager;
import it.uniroma2.art.semanticturkey.multiverse.WorldScope;
import it.uniroma2.art.semanticturkey.mvc.IntrospectableController;
import it.uniroma2.art.semanticturkey.mvc.LegacyAndNewStyleServiceConnectioManagementHandlerInterceptor;
import it.uniroma2.art.semanticturkey.mvc.RequestListenerHandlerInterceptor;
import it.uniroma2.art.semanticturkey.mvc.RequestMappingHandlerAdapterPostProcessor;
import it.uniroma2.art.semanticturkey.search.SearchUpdatePostProcessor;
import it.uniroma2.art.semanticturkey.services.STServiceContext;
import it.uniroma2.art.semanticturkey.services.aspects.RejectedTermsBlacklistingPostProcessor;
import it.uniroma2.art.semanticturkey.services.aspects.ResourceLifecycleEventPublisherPostProcessor;
import it.uniroma2.art.semanticturkey.services.aspects.WritabilityCheckerPostProcessor;
import it.uniroma2.art.semanticturkey.services.http.STServiceHTTPContext;
import it.uniroma2.art.semanticturkey.tx.RDF4JRepositoryTransactionManagerFactoryBean;
import it.uniroma2.art.semanticturkey.tx.STServiceAspect;
import it.uniroma2.art.semanticturkey.user.SchemesOwnershipPostProcessor;
import jakarta.validation.Validator;
import org.aopalliance.aop.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.DelegatingWebMvcConfiguration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.resource.ResourceUrlProvider;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.function.Supplier;

@Configuration
@ComponentScan(basePackageClasses = IntrospectableController.class)
//@EnableWebMvc
@EnableTransactionManagement(proxyTargetClass = true)
@EnableMethodSecurity
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableWebSecurity
public class STBaseServiceConfiguration extends DelegatingWebMvcConfiguration {
    public class HandlerInterceptors {
        private Object[] interceptors;

        public HandlerInterceptors(Object[] interceptors) {
            this.interceptors = interceptors;
        }

        public Object[] get() {
            return interceptors;
        }
    }

    @Autowired
    protected ExtensionPointManager exptMgr;

    @Autowired
    protected MultiverseManager multiverseManager;

    @Autowired
    protected ConfigurableBeanFactory beanFactory;

    @Bean
    public static CustomScopeConfigurer customScopeConfigurer(MultiverseManager multiverseManager) {
        CustomScopeConfigurer configurer = new CustomScopeConfigurer();
        configurer.addScope("world", new WorldScope(multiverseManager));
        return configurer;
    }

    @Bean
    public static MultiverseEventListenerFactory multiverseEventListenerFactory(MultiverseManager multiverseManager) {
        return new MultiverseEventListenerFactory(multiverseManager);
    }

    @Bean
    public STServiceContext stServiceContext() {
        return new STServiceHTTPContext();
    }

    @Bean
    public SessionRegistry sessionRepository() {
        return new SessionRegistryImpl();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
    public RDF4JRepositoryTransactionManagerFactoryBean transactionManager() {
        return new RDF4JRepositoryTransactionManagerFactoryBean();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);

        registry.addInterceptor(new LegacyAndNewStyleServiceConnectioManagementHandlerInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(new RequestListenerHandlerInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(new MultiverseHandlerInterceptor(multiverseManager)).addPathPatterns("/**");
    }

    @Bean
    public HandlerInterceptors handlerInterceptors(FormattingConversionService mvcConversionService,
                                                   ResourceUrlProvider mvcResourceUrlProvider) {
        return new HandlerInterceptors(getInterceptors(mvcConversionService, mvcResourceUrlProvider));
    }
    @Bean
    public ResourceBundleMessageSource messageSource() {
        var source = new ResourceBundleMessageSource();
        source.setDefaultEncoding(StandardCharsets.UTF_8.name()); // use UTF-8 to avoid the need for escaping non-ascii characters
        source.setFallbackToSystemLocale(false); // fallback to the root local "" instead of the system locale whatever it is
        source.addBasenames("it.uniroma2.art.semanticturkey.l10n.ValidationMessages");
        return source;
    }

    @Bean
    public CookieLocaleResolver localeResolver() {
        var resolver = new CookieLocaleResolver(I18NConstants.LANG_COOKIE_NAME);
        resolver.setCookieMaxAge(Duration.ofSeconds(315_400_000)); // 10 years (in seconds) as in the client
        return resolver;
    }

    @Bean
    public LocalValidatorFactoryBean validatorFactory() {
        var validatorFactoryBean = new LocalValidatorFactoryBean();
        validatorFactoryBean.setValidationMessageSource(messageSource());
        return validatorFactoryBean;
    }

    @Bean
    public InternalResourceViewResolver viewResolver() {
        var viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/pages/");
        viewResolver.setSuffix(".html");
        viewResolver.setOrder(1);
        return viewResolver;
    }

    /* static and lazy because this is a BeanPostProcessor, to avoid the instantiation of the whole configuration class */
    @Bean
    public static WritabilityCheckerPostProcessor writabilityCheckerPostProcessor(@Lazy STServiceContext stServiceContext) {
        var bean = new WritabilityCheckerPostProcessor(stServiceContext);
        bean.setProxyTargetClass(true);
        bean.setOrder(90);
        return bean;
    }

    /* static and lazy because this is a BeanPostProcessor, to avoid the instantiation of the whole configuration class */
    @Bean
    public static SchemesOwnershipPostProcessor schemesOwnershipPostProcessor(@Lazy STServiceContext stServiceContext) {
        var bean = new SchemesOwnershipPostProcessor(stServiceContext);
        bean.setProxyTargetClass(true);
        bean.setOrder(95);
        return bean;
    }

    /* static and lazy because this is a BeanPostProcessor, to avoid the instantiation of the whole configuration class */
    @Bean
    public static HistoryMetadataPostProcessor historyMetadataPostProcessor(@Lazy STServiceContext stServiceContext) {
        var bean = new HistoryMetadataPostProcessor(stServiceContext);
        bean.setProxyTargetClass(true);
        bean.setOrder(250);
        return bean;
    }

    /* static and lazy because this is a BeanPostProcessor, to avoid the instantiation of the whole configuration class */
    @Bean
    public static ResourceLifecycleEventPublisherPostProcessor resourceLifecycleEventPublisherPostProcessor(@Lazy STServiceContext stServiceContext,
                                                                                                            @Lazy ApplicationContext applicationContext) {
        var bean = new ResourceLifecycleEventPublisherPostProcessor(stServiceContext, applicationContext);
        bean.setProxyTargetClass(true);
        bean.setOrder(251);
        return bean;
    }

    /* static and lazy because this is a BeanPostProcessor, to avoid the instantiation of the whole configuration class */
    @Bean
    public static RejectedTermsBlacklistingPostProcessor rejectedTermsBlacklistingPostProcessor(@Lazy STServiceContext stServiceContext) {
        var bean = new RejectedTermsBlacklistingPostProcessor(stServiceContext);
        bean.setProxyTargetClass(true);
        bean.setOrder(252);
        return bean;
    }

    /* static and lazy because this is a BeanPostProcessor, to avoid the instantiation of the whole configuration class */
    @Bean
    public static SearchUpdatePostProcessor searchUpdatePostProcessor(@Lazy STServiceContext stServiceContext,
                                                                      @Lazy ExtensionPointManager exptManager) {
        var bean = new SearchUpdatePostProcessor(stServiceContext, exptManager);
        bean.setProxyTargetClass(true);
        bean.setOrder(253);
        return bean;
    }

    /* static and lazy because this is a BeanPostProcessor, to avoid the instantiation of the whole configuration class */
    @Bean
    public static MethodValidationPostProcessor methodValidationPostProcessor(@Lazy ApplicationContext applicationContext) {
        var bean = new MethodValidationPostProcessor() {
            // Use explicitly the ValidatorFactory enabling dependency injection
            // on constraint validators

            @Override
            protected Advice createMethodValidationAdvice(Supplier<Validator> validator) {
                return super.createMethodValidationAdvice(() -> (LocalValidatorFactoryBean)applicationContext.getBean("validatorFactory"));
            }
        };
        bean.setProxyTargetClass(true);
        bean.setOrder(300);
        return bean;
    }

    /* static and lazy because this is a BeanPostProcessor, to avoid the instantiation of the whole configuration class */
    @Bean
    public static RequestMappingHandlerAdapterPostProcessor requestMappingHandlerAdapterPostProcessor(@Lazy ConfigurableBeanFactory beanFactory, @Lazy STServiceContext stServiceContext) {
        return new RequestMappingHandlerAdapterPostProcessor(beanFactory, stServiceContext);
    }

    @Bean
    public STServiceAspect stServiceAspect() {
        var bean = new STServiceAspect();
        bean.setOrder(100);
        return bean;
    }

    // --- security

//    @Bean
//    public SecurityFilterChain filterChain(HttpSecurity http,
//                                           STAuthenticationEntryPoint stAuthenticationEntryPoint,
//                                           STUserDetailsService userDetailsService,
//                                           STAuthenticationSuccessHandler authSuccessHandler,
//                                           STAuthenticationFailureHandler authFailHandler,
//                                           STLogoutSuccessHandler logoutSuccessHandler,
//                                           CORSFilter corsFilter
//    ) throws Exception {
//        http
//                .authorizeHttpRequests(requests ->
//                        requests.requestMatchers("/Users/getUser",
//                                "/saml/**",
//                                "/Users/getUserFormFields",
//                                "/Users/registerUser",
//                                "/Users/forgotPassword",
//                                "/Users/resetPassword",
//                                "/Users/verifyUserEmail",
//                                "/Users/activateRegisteredUser",
//                                "/Settings/getStartupSettings",
//                                "/Administration/downloadPrivacyStatement",
//                                "/HttpResolution/contentNegotiation",
//                                "/HttpResolution/contentNegotiation"
//                        ).permitAll().anyRequest().authenticated()
//                )
//                .userDetailsService(userDetailsService)
//                .rememberMe(rememberMe ->
//                        rememberMe
//                                .key("vocbench_rememberme_key")
//                                .tokenValiditySeconds(2592000)
//                                .userDetailsService(userDetailsService)
//                                .authenticationSuccessHandler(authSuccessHandler))
//                .addFilterAt(corsFilter, ChannelProcessingFilter.class)
//                .csrf(c -> c.ignoringRequestMatchers(("/**")))
//                .exceptionHandling(exceptionHandlingCustomizer -> {
//                    exceptionHandlingCustomizer.authenticationEntryPoint(stAuthenticationEntryPoint);
//                });
//        return http.build();
//    }

    @Bean
    public StandardServletMultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver(); // enable resolution of multipart uploads
    }
}
