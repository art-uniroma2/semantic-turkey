package it.uniroma2.art.semanticturkey.settings.download;

import it.uniroma2.art.semanticturkey.extension.settings.ProjectSettingsManager;
import org.springframework.stereotype.Component;


/**
 * @author <a href="mailto:turbati@info.uniroma2.it">Andrea Turbati</a>
 */
@Component
public class DownloadSettingsManager implements ProjectSettingsManager<DownloadProjectSettings> {

    @Override
    public String getId() {
        return DownloadSettingsManager.class.getName();
    }
}
