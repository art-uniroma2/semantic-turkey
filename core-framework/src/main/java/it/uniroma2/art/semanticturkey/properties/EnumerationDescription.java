package it.uniroma2.art.semanticturkey.properties;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.reflect.TypeUtils;

/**
 * Describes an enumeration.
 * 
 * @author <a href="fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class EnumerationDescription {
	private final List<String> values;
	private final boolean open;

	public EnumerationDescription(Collection<String> values, boolean open) {
		this.values = ImmutableList.copyOf(values);
		this.open = open;
	}

	public EnumerationDescription(String[] values, boolean open) {
		this(Arrays.asList(values), open);
	}

	public List<String> getValues() {
		return values;
	}

	public boolean isOpen() {
		return open;
	}

	public static EnumerationDescription fromAnnotation(Enumeration annot) {
		return new EnumerationDescription(Arrays.asList(annot.value()), annot.open());
	}

	public List<Object> getDeserializedAllowedValues(Type propType) throws JsonProcessingException {
		ObjectMapper om = STPropertiesManager.createObjectMapper();

		Type elementType;
		if (TypeUtils.isAssignable(propType, Collection.class)) {
			elementType = TypeUtils.getTypeArguments(propType, Iterable.class).values().iterator().next();
		} else {
			elementType = propType;
		}
		List<Object> allowedValues;

		if (!elementType.equals(String.class)) {
			allowedValues = new ArrayList<>();
			JavaType javaElementType = om.constructType(elementType);
			for (String tempValue : this.getValues()) {
				Object aValue = om.readValue(tempValue, javaElementType);
				allowedValues.add(aValue);
			}
		} else {
			allowedValues = new ArrayList<>(this.getValues());
		}
		return allowedValues;
	}

}
