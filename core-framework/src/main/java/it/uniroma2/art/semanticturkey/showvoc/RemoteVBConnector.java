package it.uniroma2.art.semanticturkey.showvoc;

import it.uniroma2.art.semanticturkey.email.EmailApplicationContext;
import it.uniroma2.art.semanticturkey.properties.STPropertiesManager;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.settings.core.CoreSystemSettings;
import it.uniroma2.art.semanticturkey.settings.core.ShowVocSettings;
import it.uniroma2.art.semanticturkey.settings.core.SemanticTurkeyCoreSettingsManager;
import it.uniroma2.art.semanticturkey.settings.core.VocBenchConnectionShowVocSettings;

import java.util.Arrays;
import java.util.List;

public class RemoteVBConnector extends RemoteSTConnector {

	private RemoteVBConnector(List<String> parameters) throws STPropertyAccessException {
		super(parameters.get(0), parameters.get(1), parameters.get(2), parameters.get(3), EmailApplicationContext.VB);
	}
	public RemoteVBConnector(SemanticTurkeyCoreSettingsManager coreSettingsManager) throws STPropertyAccessException {
		this(getParameters(coreSettingsManager));
	}

	private static List<String> getParameters(SemanticTurkeyCoreSettingsManager coreSettingsManager) throws STPropertyAccessException {
		VocBenchConnectionShowVocSettings vbConnectionSettings = null;

		CoreSystemSettings systemSettings = coreSettingsManager.getSystemSettings();
		ShowVocSettings svSettings = systemSettings.showvoc;
		if (svSettings != null) {
			vbConnectionSettings = svSettings.vbConnectionConfig;
		}

		if (vbConnectionSettings == null) {
			throw new IllegalStateException("No configuration found for connecting to a remote VocBench instance");
		}

		String stHost = vbConnectionSettings.stHost;
		String vbUrl = vbConnectionSettings.vbURL;
		String adminEmail = vbConnectionSettings.adminEmail;
		String adminPwd = vbConnectionSettings.adminPassword;

		return Arrays.asList(stHost, vbUrl, adminEmail, adminPwd);
	}


}
