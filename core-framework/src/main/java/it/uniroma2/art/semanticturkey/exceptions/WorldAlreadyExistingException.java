package it.uniroma2.art.semanticturkey.exceptions;

/**
 * Exception thrown when creating a world with the same name of an existing world.
 *
 * <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class WorldAlreadyExistingException extends DeniedOperationException {
    public WorldAlreadyExistingException(String world) {
        super(WorldAlreadyExistingException.class.getName() + ".message", new Object[]{ world});
    }
}
