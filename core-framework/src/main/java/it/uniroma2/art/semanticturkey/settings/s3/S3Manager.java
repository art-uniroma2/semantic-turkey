package it.uniroma2.art.semanticturkey.settings.s3;

import it.uniroma2.art.semanticturkey.extension.settings.ProjectSettingsManager;
import org.springframework.stereotype.Component;

/**
 * A settings manager for the association of a project with S3 configs.
 *
 * @author <a href="mailto:tiziano.dicondina@devit.cloud">Tiziano Di Condina</a>
 */
@Component
public class S3Manager implements ProjectSettingsManager<S3ProjectSettings> {

    @Override
    public String getId() {
        return S3Manager.class.getName();
    }
}
