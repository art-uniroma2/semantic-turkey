package it.uniroma2.art.semanticturkey.security;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * 
 * @author Tiziano
 *
 * Used in SecurityConfig.configure() in order to apply the CORS policy headers to all the responses
 */
public class CORSFilter implements Filter {

	private final CspWhitelistService cspWhitelistService;

	public CORSFilter(CspWhitelistService cspWhitelistService) {
		this.cspWhitelistService = cspWhitelistService;
	}
	
	@Override
	public void init(FilterConfig filterConfig) { }

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
		response.setHeader("Access-Control-Allow-Credentials", "true");
		if (!request.getRequestURI().contains("saml")) {
			/*
			Add the following header (requested by OP to prevent vulnerabilities) only if request is not
			the one about saml/login (/saml2/sso/{registrationId} or /authenticate/{registrationId})
			which returns an HTML with the following in its content
			<body onload="document.forms[0].submit()">...</body>
			This is blocked with the following header, so it interrupts the saml login flow

			"Refused to execute inline event handler because it violates the following Content Security Policy directive:
			"default-src 'self'..."
			*/

			String cspValue = "default-src 'self'; style-src 'self' 'unsafe-inline'; img-src 'self' data: https:;";

			//allows connections to self domain and all domains in HTTPS
			String connectSrc = "connect-src 'self' https:";
			//dynamically add further connect-src domain (useful for Sepia connections that would be blocked otherwise)
			String cspWhitelist = cspWhitelistService.getWhitelist();
			if (!cspWhitelist.isEmpty()) {
				connectSrc += " " + cspWhitelist;
			}
			cspValue += " " + connectSrc + ";";

			response.setHeader("Content-Security-Policy", cspValue);
		}

		chain.doFilter(req, res);
	}

	@Override
	public void destroy() {
	}

}