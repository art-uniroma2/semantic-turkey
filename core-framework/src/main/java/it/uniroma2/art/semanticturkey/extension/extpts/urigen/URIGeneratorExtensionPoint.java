package it.uniroma2.art.semanticturkey.extension.extpts.urigen;

import it.uniroma2.art.semanticturkey.extension.ExtensionPoint;
import it.uniroma2.art.semanticturkey.resources.Scope;
import org.springframework.stereotype.Component;

@Component
public class URIGeneratorExtensionPoint implements ExtensionPoint {

	@Override
	public Class<?> getInterface() {
		return URIGenerator.class;
	}

	@Override
	public Scope getScope() {
		return Scope.PROJECT;
	}

}
