package it.uniroma2.art.semanticturkey.pf4j.events;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;

public class PluginInitialized extends PluginEvent{
    public PluginInitialized(STPlugin source) {
        super(source);
    }
}
