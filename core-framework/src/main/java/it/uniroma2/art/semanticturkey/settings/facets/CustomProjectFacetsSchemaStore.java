package it.uniroma2.art.semanticturkey.settings.facets;

import it.uniroma2.art.semanticturkey.extension.settings.SystemSettingsManager;
import it.uniroma2.art.semanticturkey.properties.dynamic.STPropertiesSchema;
import org.springframework.stereotype.Component;

/**
 * A store for the schema of custom project facets.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>.
 *
 */
@Component
public class CustomProjectFacetsSchemaStore implements SystemSettingsManager<STPropertiesSchema> {

	/*
	 * (Non-Javadoc) See ProjectFacetsStore#isStatic()
	 */
	@Override
	public boolean isStatic() {
		return true;
	}

	@Override
	public String getId() {
		return CustomProjectFacetsSchemaStore.class.getName();
	}

}
