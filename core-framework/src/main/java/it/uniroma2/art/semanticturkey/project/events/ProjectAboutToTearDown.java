package it.uniroma2.art.semanticturkey.project.events;

import it.uniroma2.art.semanticturkey.event.Event;
import it.uniroma2.art.semanticturkey.project.Project;

/**
 * A {@link Event} raised just before a project is torn down.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class ProjectAboutToTearDown extends ProjectEvent {

    public ProjectAboutToTearDown(Project project) {
        super(project);
    }
}
