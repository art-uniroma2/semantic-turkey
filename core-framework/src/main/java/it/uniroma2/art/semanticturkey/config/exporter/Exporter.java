package it.uniroma2.art.semanticturkey.config.exporter;

import it.uniroma2.art.semanticturkey.config.ConfigurationManager;
import it.uniroma2.art.semanticturkey.extension.PUScopedConfigurableComponent;
import org.springframework.stereotype.Component;

/**
 * A {@link ConfigurationManager} for managing stored exporter configurations (see
 * {@link StoredExportConfiguration}.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
@Component
public class Exporter implements PUScopedConfigurableComponent<StoredExportConfiguration> {

	@Override
	public String getId() {
		return Exporter.class.getName();
	}

}
