package it.uniroma2.art.semanticturkey.multiverse;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;

/**
 * A custom {@link Scope} sensible to the multiverse of settings.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class WorldScope implements Scope {
    private final MultiverseManager multiverseManager;

    public WorldScope(MultiverseManager multiverseManager) {
        this.multiverseManager = multiverseManager;
    }

    @Override
    public Object get(String name, ObjectFactory<?> objectFactory) {
        var worldScopedObjects = multiverseManager.getWorldScopedObjects(MultiverseManager.getCurrentWorld());
        return worldScopedObjects.computeIfAbsent(name, objectFactory);
    }

    @Override
    public Object remove(String name) {
        var worldScopedObjects = multiverseManager.getWorldScopedObjects(MultiverseManager.getCurrentWorld());
        return worldScopedObjects.remove(name);
    }

    @Override
    public void registerDestructionCallback(String name, Runnable callback) {
        var worldScopedObjects = multiverseManager.getWorldScopedObjects(MultiverseManager.getCurrentWorld());
        worldScopedObjects.registerDestructionCallback(name, callback);
    }

    @Override
    public Object resolveContextualObject(String key) {
        return null;
    }

    @Override
    public String getConversationId() {
        return null;
    }
}
