package it.uniroma2.art.semanticturkey.project;

import com.google.common.collect.MapMaker;

import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Utility class for {@link Project}s.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
public abstract class ProjectUtils {
	private static ConcurrentMap<String, ReadWriteLock> projectDirLocks = new MapMaker()
			.concurrencyLevel(Runtime.getRuntime().availableProcessors())
			.weakValues()
			.makeMap();

	/**
	 * Computes the name of the (local) repository holding a version dump.
	 * 
	 * @param versionId
	 * @return
	 */
	public static String computeVersionRepository(String versionId) {
		return "version-" + versionId;
	}

	/**
	 * Returns a reentrant read lock associated with the project directory
	 *
	 * @param projectName
	 * @return
	 */
	public static Lock getReadLockProjectDirectory(String projectName) {
		return getReadWriteLockProjectDirectory(projectName).readLock();
	}

	/**
	 * Returns a reentrant write lock associated with the project directory
	 *
	 * @param projectName
	 * @return
	 */
	public static Lock getWriteLockProjectDirectory(String projectName) {
		return getReadWriteLockProjectDirectory(projectName).writeLock();
	}

	private static ReadWriteLock getReadWriteLockProjectDirectory(String projectName) {
		return projectDirLocks.computeIfAbsent(projectName, key -> new ReentrantReadWriteLock(true));
	}
}
