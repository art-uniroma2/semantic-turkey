package it.uniroma2.art.semanticturkey.settings.core;

import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.properties.Enumeration;
import it.uniroma2.art.semanticturkey.properties.Language;
import it.uniroma2.art.semanticturkey.properties.STProperty;

import java.util.List;
import java.util.Map;

public class CoreProjectSettings implements Settings {

	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.core.CoreProjectSettings";

		public static final String shortName = keyBase + ".shortName";
		public static final String languages$description = keyBase
				+ ".languages.description";
		public static final String languages$displayName = keyBase
				+ ".languages.displayName";
		public static final String labelClashMode$description = keyBase
				+ ".labelClashMode.description";
		public static final String labelClashMode$displayName = keyBase
				+ ".labelClashMode.displayName";
		public static final String resourceView$description = keyBase
				+ ".resourceView.description";
		public static final String resourceView$displayName = keyBase
				+ ".resourceView.displayName";
		public static final String resViewPredLabelMappings$description = keyBase
				+ ".resViewPredLabelMappings.description";
		public static final String resViewPredLabelMappings$displayName = keyBase
				+ ".resViewPredLabelMappings.displayName";
		public static final String resViewSectionsCustomization$description = keyBase
				+ ".resViewSectionsCustomization.description";
		public static final String resViewSectionsCustomization$displayName = keyBase
				+ ".resViewSectionsCustomization.displayName";
		public static final String dataPanelLabelMappings$description = keyBase
				+ ".dataPanelLabelMappings.description";
		public static final String dataPanelLabelMappings$displayName = keyBase
				+ ".dataPanelLabelMappings.displayName";
		public static final String timeMachineEnabled$description = keyBase
				+ ".timeMachineEnabled.description";
		public static final String timeMachineEnabled$displayName = keyBase
				+ ".timeMachineEnabled.displayName";
		public static final String xkosTabEnabled$description = keyBase
				+ ".xkosTabEnabled.description";
		public static final String xkosTabEnabled$displayName = keyBase
				+ ".xkosTabEnabled.displayName";

		public static final String allowConceptTreeVisualizationChange$description = keyBase
				+ ".allowConceptTreeVisualizationChange.description";
		public static final String allowConceptTreeVisualizationChange$displayName = keyBase
				+ ".allowConceptTreeVisualizationChange.displayName";

		public static final String allowInstanceListVisualizationChange$description = keyBase
				+ ".allowInstanceListVisualizationChange.description";
		public static final String allowInstanceListVisualizationChange$displayName = keyBase
				+ ".allowInstanceListVisualizationChange.displayName";

		public static final String allowLexEntryListVisualizationChange$description = keyBase
				+ ".allowLexEntryListVisualizationChange.description";
		public static final String allowLexEntryListVisualizationChange$displayName = keyBase
				+ ".allowLexEntryListVisualizationChange.displayName";

		public static final String allowLexEntryIndexLengthChange$description = keyBase
				+ ".allowLexEntryIndexLengthChange.description";
		public static final String allowLexEntryIndexLengthChange$displayName = keyBase
				+ ".allowLexEntryIndexLengthChange.displayName";
	}

	@Override
	public String getShortName() {
		return "{" + MessageKeys.shortName + "}";
	}

	@STProperty(description = "{" + MessageKeys.languages$description
			+ "}", displayName = "{" + MessageKeys.languages$displayName + "}")
	public List<Language> languages;

	@STProperty(description = "{" + MessageKeys.labelClashMode$description
			+ "}", displayName = "{" + MessageKeys.labelClashMode$displayName + "}")
	@Enumeration({"forbid", "warning", "allow"})
	public String labelClashMode;

	@STProperty(description = "{" + MessageKeys.resourceView$description
			+ "}", displayName = "{" + MessageKeys.resourceView$displayName + "}")
	public ResourceViewProjectSettings resourceView;

	@STProperty(description = "{" + MessageKeys.resViewPredLabelMappings$description + "}",
			displayName = "{" + MessageKeys.resViewPredLabelMappings$displayName +"}")
	public PredicateLabelSettings resViewPredLabelMappings;

	@STProperty(description = "{" + MessageKeys.resViewSectionsCustomization$description + "}",
			displayName = "{" + MessageKeys.resViewSectionsCustomization$displayName +"}")
	public Map<String, Map<String, ResViewSectionDisplayInfo>> resViewSectionsCustomization; //map lang -> resViewSection -> customization

	@STProperty(description = "{" + MessageKeys.dataPanelLabelMappings$description + "}",
			displayName = "{" + MessageKeys.dataPanelLabelMappings$displayName +"}")
	public Map<String, Map<String, String>> dataPanelLabelMappings; //map lang => panel => label

	@STProperty(description = "{" + MessageKeys.timeMachineEnabled$description
			+ "}", displayName = "{" + MessageKeys.timeMachineEnabled$displayName + "}")
	public Boolean timeMachineEnabled = true;

	@STProperty(description = "{" + MessageKeys.xkosTabEnabled$description
			+ "}", displayName = "{" + MessageKeys.xkosTabEnabled$displayName + "}")
	public Boolean xkosTabEnabled = false;

	@STProperty(description = "{" + MessageKeys.allowConceptTreeVisualizationChange$description
			+ "}", displayName = "{" + MessageKeys.allowConceptTreeVisualizationChange$displayName + "}")
	public Boolean allowConceptTreeVisualizationChange = true;

	@STProperty(description = "{" + MessageKeys.allowInstanceListVisualizationChange$description
			+ "}", displayName = "{" + MessageKeys.allowInstanceListVisualizationChange$displayName + "}")
	public Boolean allowInstanceListVisualizationChange = true;

	@STProperty(description = "{" + MessageKeys.allowLexEntryListVisualizationChange$description
			+ "}", displayName = "{" + MessageKeys.allowLexEntryListVisualizationChange$displayName + "}")
	public Boolean allowLexEntryListVisualizationChange = true;

	@STProperty(description = "{" + MessageKeys.allowLexEntryIndexLengthChange$description
			+ "}", displayName = "{" + MessageKeys.allowLexEntryIndexLengthChange$displayName + "}")
	public Boolean allowLexEntryIndexLengthChange = true;

}
