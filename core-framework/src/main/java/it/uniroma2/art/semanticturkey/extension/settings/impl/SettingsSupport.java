package it.uniroma2.art.semanticturkey.extension.settings.impl;

import it.uniroma2.art.semanticturkey.extension.ExtensionPointManagerHolder;
import it.uniroma2.art.semanticturkey.multiverse.MultiverseManager;
import it.uniroma2.art.semanticturkey.multiverse.World;
import jakarta.annotation.Nullable;


import com.fasterxml.jackson.databind.node.ObjectNode;

import it.uniroma2.art.semanticturkey.extension.ExtensionPointManager;
import it.uniroma2.art.semanticturkey.extension.settings.PUSettingsManager;
import it.uniroma2.art.semanticturkey.extension.settings.ProjectSettingsManager;
import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.extension.settings.SettingsManager;
import it.uniroma2.art.semanticturkey.extension.settings.SystemSettingsManager;
import it.uniroma2.art.semanticturkey.extension.settings.UserSettingsManager;
import it.uniroma2.art.semanticturkey.properties.STPropertiesManager;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.properties.WrongPropertiesException;
import it.uniroma2.art.semanticturkey.resources.Scope;
import it.uniroma2.art.semanticturkey.utilities.ReflectionUtilities;
import org.springframework.context.ApplicationEventPublisher;

public abstract class SettingsSupport {
	private static volatile ApplicationEventPublisher eventPublisher = null;

	public static void setEventPublisher(ApplicationEventPublisher newEventPublisher) {
		eventPublisher = newEventPublisher;
	}

	public static ApplicationEventPublisher getEventPublisher() {
		var temp = eventPublisher;
		if (temp == null) {
			throw new IllegalStateException("Cannot determine the application context of the ST core framework");
		}

		return temp;
	}

	public static <T extends Settings> Class<T> getSettingsClass(SettingsManager settingsManager, Scope scope) {
		Class<?> managerTarget;

		switch (scope) {
		case SYSTEM:
			managerTarget = SystemSettingsManager.class;
			break;
		case PROJECT:
			managerTarget = ProjectSettingsManager.class;
			break;
		case USER:
			managerTarget = UserSettingsManager.class;
			break;
		case PROJECT_USER:
			managerTarget = PUSettingsManager.class;
			break;
		default:
			throw new IllegalArgumentException("Unrecognized scope: " + scope); // it should not happen
		}

		return ReflectionUtilities.getInterfaceArgumentTypeAsClass(settingsManager.getClass(), managerTarget,
				0);
	}

	public static Settings createSettings(SettingsManager settingsManager, Scope scope, ObjectNode settings)
			throws WrongPropertiesException, STPropertyAccessException {
		@Nullable
		ExtensionPointManager exptManager = ExtensionPointManagerHolder.getExtensionPointManager();

		Class<Settings> settingsClass = getSettingsClass(settingsManager, scope);
		return STPropertiesManager.loadSTPropertiesFromObjectNodes(settingsClass, false, STPropertiesManager.createObjectMapper(exptManager), settings);
	}

	/**
	 * Returns the {@link World} that should be used by the provided settings manager. If {@link SettingsManager#isStatic()},
	 * then the operation returns {@link World#MAIN}; otherwise, the operation returns {@link MultiverseManager#getCurrentWorld()}.
	 *
	 * @param settingsManager
	 * @return
	 */
	public static World getRelevantWorld(SettingsManager settingsManager) {
		if (settingsManager.isStatic()) {
			return World.MAIN;
		} else {
			return MultiverseManager.getCurrentWorld();
		}
	}
}
