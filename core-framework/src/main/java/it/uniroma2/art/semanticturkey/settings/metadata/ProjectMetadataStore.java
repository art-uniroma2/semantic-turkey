package it.uniroma2.art.semanticturkey.settings.metadata;

import it.uniroma2.art.semanticturkey.extension.settings.ProjectSettingsManager;
import it.uniroma2.art.semanticturkey.extension.settings.SystemSettingsManager;
import org.springframework.stereotype.Component;

/**
 * A storage for project metadata
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
@Component
public class ProjectMetadataStore implements SystemSettingsManager<StoredProjectMetadata>,
		ProjectSettingsManager<StoredProjectMetadata> {

	/*
	 (non-Javadoc) made static because this metadata are stored globally (regardless of the actual world) in the MDR
	 */
	@Override
	public boolean isStatic() {
		return true;
	}

	@Override
	public String getId() {
		return ProjectMetadataStore.class.getName();
	}

}
