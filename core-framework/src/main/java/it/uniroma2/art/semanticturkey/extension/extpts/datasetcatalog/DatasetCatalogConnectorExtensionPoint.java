package it.uniroma2.art.semanticturkey.extension.extpts.datasetcatalog;

import it.uniroma2.art.semanticturkey.extension.ExtensionPoint;
import it.uniroma2.art.semanticturkey.resources.Scope;
import org.springframework.stereotype.Component;

@Component
public class DatasetCatalogConnectorExtensionPoint implements ExtensionPoint {

	@Override
	public Class<?> getInterface() {
		return DatasetCatalogConnector.class;
	}

	@Override
	public Scope getScope() {
		return Scope.SYSTEM;
	}

}
