package it.uniroma2.art.semanticturkey.settings.facets;

import it.uniroma2.art.semanticturkey.extension.settings.ProjectSettingsManager;
import org.springframework.stereotype.Component;

/**
 * A storage for project facets.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
@Component
public class ProjectFacetsStore implements ProjectSettingsManager<ProjectFacets> {

	@Override
	public String getId() {
		return ProjectFacetsStore.class.getName();
	}

	/*
	  (Non-Javadoc) project facets are static, in that this is a global settings that should not change across worlds;
	  moreover, there is the technical problem that facets are store in a Lucene index, which is not aware about the
	  multiverse.
	 */
	@Override
	public boolean isStatic() {
		return true;
	}
}
