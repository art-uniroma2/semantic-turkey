package it.uniroma2.art.semanticturkey.config.customservice;

import it.uniroma2.art.semanticturkey.extension.SystemScopedConfigurableComponent;
import org.springframework.stereotype.Component;

/**
 * A storage for custom service definitions.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 * 
 */
@Component
public class CustomServiceDefinitionStore implements SystemScopedConfigurableComponent<CustomService> {

	@Override
	public String getId() {
		return CustomServiceDefinitionStore.class.getName();
	}

}
