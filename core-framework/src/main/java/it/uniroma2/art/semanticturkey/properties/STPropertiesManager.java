package it.uniroma2.art.semanticturkey.properties;

import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactoryBuilder;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import it.uniroma2.art.semanticturkey.extension.ExtensionPointManager;
import it.uniroma2.art.semanticturkey.extension.ExtensionPointManagerHolder;
import it.uniroma2.art.semanticturkey.extension.NoSuchSettingsManager;
import it.uniroma2.art.semanticturkey.multiverse.World;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.properties.yaml.RDF4JBNodeDeserializer;
import it.uniroma2.art.semanticturkey.properties.yaml.RDF4JIRIDeserializer;
import it.uniroma2.art.semanticturkey.properties.yaml.RDF4JLiteralDeserializer;
import it.uniroma2.art.semanticturkey.properties.yaml.RDF4JResourceDeserializer;
import it.uniroma2.art.semanticturkey.properties.yaml.RDF4JValueDeserializer;
import it.uniroma2.art.semanticturkey.properties.yaml.RDF4JValueSerializer;
import it.uniroma2.art.semanticturkey.properties.yaml.STPropertiesPersistenceDeserializer;
import it.uniroma2.art.semanticturkey.properties.yaml.STPropertiesPersistenceSerializer;
import it.uniroma2.art.semanticturkey.resources.Resources;
import it.uniroma2.art.semanticturkey.resources.Scope;
import it.uniroma2.art.semanticturkey.user.ProjectUserBindingsManager;
import it.uniroma2.art.semanticturkey.user.STUser;
import it.uniroma2.art.semanticturkey.user.UsersGroup;
import jakarta.annotation.Nullable;
import org.apache.commons.lang3.ObjectUtils;
import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.LoaderOptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class STPropertiesManager {

    private static final Logger logger = LoggerFactory.getLogger(STPropertiesManager.class);

    private static final String USER_SETTINGS_FILE_NAME = "settings.props";
    private static final String PU_SETTINGS_FILE_NAME = "settings.props";
    private static final String PG_SETTINGS_FILE_NAME = "settings.props";
    private static final String PROJECT_SETTINGS_FILE_NAME = "settings.props";
    private static final String SYSTEM_SETTINGS_FILE_NAME = "settings.props";

    private static final String USER_SETTINGS_DEFAULTS_FILE_NAME = "user-settings-defaults.props";

    private static final String PU_SETTINGS_USER_DEFAULTS_FILE_NAME = "pu-settings-defaults.props";
    private static final String PU_SETTINGS_PROJECT_DEFAULTS_FILE_NAME = "pu-settings-defaults.props";
    private static final String PU_SETTINGS_SYSTEM_DEFAULTS_FILE_NAME = "pu-settings-defaults.props";

    public static final String PROJECT_SETTINGS_DEFAULTS_FILE_NAME = "project-settings-defaults.props";

    public static final String SETTINGS_TYPE_PROPERTY = "@type";

    @Nullable
    private static Integer snakeYAMLCodePointLimit = null;

    //// Setter/Getter for settings and defaults at each level

    /*
     * Getter/Setter <STData>/pu_binding/<projectname>/<username>/plugins/<plugin>/settings.props
     */

    /**
     * Returns the pu_settings about a plugin. If the setting has no value for the
     * user, it looks for the value in the following order:
     * <ul>
     * <li>the value for the group of the user (if any)</li>
     * <li>the default value at project level</li>
     * <li>the default value at user level</li>
     * <li>the default value at system level.</li>
     * </ul>
     *
     * @param valueType
     * @param world
     * @param project
     * @param user
     * @param pluginID
     * @throws STPropertyAccessException
     */
    public static <T extends STProperties> T getPUSettings(Class<T> valueType, World world, Project project, STUser user,
                                                           String pluginID) throws STPropertyAccessException {
        return getPUSettings(valueType, world, project, user, pluginID, false);
    }

    public static <T extends STProperties> T getPUSettings(Class<T> valueType, World world, Project project, STUser user,
                                                           String pluginID, boolean explicit) throws STPropertyAccessException {
        File propFile = getPUSettingsFile(world, project, user, pluginID);

        if (explicit) {
            return loadSettings(valueType, propFile);
        } else {
            List<File> propFiles = new ArrayList<>(5);

            File systemDefaultPropFile = getPUSettingsSystemDefaultsFile(world, pluginID);
            File userDefaultPropFile = getPUSettingsUserDefaultsFile(world, user, pluginID);
            File projectDefaultPropFile = getPUSettingsProjectDefaultsFile(world, project, pluginID);

            // adds the "scopes" from the lowest to the highest priority: system, user, project, project-group, pu (explicit)

            propFiles.add(systemDefaultPropFile);
            propFiles.add(userDefaultPropFile);
            propFiles.add(projectDefaultPropFile);

            UsersGroup group = ProjectUserBindingsManager.getUserGroup(user, project);
            if (group != null) {
                File pgFile = getPGSettingsFile(world, project, group, pluginID);
                propFiles.add(pgFile);
            }

            propFiles.add(propFile);


            return loadSettings(valueType, propFiles.toArray(new File[0]));
        }
    }

    /**
     * Sets the values of pu_setting related to the given project-user-plugin
     *
     * @param preferences
     * @param world
     * @param project
     * @param user
     * @param pluginID
     * @param allowIncompletePropValueSet
     */
    public static void setPUSettings(STProperties preferences, World world, Project project, STUser user, String pluginID,
                                     boolean allowIncompletePropValueSet) throws STPropertyUpdateException {
        try {
            STPropertiesChecker preferencesChecker = STPropertiesChecker
                    .getModelConfigurationChecker(preferences).allowIncomplete(allowIncompletePropValueSet);
            if (!preferencesChecker.isValid()) {
                throw new InvalidSettingsUpdateException(preferencesChecker.getErrorMessage());
            }
            File propFile = getPUSettingsFile(world, project, user, pluginID);
            storeSTPropertiesInYAML(preferences, propFile, false);
        } catch (IOException e) {
            throw new STPropertyUpdateException(e);
        }
    }

    /*
     * Getter/Setter <STData>/pg_binding/<projectname>/<group>/plugins/<plugin>/settings.props
     */

    public static <T extends STProperties> T getPGSettings(Class<T> valueType, World world, Project project, UsersGroup group,
                                                           String pluginID, boolean explicit) throws STPropertyAccessException {
        File propFile = getPGSettingsFile(world, project, group, pluginID);
        return loadSettings(valueType, propFile);

    }

    /**
     * Sets the values of pg_setting related to the given project-group-plugin
     *
     * @param preferences
     * @param world
     * @param project
     * @param group
     * @param pluginID
     * @param allowIncompletePropValueSet
     */
    public static void setPGSettings(STProperties preferences, World world, Project project, UsersGroup group,
                                     String pluginID, boolean allowIncompletePropValueSet) throws STPropertyUpdateException {
        try {
            STPropertiesChecker preferencesChecker = STPropertiesChecker
                .getModelConfigurationChecker(preferences).allowIncomplete(allowIncompletePropValueSet);
            if (!preferencesChecker.isValid()) {
                throw new InvalidSettingsUpdateException(preferencesChecker.getErrorMessage());
            }

            File propFile = getPGSettingsFile(world, project, group, pluginID);
            storeSTPropertiesInYAML(preferences, propFile, false);
        } catch (IOException e) {
            throw new STPropertyUpdateException(e);
        }
    }

    /*
     * Getter/Setter <STData>/projects/<projectname>/plugins/<plugin>/pu-settings-defaults.props
     */

    /**
     * Returns the value of a default pu_settings at project level
     *
     * @param valueType
     * @param world
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    public static <T extends STProperties> T getPUSettingsProjectDefault(Class<T> valueType, World world, Project project, String pluginID) throws STPropertyAccessException {
        File defaultPropFile = getPUSettingsProjectDefaultsFile(world, project, pluginID);

        return loadSettings(valueType, defaultPropFile);
    }

    /**
     * Returns the value of a default pu_settings at user level
     *
     * @param valueType
     * @param world
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    public static <T extends STProperties> T getPUSettingsUserDefault(Class<T> valueType, World world, STUser user, String pluginID) throws STPropertyAccessException {
        File defaultPropFile = getPUSettingsUserDefaultsFile(world, user, pluginID);

        return loadSettings(valueType, defaultPropFile);
    }

    /**
     * Returns the value of a default pu_settings at system level
     *
     * @param valueType
     * @param world
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    public static <T extends STProperties> T getPUSettingsSystemDefault(Class<T> valueType, World world, String pluginID) throws STPropertyAccessException {
        File defaultPropFile = getPUSettingsSystemDefaultsFile(world, pluginID);

        return loadSettings(valueType, defaultPropFile);
    }

    /**
     * Sets the value of a default pu-setting at project level.
     *
     * @param settings
     * @param world
     * @param project
     * @param pluginID
     * @param allowIncompletePropValueSet
     */
    public static void setPUSettingsProjectDefault(STProperties settings, World world, Project project, String pluginID,
                                                   boolean allowIncompletePropValueSet) throws STPropertyUpdateException {
        try {
            STPropertiesChecker settingsChecker = STPropertiesChecker
                    .getModelConfigurationChecker(settings).allowIncomplete(allowIncompletePropValueSet);
            if (!settingsChecker.isValid()) {
                throw new InvalidSettingsUpdateException(settingsChecker.getErrorMessage());
            }
            File settingsFile = getPUSettingsProjectDefaultsFile(world, project, pluginID);
            storeSTPropertiesInYAML(settings, settingsFile, false);
        } catch (IOException e) {
            throw new STPropertyUpdateException(e);
        }
    }

    /*
     * Getter/Setter <STData>/users/<username>/plugins/<plugin>/pu_settings-defaults.props
     */

    /**
     * Sets the value of a default pu-setting at user level.
     *
     * @param settings
     * @param world
     * @param user
     * @param pluginID
     * @param allowIncompletePropValueSet
     */
    public static void setPUSettingsUserDefault(STProperties settings, World world, STUser user, String pluginID,
                                                boolean allowIncompletePropValueSet) throws STPropertyUpdateException {
        try {
            STPropertiesChecker settingsChecker = STPropertiesChecker
                    .getModelConfigurationChecker(settings).allowIncomplete(allowIncompletePropValueSet);
            if (!settingsChecker.isValid()) {
                throw new InvalidSettingsUpdateException(settingsChecker.getErrorMessage());
            }
            File settingsFile = getPUSettingsUserDefaultsFile(world, user, pluginID);
            storeSTPropertiesInYAML(settings, settingsFile, false);
        } catch (IOException e) {
            throw new STPropertyUpdateException(e);
        }
    }

    /*
     * Getter/Setter <STData>/system/plugins/<plugin>/project-preference-defaults.props
     */

    /**
     * Sets the value of a default pu-setting at system level.
     *
     * @param settings
     * @param world
     * @param pluginID
     * @param allowIncompletePropValueSet
     */
    public static void setPUSettingsSystemDefault(STProperties settings, World world, String pluginID,
                                                  boolean allowIncompletePropValueSet) throws STPropertyUpdateException {
        try {
            STPropertiesChecker settingsChecker = STPropertiesChecker
                    .getModelConfigurationChecker(settings).allowIncomplete(allowIncompletePropValueSet);
            if (!settingsChecker.isValid()) {
                throw new InvalidSettingsUpdateException(settingsChecker.getErrorMessage());
            }
            File settingsFile = getPUSettingsSystemDefaultsFile(world, pluginID);
            storeSTPropertiesInYAML(settings, settingsFile, false);
        } catch (IOException e) {
            throw new STPropertyUpdateException(e);
        }
    }

    /*
     * Getter/Setter <STData>/users/<username>/plugins/<plugin>/settings.props
     */

    public static <T extends STProperties> T getUserSettings(Class<T> valueType, World world, STUser user, String pluginID,
                                                             boolean explicit) throws STPropertyAccessException {
        File propFile = getUserSettingsFile(world, user, pluginID);
        File defaultPropFile = getUserSettingsDefaultsFile(world, pluginID);

        if (explicit) {
            return loadSettings(valueType, propFile);
        } else {
            return loadSettings(valueType, defaultPropFile, propFile);
        }
    }

    public static void setUserSettings(STProperties preferences, World world, STUser user, String pluginID,
                                       boolean allowIncompletePropValueSet) throws STPropertyUpdateException {
        try {
            STPropertiesChecker preferencesChecker = STPropertiesChecker
                    .getModelConfigurationChecker(preferences).allowIncomplete(allowIncompletePropValueSet);
            if (!preferencesChecker.isValid()) {
                throw new InvalidSettingsUpdateException(preferencesChecker.getErrorMessage());
            }
            File settingsFile = getUserSettingsFile(world, user, pluginID);
            storeSTPropertiesInYAML(preferences, settingsFile, false);
        } catch (IOException e) {
            throw new STPropertyUpdateException(e);
        }
    }

    /*
     * Getter/Setter <STData>/system/plugins/<plugin>/system-preference-defaults.props
     */

    /**
     * Returns the value of a default user settings at system level
     *
     * @param valueType
     * @param world
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    public static <T extends STProperties> T getUserSettingsDefault(Class<T> valueType, World world, String pluginID) throws STPropertyAccessException {
        File defaultPropFile = getUserSettingsDefaultsFile(world, pluginID);

        return loadSettings(valueType, defaultPropFile);
    }

    /**
     * Sets the value of a default user setting at system level.
     *
     * @param settings
     * @param world
     * @param pluginID
     * @param allowIncompletePropValueSet
     */
    public static void setUserSettingsDefault(STProperties settings, World world, String pluginID,
                                              boolean allowIncompletePropValueSet) throws STPropertyUpdateException {
        try {
            STPropertiesChecker settingsChecker = STPropertiesChecker
                    .getModelConfigurationChecker(settings).allowIncomplete(allowIncompletePropValueSet);
            if (!settingsChecker.isValid()) {
                throw new InvalidSettingsUpdateException(settingsChecker.getErrorMessage());
            }
            File settingsFile = getUserSettingsDefaultsFile(world, pluginID);
            storeSTPropertiesInYAML(settings, settingsFile, false);
        } catch (IOException e) {
            throw new STPropertyUpdateException(e);
        }
    }


    /*
     * Getter/Setter <STData>/projects/<projectname>/plugins/<plugin>/settings.props
     */


    /**
     * Returns the project settings about a plugin. The returned settings are (in descending order of
     * priority):
     * <ul>
     * <li>the values stored in the project-settings for the plugin</li>
     * <li>the default values stored at the system level</li>
     * <li>the default value hard-wired in the provided {@link STProperties} object</li>
     * </ul>
     *
     * @param valueType
     * @param world
     * @param project
     * @param pluginID
     * @throws STPropertyAccessException
     */
    // TODO: remove external references
    public static <T extends STProperties> T getProjectSettings(Class<T> valueType, World world, Project project,
                                                                String pluginID) throws STPropertyAccessException {
        return getProjectSettings(valueType, world, project, pluginID, false);
    }

    /**
     * Returns the project settings about a plugin. The returned settings are (in descending order of
     * priority):
     * <ul>
     * <li>the values stored in the project-settings for the plugin</li>
     * <li>the default values stored at the system level</li>
     * <li>the default value hard-wired in the provided {@link STProperties} object</li>
     * </ul>
     *
     * @param valueType
     * @param world
     * @param project
     * @param pluginID
     * @param explicit
     * @throws STPropertyAccessException
     */
    public static <T extends STProperties> T getProjectSettings(Class<T> valueType, World world, Project project,
                                                                String pluginID, boolean explicit) throws STPropertyAccessException {
        File defaultPropFile = getProjectSettingsDefaultsFile(world, pluginID);
        File propFile = getProjectSettingsFile(world, project, pluginID);

        if (explicit) {
            return loadSettings(valueType, propFile);
        } else {
            return loadSettings(valueType, defaultPropFile, propFile);
        }
    }

    /**
     * Returns the value of a default project settings at system level
     *
     * @param valueType
     * @param world
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    // TODO: remove external references
    public static <T extends STProperties> T getProjectSettingsDefault(Class<T> valueType, World world, String pluginID) throws STPropertyAccessException {
        File defaultPropFile = getProjectSettingsDefaultsFile(world, pluginID);

        return loadSettings(valueType, defaultPropFile);
    }

    /**
     * Sets the values of project settings related to a plugin.
     *
     * @param settings
     * @param world
     * @param project
     * @param pluginID
     * @param allowIncompletePropValueSet
     */
    // TODO: remove external references
    public static void setProjectSettings(STProperties settings, World world, Project project, String pluginID,
                                          boolean allowIncompletePropValueSet) throws STPropertyUpdateException {
        try {
            STPropertiesChecker settingsChecker = STPropertiesChecker
                    .getModelConfigurationChecker(settings).allowIncomplete(allowIncompletePropValueSet);
            if (!settingsChecker.isValid()) {
                throw new InvalidSettingsUpdateException(settingsChecker.getErrorMessage());
            }
            File settingsFile = getProjectSettingsFile(world, project, pluginID);
            storeSTPropertiesInYAML(settings, settingsFile, false);
        } catch (IOException e) {
            throw new STPropertyUpdateException(e);
        }
    }

    /*
     * Getter/Setter <STData>/system/plugins/<plugin>/project-settings-defaults.props
     */

    /**
     * Sets the value of a default project setting at system level.
     *
     * @param settings
     * @param world
     * @param pluginID
     * @param allowIncompletePropValueSet
     */
    // TODO: remove external references
    public static void setProjectSettingsDefault(STProperties settings, World world, String pluginID,
                                                 boolean allowIncompletePropValueSet) throws STPropertyUpdateException {
        try {
            STPropertiesChecker settingsChecker = STPropertiesChecker
                    .getModelConfigurationChecker(settings).allowIncomplete(allowIncompletePropValueSet);
            if (!settingsChecker.isValid()) {
                throw new InvalidSettingsUpdateException(settingsChecker.getErrorMessage());
            }

            File settingsFile = getProjectSettingsDefaultsFile(world, pluginID);
            storeSTPropertiesInYAML(settings, settingsFile, false);
        } catch (IOException e) {
            throw new STPropertyUpdateException(e);
        }
    }

    /*
     * SYSTEM SETTING
     */

    /*
     * Getter/Setter <STData>/system/plugins/<plugin>/settings.props
     */
    // TODO: remove external references
    public static <T extends STProperties> T getSystemSettings(Class<T> valueType, String pluginID)
            throws STPropertyAccessException {
        return loadSettings(valueType, getSystemSettingsFile(pluginID));
    }

    public static void setSystemSettings(STProperties settings, String pluginID)
            throws STPropertyUpdateException {
        setSystemSettings(settings, pluginID, false);
    }

    public static void setSystemSettings(STProperties settings, String pluginID,
                                         boolean allowIncompletePropValueSet) throws STPropertyUpdateException {
        try {
            STPropertiesChecker settingsChecker = STPropertiesChecker
                    .getModelConfigurationChecker(settings).allowIncomplete(allowIncompletePropValueSet);
            if (!settingsChecker.isValid()) {
                throw new InvalidSettingsUpdateException(settingsChecker.getErrorMessage());
            }

            File settingsFile = getSystemSettingsFile(pluginID);
            storeSTPropertiesInYAML(settings, settingsFile, false);
        } catch (IOException e) {
            throw new STPropertyUpdateException(e);
        }
    }


    //// Folder and file management. This is influenced by the Multiverse.

    /*
     * In a world other than the "main" one, the "plugins" folder is extended with the suffix "_<world>".
     * Note that system settings are NOT subject to the multiverse.
     */

    /*
     * https://semanticturkey.uniroma2.it/doc/user/settings.jsf
     * Methods to retrieve the following Properties files User Settings Defaults:
     * <STData>/system/plugins/<plugin>/user-settings-defaults.props -> User Defaults at System level
     * <STData>/system/plugins/<plugin>/pu-settings-defaults.props -> PU Defaults at System level
     * <STData>/system/plugins/<plugin>/project-settings-defaults.props -> Project Defaults at System level
     * <STData>/system/plugins/<plugin>/settings.props -> System Settings:
     * <STData>/projects/<projName>/plugins/<plugin>/pu-settings-defaults.props -> PU Defaults at Project level
     * <STData>/projects/<projName>/plugins/<plugin>/settings.props -> Project Settings
     * <STData>/users/<user>/plugins/<plugin>/pu-settings-defaults.props -> PU Defaults at User level
     * <STData>/users/<user>/plugins/<plugin>/settings.props -> User Settings
     * <STData>/pu_bindings/<projName>/<user>/plugins/<plugin>/settings.props -> PU Settings
     * <STData>/pg_bindings/<projName>/<group>/plugins/<plugin>/settings.props -> PG Settings
     */

    /**
     * Returns the Properties file <STData>/system/plugins/<plugin>/user-settings-defaults.props
     *
     * @param world
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    private static File getUserSettingsDefaultsFile(World world, String pluginID) {
        return new File(
                getSystemPropertyFolder(world, pluginID) + File.separator + USER_SETTINGS_DEFAULTS_FILE_NAME);
    }

    public static File getPUSettingsSystemDefaultsFile(World world, String pluginID) {
        return new File(
                getSystemPropertyFolder(world, pluginID) + File.separator + PU_SETTINGS_SYSTEM_DEFAULTS_FILE_NAME);
    }


    /**
     * Returns the Properties file <STData>/system/plugins_<world>/<plugin>/project-settings-defaults.props
     *
     * @param world
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    public static File getProjectSettingsDefaultsFile(World world, String pluginID) {
        return new File(
                getSystemPropertyFolder(world, pluginID) + File.separator + PROJECT_SETTINGS_DEFAULTS_FILE_NAME);
    }


    /**
     * Returns the Properties file <STData>/system/plugins/<plugin>/settings.props
     *
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    public static File getSystemSettingsFile(String pluginID) {
        return new File(getSystemPropertyFolder(World.MAIN, pluginID) + File.separator + SYSTEM_SETTINGS_FILE_NAME);
    }

    /**
     * Returns the Properties file <STData>/projects/<projName>/plugins/<plugin>/pu-settings-defaults.props
     *
     * @param world
     * @param project
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    private static File getPUSettingsProjectDefaultsFile(World world, Project project, String pluginID) {
        return new File(getProjectPropertyFolder(world, project, pluginID) + File.separator
                + PU_SETTINGS_PROJECT_DEFAULTS_FILE_NAME);
    }

    /**
     * Returns the Properties file <STData>/projects/<projName>/plugins/<plugin>/settings.props
     *
     * @param world
     * @param project
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    private static File getProjectSettingsFile(World world, Project project, String pluginID) {
        return new File(getProjectPropertyFolder(world, project, pluginID) + File.separator + PROJECT_SETTINGS_FILE_NAME);
    }

    /**
     * Returns the Properties file <STData>/users/<user>/plugins/<plugin>/pu-settings-defaults.props
     *
     * @param world
     * @param user
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    private static File getPUSettingsUserDefaultsFile(World world, STUser user, String pluginID) {
        return new File(getUserPropertyFolder(world, user, pluginID) + File.separator + PU_SETTINGS_USER_DEFAULTS_FILE_NAME);
    }

    /**
     * Returns the Properties file <STData>/users/<user>/plugins/<plugin>/settings.props
     *
     * @param world
     * @param user
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    private static File getUserSettingsFile(World world, STUser user, String pluginID) {
        return new File(getUserPropertyFolder(world, user, pluginID) + File.separator + USER_SETTINGS_FILE_NAME);
    }

    /**
     * Returns the Properties file <STData>/pu_bindings/<projName>/<user>/plugins/<plugin>/settings.props
     *
     * @param world
     * @param project
     * @param user
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    private static File getPUSettingsFile(World world, Project project, STUser user, String pluginID) {
        return new File(
                getPUBindingPropertyFolder(world, project, user, pluginID) + File.separator + PU_SETTINGS_FILE_NAME);
    }

    /**
     * Returns the Properties file <STData>/pg_bindings/<projName>/<group>/plugins/<plugin>/settings.props
     *
     * @param world
     * @param project
     * @param group
     * @param pluginID
     * @return
     * @throws STPropertyAccessException
     */
    private static File getPGSettingsFile(World world, Project project, UsersGroup group, String pluginID) {
        return new File(getPGBindingPropertyFolder(world, project, group, pluginID) + File.separator
                + PG_SETTINGS_FILE_NAME);
    }

    /*
     * Methods to retrieve the following folders: <STData>/system/plugins/<plugin>/
     * <STData>/projects/<projectname>/plugins/<plugin>/ <STData>/users/<username>/plugins/<plugin>/
     * <STData>/pu_binding/<projectname>/<username>/plugins/<plugin>/
     * <STData>/pg_binding/<projectname>/<group>/plugins/<plugin>/
     */

    /**
     * Returns the folder <STData>/system/plugins/<plugin>/
     *
     * @param pluginID
     * @return
     */
    public static File getSystemPropertyFolder(World world, String pluginID) {
        return new File(Resources.getSystemDir() + File.separator + getPluginsSubfolder(world) + File.separator + pluginID);
    }


    /**
     * Returns the folder <STData>/projects/<projectName>/plugins/<plugin>/
     *
     * @param world
     * @param project
     * @param pluginID
     * @return
     */
    public static File getProjectPropertyFolder(World world, Project project, String pluginID) {
        return new File(Resources.getProjectsDir() + File.separator + project.getName() + File.separator
                + getPluginsSubfolder(world) + File.separator + pluginID);
    }

    /**
     * Returns the "plugins" directory suitable for the provided world. The directory will be "plugins_<world>", except
     * for the main world in which case it is simply "plugins".
     *
     * @param world
     * @return
     */
    public static String getPluginsSubfolder(World world) {
        if (Objects.equals(world, World.MAIN)){
            return "plugins";
        } else {
            return "plugins_" + world.getName();
        }
    }

    /**
     * Returns the folder <STData>/users/<user>/plugins/<plugin>/
     *
     * @param world
     * @param user
     * @param pluginID
     * @return
     */
    public static File getUserPropertyFolder(World world, STUser user, String pluginID) {
        return new File(Resources.getUsersDir() + File.separator + STUser.encodeUserIri(user.getIRI())
                + File.separator + getPluginsSubfolder(world) + File.separator + pluginID);
    }

    /**
     * Returns the folder <STData>/pu_bindings/<projectName>/<user>/plugins/<plugin>/
     *
     * @param world
     * @param project
     * @param user
     * @param pluginID
     * @return
     */
    public static File getPUBindingPropertyFolder(World world, Project project, STUser user, String pluginID) {
        return new File(Resources.getProjectUserBindingsDir() + File.separator + project.getName()
                + File.separator + STUser.encodeUserIri(user.getIRI()) + File.separator + getPluginsSubfolder(world)
                + File.separator + pluginID);
    }

    /**
     * Returns the folder <STData>/pu_bindings/<projectName>/<group>/plugins/<plugin>/
     *
     * @param world
     * @param project
     * @param group
     * @param pluginID
     * @return
     */
    public static File getPGBindingPropertyFolder(World world, Project project, UsersGroup group, String pluginID) {
        return new File(Resources.getProjectGroupBindingsDir() + File.separator + project.getName()
                + File.separator + UsersGroup.encodeGroupIri(group.getIRI()) + File.separator + getPluginsSubfolder(world)
                + File.separator + pluginID);
    }

    public static void loadDefaultsFromSettings(STProperties props, Project project, STUser user) throws PropertyNotFoundException {
        @Nullable
        ExtensionPointManager exptManager = ExtensionPointManagerHolder.getExtensionPointManager();

        for (String prop : props.getProperties()) {
            Object currentValue = props.getPropertyValue(prop);
            if (ObjectUtils.isNotEmpty(currentValue)) continue; // skip properties with already a value

            Object defaultValue = getSinglePropertyDefaultFromSettings(props, project, user, exptManager, prop);
            if (ObjectUtils.isNotEmpty(defaultValue)) {
                try {
                    props.setPropertyValue(prop, defaultValue);
                } catch (WrongPropertiesException e) {
                    logger.error("An error occurred when setting the default value for a property", e);
                }
            }
        }
    }

    public static Object getSinglePropertyDefaultFromSettings(STProperties props, Project project, STUser user, ExtensionPointManager exptManager, String prop) {
        FallbackSetting fallbackSetting = null;
        try {
            fallbackSetting = (FallbackSetting) Arrays.stream(props.getAnnotations(prop)).filter(ann -> ann.annotationType() == FallbackSetting.class).findFirst().orElse(null);
        } catch (PropertyNotFoundException e) {
            logger.error("An exception occurred when retrieving the FallbackSetting", e);
            // Swallow exception
        }
        if (fallbackSetting != null) {
            String manager = fallbackSetting.manager();
            Scope scope = fallbackSetting.scope();
            String path = fallbackSetting.path();

            try {
                if (Arrays.asList(Scope.PROJECT, Scope.PROJECT_GROUP, Scope.PROJECT_USER).contains(scope)) {
                    if (project == null) return null;
                }
                Object value = exptManager.getSettings(project, user, null, manager, scope);
                for (String pathComponent : path.split("/")) {
                    if (value == null) break;
                    value = ((STProperties) value).getPropertyValue(pathComponent);
                }
                return value;
            } catch (STPropertyAccessException | NoSuchSettingsManager | PropertyNotFoundException e) {
                logger.error("An exception occurred when setting tha value of a property", e);
                // Swallow exception
            }
        }
        return null;
    }


    //// Marshalling and storage implementation

    public static ObjectMapper createObjectMapper() {
        return createObjectMapper(null);
    }

    public static ObjectMapper createObjectMapper(ExtensionPointManager exptManager) {
        LoaderOptions loaderOptions = new LoaderOptions();
        if (snakeYAMLCodePointLimit != null && snakeYAMLCodePointLimit > 0) {
            loaderOptions.setCodePointLimit(snakeYAMLCodePointLimit);
        }
        YAMLFactory fact =  YAMLFactory.builder()
                .enable(YAMLGenerator.Feature.MINIMIZE_QUOTES)
                .disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER)
                .loaderOptions(loaderOptions)
                .build();

        SimpleModule stPropsModule = new SimpleModule();
        stPropsModule.addDeserializer(Value.class, new RDF4JValueDeserializer());
        stPropsModule.addDeserializer(Resource.class, new RDF4JResourceDeserializer());
        stPropsModule.addDeserializer(BNode.class, new RDF4JBNodeDeserializer());
        stPropsModule.addDeserializer(IRI.class, new RDF4JIRIDeserializer());
        stPropsModule.addDeserializer(Literal.class, new RDF4JLiteralDeserializer());
        stPropsModule.addSerializer(new RDF4JValueSerializer());
        stPropsModule.addSerializer(new STPropertiesPersistenceSerializer());
        // see: https://stackoverflow.com/a/18405958
        stPropsModule.setDeserializerModifier(new BeanDeserializerModifier() {
            @Override
            public JsonDeserializer<?> modifyDeserializer(DeserializationConfig config,
                                                          BeanDescription beanDesc, JsonDeserializer<?> deserializer) {
                // create an STProperties deserializer targeting a given type
                if (STProperties.class.isAssignableFrom(beanDesc.getBeanClass())) {
                    return new STPropertiesPersistenceDeserializer(beanDesc.getBeanClass(), exptManager);
                }

                return deserializer;
            }
        });
        // ensures that the order of the items in a set is preserved
        stPropsModule.addAbstractTypeMapping(Set.class, LinkedHashSet.class);
        ObjectMapper mapper = new ObjectMapper(fact);
        mapper.registerModule(stPropsModule);
        return mapper;
    }

    public static ObjectNode storeSTPropertiesToObjectNode(STProperties properties, boolean storeObjType) {
        ObjectMapper mapper = createObjectMapper();
        return storeSTPropertiesToObjectNode(mapper, properties, storeObjType);
    }

    public static ObjectNode storeSTPropertiesToObjectNode(ObjectMapper mapper, STProperties properties,
                                                           boolean storeObjType) {
        ObjectNode objectNode = mapper.valueToTree(properties);

        if (storeObjType) {
            ObjectNode newObjectNode = JsonNodeFactory.instance.objectNode();
            newObjectNode.put(SETTINGS_TYPE_PROPERTY, properties.getClass().getName());
            newObjectNode.setAll(objectNode);

            objectNode = newObjectNode;
        }

        return objectNode;
    }

    public static void storeSTPropertiesInYAML(STProperties properties, File propertiesFile,
                                               boolean storeObjType) throws IOException {
        ObjectMapper mapper = createObjectMapper();
        ObjectNode objectNode = storeSTPropertiesToObjectNode(mapper, properties, storeObjType);

        storeObjectNodeInYAML(objectNode, propertiesFile);
    }

    public static void storeObjectNodeInYAML(ObjectNode objectNode, File propertiesFile) throws IOException {
        if (!propertiesFile.getParentFile().exists()) { // if path doesn't exist, first create it
            propertiesFile.getParentFile().mkdirs();
        }

        ObjectMapper mapper = createObjectMapper();
        if (objectNode.isEmpty()) { //prevent writing an empty objectNode ({}) in yaml file. Write an empty file instead
            try (FileWriter writer = new FileWriter(propertiesFile)) {
                writer.write("");
            }
        } else {
            mapper.writeValue(propertiesFile, objectNode);
        }
    }

    public static <T extends STProperties> T loadSTPropertiesFromYAMLFiles(Class<T> valueType,
                                                                           boolean loadObjType, File... propFiles) throws STPropertyAccessException {
        return loadSTPropertiesFromYAMLFiles(valueType, loadObjType, null, propFiles);
    }

    public static <T extends STProperties> T loadSTPropertiesFromYAMLFiles(Class<T> valueType,
                                                                           boolean loadObjType, ExtensionPointManager exptManager, File... propFiles)
            throws STPropertyAccessException {
        try {
            ObjectMapper objectMapper = createObjectMapper(exptManager);
            ObjectReader objReader = objectMapper.reader();

            List<ObjectNode> objs = new ArrayList<>(propFiles.length + 1);

            for (File propFile : propFiles) {
                if (!propFile.exists())
                    continue;

                try (FileInputStream is = new FileInputStream(propFile); Reader reader = new InputStreamReader(is,
                        StandardCharsets.UTF_8)) {
                    JsonNode jsonNode = objReader.readTree(reader);
                    if (jsonNode != null && !jsonNode.isMissingNode()) {
                        if (!(jsonNode instanceof ObjectNode))
                            throw new STPropertyAccessException(
                                    "YAML file not containing an object node: " + propFile);

                        objs.add((ObjectNode) jsonNode);
                    }
                } catch (JsonMappingException e) {
                    // Swallow exception due to empty property files
                    if (!(e.getPath().isEmpty() && e.getMessage().contains("end-of-input"))) {
                        throw e;
                    }
                }

            }

            if (objs.isEmpty()) {
                objs.add(objectMapper.createObjectNode()); // fallback empty object
            }
            return loadSTPropertiesFromObjectNodes(valueType, loadObjType, objectMapper, objs.toArray(new ObjectNode[0]));
        } catch (IOException e) {
            throw new STPropertyAccessException(e);
        }
    }

    public static <T extends STProperties> T loadSTPropertiesFromObjectNodes(Class<T> valueType,
                                                                             boolean loadObjType, ObjectNode... objs) throws STPropertyAccessException {
        return loadSTPropertiesFromObjectNodes(valueType, loadObjType, createObjectMapper(null), objs);
    }

    @SuppressWarnings("unchecked")
    public static <T extends STProperties> T loadSTPropertiesFromObjectNodes(Class<T> valueType,
                                                                             boolean loadObjType, ObjectMapper objectMapper, ObjectNode... objs) throws STPropertyAccessException {
        try {
            if (objs == null || objs.length < 1) {
                throw new IllegalArgumentException("Missing ObjectNode to deserialize into an STProperties");
            }

            T result = null;
            for (ObjectNode obj : objs) {
                if (!loadObjType && obj.hasNonNull(SETTINGS_TYPE_PROPERTY)) {
                    obj.remove(SETTINGS_TYPE_PROPERTY);
                }

                T aPropertyObj = objectMapper.readValue(objectMapper.treeAsTokens(obj), valueType);

                if (result == null) {
                    result = aPropertyObj;
                } else {
                    STProperties.deepMerge(result, aPropertyObj);
                }
            }

            return result;
        } catch (IOException e) {
            throw new STPropertyAccessException(e);
        }
    }

    private static <T extends STProperties> T loadSettings(Class<T> valueType, File... settingsFiles)
            throws STPropertyAccessException {
        @Nullable
        ExtensionPointManager exptManager = ExtensionPointManagerHolder.getExtensionPointManager();
        return loadSTPropertiesFromYAMLFiles(valueType, false, exptManager, settingsFiles);
    }

    public static void setSnakeYAMLCodePointLimit(@Nullable Integer value) {
        snakeYAMLCodePointLimit = value;
    }
}