package it.uniroma2.art.semanticturkey.config.customview;

import it.uniroma2.art.semanticturkey.extension.ProjectScopedConfigurableComponent;
import org.springframework.stereotype.Component;

@Component
public class CustomViewAssociationStore implements ProjectScopedConfigurableComponent<CustomViewAssociation> {

    @Override
    public String getId() {
        return CustomViewAssociationStore.class.getName();
    }
}