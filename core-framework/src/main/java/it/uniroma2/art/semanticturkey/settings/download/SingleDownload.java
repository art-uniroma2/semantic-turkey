package it.uniroma2.art.semanticturkey.settings.download;

import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.properties.Enumeration;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;

import java.util.Map;

/**
 * @author <a href="mailto:turbati@info.uniroma2.it">Andrea Turbati</a>
 */
public class SingleDownload implements STProperties {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.download.SingleDownload";

        public static final String shortName = keyBase + ".shortName";
        public static final String fileName$description = keyBase
                + ".fileName.description";
        public static final String fileName$displayName = keyBase
                + ".fileName.displayName";
        public static final String langToLocalizedMap$description = keyBase
                + ".langToLocalizedMap.description";
        public static final String langToLocalizedMap$displayName = keyBase
                + ".langToLocalizedMap.displayName";
        public static final String timestamp$description = keyBase
                + ".timestamp.description";
        public static final String timestamp$displayName = keyBase
                + ".timestamp.displayName";
        public static final String format$description = keyBase
                + ".format.description";
        public static final String format$displayName = keyBase
                + ".format.displayName";
        public static final String distribution$description = keyBase
                + ".distribution.description";
        public static final String distribution$displayName = keyBase
                + ".distribution.displayName";
        public static final String type$description = keyBase
                + ".type.description";
        public static final String type$displayName = keyBase
                + ".type.displayName";

    }

    @Override
    public String getShortName() {
        return "{" + SingleDownload.MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + SingleDownload.MessageKeys.langToLocalizedMap$description
            + "}", displayName = "{" + SingleDownload.MessageKeys.langToLocalizedMap$displayName + "}")
    public Map<String, String> langToLocalizedMap;


    @STProperty(description = "{" + SingleDownload.MessageKeys.timestamp$description
            + "}", displayName = "{" + SingleDownload.MessageKeys.timestamp$displayName + "}")
    public long timestamp;


    @STProperty(description = "{" + SingleDownload.MessageKeys.type$description
            + "}", displayName = "{" + SingleDownload.MessageKeys.type$displayName + "}")
    @Enumeration({"local", "external"})
    public DownloadType type = DownloadType.local;

    @STProperty(description = "{" + SingleDownload.MessageKeys.distribution$description
            + "}", displayName = "{" + SingleDownload.MessageKeys.distribution$displayName + "}")
    public Boolean distribution = true;

    /**
     * If type local => name of the local stored file
     * If type external => link to the dist
     */
    @STProperty(description = "{" + SingleDownload.MessageKeys.fileName$description
            + "}", displayName = "{" + SingleDownload.MessageKeys.fileName$displayName + "}")
    public String fileName;

    @STProperty(description = "{" + SingleDownload.MessageKeys.format$description
            + "}", displayName = "{" + SingleDownload.MessageKeys.format$displayName + "}")
    public String format;


}
