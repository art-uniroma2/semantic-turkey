package it.uniroma2.art.semanticturkey.email;

import it.uniroma2.art.semanticturkey.settings.core.SemanticTurkeyCoreSettingsManager;

public class EmailServiceFactory {

    public static EmailService getService(EmailApplicationContext ctx, SemanticTurkeyCoreSettingsManager coreSettingsManager) {
        if (ctx == EmailApplicationContext.SHOWVOC) {
            return new ShowVocEmailService(coreSettingsManager);
        } else {
            return new VbEmailService(coreSettingsManager);
        }
    }

}
