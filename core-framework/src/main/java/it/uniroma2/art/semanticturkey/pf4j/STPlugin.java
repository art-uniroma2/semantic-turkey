package it.uniroma2.art.semanticturkey.pf4j;

import it.uniroma2.art.semanticturkey.spring.STBaseServiceConfiguration;

import org.pf4j.PluginWrapper;
import org.pf4j.spring.SpringPlugin;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotatedBeanDefinitionReader;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;

import java.util.Optional;
import java.util.UUID;

public class STPlugin extends SpringPlugin {

    private final String extensionPath;
    private final boolean extensionPathMapped;

    public STPlugin(PluginWrapper wrapper) {
        this(wrapper, UUID.randomUUID().toString(), true);
    }

    public STPlugin(PluginWrapper wrapper, String extensionPath, boolean extensionPathMapped) {
        super(wrapper);
        this.extensionPath = extensionPath;
        this.extensionPathMapped = extensionPathMapped;
    }

/*
    @Override
    public void start() {
        super.start();
        createApplicationContext(); // force creation of the application context
    }
*/

    public String getExtensionPath() {
        return extensionPath;
    }

    public boolean isExtensionPathMapped() {
        return extensionPathMapped;
    }

    protected Class<?> getConfigurationClass() {
        return null;
    }

    @Override
    protected ApplicationContext createApplicationContext() {
        STPluginManager pluginManager = (STPluginManager) getWrapper().getPluginManager();

        String pluginId = getWrapper().getPluginId();
        ClassLoader pluginClassLoader = getWrapper().getPluginClassLoader();

        var parentApplicationContext = pluginManager.getApplicationContext();

//        if (StringUtils.isAllBlank(extensionPath)) {
//            var applicationContext = new AnnotationConfigApplicationContext();
//            applicationContext.setParent(parentApplicationContext);
//            applicationContext.setClassLoader(pluginClassLoader);
//            applicationContext.register(BaseConfig.class);
//            // applicationContext.register(STBaseConfiguration.class);
//            Optional.ofNullable(getConfigurationClass()).ifPresent(applicationContext::register);
//            applicationContext.registerBean(STPlugin.class, () -> this);
//
//            ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(applicationContext);
//            scanner.scan(getWrapper().getPlugin().getClass().getPackageName());
//
//            return applicationContext;
//        } else {
            var applicationContext = new STPluginWebApplicationContext();
            applicationContext.setClassLoader(pluginClassLoader);
            AnnotatedBeanDefinitionReader reader = new AnnotatedBeanDefinitionReader(applicationContext);
            reader.register(BaseConfig.class);
            applicationContext.registerBean(STPlugin.class, () -> this);
            Optional.ofNullable(getConfigurationClass()).ifPresentOrElse(
                    reader::register,
                    () -> {
                        reader.register(STBaseServiceConfiguration.class);
                        ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(applicationContext);
                        scanner.scan(getWrapper().getPlugin().getClass().getPackageName());
                    });


            return applicationContext;
//        }
    }

}
