package it.uniroma2.art.semanticturkey.security;

import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.settings.core.SemanticTurkeyCoreSettingsManager;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DynamicAuthorizationFilter extends OncePerRequestFilter {

    /**
     * This filter implements the logic for requests authorization.
     * In {@link it.uniroma2.art.semanticturkey.spring.STCoreFrameworkConfiguration} any requests is permitted to all
     * (<code>permitAll</code>).
     * This filter checks if the incoming request is about a public path (allowed for any users, authenticated or not)
     * and in such case do nothing. In any other cases, if:
     * <ul>
     *     <li>user is authenticated, allows the request (it is equivalent to <code>authenticated()</code>)</li>
     *     <li>
     *         user it not authenticated, it checks the "allowAnonymouse" system setting, if:
     *         <ul>
     *             <li>true: allows request, even if unauthenticated</li>
     *             <li>false: denies request</li>
     *         </ul>
     *     </li>
     * </ul>
     * Note that <code>@PreAuthorize</code> annotation still works on any request
     */

    private final SemanticTurkeyCoreSettingsManager coreSettingsManager;
    private final STAuthenticationEntryPoint authEntryPoint;
    private final List<String> publicResourcePaths;

    public DynamicAuthorizationFilter(SemanticTurkeyCoreSettingsManager coreSettingsManager, STAuthenticationEntryPoint authEntryPoint,
            List<String> publicResourcePaths) {
        this.coreSettingsManager = coreSettingsManager;
        this.authEntryPoint = authEntryPoint;
        this.publicResourcePaths = publicResourcePaths;
    }

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull FilterChain filterChain)
            throws ServletException, IOException {

        //if the path is public, allow request
        if (isPermitAllPath(request)) {
            filterChain.doFilter(request, response);
            return;
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            // authenticated => go ahead
            filterChain.doFilter(request, response);
        } else { //not authenticated, check allowAnonymous
            boolean allowAnonymous = false;
            try {
                allowAnonymous = coreSettingsManager.getSystemSettings().allowAnonymous;
            } catch (STPropertyAccessException ignored) {}
            if (allowAnonymous) {
                //allowAnonymous true => allow all requests, even if unauthenticated
                filterChain.doFilter(request, response);
            } else {
                // not authenticated => Delegate handling to the STAuthenticationEntryPoint
                authEntryPoint.commence(request, response, new AuthenticationServiceException("Access Denied"));
            }
        }
    }


    // This method checks if the request URI is in the public paths
    private boolean isPermitAllPath(HttpServletRequest request) {
        List<AntPathRequestMatcher> permittedMatchers = new ArrayList<>();

        // Add hardcoded permitted paths
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/Auth/login"));
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/Users/getUser"));
        //new saml location patterns
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/saml2/**"));
        permittedMatchers.add(new AntPathRequestMatcher("/saml2/**"));
        //old saml location patterns (assertion consumer and metadata filter), temporary left for retro-compatibility tests
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/saml/**"));
        permittedMatchers.add(new AntPathRequestMatcher("/saml/**"));
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/Users/getUserFormFields"));
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/Users/registerUser"));
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/Users/forgotPassword"));
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/Users/resetPassword"));
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/Users/verifyUserEmail"));
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/Users/activateRegisteredUser"));
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/Settings/getStartupSettings"));
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/Administration/downloadPrivacyStatement"));
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/HttpResolution/contentNegotiation"));
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/HttpResolution/rdfProvider"));
        /* this operation still uses PreAuthorize to see whether the ctx_project is public */
        permittedMatchers.add(new AntPathRequestMatcher("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/ShowVoc/dataDump"));

        // Add dynamic publicResourcePaths to permittedMatchers
        publicResourcePaths.forEach(path -> permittedMatchers.add(new AntPathRequestMatcher(path)));

        // Check if the request matches any permitted path
        return permittedMatchers.stream().anyMatch(matcher -> matcher.matches(request));
    }

}
