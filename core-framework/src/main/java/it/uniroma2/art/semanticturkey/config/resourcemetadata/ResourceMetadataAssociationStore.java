package it.uniroma2.art.semanticturkey.config.resourcemetadata;

import it.uniroma2.art.semanticturkey.extension.ProjectScopedConfigurableComponent;
import org.springframework.stereotype.Component;

@Component
public class ResourceMetadataAssociationStore implements ProjectScopedConfigurableComponent<ResourceMetadataAssociation> {

	@Override
	public String getId() {
		return ResourceMetadataAssociationStore.class.getName();
	}

}