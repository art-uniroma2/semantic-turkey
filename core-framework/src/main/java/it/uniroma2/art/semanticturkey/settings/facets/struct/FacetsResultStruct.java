package it.uniroma2.art.semanticturkey.settings.facets.struct;

import it.uniroma2.art.semanticturkey.settings.facets.ProjectFacetsIndexLuceneUtils;

import java.util.HashMap;
import java.util.Map;

public class FacetsResultStruct {
    Map<String, Map<String, Integer>> facetToValueToCountMapMap = new HashMap<>();

    public FacetsResultStruct() {
    }

    public void addFacetValueCount(String facetName, String facetValue, int count) {
        if (!facetToValueToCountMapMap.containsKey(facetName)) {
            facetToValueToCountMapMap.put(facetName, new HashMap<>());
        }
        facetToValueToCountMapMap.get(facetName).put(facetValue, count);
    }

    public Map<String, Map<String, Integer>> getFacetToValueToCountMapMap(boolean excludeProjNameAndProjDesc) {
        Map<String, Map<String, Integer>> tempFacetToValueToCountMapMap = new HashMap<>();
        if (!excludeProjNameAndProjDesc) {
            //return ALL the facets and their values and counts
            return facetToValueToCountMapMap;
        } else {
            //exclude the PROJECT_NAME and PROJECT_DESCRIPTION
            for (String facetName : facetToValueToCountMapMap.keySet()) {
                if (!facetName.equals(ProjectFacetsIndexLuceneUtils.PROJECT_NAME) &&
                        !facetName.equals(ProjectFacetsIndexLuceneUtils.PROJECT_DESCRIPTION)) {
                    tempFacetToValueToCountMapMap.put(facetName, facetToValueToCountMapMap.get(facetName));
                }
            }
            return tempFacetToValueToCountMapMap;
        }
    }
}
