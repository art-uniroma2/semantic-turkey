package it.uniroma2.art.semanticturkey.event.support;

import it.uniroma2.art.semanticturkey.event.Event;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.ApplicationListener;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * An {@link ApplicationListener} that listens for {@link Event} objects and propagates them to other plugins application
 * contexts
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class CrossContextPropagationEventListener implements ApplicationListener<Event>, ApplicationEventPublisherAware {
	private ApplicationEventPublisher applicationEventPublisher;
//
//	private ServiceTracker serviceTracker;
//
	private static Set<Event> alreadyPropagated = ConcurrentHashMap.newKeySet();

	@Override
	public void onApplicationEvent(Event event) {
//		boolean notAlreadyPropagated = alreadyPropagated.add(event);
//
//		if (notAlreadyPropagated) {
//			try {
//				Object[] eventPublishers = serviceTracker.getServices();
//
//				for (Object pub : eventPublishers) {
//					((ApplicationEventPublisher) pub).publishEvent(event);
//				}
//			} finally {
//				alreadyPropagated.remove(event);
//			}
//		}
	}

	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}
}
