package it.uniroma2.art.semanticturkey.security;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 * 
 * @author Tiziano
 * Specifies what happen when an authenticated user perform a request for which has not the required role
 * Simply returns a 403 response (forbidden) with a json content
 * (Referenced in WEB-INF/spring-security.xml)
 *
 */
public class STAccessDeniedHandler implements AccessDeniedHandler {

	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
	        throws IOException, ServletException {
		
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		ServletOutputStream out = response.getOutputStream();

		out.print("Access denied. You don't have enough privileges for this operation");
	}

}
