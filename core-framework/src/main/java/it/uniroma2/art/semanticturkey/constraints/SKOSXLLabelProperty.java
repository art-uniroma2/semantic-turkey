package it.uniroma2.art.semanticturkey.constraints;

import it.uniroma2.art.semanticturkey.validators.SKOSXLLabelPropertyValidator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * Requires that a property is subPropertyOf a given property
 * 
 * @author Tiziano Lorenzetti
 */
@Documented
@Constraint(validatedBy = SKOSXLLabelPropertyValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface SKOSXLLabelProperty {
	String message() default "{it.uniroma2.art.semanticturkey.constraints.SKOSXLLabelProperty.message}";
	
	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}