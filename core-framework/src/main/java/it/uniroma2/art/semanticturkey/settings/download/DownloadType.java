package it.uniroma2.art.semanticturkey.settings.download;

public enum DownloadType {
    local, external
}
