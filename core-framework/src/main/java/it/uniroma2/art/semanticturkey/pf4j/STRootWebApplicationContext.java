package it.uniroma2.art.semanticturkey.pf4j;

import it.uniroma2.art.semanticturkey.event.Event;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.ResolvableType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.function.Predicate.not;

public class STRootWebApplicationContext extends AnnotationConfigServletWebServerApplicationContext {
    protected Set<Event> alreadySeenEvents = ConcurrentHashMap.newKeySet();

    @Override
    protected void publishEvent(Object event, ResolvableType eventType) {
        if (event instanceof Event stEvent) {
            boolean firstTimeSeen = false;
            try {
                firstTimeSeen = alreadySeenEvents.add(stEvent);
                if (firstTimeSeen) {  // ignores ST Events that have already been processed
                    super.publishEvent(event, eventType); // usual dispatch to event listeners in this context

                    var stPluginManager = getBean(STPluginManager.class);
                    var indexedApplicationContexts = stPluginManager.getIndexedApplicationContexts();

                    indexedApplicationContexts
                            .stream()
                            .filter(not(this::equals))
                            .filter(ctx -> !ctx.getBeansOfType(STPlugin.class).isEmpty())
                            .forEach(ctx -> ctx.publishEvent(stEvent));

                }
            } finally {
                if (firstTimeSeen) {
                    alreadySeenEvents.remove(stEvent);
                }
            }

        } else {
            super.publishEvent(event, eventType);
        }
    }
}
