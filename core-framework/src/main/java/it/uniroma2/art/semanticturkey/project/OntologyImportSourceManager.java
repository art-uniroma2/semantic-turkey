package it.uniroma2.art.semanticturkey.project;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.uniroma2.art.semanticturkey.exceptions.ProjectUpdateException;
import it.uniroma2.art.semanticturkey.exceptions.ReservedPropertyUpdateException;
import it.uniroma2.art.semanticturkey.properties.STPropertyUpdateException;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class OntologyImportSourceManager {
    private Project project;

    // project -> import base uri -> source project
    private Map<String, Map<String, String>> project2OntologyImportSources;

    public OntologyImportSourceManager(Project project) {
        this.project = project;

        String sourceMapString = project.getProperty(Project.ONTOLOGY_IMPORT_SOURCES_PROP);

        ObjectMapper om = new ObjectMapper();

        if (StringUtils.isBlank(sourceMapString)) {
            project2OntologyImportSources = new HashMap<>();
        } else {
            try {
                project2OntologyImportSources = om.readValue(sourceMapString, new TypeReference<Map<String, Map<String, String>> >() {
                });
            } catch (JsonProcessingException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public synchronized  Map<String, String> getImportSources(String repo) {
        return Collections.unmodifiableMap(project2OntologyImportSources.getOrDefault(repo, Collections.emptyMap()));
    }

    public synchronized  void setImportSources(String repo, Map<String, String> importSourcesMap) throws STPropertyUpdateException {
        try {
            ObjectMapper om = new ObjectMapper();

            project2OntologyImportSources.put(repo, new HashMap<>(importSourcesMap));

            String updatedSourceMapString = om.writeValueAsString(project2OntologyImportSources);
            project.setProperty(Project.ONTOLOGY_IMPORT_SOURCES_PROP, updatedSourceMapString);
        } catch (JsonProcessingException | ProjectUpdateException | ReservedPropertyUpdateException e) {
            throw new STPropertyUpdateException(e);
        }
    }
}
