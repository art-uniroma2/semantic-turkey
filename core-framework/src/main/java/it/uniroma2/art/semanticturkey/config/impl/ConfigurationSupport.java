package it.uniroma2.art.semanticturkey.config.impl;

import it.uniroma2.art.semanticturkey.config.Configuration;
import it.uniroma2.art.semanticturkey.config.ConfigurationManager;
import it.uniroma2.art.semanticturkey.config.ConfigurationNotFoundException;
import it.uniroma2.art.semanticturkey.config.ConfigurationReflectionException;
import it.uniroma2.art.semanticturkey.config.PUConfigurationManager;
import it.uniroma2.art.semanticturkey.config.ProjectConfigurationManager;
import it.uniroma2.art.semanticturkey.config.SystemConfigurationManager;
import it.uniroma2.art.semanticturkey.config.UserConfigurationManager;
import it.uniroma2.art.semanticturkey.extension.ExtensionPointManager;
import it.uniroma2.art.semanticturkey.extension.ExtensionPointManagerHolder;
import it.uniroma2.art.semanticturkey.multiverse.MultiverseManager;
import it.uniroma2.art.semanticturkey.multiverse.World;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.properties.InvalidConfigurationUpdateException;
import it.uniroma2.art.semanticturkey.properties.STPropertiesChecker;
import it.uniroma2.art.semanticturkey.properties.STPropertiesManager;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.properties.STPropertyUpdateException;
import it.uniroma2.art.semanticturkey.properties.WrongPropertiesException;
import it.uniroma2.art.semanticturkey.user.STUser;
import it.uniroma2.art.semanticturkey.utilities.ReflectionUtilities;
import jakarta.annotation.Nullable;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import static java.util.stream.Collectors.toList;

public abstract class ConfigurationSupport {

	protected static Logger logger = LoggerFactory.getLogger(ConfigurationSupport.class);

	public static final String CONFIG_TYPE_PARAM = "@type";

	public static File getConfigurationFolder(SystemConfigurationManager<?> manager) {
		String pluginID = manager.getId();
		return STPropertiesManager.getSystemPropertyFolder(World.MAIN, pluginID);
	}

	public static File getConfigurationFolder(ProjectConfigurationManager<?> manager, Project project) {
		return STPropertiesManager.getProjectPropertyFolder(MultiverseManager.getCurrentWorld(), project, manager.getId());
	}

	public static File getConfigurationFolder(UserConfigurationManager<?> manager, STUser user) {
		return STPropertiesManager.getUserPropertyFolder(MultiverseManager.getCurrentWorld(), user, manager.getId());
	}

	public static File getConfigurationFolder(PUConfigurationManager<?> manager, Project project,
			STUser user) {
		return STPropertiesManager.getPUBindingPropertyFolder(MultiverseManager.getCurrentWorld(), project, user, manager.getId());
	}

	public static Collection<String> listConfigurationIdentifiers(File folder) {
		if (!folder.exists())
			return Collections.emptyList();
		return Arrays.stream(folder.list(new WildcardFileFilter("*.cfg")))
				.map(name -> name.substring(0, name.length() - 4 /* .cfg length */)).collect(toList());
	}

	public static <CONFTYPE extends Configuration> CONFTYPE loadConfiguration(
			ConfigurationManager<CONFTYPE> configurationManager, File folder, String identifier)
			throws STPropertyAccessException {
		File configFile = new File(folder, configurationFilename(identifier));

		Class<CONFTYPE> configBaseClass = ReflectionUtilities.getInterfaceArgumentTypeAsClass(
				configurationManager.getClass(), ConfigurationManager.class, 0);

		@Nullable
		ExtensionPointManager exptManager = ExtensionPointManagerHolder.getExtensionPointManager();

		return STPropertiesManager.loadSTPropertiesFromYAMLFiles(configBaseClass, true, exptManager, configFile);
	}

	public static Class<?> getConfigurationClass(ConfigurationManager<?> systemConfigurationManager,
			@Nullable String configType) throws ConfigurationReflectionException {
		Class<?> configClass;
		Class<?> baseConfigurationClass = ReflectionUtilities.getInterfaceArgumentTypeAsClass(
				systemConfigurationManager.getClass(), SystemConfigurationManager.class, 0);

		if (configType == null) {
			configClass = baseConfigurationClass;
		} else {
			try {
				configClass = systemConfigurationManager.getClass().getClassLoader().loadClass(configType);
				if (!baseConfigurationClass.isAssignableFrom(configClass)) {
					throw new ConfigurationReflectionException(
							"Invalid configuration class: " + configClass.getName());
				}

			} catch (ClassNotFoundException e) {
				throw new ConfigurationReflectionException(e);
			}
		}

		if (!Configuration.class.isAssignableFrom(configClass)) {
			throw new ConfigurationReflectionException("Not a Configuration class: " + configClass.getName());
		}
		return configClass;
	}

	private static String configurationFilename(String identifier) {
		return identifier + ".cfg";
	}

	public static <CONFTYPE extends Configuration> void storeConfiguration(File folder, String identifier,
			CONFTYPE configuration) throws IOException, WrongPropertiesException, STPropertyUpdateException {
		STPropertiesChecker checker = STPropertiesChecker.getModelConfigurationChecker(configuration);

		if (!checker.isValid()) {
			throw new InvalidConfigurationUpdateException(checker.getErrorMessage());
		}
		File configFile = new File(folder, configurationFilename(identifier));
		STPropertiesManager.storeSTPropertiesInYAML(configuration, configFile, true);
	}

	public static void deleteConfiguration(File folder, String identifier)
			throws ConfigurationNotFoundException {
		File configFile = new File(folder, configurationFilename(identifier));
		if (!configFile.exists())
			throw new ConfigurationNotFoundException("Unable to find configuration: " + identifier);
		if (!configFile.delete()) {
			logger.warn("Failed to delete file " + configFile.getPath());
		}
	}

	public static Configuration createConfiguration(ConfigurationManager<?> configurationManager,
			Map<String, Object> properties) throws WrongPropertiesException {
		@Nullable
		String configType = Optional.ofNullable(properties.get(CONFIG_TYPE_PARAM)).map(Object::toString)
				.orElse(null);
		if (configType != null) {
			properties.remove(CONFIG_TYPE_PARAM);
		}
		Class<?> configClass = getConfigurationClass(configurationManager, configType);
		Configuration config;
		try {
			config = (Configuration) configClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new ConfigurationReflectionException(e);
		}
		Properties props = new Properties();
		properties.forEach((k, v) -> {
			if (v != null)
				props.setProperty(k, v.toString());
		});
		config.setProperties(props);
		return config;
	}

}
