package it.uniroma2.art.semanticturkey.search;

import it.uniroma2.art.semanticturkey.extension.ExtensionPointManager;
import it.uniroma2.art.semanticturkey.services.STServiceContext;
import org.springframework.aop.Pointcut;
import org.springframework.aop.framework.AbstractAdvisingBeanPostProcessor;
import org.springframework.aop.support.ComposablePointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMethodMatcher;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;

import it.uniroma2.art.semanticturkey.aop.STServiceOperationPointcut;
import it.uniroma2.art.semanticturkey.services.annotations.Write;
import org.springframework.context.annotation.Lazy;

/**
 * A convenient {@link BeanPostProcessor} implementation that advises eligible beans (i.e. Semantic Turkey
 * services) in order to update service-related resources.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
@SuppressWarnings("serial")
public class SearchUpdatePostProcessor extends AbstractAdvisingBeanPostProcessor
		implements InitializingBean {

	private STServiceContext stServiceContext;
	private ExtensionPointManager exptManager;

	public SearchUpdatePostProcessor(STServiceContext stServiceContext, ExtensionPointManager exptManager) {
		this.stServiceContext = stServiceContext;
		this.exptManager = exptManager;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Pointcut pointcut = new ComposablePointcut(new STServiceOperationPointcut())
				.intersection(new AnnotationMethodMatcher(Write.class));
		SearchUpdateInterceptor advice = new SearchUpdateInterceptor(stServiceContext, exptManager);
		this.advisor = new DefaultPointcutAdvisor(pointcut, advice);
	}

}
