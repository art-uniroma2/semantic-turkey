package it.uniroma2.art.semanticturkey.multiverse;

import it.uniroma2.art.semanticturkey.exceptions.WorldAlreadyExistingException;
import it.uniroma2.art.semanticturkey.extension.ExtensionPointManagerHolder;
import it.uniroma2.art.semanticturkey.extension.NoSuchSettingsManager;
import it.uniroma2.art.semanticturkey.project.events.ProjectEvent;
import it.uniroma2.art.semanticturkey.properties.STPropertiesManager;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.properties.STPropertyUpdateException;
import it.uniroma2.art.semanticturkey.resources.Resources;
import it.uniroma2.art.semanticturkey.settings.core.CoreSystemSettings;
import it.uniroma2.art.semanticturkey.settings.core.SemanticTurkeyCoreSettingsManager;
import jakarta.annotation.Nullable;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.function.FailableRunnable;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Multiverse management. The multiverse consists of multiple <em>worlds</em> (including the default one, called
 * <em>main</em> [{@link World#MAIN}]), each with its own view over the settings (and their defaults).
 *
 * System settings are currently shared by all worlds, but system-level defaults are not: different worlds can have
 * different system defaults.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class MultiverseManager {

    private static Logger logger = LoggerFactory.getLogger(MultiverseManager.class);

    private static ThreadLocal<World> currentWorldHolder = ThreadLocal.withInitial(() -> World.MAIN);

    private Map<World, MutableInt> alternativeWorldUsageCount = new HashMap<>(); // should not contain a mapping for World.MAIN
    private Map<World, WorldScopedObjects> world2scopedObjectsMap = new ConcurrentHashMap<>();

    /**
     * Returns a reference to the current world.
     *
     * @return
     */
    public static World getCurrentWorld() {
        return currentWorldHolder.get();
    }

    /**
     * Sets the current world (for the current thread).
     * @param world
     */
    synchronized void setCurrentWorld(@Nullable String world) {
        var worldNames = listWorldNames(true);

        var newWorld = world == null ? World.MAIN : new World(world);
        if (!worldNames.contains(newWorld.getName())) {
            throw new IllegalArgumentException("World not existing: " + newWorld.getName());
        }

        var currentWorld = currentWorldHolder.get();

        if (currentWorld.equals(newWorld)) return; // Nothing to do

        // Update the reference to the current world
        currentWorldHolder.set(newWorld);

        // decrement the count for the world being left (if available: e.g. main is not counted)
        var currentWorldCount = alternativeWorldUsageCount.get(currentWorld);
        if (currentWorldCount != null && currentWorldCount.intValue() > 0) {
            currentWorldCount.decrement();
        }

        // increment the count for the world being entered
        alternativeWorldUsageCount.computeIfAbsent(newWorld, w -> new MutableInt(0)).increment();
    }

    /**
     * Releases the current world, and switch back to the default one.
     */
    synchronized void releaseCurrentWorld() {
        setCurrentWorld(World.MAIN_NAME);
    }

    /**
     * Creates a new world.
     *
     * @param name name of the world to create
     */
    public synchronized void createWorld(String name) throws IOException, WorldAlreadyExistingException {
        // Checks whether a world with the same name exists. Note that we can't lock the settings manager per se,
        // but using a synchronized method we prevent race conditions with concurrent invocations
        var existingWorldNames = listWorldNames(false);
        if (existingWorldNames.contains(name) || World.MAIN_NAME.equals(name)) {
            throw new WorldAlreadyExistingException(name);
        }

        // Set up the world: create the default files
        Resources.initializeSystemDefaults(new World(name));

        // Stores the new world

        existingWorldNames.add(name); // add the newly created world and store the updated list

        SemanticTurkeyCoreSettingsManager systemSettingsManager;
        CoreSystemSettings systemSettings;
        try {
            systemSettingsManager = (SemanticTurkeyCoreSettingsManager) ExtensionPointManagerHolder.getExtensionPointManager().getSettingsManager(SemanticTurkeyCoreSettingsManager.class.getName());
            systemSettings = systemSettingsManager.getSystemSettings();
            systemSettings.multiverse = existingWorldNames;
            systemSettingsManager.storeSystemSettings(systemSettings);
        } catch (NoSuchSettingsManager | STPropertyAccessException | STPropertyUpdateException e) {
            ExceptionUtils.rethrow(e); // just rethrow this exception that should never happen
            return;
        }
    }

    /**
     * Destroys a world.
     *
     * @param name name of the world to destroy.
     *
     */
    public synchronized void destroyWorld(String name) {

        //// Checks

        // Check that the world exists
        var worldNames = listWorldNames(true);

        if (!worldNames.contains(name)) {
            throw new IllegalArgumentException("World not existing: " + name);
        }

        // Check that the world is not the main one
        World world2Destroy = new World(name);
        if (World.MAIN.equals(world2Destroy)) {
            throw new IllegalArgumentException("Cannot destroy the main world");
        }

        // Check that the (alternative) world is not in use by any user
        if (alternativeWorldUsageCount.getOrDefault(world2Destroy, new MutableInt(0)).intValue() > 0) {
            throw new IllegalArgumentException("Cannot destroy world in use: " + name);
        }

        //// Operations

        // Remove the world from the multiverse stored as system settings

        try {
            var systemSettingsManager = (SemanticTurkeyCoreSettingsManager) ExtensionPointManagerHolder.getExtensionPointManager().getSettingsManager(SemanticTurkeyCoreSettingsManager.class.getName());
            var systemSettings = systemSettingsManager.getSystemSettings();
            if (SetUtils.emptyIfNull(systemSettings.multiverse).contains(world2Destroy.getName())) { // check that the world actually appears in the multiverse
                systemSettings.multiverse = new HashSet<>(systemSettings.multiverse);
                systemSettings.multiverse.remove(world2Destroy.getName());

                systemSettingsManager.storeSystemSettings(systemSettings);
            }
        } catch (STPropertyAccessException | NoSuchSettingsManager | STPropertyUpdateException e) {
            ExceptionUtils.rethrow(e); // just rethrow this exception that should never happen
        }

        try {
            // Destroys the scoped beans
            WorldScopedObjects scopedObjects = world2scopedObjectsMap.remove(world2Destroy);
            if (scopedObjects != null) {
                scopedObjects.executeDestructionCallbacks();
            }

            // Destroy the settings directories
            // 1. system folder
            FileUtils.deleteDirectory(new File(Resources.getSystemDir(), STPropertiesManager.getPluginsSubfolder(world2Destroy)));

            // 2. individual user directories
            File[] userDirectories = Resources.getUsersDir().listFiles(File::isDirectory);
            if (userDirectories != null) {
                for (File userDir : userDirectories) {
                    FileUtils.deleteDirectory(new File(userDir, STPropertiesManager.getPluginsSubfolder(world2Destroy)));

                }
            }

            // 3. individual project directories
            File[] projectDirectories = Resources.getProjectsDir().listFiles(File::isDirectory);
            if (projectDirectories != null) {
                for (File projectDir : projectDirectories) {
                    FileUtils.deleteDirectory(new File(projectDir, STPropertiesManager.getPluginsSubfolder(world2Destroy)));
                }
            }

            // 4. project-user binding directories
            // ...for each project
            File[] projectUsersBindingDirectories = Resources.getProjectUserBindingsDir().listFiles(File::isDirectory);
            if (projectUsersBindingDirectories != null) {
                for (File projectUsersBindingDir : projectUsersBindingDirectories) {
                    // ... for each user
                    File[] puBindingDirectories = projectUsersBindingDir.listFiles(File::isDirectory);
                    if (puBindingDirectories != null) {
                        for (File puBindingDir : puBindingDirectories) {
                            FileUtils.deleteDirectory(new File(puBindingDir, STPropertiesManager.getPluginsSubfolder(world2Destroy)));
                        }
                    }
                }
            }

            // 5. project-group binding directories
            // ...for each project
            File[] projectGroupsBindingDirectories = Resources.getProjectGroupBindingsDir().listFiles(File::isDirectory);
            if (projectGroupsBindingDirectories != null) {
                for (File projectGroupBindingDir : projectGroupsBindingDirectories) {
                    // ... for each group
                    File[] pgBindingDirectories = projectGroupBindingDir.listFiles(File::isDirectory);
                    if (pgBindingDirectories != null) {
                        for (File pgBindingDir : pgBindingDirectories) {
                            FileUtils.deleteDirectory(new File(pgBindingDir, STPropertiesManager.getPluginsSubfolder(world2Destroy)));
                        }
                    }
                }
            }
        } catch (IOException e) {
            ExceptionUtils.rethrow(e); // just rethrow this exception that should never happen
        }


        // Remove the usage count
        alternativeWorldUsageCount.remove(world2Destroy);
    }


    /**
     * Lists the names of the worlds that make up the multiverse. Optionally, include the default one.
     *
     * @param mainIncluded
     * @return
     */
    public synchronized Set<String> listWorldNames(boolean mainIncluded) {
        SemanticTurkeyCoreSettingsManager systemSettingsManager;
        CoreSystemSettings systemSettings;
        try {
            systemSettingsManager = (SemanticTurkeyCoreSettingsManager) ExtensionPointManagerHolder.getExtensionPointManager().getSettingsManager(SemanticTurkeyCoreSettingsManager.class.getName());
            systemSettings = systemSettingsManager.getSystemSettings();
            var temp = new HashSet<>(SetUtils.emptyIfNull(systemSettings.multiverse));
            if (mainIncluded) {
                temp.add(World.MAIN_NAME);
            }
            return temp;
        } catch (NoSuchSettingsManager | STPropertyAccessException e) {
            ExceptionUtils.rethrow(e); // just rethrow this exception that should never happen
            return Collections.emptySet();
        }

    }

    /**
     * Overload of {@link #listWorldNames(boolean)} with the parameter set to false
     *
     * @return
     */
    public synchronized Set<String> listWorldNames() {
        return listWorldNames(false);
    }

    /**
     * Returns concise information about the alternative worlds (i.e. the default world is excluded)
     *
     * @return
     */
    public synchronized Collection<WorldInfo> listAlternativeWorldInfos() {
        return listWorldNames() // excluded MAIN
                .stream().map(worldName -> {
            var worldObj = new World(worldName);
            var userCountHolder = alternativeWorldUsageCount.get(worldObj);
            var userCount = userCountHolder == null ?  0 : userCountHolder.intValue();
            return new WorldInfo(worldName, userCount);
        }).collect(Collectors.toList());
    }

    /**
     * Returns the objects scoped to a given world
     *
     * @param world
     * @return
     */
    protected WorldScopedObjects getWorldScopedObjects(World world) {
        // we do not check that the world indeed exists, because we assume that this method is only invoked after the
        // existence has been checked
        return world2scopedObjectsMap.computeIfAbsent(world, w -> new WorldScopedObjects());
    }

    /**
     * Executes the provided @{@link Runnable} in each world in the multiverse.
     * @param runnable
     * @throws Throwable
     */
    protected void executeInAllWorlds(FailableRunnable<Throwable> runnable) throws Throwable{
        // increases all alternative worlds count, thus ensuring that they can't be deleted in the meanwhile
        List<World> alternateWorlds;
        synchronized (this) {
            alternateWorlds = listWorldNames(false).stream().map(World::new).toList();
            alternateWorlds.forEach(w -> alternativeWorldUsageCount.computeIfAbsent(w, (world -> new MutableInt(0))).increment());
        }
        RuntimeException globalException;
        MutableBoolean suppressed;
        World currentWorldTemp = currentWorldHolder.get();
        logger.atTrace().setMessage("Initial world: {}").addArgument(()->MultiverseManager.getCurrentWorld().getName()).log();

        try {
            globalException = new RuntimeException("Exception suppressed on multiverse-replicated method invocation");
            suppressed = new MutableBoolean(false);
            Stream.concat(Stream.of(World.MAIN), alternateWorlds.stream()).forEach(
                    w -> {
                        try {
                            logger.trace("Executing method in world {}", w.getName());
                            currentWorldHolder.set(w); // temporarily switch to a given world
                            runnable.run();
                        } catch (Throwable e) {
                            e.addSuppressed(e);
                            suppressed.setTrue();
                        }
                    }
            );
        } finally {
            // restore original current world
           currentWorldHolder.set(currentWorldTemp);
           logger.atTrace().setMessage("Restored world: {}").addArgument(()->MultiverseManager.getCurrentWorld().getName()).log();

            // decreases all alternative worlds count
            synchronized (this) {
                alternateWorlds.forEach(w -> alternativeWorldUsageCount.get(w).decrement());
            }
        }

        if (suppressed.isTrue()) {
            throw globalException;
        }
    }

    protected void onProjectEvent(ProjectEvent event) {

    }
}
