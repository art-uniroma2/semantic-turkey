package it.uniroma2.art.semanticturkey.pf4j;

import it.uniroma2.art.semanticturkey.extension.impl.ExtensionPointManagerImpl;
import it.uniroma2.art.semanticturkey.pf4j.events.AllPluginsInitialized;
import it.uniroma2.art.semanticturkey.pf4j.events.PluginInitialized;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.MultipartConfigElement;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import org.apache.commons.io.file.PathUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.pf4j.DevelopmentPluginRepository;
import org.pf4j.ExtensionFactory;
import org.pf4j.PluginRepository;
import org.pf4j.spring.SpringExtensionFactory;
import org.pf4j.spring.SpringPluginManager;
import org.pf4j.util.AndFileFilter;
import org.pf4j.util.NameFileFilter;
import org.pf4j.util.NotFileFilter;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.servlet.DispatcherServlet;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class STPluginManager extends SpringPluginManager implements ServletContextInitializer {
    private ApplicationContext context;
    private ExtensionPointManagerImpl exptMgr;
    private Set<ApplicationContext> indexedApplicationContexts = new HashSet<>();
    protected Set<ApplicationContext> applicationContextsWaitingForInitialization = new HashSet<>();

    private ObjectProvider<MultipartConfigElement> multipartConfigElement;

    public STPluginManager(ApplicationContext context, ExtensionPointManagerImpl exptMgr, ObjectProvider<MultipartConfigElement> multipartConfigElement) {
        this.context = context;
        this.exptMgr = exptMgr;
        this.multipartConfigElement = multipartConfigElement;

        if (isDevelopment()) {
            try {
                try (var s = Files.list(Path.of("core-extensions"))) {
                    s.filter(PathUtils::isDirectory)
                            .filter(p -> !p.endsWith("target"))
                            .forEach(pluginsRoots::add);
                }
            } catch (Exception e) {
                ExceptionUtils.rethrow(e);
                return;
            }
        }
    }

    @Override
    protected PluginRepository createPluginRepository() {
        if (isDevelopment()) {
            return new DevelopmentPluginRepository(pluginsRoots) {
                {
                    filter = new AndFileFilter(filter)
                            .addFileFilter(new NotFileFilter(new NameFileFilter("core")))
                            .addFileFilter(new NotFileFilter(new NameFileFilter("bindings")));
                }

                @Override
                public List<Path> getPluginPaths() {
                    List<Path> paths = super.getPluginPaths();
                    paths.add(Path.of(".").resolve("core-services"));
                    paths.add(Path.of(".").resolve("metadata-registry/services"));
                    return paths;
                }
            };
        } else {
            return super.createPluginRepository();
        }
    }

    @Override
    protected ExtensionFactory createExtensionFactory() {
        return new STPF4JExtensionFactory(this);
    }

    @PostConstruct
    public void init() {
        // we override super.init() because extension injector is causing problems and we want to defer plugin intialization
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        loadPlugins();
        startPlugins();

        applicationContextsWaitingForInitialization.clear();
        applicationContextsWaitingForInitialization.add(getApplicationContext());
        getPlugins().forEach(pluginWrapper -> {
            if (pluginWrapper.getPlugin() instanceof STPlugin stPlugin) {
                var pluginApplicationContext = (ConfigurableApplicationContext) stPlugin.getApplicationContext();
                var dispatcherServlet = new DispatcherServlet();
                dispatcherServlet.setApplicationContext(pluginApplicationContext);

                var servletRegistration = servletContext
                        .addServlet(stPlugin.getWrapper().getPluginId() + "-servlet", dispatcherServlet);
                if (stPlugin.isExtensionPathMapped()) {
                    servletRegistration.addMapping("/semanticturkey/" + stPlugin.getExtensionPath() + "/*");
                } else {
                    servletRegistration.addMapping("/semanticturkey/*");
                }
                multipartConfigElement.ifAvailable(servletRegistration::setMultipartConfig);
                servletRegistration.setLoadOnStartup(1);
                applicationContextsWaitingForInitialization.add(pluginApplicationContext);
            }
        });
    }

    @EventListener
    public void onContextRefreshed(ContextRefreshedEvent event) {
        var refreshedApplicationContext = event.getApplicationContext();
        var stPluginOptional = refreshedApplicationContext.getBeansOfType(STPlugin.class).values().stream().findAny();
        indexedApplicationContexts.add(refreshedApplicationContext);
        exptMgr.indexApplicationContext(refreshedApplicationContext);
        stPluginOptional.ifPresent(stPlugin -> {
            refreshedApplicationContext.publishEvent(new PluginInitialized(stPlugin));
        });
        // we should take care of the root application context which is not associated with a plugin
        if (applicationContextsWaitingForInitialization.remove(refreshedApplicationContext) && applicationContextsWaitingForInitialization.isEmpty()) { // last plugin initialized
            getApplicationContext().publishEvent(new AllPluginsInitialized(this));
        }
    }

    public Set<ApplicationContext> getIndexedApplicationContexts() {
        return indexedApplicationContexts;
    }
}
