package it.uniroma2.art.semanticturkey.settings.connections.showvoc;

import it.uniroma2.art.semanticturkey.config.Configuration;
import it.uniroma2.art.semanticturkey.constraints.HasExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.extpts.repositoryimplconfigurer.RepositoryImplConfigurer;
import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperty;

public class ConnectedShowVocProjectSettings implements Settings {
    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.connections.showvoc.ConnectedShowVocProjectSettings";

        public static final String shortName = keyBase + ".shortName";

        public static final String project$description = keyBase + ".project.description";
        public static final String project$displayName = keyBase + ".project.displayName";
        public static final String coreRepoSailConf$description = keyBase + ".coreRepoSailConf.description";
        public static final String coreRepoSailConf$displayName = keyBase + ".coreRepoSailConf.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.project$description + "}", displayName = "{"
            + MessageKeys.project$displayName + "}")
    public String project;

    @STProperty(description = "{" + MessageKeys.coreRepoSailConf$description + "}", displayName = "{" + MessageKeys.coreRepoSailConf$displayName + "}")
    @Required
    public @HasExtensionPoint(RepositoryImplConfigurer.class) Configuration coreRepoSailConf;

}
