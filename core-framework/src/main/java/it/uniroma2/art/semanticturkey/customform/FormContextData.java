package it.uniroma2.art.semanticturkey.customform;

import java.util.HashMap;
import java.util.Map;

public class FormContextData {

    public static final class Features {
        public static final String subject = "subject";
    }

    Map<String, Object> form;

    public FormContextData() {
        form = new HashMap<>();
    }

    public void addFormEntry(String feature, String value) {
        form.put(feature, value);
    }

    public void removeFormEntry(String feature) {
        form.remove(feature);
    }

    public Map<String, Object> asMap() {
        return form;
    }
}
