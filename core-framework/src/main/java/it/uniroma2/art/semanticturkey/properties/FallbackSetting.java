package it.uniroma2.art.semanticturkey.properties;

import it.uniroma2.art.semanticturkey.resources.Scope;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation associates a property with a setting to get a default value from
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface FallbackSetting {
    String manager();
    Scope scope();
    String path();
}
