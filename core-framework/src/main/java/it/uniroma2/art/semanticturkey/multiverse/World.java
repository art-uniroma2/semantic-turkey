package it.uniroma2.art.semanticturkey.multiverse;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * A reference to a <em>world</em> in the multiverse.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public final class World {

    public static final String NAME_REGEX = "[a-zA-Z0-9_\\-]+";
    public static final Pattern NAME_PATTERN = Pattern.compile(NAME_REGEX);

    static final String MAIN_NAME = "main";
    public static final World MAIN = new World(MAIN_NAME);

    private final String name;

    World(String name) {
        if (!NAME_PATTERN.matcher(name).matches()) {
            throw new IllegalArgumentException("Invalid world name: " + name);
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (obj.getClass() != this.getClass()) return false;
        return Objects.equals(name, ((World) obj).name);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
