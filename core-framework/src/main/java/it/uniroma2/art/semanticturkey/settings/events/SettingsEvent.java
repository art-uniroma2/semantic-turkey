package it.uniroma2.art.semanticturkey.settings.events;

import com.google.common.base.MoreObjects;
import it.uniroma2.art.semanticturkey.event.Event;
import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.extension.settings.SettingsManager;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.resources.Scope;
import it.uniroma2.art.semanticturkey.user.STUser;
import it.uniroma2.art.semanticturkey.user.UsersGroup;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Optional;

public abstract class SettingsEvent extends Event {
    private final Project project;
    private final STUser user;
    private final UsersGroup group;
    private final Scope scope;
    private final Settings settings;

    public SettingsEvent(SettingsManager settingsManager, Project project, STUser user, UsersGroup group, Scope scope, Settings settings) {
        super(settingsManager);
        this.project = project;
        this.user = user;
        this.group = group;
        this.scope = scope;
        this.settings = settings;
    }

    public SettingsManager getSettingsManager() {
        return (SettingsManager) getSource();
    }

    public Project getProject() {
        return project;
    }

    public STUser getUser() {
        return user;
    }

    public UsersGroup getGroup() {
        return group;
    }

    public Scope getScope() {
        return scope;
    }

    public Settings getSettings() {
        return settings;
    }

    @Override
    public String toString() {
        var sb = getToStringBuilder();
        return sb.toString();
    }

    protected ToStringBuilder getToStringBuilder() {
        var sb = new ToStringBuilder(this);
        sb.append("settingsManager", getSettingsManager().getId());
        sb.append("project", Optional.ofNullable(project).map(Project::getName).orElse(null));
        sb.append("user", Optional.ofNullable(user).map(STUser::getEmail).orElse(null));
        sb.append("group", Optional.ofNullable(group).map(UsersGroup::getShortName).orElse(null));
        sb.append("scope", scope.toString());
        return sb;
    }
}
