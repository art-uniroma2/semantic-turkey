package it.uniroma2.art.semanticturkey.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public abstract class XKOSFragment {

    /** The XKOS namespace: http://www.w3.org/2001/XMLSchema# */
    public static final String NAMESPACE = "http://rdf-vocabulary.ddialliance.org/xkos#";

    /**
     * Recommended prefix for the XKOS namespace: "xsd"
     */
    public static final String PREFIX = "xkos";

    /**
     * An immutable {@link Namespace} constant that represents the XSD namespace.
     */
    public static final Namespace NS = new SimpleNamespace(PREFIX, NAMESPACE);

    public static final IRI CORRESPONDENCE;
    public static final IRI CONCEPTASSOCIATION;
    public static final IRI MADEOF;
    public static final IRI COMPARES;
    public static final IRI SOURCECONCEPT;
    public static final IRI TARGETCONCEPT;

    static {
        ValueFactory vf = SimpleValueFactory.getInstance();

        CORRESPONDENCE = vf.createIRI(NAMESPACE, "Correspondence");
        CONCEPTASSOCIATION = vf.createIRI(NAMESPACE, "ConceptAssociation");
        MADEOF = vf.createIRI(NAMESPACE, "madeOf");
        COMPARES = vf.createIRI(NAMESPACE, "compares");
        SOURCECONCEPT = vf.createIRI(NAMESPACE, "sourceConcept");
        TARGETCONCEPT = vf.createIRI(NAMESPACE, "targetConcept");
    }
}
