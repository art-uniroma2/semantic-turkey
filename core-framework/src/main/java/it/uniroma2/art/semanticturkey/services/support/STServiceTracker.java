package it.uniroma2.art.semanticturkey.services.support;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import jakarta.annotation.PreDestroy;
import org.eclipse.rdf4j.model.IRI;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PathPatternsRequestCondition;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.util.pattern.PathPattern;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * This singleton object tracks services inside Semantic Turkey.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 */
public class STServiceTracker implements ApplicationListener<ContextRefreshedEvent> {

	private Map<String, Map<String, Map<String, OperationDescription>>> extensionPath2serviceClass2operation2meta;
	private Map<IRI, OperationDescription> iri2operationDescription;

	private static final Pattern regex = Pattern.compile("^\\/+([^/]+)\\/+([^/]+)\\/+([^/]+)\\/+([^/]+)$");
	private static final Pattern regex_with_mapped = Pattern.compile("^\\/+([^/]+)\\/+([^/]+)$");

	public STServiceTracker(ApplicationContext applicationContext) {
		this.extensionPath2serviceClass2operation2meta = new ConcurrentHashMap<>();
		this.iri2operationDescription = new ConcurrentHashMap<>();
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		indexApplicationContext(event.getApplicationContext());
	}

	public void indexApplicationContext(ApplicationContext applicationContext) {
		try {
			RequestMappingHandlerMapping handlerMapping = applicationContext
					.getBean(RequestMappingHandlerMapping.class);
			for (Entry<RequestMappingInfo, HandlerMethod> entry : handlerMapping
					.getHandlerMethods().entrySet()) {
				Set<PathPattern> patterns = Optional.ofNullable(entry.getKey().getPathPatternsCondition()).map(PathPatternsRequestCondition::getPatterns).orElse(Collections.emptySet());
				if (patterns.size() != 1) continue;
				String pattern = patterns.iterator().next().getPatternString();
				applicationContext.getBeansOfType(STPlugin.class).values().stream().findAny().ifPresent(
						stPlugin -> {
							String extensionPath = null;
							String serviceClass = null;
							String operation = null;

							if (stPlugin.isExtensionPathMapped()) {
								Matcher matcher = regex_with_mapped.matcher(pattern);
								if (matcher.find()) {
									extensionPath = stPlugin.getExtensionPath();
									serviceClass = matcher.group(1);
									operation = matcher.group(2);
								}
							} else {
								Matcher matcher = regex.matcher(pattern);
								if (matcher.find()) {
									extensionPath = matcher.group(1) + "/" + matcher.group(2);
									serviceClass = matcher.group(3);
									operation = matcher.group(4);
								}
							}
							if (extensionPath != null) {
								Map<String, Map<String, OperationDescription>> serviceClass2operation2meta = extensionPath2serviceClass2operation2meta
										.computeIfAbsent(extensionPath,
												key -> new ConcurrentHashMap<>());
								Map<String, OperationDescription> operation2meta = serviceClass2operation2meta
										.computeIfAbsent(serviceClass, key -> new ConcurrentHashMap<>());
								OperationDescription operationDescription = OperationDescription
										.create(applicationContext, extensionPath, serviceClass, operation, entry);
								operation2meta.put(operation, operationDescription);
								iri2operationDescription.put(operationDescription.getOperationIRI(),
										operationDescription);
							}
						}
				);
			}

		} catch (NoSuchBeanDefinitionException e) {
		}
	}

	@PreDestroy
	public void destroy() {

	}

	public Collection<String> getExtensionPaths() {
		return extensionPath2serviceClass2operation2meta.keySet();
	}

	public Collection<String> getServiceClasses(String extensionPath) {
		return extensionPath2serviceClass2operation2meta.getOrDefault(extensionPath, Collections.emptyMap())
				.keySet();
	}

	public Collection<String> getServiceOperations(String extensionPath, String serviceClass) {
		return extensionPath2serviceClass2operation2meta.getOrDefault(extensionPath, Collections.emptyMap())
				.getOrDefault(serviceClass, Collections.emptyMap()).keySet().stream()
				.map(operation -> "http://semanticturkey.uniroma2.it/services/" + extensionPath + "/"
						+ serviceClass + "/" + operation)
				.collect(Collectors.toSet());

	}

	public Optional<OperationDescription> getOperationDescription(IRI operationIRI) {
		return Optional.ofNullable(iri2operationDescription.get(operationIRI));
	}
}
