package it.uniroma2.art.semanticturkey.history;

import it.uniroma2.art.semanticturkey.services.STServiceContext;
import org.springframework.aop.Pointcut;
import org.springframework.aop.framework.AbstractAdvisingBeanPostProcessor;
import org.springframework.aop.support.ComposablePointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMethodMatcher;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import it.uniroma2.art.semanticturkey.aop.STServiceOperationPointcut;
import it.uniroma2.art.semanticturkey.services.annotations.Write;
import org.springframework.context.annotation.Lazy;

/**
 * A convenient {@link BeanPostProcessor} implementation that advises eligible beans (i.e. Semantic Turkey
 * services) in order to manage history-relevant metadata.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
@SuppressWarnings("serial")
public class HistoryMetadataPostProcessor extends AbstractAdvisingBeanPostProcessor
		implements InitializingBean {
	private STServiceContext stServiceContext;

	public HistoryMetadataPostProcessor(STServiceContext stServiceContext) {
		this.stServiceContext = stServiceContext;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Pointcut pointcut = new ComposablePointcut(new STServiceOperationPointcut())
				.intersection(new AnnotationMethodMatcher(Write.class));
		HistoryMetadataInterceptor advice = new HistoryMetadataInterceptor(stServiceContext);
		this.advisor = new DefaultPointcutAdvisor(pointcut, advice);
	}

}
