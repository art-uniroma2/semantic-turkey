package it.uniroma2.art.semanticturkey.security;

import it.uniroma2.art.semanticturkey.user.STUser;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import java.io.IOException;

public class SAMLAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Autowired
    ActiveUserStore activeUserStore;

    @Value("${spring.security.saml2.relyingparty.registration.st_saml.redirect-url}")
    String redirectUrl;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException {

        STUser loggedUser = (STUser) authentication.getPrincipal();

        //adds the user attribute to the session for keeping track of active users. See {@link LoggedUser}
        HttpSession session = request.getSession(false);
        if (session != null) {
            LoggedUser user = new LoggedUser(loggedUser, activeUserStore);
            session.setAttribute("user", user);
        }

        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        response.sendRedirect(redirectUrl);

    }

}
