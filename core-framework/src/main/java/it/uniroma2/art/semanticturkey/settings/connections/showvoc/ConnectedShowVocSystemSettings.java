package it.uniroma2.art.semanticturkey.settings.connections.showvoc;

import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;

public class ConnectedShowVocSystemSettings implements Settings {
	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.connections.showvoc.ConnectedShowVocSystemSettings";

		public static final String shortName = keyBase + ".shortName";
		public static final String htmlWarning = keyBase + ".htmlWarning";

		public static final String apiBaseURL$description = keyBase + ".apiBaseURL.description";
		public static final String apiBaseURL$displayName = keyBase + ".apiBaseURL.displayName";
		public static final String frontendBaseURL$description = keyBase + ".frontendBaseURL.description";
		public static final String frontendBaseURL$displayName = keyBase + ".frontendBaseURL.displayName";
		public static final String username$description = keyBase + ".username.description";
		public static final String username$displayName = keyBase + ".username.displayName";
		public static final String password$description = keyBase + ".password.description";
		public static final String password$displayName = keyBase + ".password.displayName";
	}

	@Override
	public String getShortName() {
		return "{" + MessageKeys.shortName + "}";
	}

	@Override
	public String getHTMLWarning() {
		return "{" + MessageKeys.htmlWarning + "}";
	}

	@STProperty(description = "{" + MessageKeys.apiBaseURL$description + "}", displayName = "{"
			+ MessageKeys.apiBaseURL$displayName + "}")
	public String apiBaseURL;

	@STProperty(description = "{" + MessageKeys.frontendBaseURL$description + "}", displayName = "{"
			+ MessageKeys.frontendBaseURL$displayName + "}")
	public String frontendBaseURL;

	@STProperty(description = "{" + MessageKeys.username$description + "}", displayName = "{"
			+ MessageKeys.username$displayName + "}")
	public String username;

	@STProperty(description = "{" + MessageKeys.password$description + "}", displayName = "{"
			+ MessageKeys.password$displayName + "}")
	public String password;

}
