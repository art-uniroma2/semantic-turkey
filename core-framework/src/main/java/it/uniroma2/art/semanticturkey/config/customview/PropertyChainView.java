package it.uniroma2.art.semanticturkey.config.customview;

import it.uniroma2.art.semanticturkey.customviews.CustomViewData;
import it.uniroma2.art.semanticturkey.customviews.CustomViewModelEnum;
import it.uniroma2.art.semanticturkey.customviews.CustomViewRenderedValue;
import it.uniroma2.art.semanticturkey.customviews.SingleValueCVDescription;
import it.uniroma2.art.semanticturkey.customviews.UpdateInfo;
import it.uniroma2.art.semanticturkey.customviews.UpdateMode;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import it.uniroma2.art.semanticturkey.services.AnnotatedValue;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.query.impl.SimpleDataset;
import org.eclipse.rdf4j.queryrender.RenderUtils;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

/**
 * CV based on a property chain.
 * e.g. :res -> :prop1 -> :node_1 -> :prop2 -> :node_2 -> ... -> :propN -> rendering_value
 * The description of a value (CustomViewObjectDescription.description) for this CV consists in a list of CustomViewRenderedValue.
 * Most of the case there will be just one CustomViewRenderedValue for each CustomViewObjectDescription.resource, but
 * in case of validation (or some "strange" cases of reified resources, e.g. reified note where the note has multiple literal rdf:value)
 * there could be more than one
 */
public class PropertyChainView extends CustomView {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.config.customview.PropertyChainView";
        public static final String shortName = keyBase + ".shortName";
        public static final String properties$description = keyBase + ".properties.description";
        public static final String properties$displayName = keyBase + ".properties.displayName";
    }

    @Override
    public CustomViewModelEnum getModelType() {
        return CustomViewModelEnum.property_chain;
    }

    @STProperty(description = "{" + MessageKeys.properties$description + "}", displayName = "{" + MessageKeys.properties$displayName + "}")
    @Required
    public List<IRI> properties;

    @Override
    public CustomViewData getData(RepositoryConnection connection, Resource resource, IRI property, IRI workingGraph) {
        CustomViewData cvData = new CustomViewData();
        cvData.setModel(getModelType());
        cvData.setDefaultView(suggestedView);

        String query;

        if (properties.size() == 1) {
            query = "SELECT ?obj ?value ?endingG WHERE { \n" +
                    " $resource $trigprop ?obj .\n" +
                    " graph ?endingG {  ?obj " + RenderUtils.toSPARQL(properties.get(0)) + " ?value . }\n" +
                    "}";
        } else {
            String startingChainSparql = properties.subList(0, properties.size()-1).stream().map(RenderUtils::toSPARQL).collect(joining("/"));
            String endingPropSparql = RenderUtils.toSPARQL(properties.get(properties.size()-1));
            query = "SELECT ?obj ?value ?endingG WHERE { \n" +
                    " $resource $trigprop ?obj .\n" +
                    " ?obj " + startingChainSparql + " ?lastNode .\n" +
                    " graph ?endingG {  ?lastNode " + endingPropSparql + " ?value . }\n" +
                    "}";
        }

        TupleQuery tupleQuery = connection.prepareTupleQuery(query);
        tupleQuery.setBinding("resource", resource);
        tupleQuery.setBinding("trigprop", property);


        SimpleDataset dataset = new SimpleDataset();
        tupleQuery.setDataset(dataset);

        try (TupleQueryResult results = tupleQuery.evaluate()) {

            List<SingleValueCVDescription> valueDescriptions = new ArrayList<>();

            //iterate over the query results and populate the list of SingleValueCVDescription
            //(which represents the description of returned CVData)
            while (results.hasNext()) {
                BindingSet bs = results.next();
                Resource object = (Resource) bs.getValue("obj");
                Value value = bs.getValue("value");
                IRI endingGraph = (IRI) bs.getValue("endingG");


                /*
                Search for a SingleValueCVDescription about the current (reified) resource being described
                 */
                SingleValueCVDescription valueDesc = null;
                for (SingleValueCVDescription vd : valueDescriptions) {
                    if (vd.getResource().equals(object)) {
                        valueDesc = vd;
                        break;
                    }
                }
                if (valueDesc == null) { //no SingleValueCVDescription for the current resource => create a new one
                    valueDesc = new SingleValueCVDescription();
                    valueDesc.setResource(object);
                    valueDescriptions.add(valueDesc);
                }

                /*
                list of values to render/show for the same reified resource
                (it's a list because the same reified resource could have multiple value to show,
                e.g. reified note with more than one lit form or lit form in staging add/remove)
                 */
                List<CustomViewRenderedValue> renderedValues;
                renderedValues = valueDesc.getDescription();
                if (renderedValues == null) {
                    renderedValues = new ArrayList<>();
                    valueDesc.setDescription(renderedValues);
                }

                /*
                search if the value to render is already been added (it can happen in case the value is in pending
                remove, so it is in both main graph and staging-remove graph
                 */
                CustomViewRenderedValue renderedValue = null;
                for (CustomViewRenderedValue rv : renderedValues) {
                    if (rv.getResource().getValue().equals(value)) {
                        renderedValue = rv;
                        break;
                    }
                }
                if (renderedValue == null) { //not already added => create new
                    renderedValue = new CustomViewRenderedValue("value", new AnnotatedValue<>(value));
                    renderedValues.add(renderedValue);
                }

                updateGraphs(renderedValue.getResource(), endingGraph); //update the set of graphs

                renderedValue.setUpdateInfo(new UpdateInfo(UpdateMode.widget));

            }
            cvData.setData(valueDescriptions);
        }

        return cvData;
    }

    private void updateGraphs(AnnotatedValue<Value> annValue, Resource graph) {
        Set<Resource> graphs;
        if (annValue.getAttributes().get("graphs") != null) {
            String graphsAttr = annValue.getAttributes().get("graphs").stringValue();
            graphs = Arrays.stream(graphsAttr.split(",")).map(g -> SimpleValueFactory.getInstance().createIRI(g)).collect(Collectors.toSet());
        } else {
            graphs = new HashSet<>();
        }
        graphs.add(graph);
        annValue.setAttribute("graphs", graphs.stream().map(g -> g.toString()).collect(joining(",")));
    }

    public void updateData(RepositoryConnection connection, Resource resource, IRI property, Value oldValue, Value newValue, IRI workingGraph) {
        String updateQuery;
        if (properties.size() == 1) {
            updateQuery = "DELETE {  ?pivot " + RenderUtils.toSPARQL(properties.get(0)) + " ?oldValue . } \n" +
                    "INSERT {  ?pivot " + RenderUtils.toSPARQL(properties.get(0)) + " ?value . } \n" +
                    "WHERE { \n" +
                    "   $resource $trigprop ?pivot . \n" +
                    "   ?pivot " + RenderUtils.toSPARQL(properties.get(0)) + " ?oldValue . \n" +
                    "}";
        } else {
            IRI lastProp = properties.get(properties.size()-1);
            List<IRI> leadingProps = properties.subList(0, properties.size()-1);
            String chain = leadingProps.stream().map(RenderUtils::toSPARQL).collect(joining("/"));
            updateQuery = "DELETE {  ?pivot " + RenderUtils.toSPARQL(lastProp) + " ?oldValue . } \n" +
                    "INSERT {  ?pivot " + RenderUtils.toSPARQL(lastProp) + " ?value . } \n" +
                    "WHERE { \n" +
                    "   $resource $trigprop ?o . \n" +
                    "   ?o " + chain + " ?pivot . \n" +
                    "   ?pivot " + RenderUtils.toSPARQL(lastProp) + " ?oldValue . \n" +
                    "}";
        }

        Update u = connection.prepareUpdate(updateQuery);

        //bind placeholders and provided variables
        u.setBinding("resource", resource);
        u.setBinding("trigprop", property);
        u.setBinding("value", newValue);
        u.setBinding("oldValue", oldValue);

        SimpleDataset dataset = new SimpleDataset();
        dataset.setDefaultInsertGraph(workingGraph);
        dataset.addDefaultGraph(workingGraph);
        dataset.addDefaultRemoveGraph(workingGraph);
        u.setDataset(dataset);

        u.execute();
    }


    public void deleteData(RepositoryConnection connection, Resource resource, IRI property, Value value, IRI workingGraph) {
        String updateQuery;
        if (properties.size() == 1) {
            updateQuery = "DELETE {  ?pivot " + RenderUtils.toSPARQL(properties.get(0)) + " ?value . } \n" +
                    "WHERE { \n" +
                    "   $resource $trigprop ?pivot . \n" +
                    "   ?pivot " + RenderUtils.toSPARQL(properties.get(0)) + " ?value . \n" +
                    "}";
        } else {
            IRI lastProp = properties.get(properties.size()-1);
            List<IRI> leadingProps = properties.subList(0, properties.size()-1);
            String chain = leadingProps.stream().map(RenderUtils::toSPARQL).collect(joining("/"));
            updateQuery = "DELETE {  ?pivot " + RenderUtils.toSPARQL(lastProp) + " ?value . } \n" +
                    "WHERE { \n" +
                    "   $resource $trigprop ?o . \n" +
                    "   ?o " + chain + " ?pivot . \n" +
                    "   ?pivot " + RenderUtils.toSPARQL(lastProp) + " ?value . \n" +
                    "}";
        }

        Update u = connection.prepareUpdate(updateQuery);

        //bind placeholders and provided variables
        u.setBinding("resource", resource);
        u.setBinding("trigprop", property);
        u.setBinding("value", value);

        SimpleDataset dataset = new SimpleDataset();
        dataset.setDefaultInsertGraph(workingGraph);
        dataset.addDefaultGraph(workingGraph);
        dataset.addDefaultRemoveGraph(workingGraph);
        u.setDataset(dataset);

        u.execute();
    }

}
