package it.uniroma2.art.semanticturkey.multiverse;

import org.springframework.beans.factory.ObjectFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Holds objectes scoped to a given world. An instance of this class should be handled with a lock held on the MultiverseM {@link MultiverseManager}.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class WorldScopedObjects {
    private final Map<String, Object> scopedObjects = new HashMap<>();
    private final Map<String, Runnable> destructionCallbacks = new HashMap<>();

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public Object get(String name) {
        var readLock = lock.readLock();
        try {
            readLock.lock();
            return scopedObjects.get(name);
        } finally {
            readLock.unlock();
        }
    }

    public void put(String name, Object scopedObject) {
        var writeLock = lock.writeLock();
        try {
            writeLock.lock();
            scopedObjects.put(name, scopedObject);
        } finally {
            writeLock.unlock();
        }
    }

    public Object remove(String name) {
        var writeLock = lock.writeLock();
        try {
            writeLock.lock();
            Object rv = scopedObjects.remove(name);
            destructionCallbacks.remove(name);
            return rv;
        } finally {
            writeLock.unlock();
        }
    }

    public void registerDestructionCallback(String name, Runnable callback) {
        var writeLock = lock.writeLock();
        try {
            writeLock.lock();
            destructionCallbacks.put(name, callback);
        } finally {
            writeLock.unlock();
        }
    }

    public void executeDestructionCallbacks() {
        var readLock = lock.readLock();
        try {
            readLock.lock();
            destructionCallbacks.values().forEach(Runnable::run);
        } finally {
            readLock.unlock();
        }
    }

    public Object computeIfAbsent(String name, ObjectFactory<?> objectFactory) {
        Object scopedObject;

        var readLock = lock.readLock();
        try {
            readLock.lock();

            scopedObject = scopedObjects.get(name);
        } finally {
            readLock.unlock();
        }

        // NOTE: Java doesn't support upgrading a read-lock to a write-lock. Therefore, we need to first release the
        // read-lock and acquire a write lock.

        if (scopedObject == null) {
            var writeLock = lock.writeLock();
            try {
                writeLock.lock();

                // NOTE: Do NOT modify the following to use Map::computeIfAbsent. For details,
                // see https://github.com/spring-projects/spring-framework/issues/25801.

                scopedObject = scopedObjects.get(name); // double-checked locking necessary because we have released the lock in the meanwhile
                if (scopedObject == null) {
                    scopedObject = objectFactory.getObject();
                    scopedObjects.put(name, scopedObject);
                }
            } finally {
                writeLock.unlock();
            }
        }

        return scopedObject;
    }
}
