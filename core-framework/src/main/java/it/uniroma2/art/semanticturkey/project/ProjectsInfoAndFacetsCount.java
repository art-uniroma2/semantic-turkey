package it.uniroma2.art.semanticturkey.project;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectsInfoAndFacetsCount {
    //this class is used to represent a map of facets, facetNameToFacetValueToCountMapMap, and a map of ProjectInfo,
    // facetValueToProjetInfoListMap

    Map<String, Map<String, Integer>> facetNameToFacetValueToCountMapMap = new HashMap<>();
    Map<String, List<ProjectInfo>> facetValueToProjetInfoListMap = new HashMap<>();

    @JsonCreator
    public ProjectsInfoAndFacetsCount(
            @JsonProperty("facetNameToFacetValueToCountMapMap")Map<String, Map<String, Integer>> facetNameToFacetValueToCountMapMap,
            @JsonProperty("facetValueToProjetInfoListMap")Map<String, List<ProjectInfo>> facetValueToProjetInfoListMap) {
        this.facetNameToFacetValueToCountMapMap = facetNameToFacetValueToCountMapMap;
        this.facetValueToProjetInfoListMap = facetValueToProjetInfoListMap;
    }

    public ProjectsInfoAndFacetsCount() {
    }

    public void addFacetNameFacetValueAndCount(String facetName, String facetValue, int count){
        if (!facetNameToFacetValueToCountMapMap.containsKey(facetName)) {
            facetNameToFacetValueToCountMapMap.put(facetName, new HashMap<>());
        }
        facetNameToFacetValueToCountMapMap.get(facetName).put(facetValue, count);
    }

    public void addFacetNameFacetValueAndCount(Map<String, Map<String, Integer>> inputFacetNameToFacetValueToCountMapMap){
        for (String facetName : inputFacetNameToFacetValueToCountMapMap.keySet()) {
            Map<String, Integer> facetValueToCountMap = inputFacetNameToFacetValueToCountMapMap.get(facetName);
            for (String facetValue : facetValueToCountMap.keySet()) {
                addFacetNameFacetValueAndCount(facetName, facetValue, facetValueToCountMap.get(facetValue));
            }
        }

    }

    public void addProjetInfoWithFacetValue(String facetValue, ProjectInfo projectInfo) {
        if (!facetValueToProjetInfoListMap.containsKey(facetValue)) {
            facetValueToProjetInfoListMap.put(facetValue, new ArrayList<>());
        }
        facetValueToProjetInfoListMap.get(facetValue).add(projectInfo);
    }

    public void setFacetNameToFacetValueToCountMapMap(Map<String, Map<String, Integer>> facetNameToFacetValueToCountMapMap) {
        this.facetNameToFacetValueToCountMapMap = facetNameToFacetValueToCountMapMap;
    }

    public void setFacetValueToProjetInfoListMap(Map<String, List<ProjectInfo>> facetValueToProjetInfoListMap) {
        this.facetValueToProjetInfoListMap = facetValueToProjetInfoListMap;
    }

    public Map<String, Map<String, Integer>> getFacetNameToFacetValueToCountMapMap() {
        return facetNameToFacetValueToCountMapMap;
    }

    public Map<String, List<ProjectInfo>> getFacetValueToProjetInfoListMap() {
        return facetValueToProjetInfoListMap;
    }
}
