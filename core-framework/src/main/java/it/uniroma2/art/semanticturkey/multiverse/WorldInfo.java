package it.uniroma2.art.semanticturkey.multiverse;

/**
 * Concise information about a world.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class WorldInfo {
    private final String worldName;
    private final int userCount;

    public WorldInfo(String worldName, int userCount) {
        this.worldName = worldName;
        this.userCount = userCount;
    }

    public String getWorldName() {
        return worldName;
    }

    public int getUserCount() {
        return userCount;
    }
}
