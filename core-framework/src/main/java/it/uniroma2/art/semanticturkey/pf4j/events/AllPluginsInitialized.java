package it.uniroma2.art.semanticturkey.pf4j.events;

import it.uniroma2.art.semanticturkey.event.Event;
import it.uniroma2.art.semanticturkey.pf4j.STPluginManager;

public class AllPluginsInitialized extends Event {
    public AllPluginsInitialized(STPluginManager pluginManager) {
        super(pluginManager);
    }

    public STPluginManager getPluginManager() {
        return (STPluginManager) getSource();
    }
}
