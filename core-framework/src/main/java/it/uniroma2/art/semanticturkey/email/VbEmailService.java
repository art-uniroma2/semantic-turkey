package it.uniroma2.art.semanticturkey.email;

import it.uniroma2.art.semanticturkey.settings.core.SemanticTurkeyCoreSettingsManager;

public class VbEmailService extends EmailService {

	/*
	 * After a refactor, all the VB specific methods are be shared with SV, thus moved in EmailService.
	 * Despite this class remained empty, it is kept for feature needs
	 */

	public VbEmailService(SemanticTurkeyCoreSettingsManager coreSettingsManager) {
		super(EmailApplicationContext.VB, coreSettingsManager);
	}

}
