package it.uniroma2.art.semanticturkey.settings.core;

import it.uniroma2.art.semanticturkey.properties.Enumeration;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;

import java.util.List;
import java.util.Map;

public class ResourceViewPreferences implements STProperties {
    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.core.ResourceViewPreferences";

        public static final String shortName = keyBase + ".shortName";
        public static final String defaultConceptType$description = keyBase + ".defaultConceptType.description";
        public static final String defaultConceptType$displayName = keyBase + ".defaultConceptType.displayName";
        public static final String defaultLexEntryType$description = keyBase + ".defaultLexEntryType.description";
        public static final String defaultLexEntryType$displayName = keyBase + ".defaultLexEntryType.displayName";
        public static final String rendering$description = keyBase + ".rendering.description";
        public static final String rendering$displayName = keyBase + ".rendering.displayName";
        public static final String inference$description = keyBase + ".inference.description";
        public static final String inference$displayName = keyBase + ".inference.displayName";
        public static final String showDatatypeBadge$description = keyBase + ".showDatatypeBadge.description";
        public static final String showDatatypeBadge$displayName = keyBase + ".showDatatypeBadge.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.defaultConceptType$description
            + "}", displayName = "{" + MessageKeys.defaultConceptType$displayName + "}")
    @Enumeration({"resourceView", "termView", "code"})
    public String defaultConceptType;

    @STProperty(description = "{" + MessageKeys.defaultLexEntryType$description
            + "}", displayName = "{" + MessageKeys.defaultLexEntryType$displayName + "}")
    @Enumeration({"resourceView", "lexicographerView", "code"})
    public String defaultLexEntryType;

    @STProperty(description = "{" + MessageKeys.rendering$description
            + "}", displayName = "{" + MessageKeys.rendering$displayName + "}")
    public Boolean rendering;

    @STProperty(description = "{" + MessageKeys.inference$description
            + "}", displayName = "{" + MessageKeys.inference$displayName + "}")
    public Boolean inference;

    @STProperty(description = "{" + MessageKeys.showDatatypeBadge$description
            + "}", displayName = "{" + MessageKeys.showDatatypeBadge$displayName + "}")
    public Boolean showDatatypeBadge;

}
