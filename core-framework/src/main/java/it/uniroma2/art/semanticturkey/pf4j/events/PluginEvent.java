package it.uniroma2.art.semanticturkey.pf4j.events;

import it.uniroma2.art.semanticturkey.event.Event;
import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import it.uniroma2.art.semanticturkey.pf4j.STPluginManager;

public class PluginEvent extends Event {
    public PluginEvent(STPlugin source) {
        super(source);
    }

    public STPluginManager getPlugin() {
        return (STPluginManager) getSource();
    }

}
