package it.uniroma2.art.semanticturkey.settings.core;

import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;

public class ResViewSectionDisplayInfo implements STProperties {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.core.ResViewSectionDisplayInfo";

        public static final String shortName = keyBase + ".shortName";
        public static final String label$description = keyBase + ".label.description";
        public static final String label$displayName = keyBase + ".label.displayName";
        public static final String description$description = keyBase + ".description.description";
        public static final String description$displayName = keyBase + ".description.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.label$description + "}",
            displayName = "{" + MessageKeys.label$displayName + "}")
    public String label;

    @STProperty(description = "{" + MessageKeys.description$description + "}",
            displayName = "{" + MessageKeys.description$displayName + "}")
    public String description;

}
