package it.uniroma2.art.semanticturkey.project.events;

import it.uniroma2.art.semanticturkey.event.Event;
import it.uniroma2.art.semanticturkey.project.Project;

/**
 * An event related to a project
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public abstract class ProjectEvent extends Event {
    private final Project project;

    public ProjectEvent(Project project) {
        super(project);
        this.project = project;
    }

    public Project getProject() {
        return project;
    }
}
