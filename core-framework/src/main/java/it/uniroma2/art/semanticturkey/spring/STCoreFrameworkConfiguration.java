package it.uniroma2.art.semanticturkey.spring;

import com.google.common.collect.Streams;
import it.uniroma2.art.maple.orchestration.MediationFramework;
import it.uniroma2.art.maple.orchestration.impl.MediationFrameworkImpl;
import it.uniroma2.art.maple.provisioning.ComponentRepository;
import it.uniroma2.art.semanticturkey.converters.StringToACLAccessLevelConverter;
import it.uniroma2.art.semanticturkey.converters.StringToACLLockLevelConverter;
import it.uniroma2.art.semanticturkey.converters.StringToCustomFormValueConverter;
import it.uniroma2.art.semanticturkey.converters.StringToDataSizeConverter;
import it.uniroma2.art.semanticturkey.converters.StringToMapGenericConverter;
import it.uniroma2.art.semanticturkey.converters.StringToObjectNodeConverter;
import it.uniroma2.art.semanticturkey.converters.StringToPluginSpecificationConverter;
import it.uniroma2.art.semanticturkey.converters.StringToProjectConsumerConverter;
import it.uniroma2.art.semanticturkey.converters.StringToProjectConverter;
import it.uniroma2.art.semanticturkey.converters.StringToRDF4JBNodeConverter;
import it.uniroma2.art.semanticturkey.converters.StringToRDF4JIRIConverter;
import it.uniroma2.art.semanticturkey.converters.StringToRDF4JLiteralConverter;
import it.uniroma2.art.semanticturkey.converters.StringToRDF4JRDFormatConverter;
import it.uniroma2.art.semanticturkey.converters.StringToRDF4JResourceConverter;
import it.uniroma2.art.semanticturkey.converters.StringToRDF4JValueConverter;
import it.uniroma2.art.semanticturkey.converters.StringToRepositoryAccessConverter;
import it.uniroma2.art.semanticturkey.converters.StringToResourcePositionConverter;
import it.uniroma2.art.semanticturkey.converters.StringToSpecialValueConverter;
import it.uniroma2.art.semanticturkey.converters.StringToZonedDateTimeConverter;
import it.uniroma2.art.semanticturkey.data.access.ResourceLocator;
import it.uniroma2.art.semanticturkey.extension.impl.ExtensionPointManagerImpl;
import it.uniroma2.art.semanticturkey.mdr.core.MetadataRegistryBackend;
import it.uniroma2.art.semanticturkey.multiverse.WorldScope;
import it.uniroma2.art.semanticturkey.multiverse.MultiverseManager;
import it.uniroma2.art.semanticturkey.notification.ResourceChangeNotificationManager;
import it.uniroma2.art.semanticturkey.pf4j.STPluginManager;
import it.uniroma2.art.semanticturkey.properties.Pair;
import it.uniroma2.art.semanticturkey.properties.STPropertiesManager;
import it.uniroma2.art.semanticturkey.security.AccessControlManager;
import it.uniroma2.art.semanticturkey.security.ActiveUserStore;
import it.uniroma2.art.semanticturkey.security.CORSFilter;
import it.uniroma2.art.semanticturkey.security.CspWhitelistService;
import it.uniroma2.art.semanticturkey.security.DynamicAuthorizationFilter;
import it.uniroma2.art.semanticturkey.security.SAMLAuthenticationSuccessHandler;
import it.uniroma2.art.semanticturkey.security.STAccessDeniedHandler;
import it.uniroma2.art.semanticturkey.security.STAuthenticationEntryPoint;
import it.uniroma2.art.semanticturkey.security.STAuthenticationFailureHandler;
import it.uniroma2.art.semanticturkey.security.STAuthenticationSuccessHandler;
import it.uniroma2.art.semanticturkey.security.STAuthorizationEvaluator;
import it.uniroma2.art.semanticturkey.security.STLogoutSuccessHandler;
import it.uniroma2.art.semanticturkey.security.STUserDetailsService;
import it.uniroma2.art.semanticturkey.services.STServiceContext;
import it.uniroma2.art.semanticturkey.services.http.STServiceHTTPContext;
import it.uniroma2.art.semanticturkey.services.support.STServiceTracker;
import it.uniroma2.art.semanticturkey.settings.core.SemanticTurkeyCoreSettingsManager;
import it.uniroma2.art.semanticturkey.user.STUser;
import it.uniroma2.art.semanticturkey.user.UsersManager;
import it.uniroma2.art.semanticturkey.versioning.ResourceMetadataManager;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.MultipartConfigElement;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.opensaml.security.x509.impl.KeyStoreX509CredentialAdapter;
import org.pf4j.PluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.format.FormatterRegistry;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.saml2.core.Saml2Error;
import org.springframework.security.saml2.core.Saml2ResponseValidatorResult;
import org.springframework.security.saml2.core.Saml2X509Credential;
import org.springframework.security.saml2.provider.service.authentication.OpenSaml4AuthenticationProvider;
import org.springframework.security.saml2.provider.service.authentication.Saml2AuthenticatedPrincipal;
import org.springframework.security.saml2.provider.service.authentication.Saml2Authentication;
import org.springframework.security.saml2.provider.service.metadata.OpenSamlMetadataResolver;
import org.springframework.security.saml2.provider.service.registration.InMemoryRelyingPartyRegistrationRepository;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistration;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistrationRepository;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistrations;
import org.springframework.security.saml2.provider.service.registration.Saml2MessageBinding;
import org.springframework.security.saml2.provider.service.web.DefaultRelyingPartyRegistrationResolver;
import org.springframework.security.saml2.provider.service.web.Saml2MetadataFilter;
import org.springframework.security.saml2.provider.service.web.authentication.Saml2WebSsoAuthenticationFilter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.springframework.security.saml2.provider.service.authentication.OpenSaml4AuthenticationProvider.createDefaultResponseAuthenticationConverter;

/**
 * A @Configuration class for the core framework.
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@ComponentScan(value = {
        "it.uniroma2.art.semanticturkey.settings",       // core settings managers
        "it.uniroma2.art.semanticturkey.config",         // core configuration managers
        "it.uniroma2.art.semanticturkey.customform",     // custom forms-related beans
        "it.uniroma2.art.semanticturkey.customviews",    // custom views-related beans
        "it.uniroma2.art.semanticturkey.extension.extpts"// extension points
})
public class STCoreFrameworkConfiguration implements WebMvcConfigurer {

    private static final Logger logger = LoggerFactory.getLogger(STCoreFrameworkConfiguration.class);

    private List<String> publicResourcePaths = null;

    @Autowired
    private ResourcePatternResolver resolver;

    @Autowired
    @Lazy
    private MetadataRegistryBackend mdr;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${semanticturkey.properties.snakeyaml.code-point-limit:#{null}}")
    private Integer snakeYamlCodePointLimit;

    @PostConstruct
    public void init() throws IOException {
        publicResourcePaths = Streams.concat(Arrays.stream(resolver.getResources("classpath*:/public/*/")).map(r -> { // for directories
            try {
                String directoryName = FilenameUtils.getName(StringUtils.stripEnd(r.getURL().getPath(), "/"));
                return "/" + directoryName + "/**";
            } catch (IOException e) {
                ExceptionUtils.rethrow(e);
                return ""; // never executed
            }
        }), Arrays.stream(resolver.getResources("classpath*:/public/*")).map(r -> { // for root files
            try {
                String fileName = FilenameUtils.getName(r.getURL().getPath());
                return "/" + fileName;
            } catch (IOException e) {
                ExceptionUtils.rethrow(e);
                return ""; // never executed
            }
        })).filter(StringUtils::isNotEmpty).distinct().toList();
        RootApplicationContextHolder.set(applicationContext);
        STPropertiesManager.setSnakeYAMLCodePointLimit(snakeYamlCodePointLimit);
    }

    @Bean
    public STServiceTracker stServiceTracker(ApplicationContext applicationContext) {
        return new STServiceTracker(applicationContext);
    }

    @Bean
    public ExtensionPointManagerImpl extensionPointManager(ApplicationContext context) {
        return new ExtensionPointManagerImpl(context);
    }


    @Bean
    public static MultiverseManager multiverseManager() {
        return new MultiverseManager();
    }

    @Bean
    public static CustomScopeConfigurer customScopeConfigurer(MultiverseManager multiverseManager) {
        CustomScopeConfigurer configurer = new CustomScopeConfigurer();
        configurer.addScope("world", new WorldScope(multiverseManager));
        return configurer;
    }

    @Bean
    @DependsOn("metadataRegistryBackend")
    public STPluginManager pluginManager(ApplicationContext context, ExtensionPointManagerImpl exptMgr, ObjectProvider<MultipartConfigElement> multipartConfigElement) {
        return new STPluginManager(context, exptMgr, multipartConfigElement);
    }

    @Bean
    public MediationFrameworkImpl mediationFramework(PluginManager pluginManager) throws IOException {
        var mediationFramework = new MediationFrameworkImpl();
        var componentRepository = new ComponentRepository(pluginManager);
        componentRepository.setDataDumps(new URL[]{MediationFramework.class.getResource("/META-INF/it.uniroma2.art.maple/access_implementations.ttl")});
        componentRepository.initialize();
        mediationFramework.setComponentRepository(componentRepository);
        return mediationFramework;
    }

    @Bean
    public ResourceLocator resourceLocator() {
        return new ResourceLocator();
    }

    @Bean
    public ResourceChangeNotificationManager resourceChangeNotificationManager() {
        return new ResourceChangeNotificationManager();
    }

    @Bean
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(10);
        taskScheduler.initialize();
        return taskScheduler;
    }

    @Bean
    public ResourceMetadataManager resourceMetadataManager() {
        return new ResourceMetadataManager();
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        publicResourcePaths.stream().filter(path -> StringUtils.endsWith(path, "/**")).forEach(
                path -> {
                    var path2 = StringUtils.removeEnd(path, "/**");
                    registry.addRedirectViewController(path2, path2 + "/");
                    registry.addViewController(path2 + "/").setViewName("forward:" + path2 + "/index.html");
                }
        );
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToACLAccessLevelConverter());
        registry.addConverter(new StringToACLLockLevelConverter());
        registry.addConverter(new StringToProjectConsumerConverter());
        registry.addConverter(new StringToProjectConverter());
        registry.addConverter(stringToResourcePositionConverter(mdr));
        registry.addConverter(new StringToRDF4JBNodeConverter());
        registry.addConverter(new StringToRDF4JIRIConverter());
        registry.addConverter(new StringToRDF4JLiteralConverter());
        registry.addConverter(new StringToRDF4JResourceConverter());
        registry.addConverter(new StringToRDF4JValueConverter());
        registry.addConverter(new StringToObjectNodeConverter());
        registry.addConverter(new StringToPluginSpecificationConverter());
        registry.addConverter(new StringToRDF4JRDFormatConverter());
        registry.addConverter(new StringToRepositoryAccessConverter());
        registry.addConverter(new StringToMapGenericConverter(applicationContext));
        registry.addConverter(new StringToCustomFormValueConverter());
        registry.addConverter(new StringToSpecialValueConverter(applicationContext));
        registry.addConverter(new StringToDataSizeConverter());
        registry.addConverter(new StringToZonedDateTimeConverter());

    }

    @Bean
    public StringToResourcePositionConverter stringToResourcePositionConverter(MetadataRegistryBackend mdr) {
        return new StringToResourcePositionConverter(mdr);
    }


    // --- security

    @Bean
    public STServiceContext stServiceContext() {
        return new STServiceHTTPContext();
    }

    @Bean
    @DependsOn("semanticTurkeyCoreSettingsManager")  // implicit dependency through UsersManager
    public AccessControlManager accessControlManager() {
        return new AccessControlManager();
    }

    @Bean("auth")
    public STAuthorizationEvaluator stAuthorizationEvaluator() {
        return new STAuthorizationEvaluator();
    }

    /**
     * Needed for keeping track of active/online users
     * @return
     */
    @Bean
    public ActiveUserStore activeUserStore(){
        return new ActiveUserStore();
    }

    @Bean
    public CspWhitelistService cspWhitelistService() {
        return new CspWhitelistService();
    }

    @Bean
    public CORSFilter corsFilter() {
        return new CORSFilter(cspWhitelistService());
    }

    @Bean
    public DynamicAuthorizationFilter dynamicAuthorizationFilter(SemanticTurkeyCoreSettingsManager coreSettingsManager, STAuthenticationEntryPoint authEntryPoint) {
        return new DynamicAuthorizationFilter(coreSettingsManager, authEntryPoint, publicResourcePaths);
    }

    @Bean
    public STUserDetailsService stUserDetailsService() {
        return new STUserDetailsService();
    }

    @Bean
    public STAuthenticationEntryPoint authEntryPoint() {
        return new STAuthenticationEntryPoint();
    }

    @Bean
    public STAuthenticationSuccessHandler authSuccessHandler() {
        return new STAuthenticationSuccessHandler();
    }

    @Bean
    public STAuthenticationFailureHandler authFailHandler() {
        return new STAuthenticationFailureHandler();
    }

    @Bean
    public STAccessDeniedHandler accessDeniedHandler() {
        return new STAccessDeniedHandler();
    }

    @Bean
    public STLogoutSuccessHandler logoutSuccessHandler() {
        return new STLogoutSuccessHandler();
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Bean
    public SAMLAuthenticationSuccessHandler samlAuthenticationSuccessHandler() {
        return new SAMLAuthenticationSuccessHandler();
    }

    @Bean
    public SecurityFilterChain samlFilterChain(HttpSecurity http, SemanticTurkeyCoreSettingsManager coreSettingsManager) throws Exception {

        //permitAll requests, actual checks are implemented by DynamicAuthorizationFilter
        http
                .authorizeHttpRequests(requests -> requests.anyRequest().permitAll());


        //---- SAML start ----

        initConfig();

        RelyingPartyRegistrationRepository relyingPartyRegistrationRepository = relyingPartyRegistrations();
        DefaultRelyingPartyRegistrationResolver relyingPartyRegistrationResolver =
                new DefaultRelyingPartyRegistrationResolver(relyingPartyRegistrationRepository);

        //Filter for generating xml metadata file of ST (at URL /saml2/service-provider-metadata/{registrationId})
        Saml2MetadataFilter samlMetadataFilter = new Saml2MetadataFilter(relyingPartyRegistrationResolver, new OpenSamlMetadataResolver());
        samlMetadataFilter.setRequestMatcher(new AntPathRequestMatcher("/semanticturkey/saml2/service-provider-metadata/{registrationId}", "GET"));;

        OpenSaml4AuthenticationProvider authenticationProvider = getOpenSaml4AuthenticationProvider();

        http
                .saml2Login(saml2 ->
                        saml2
                                .relyingPartyRegistrationRepository(relyingPartyRegistrationRepository)
                                .authenticationManager(new ProviderManager(authenticationProvider))
                                .loginProcessingUrl(assertionConsumerPattern) //semanticturkey/saml2/login/sso/st_saml
                                .authenticationRequestUri(authenticationPattern) //semanticturkey/saml2/authenticate/st_saml
                                .successHandler(samlAuthenticationSuccessHandler())
                ).addFilterBefore(samlMetadataFilter, Saml2WebSsoAuthenticationFilter.class);
        //---- SAML end ----

        http
                .formLogin(form ->
                        form.loginProcessingUrl("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/Auth/login")
                                .usernameParameter("email")
                                .passwordParameter("password")
                                .successHandler(authSuccessHandler())
                                .failureHandler(authFailHandler()).permitAll()
                );

        STAuthenticationEntryPoint authEntryPoint = authEntryPoint();

        http
                .addFilterBefore(dynamicAuthorizationFilter(coreSettingsManager, authEntryPoint), UsernamePasswordAuthenticationFilter.class)
                .addFilterAt(corsFilter(), ChannelProcessingFilter.class)
                .exceptionHandling(exHanding -> exHanding.accessDeniedHandler(accessDeniedHandler()))
                .logout(form ->
                        form
                                .deleteCookies("JSESSIONID")
                                .logoutUrl("/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/Auth/logout")
                                .logoutSuccessHandler(logoutSuccessHandler())
                                .permitAll()
                )
                .csrf(AbstractHttpConfigurer::disable);

        //disable x-frame-options header in response
        http.headers(headers -> {
            headers.frameOptions(HeadersConfigurer.FrameOptionsConfig::disable);
        });

        http.exceptionHandling(exceptionHandlingCustomizer -> {
            exceptionHandlingCustomizer.authenticationEntryPoint(authEntryPoint);
        });
        return http.build();
    }

    /*
     * ID set in spring.security.saml2.relyingparty.registration
     * Used, in particular, in:
     * /saml2/authenticate/{registrationId} - The endpoint that generates a <saml2:AuthnRequest>
     * /login/saml2/sso/{registrationId} - The endpoint that authenticates an asserting party’s <saml2:Response>
     * ref: https://docs.spring.io/spring-security/reference/6.0/servlet/saml2/login/overview.html#servlet-saml2login-rpr-uripatterns
     */
    String samlRegistrationId = "st_saml";

    private OpenSaml4AuthenticationProvider getOpenSaml4AuthenticationProvider() {
        OpenSaml4AuthenticationProvider authenticationProvider = new OpenSaml4AuthenticationProvider();

        /*
        Override the default assertion validator that throws an error
        [invalid_assertion] Invalid assertion [...] for SAML response [...]:
        Unknown Condition '{urn:oasis:names:tc:SAML:2.0:assertion}ProxyRestriction' of type 'null' in assertion '...'
        when the AuthnResponse contains ProxyRestriction as condition
         */
        authenticationProvider.setAssertionValidator(assertionToken -> {
            Saml2ResponseValidatorResult result = OpenSaml4AuthenticationProvider
                    .createDefaultAssertionValidator()
                    .convert(assertionToken);
            if (result != null) {
                for (Saml2Error e : result.getErrors()) {
                    logger.debug("SAML ERROR " + e.toString());
                }
                //filter out error containing "ProxyRestriction"
                Collection<Saml2Error> errors = result.getErrors().stream().filter(e ->
                        !(e.getErrorCode().equals("invalid_assertion") && e.getDescription().contains("ProxyRestriction"))).toList();
                if (errors.isEmpty()) {
                    //if now there's no more errors, return a success result
                    return Saml2ResponseValidatorResult.success();
                }
            }
            //if this code is reached, result is null or has errors (different from the one filtered above)
            return result;
        });

        authenticationProvider.setResponseAuthenticationConverter(responseToken -> {
            //handler of SAML AuthnResponse
            Saml2Authentication authentication = createDefaultResponseAuthenticationConverter()
                    .convert(responseToken);

            //retrieve the expected attributes from the response
            Saml2AuthenticatedPrincipal samlPrincipal = (Saml2AuthenticatedPrincipal) authentication.getPrincipal();
            logger.debug("SAML authentication principal attributes " + samlPrincipal.getAttributes());
            String emailAddress = samlPrincipal.getFirstAttribute("emailAddress");
            String givenName = samlPrincipal.getFirstAttribute("firstName");
            String lastName = samlPrincipal.getFirstAttribute("lastName");

            AbstractAuthenticationToken auth;
            try {
                //look for the emailAddress attribute in the user base and create an auth token
                UserDetails loggedUser = stUserDetailsService().loadUserByUsername(emailAddress);
                auth = new UsernamePasswordAuthenticationToken(loggedUser, loggedUser.getPassword(), loggedUser.getAuthorities());
            } catch (UsernameNotFoundException e) {
                //if not found, create a mockup user with the related auth token
                STUser loggedUser = new STUser(emailAddress, null, givenName, lastName);
                STUser.SamlLevel samlLevel = UsersManager.listUsers().isEmpty() ? STUser.SamlLevel.LEV_1 : STUser.SamlLevel.LEV_2;
                loggedUser.setSamlLevel(samlLevel);
                auth = new UsernamePasswordAuthenticationToken(loggedUser, loggedUser.getPassword());
            }
            //add the user to the security context
            SecurityContextHolder.getContext().setAuthentication(auth);
            return auth;
        });
        return authenticationProvider;
    }

    @Bean
    public RelyingPartyRegistrationRepository relyingPartyRegistrations() {

        Pair<PrivateKey, X509Certificate> keyCertPair = getPrivateKeyAndCertificate();
        PrivateKey privateKey = keyCertPair.getFirst();
        X509Certificate certificate = keyCertPair.getSecond();

        //https://docs.spring.io/spring-security/reference/6.0/servlet/saml2/login/overview.html#servlet-saml2login-rpr-credentials
        Saml2X509Credential signing = Saml2X509Credential.signing(privateKey, certificate);
        Saml2X509Credential decryption = Saml2X509Credential.decryption(privateKey, certificate);

        RelyingPartyRegistration.Builder builder;
        if (assertingPartyMetadataLocation != null && !assertingPartyMetadataLocation.isEmpty()) {
            builder = RelyingPartyRegistrations
                    .fromMetadataLocation(assertingPartyMetadataLocation)
                    .registrationId(samlRegistrationId);
        } else {
            //in case the IDP metadata is not provided, register a relying party with minimal mandatory fields
            builder = RelyingPartyRegistration.withRegistrationId(samlRegistrationId)
                    .assertingPartyDetails(party ->
                            party.entityId("http://example.org").singleSignOnServiceLocation("http://example.org/SSO.saml2")
                    );
        }

        builder.entityId(entityId);
        builder.assertionConsumerServiceBinding(Saml2MessageBinding.POST);
        builder.assertionConsumerServiceLocation(assertionConsumerServiceLocation);

        RelyingPartyRegistration registration = builder
                .signingX509Credentials(c -> c.add(signing))
                .decryptionX509Credentials(c -> c.add(decryption))
                .build();
        return new InMemoryRelyingPartyRegistrationRepository(registration);
    }


    @Value("${spring.security.saml2.relyingparty.registration.st_saml.signing.keystore[0].location}")
    String relyingPartyKeystoreLocation;
    @Value("${spring.security.saml2.relyingparty.registration.st_saml.signing.keystore[0].alias}")
    String relyingPartyKeystoreAlias;
    @Value("${spring.security.saml2.relyingparty.registration.st_saml.signing.keystore[0].password}")
    String relyingPartyKeystorePwd;
    @Value("${spring.security.saml2.relyingparty.registration.st_saml.signing.credentials[0].certificate-location}")
    String relyingPartyCertificationLocation;
    @Value("${spring.security.saml2.relyingparty.registration.st_saml.signing.credentials[0].private-key-location}")
    RSAPrivateKey relyingPartyPrivateKey;
    @Value("${spring.security.saml2.relyingparty.registration.st_saml.identityprovider.metadata-location}")
    String assertingPartyMetadataLocation;
    @Value("${spring.security.saml2.relyingparty.registration.st_saml.redirect-url}")
    String redirectUrl;

    @Value("${spring.security.saml2.relyingparty.registration.st_saml.base-url}")
    String baseUrl;
    //just in case we want to override the default one that is {baseUrl}/saml2/service-provider-metadata/{providerId}
    @Value("${spring.security.saml2.relyingparty.registration.st_saml.entity-id}")
    String entityId;
    //just in case we want to override the default one that is {baseUrl}/login/saml2/sso/{providerId}
    @Value("${spring.security.saml2.relyingparty.registration.st_saml.assertion-consumer-service-location}")
    String assertionConsumerServiceLocation;

    String metadataFilterGeneratorPattern = "/semanticturkey/saml2/service-provider-metadata/{registrationId}";
    String assertionConsumerPattern = "/semanticturkey/saml2/login/sso/{registrationId}";
    String authenticationPattern = "/semanticturkey/saml2/authenticate/{registrationId}";


    private Pair<PrivateKey, X509Certificate> getPrivateKeyAndCertificate() {
        /*
        By default, private.key and public.cer are provided in the classpath within the code.
        Users can use these or can provide personal key and certificate.
        Alternatively users can provide credentials using JSK keystore.
        The system looks first for keystore, in case it is provided it uses it, otherwise uses key and certificate files
        (be them the default ones or the ones provided by user)
         */
        PrivateKey privateKey;
        X509Certificate certificate = null;

        if (
                relyingPartyKeystoreLocation != null && !relyingPartyKeystoreLocation.isEmpty() &&
                        relyingPartyKeystoreAlias != null && !relyingPartyKeystoreAlias.isEmpty() &&
                        relyingPartyKeystorePwd != null && !relyingPartyKeystorePwd.isEmpty()
        ) {
            //keystore+alias+pwd provided => initialize certificate and private key with JKS Keystore
            try {
                logger.debug("Initializing certificate and private key from JKS keystore: " + relyingPartyKeystoreLocation);
                KeyStore keystore = KeyStore.getInstance("JKS");
                char[] pwdArray = relyingPartyKeystorePwd.toCharArray();
                keystore.load(new DefaultResourceLoader().getResource(relyingPartyKeystoreLocation).getInputStream(), pwdArray);
                KeyStoreX509CredentialAdapter keystoreAdapter = new KeyStoreX509CredentialAdapter(keystore, relyingPartyKeystoreAlias, pwdArray);
                certificate = keystoreAdapter.getEntityCertificate();
                privateKey = keystoreAdapter.getPrivateKey();
            } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
                throw new RuntimeException(
                        "Something went wrong when initializing Relying Party " +
                                "PrivateKey and Certification from Keystore (from location: " + relyingPartyKeystoreLocation, e);
            }
        } else {
            //initialize certificate and private key with .key and .cer files
            try {
                logger.debug("Initializing certificate and private key from provided files: " + relyingPartyCertificationLocation);
                privateKey = relyingPartyPrivateKey;
                Resource resourceCert = new DefaultResourceLoader().getResource(relyingPartyCertificationLocation);
                if (resourceCert.exists()) {
                    try (InputStream certInputStream = resourceCert.getInputStream()) {
                        certificate = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(certInputStream);
                    }
                }
            } catch (IOException | CertificateException e) {
                throw new RuntimeException(
                        "Something went wrong when initializing Relying Party PrivateKey and Certification", e);
            }
        }

        return new Pair<>(privateKey, certificate);
    }


    private void initConfig() {
        logger.info("spring.security.saml2.relyingparty.registration.st_saml.base-url: " + baseUrl);
        logger.info("spring.security.saml2.relyingparty.registration.st_saml.redirect-url: " + redirectUrl);
        logger.info("spring.security.saml2.relyingparty.registration.st_saml.entity-id: " + entityId);
        logger.info("spring.security.saml2.relyingparty.registration.st_saml.identityprovider.metadata-location: " + assertingPartyMetadataLocation);
        logger.info("spring.security.saml2.relyingparty.registration.st_saml.assertion-consumer-service-location: " + assertionConsumerServiceLocation);
        logger.info("spring.security.saml2.relyingparty.registration.st_saml.signing.credentials[0].private-key-location: " + relyingPartyPrivateKey);
        logger.info("spring.security.saml2.relyingparty.registration.st_saml.signing.credentials[0].certificate-location: " + relyingPartyCertificationLocation);
        logger.info("spring.security.saml2.relyingparty.registration.st_saml.signing.keystore[0].location: " + relyingPartyKeystoreLocation);
        logger.info("spring.security.saml2.relyingparty.registration.st_saml.signing.keystore[0].alias: " + relyingPartyKeystoreAlias);
        logger.info("spring.security.saml2.relyingparty.registration.st_saml.signing.keystore[0].password: " + relyingPartyKeystorePwd);

        /*
        Defaults accessible locations/URLs:
        - metadata filter URL (which is also used as EntityID): {baseUrl}/saml2/service-provider-metadata/{registrationId}
        - Auth Request endpoint URL: {baseUrl}/saml2/authenticate/{registrationId}
        - Assertion consumer URL: {baseUrl}/login/saml2/sso/{registrationId}
            note that the assertion consumer endpoint in documentation is reported differently in several points,
            - https://docs.spring.io/spring-security/reference/6.0/servlet/saml2/login/overview.html#_runtime_expectations
                it tells that it handles /login/saml2/sso/{registrationId}
            - https://docs.spring.io/spring-security/reference/6.0/servlet/saml2/login/overview.html#servlet-saml2login-rpr-uripatterns
                it tells that it handles /saml2/login/sso/{registrationId}
            Practically, the first seems to be the actual one used.

        Here below I set the second one as default and I also add semanticturkey/ as context path to facilitate eventual proxy redirections/rules.
        In this way all the patterns related to saml flow start with semanticturkey/saml2/...

        In a nutshell we have:
        - {baseUrl}/semanticturkey/saml2/authenticate/{registrationId} -> authentication entry point
        - {baseUrl}/semanticturkey/saml2/login/sso/{registrationId} -> auth assertion consumer
        - {baseUrl}/semanticturkey/saml2/service-provider-metadata/{registrationId} -> metadata generator (and entityID)
         */

        if (baseUrl == null || baseUrl.isEmpty()) {
            baseUrl = "{baseUrl}";
        }

        /*
        normally entityId and assertionConsumerServiceLocation should not be provided in configuration.
        We foresee this possibility anyway just for testing purposes
        (e.g. replicate old ST SAML management where entityId was {baseUrl}/semanticturkey/saml/metadata
        and assertion consumer listened at {baseUrl}/semanticturkey/saml/SSO)
         */
        if (entityId == null || entityId.isEmpty()) {
            entityId = baseUrl + metadataFilterGeneratorPattern;
        }
        if (assertionConsumerServiceLocation == null || assertionConsumerServiceLocation.isEmpty()) {
            assertionConsumerServiceLocation = baseUrl + assertionConsumerPattern;
        }

        logger.info("[SAML config] - baseUrl: " + baseUrl);
        logger.info("[SAML config] - entityId: " + entityId);
        String authEntryPointUrl = baseUrl + authenticationPattern.replace("{registrationId}", samlRegistrationId);
        logger.info("[SAML config] - authEntryPointUrl: " + authEntryPointUrl);
        String metadataGeneratorUrl = baseUrl + metadataFilterGeneratorPattern.replace("{registrationId}", samlRegistrationId);
        logger.info("[SAML config] - metadataGeneratorUrl: " + metadataGeneratorUrl);
        String assertionConsumerServiceUrl = assertionConsumerServiceLocation.replace("{registrationId}", samlRegistrationId);
        logger.info("[SAML config] - assertionConsumerServiceUrl: " + assertionConsumerServiceUrl);
    }

}
