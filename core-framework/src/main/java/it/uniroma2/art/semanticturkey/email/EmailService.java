package it.uniroma2.art.semanticturkey.email;

import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.settings.core.CoreSystemSettings;
import it.uniroma2.art.semanticturkey.settings.core.SemanticTurkeyCoreSettingsManager;
import it.uniroma2.art.semanticturkey.user.STUser;
import it.uniroma2.art.semanticturkey.user.UserException;
import it.uniroma2.art.semanticturkey.user.UsersManager;

import jakarta.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.List;

public abstract class EmailService {

	protected final String appName;
	protected final SemanticTurkeyCoreSettingsManager coreSettingsManager;

	//placeholders
	private static final String USER_EMAIL_PH = "{{user.email}}";
	private static final String USER_GIVEN_NAME_PH = "{{user.givenName}}";
	private static final String USER_FAMILY_NAME_PH = "{{user.familyName}}";
	private static final String ADMIN_EMAIL_PH = "{{admin.email}}";
	private static final String ADMIN_GIVEN_NAME_PH = "{{admin.givenName}}";
	private static final String ADMIN_FAMILY_NAME_PH = "{{admin.familyName}}";
	private static final String ADMIN_LIST_PH = "{{adminList}}";
    private static final String CLIENT_ACTIONS_ROUTE = "UserActions";
    private static final String CLIENT_ACTIONS_VERIFY = "verify";
    private static final String CLIENT_ACTIONS_ACTIVATE = "activate";

	EmailService(EmailApplicationContext ctx, SemanticTurkeyCoreSettingsManager coreSettingsManager) {
		this.appName = ctx == EmailApplicationContext.VB ? "VocBench" : "ShowVoc";
		this.coreSettingsManager = coreSettingsManager;
	}

		/**
	 * Sends an email to a new registered user. Email asks for email verification if required, otherwise
	 * inform the user to wait to be activated by admin
	 * @param user
	 * @param clientHostAddress
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 * @throws STPropertyAccessException
	 */
	public void sendRegistrationMailToUser(STUser user, String clientHostAddress)
			throws MessagingException, UnsupportedEncodingException, STPropertyAccessException {

		String text = "Dear " + USER_GIVEN_NAME_PH + " " + USER_FAMILY_NAME_PH + ",<br>" +
				"thank you for registering to " + appName + ".<br>";

		CoreSystemSettings systemSettings = coreSettingsManager.getSystemSettings();
		boolean defaultAuthService = systemSettings.authService.equals("Default");
		boolean emailVerificationRequired = systemSettings.emailVerification;

		//Customize the content of the email according some settings:
		if (defaultAuthService) { //default authentication mode
			if (emailVerificationRequired) {
                /*
                system setting "emailVerification" is enabled, the notification to user must contain
                the link for verifying the email address
                 */
                clientHostAddress = clientHostAddress.endsWith("/") ? clientHostAddress : clientHostAddress + "/";
                String verificationUrl = clientHostAddress + "#/" + CLIENT_ACTIONS_ROUTE +
                        "?action=" + CLIENT_ACTIONS_VERIFY +
                        "&token=" + user.getVerificationToken() +
                        "&email=" + user.getEmail() ;
                String verificationLink = "<a href=\"" + verificationUrl + "\">Verifiy</a>";
				text +=
                        "In order to complete the registration you need to verify your email address by clicking on the link below:" +
                        "<br><br>" + verificationLink + "<br><br>" +
						"The above link will expire after " + UsersManager.EMAIL_VERIFICATION_EXPIRATION_HOURS + " hours. " +
						"If this occurs, you can register again and then activate your account using the activation link in the new email.<br>" +
						"If you receive this email without signing up, please ignore this message.";
			} else {
                //system setting "emailVerification" disabled, registered user just needs to be enabled by admin
				text +=
                        "Your registration has been now notified to the administrator(s). " +
                        "Please wait for the administrator(s) to approve it.<br>" +
                        "After the approval, you can log into " + appName + " with the e-mail " + USER_EMAIL_PH + " and your chosen password.<br>" +
                        "Thanks for your interest.<br><br>";
            }
		} else {
            //SAML authentication, user activation and email verification not needed, they're already ensured by the IDP
			text += "You can now log into " + appName + " with your account.";
		}

		text +=	"<br><br>Kind regards";

		//null is admin user, this is safe cause no placeholders about admin are in the email content
		String emailContent = replacePlaceholders(text, user, null);

		EmailSender.sendMail(coreSettingsManager, user.getEmail(), appName + " registration", emailContent);
	}

		/**
		 * Sends an email to the system administrators to inform about a new user registration request and requiring
		 * to be activated
		 * @throws MessagingException
		 * @throws UnsupportedEncodingException
		 * @throws STPropertyAccessException
		 */
		public void sendRegistrationMailToAdmin(STUser user, String clientHostAddress)
				throws UnsupportedEncodingException, MessagingException, STPropertyAccessException, UserException {

			CoreSystemSettings systemSettings = coreSettingsManager.getSystemSettings();
			boolean defaultAuthService = systemSettings.authService.equals("Default");

			String text = "Dear " + ADMIN_GIVEN_NAME_PH + " " + ADMIN_FAMILY_NAME_PH + ",<br>" +
					"there is a new user registered to " + appName + ".<br>" +
					"Given Name: <i>" + USER_GIVEN_NAME_PH + "</i><br>" +
					"Family Name: <i>" + USER_FAMILY_NAME_PH + "</i><br>" +
					"Email: <i>" + USER_EMAIL_PH + "</i>";
			if (defaultAuthService) {
				//default auth mode (so not SAML) requires user activation by admin
				clientHostAddress = clientHostAddress.endsWith("/") ? clientHostAddress : clientHostAddress + "/";
				String activationUrl = clientHostAddress + "#/" + CLIENT_ACTIONS_ROUTE +
						"?action=" + CLIENT_ACTIONS_ACTIVATE +
						"&token=" + user.getActivationToken() +
						"&email=" + user.getEmail();
				String activationLink = "<a href=\"" + activationUrl + "\">Activate</a>";
				text += "<br>You can activate the account from the administration page in " + appName +
						" or by clicking on the link below:<br><br>" + activationLink;
			}
			text += "<br><br>Kind regards";

			for (String adminEmail : UsersManager.getAdminEmailList()) {
				STUser admin = UsersManager.getUser(adminEmail);
				String emailContent = replacePlaceholders(text, user, admin);
				EmailSender.sendMail(coreSettingsManager, adminEmail, appName + ": new user registered", emailContent);
			}
		}

	/**
	 * Sends an email to a user to inform that his/her account has been enabled
	 *
	 * @param user
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 * @throws STPropertyAccessException
	 */
	public void sendEnabledMailToUser(STUser user) throws MessagingException, UnsupportedEncodingException, STPropertyAccessException {
		String text = "Dear " + USER_GIVEN_NAME_PH + " " + USER_FAMILY_NAME_PH + ",<br>" +
				"the administrator has enabled your account. You can now log into " + appName + " with the email " +
				"<i>" + USER_EMAIL_PH + "</i> and your chosen password.<br><br>" +
				"Kind regards";
		//null is admin, can be safely omitted cause no admin placeholders are in the email text
		text = replacePlaceholders(text, user, null);
		EmailSender.sendMail(coreSettingsManager, user.getEmail(), appName + " account enabled", text);
	}

	/**
	 * Sends an email to a verified user informing to wait to be enabled
	 * @param user
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 * @throws STPropertyAccessException
	 */
	public void sendVerifiedMailToUser(STUser user) throws MessagingException, UnsupportedEncodingException, STPropertyAccessException {
		String text = "Dear " + USER_GIVEN_NAME_PH + " " + USER_FAMILY_NAME_PH + ",<br>" +
				"your email has been verified. Your registration has been now notified to the administrator. " +
				"Please wait for the administrator to approve it.<br>" +
				"After the approval, you can log into " + appName + " with the e-mail " + USER_EMAIL_PH + " and your chosen password.<br>" +
				"Thanks for your interest.<br><br>" +
				"Kind regards";
		//null is admin, can be safely omitted cause no admin placeholders are in the email text
		text = replacePlaceholders(text, user, null);
		EmailSender.sendMail(coreSettingsManager, user.getEmail(), appName + " email verified", text);
	}

	/*
	 * The following emails are not configurable. The reset password confirmed is not configurable because it must contains the
	 * new generated temporary password. The same for the reset password requested that must contains the link to
	 * complete the procedure.
	 * Both of these two mail could be made configurable with dedicated placeholders (e.g. {{newPwd}} and {{resetPwdLink}})
	 * in the setting value, but this could be risky, in fact if the placeholders were removed, the reset password
	 * procedure would be corrupted.
	 */

	/**
	 * Sends an email to the given address just for testing the email service configuration
	 * @param mailTo
	 */
	public void sendMailServiceConfigurationTest(String mailTo) throws UnsupportedEncodingException, MessagingException, STPropertyAccessException {
		String text = "This message has been sent to check the configuration of " + appName + " email service.<br>" +
				"If you did not request to send this email, please ignore it." +
				"<br><br>Kind regards.";
		EmailSender.sendMail(coreSettingsManager, mailTo, appName + " email service configuration check", text);
	}

	/**
	 * Sends an email that informs the given user that its password has been replaced with the tempPassword
	 * @param user
	 * @param tempPassword
	 * @throws UnsupportedEncodingException
	 * @throws MessagingException
	 * @throws STPropertyAccessException
	 */
	public void sendResetPasswordConfirmedMail(STUser user, String tempPassword) throws UnsupportedEncodingException,
			MessagingException, STPropertyAccessException {
		String text = "Dear " + USER_GIVEN_NAME_PH + " " + USER_FAMILY_NAME_PH + ","
				+ "<br>we confirm you that your password has been reset."
				+ "<br>This is your new temporary password:"
				+ "<br><br>" + tempPassword
				+ "<br><br>After the login we strongly recommend you to change the password."
				+ "<br><br>Kind regards.";
		text = replacePlaceholders(text, user, null);
		EmailSender.sendMail(coreSettingsManager, user.getEmail(), appName + " password reset", text);
	}

	/**
	 * Sends an email that provides to the given user the info for resetting the password
	 * @param user
	 * @param forgotPasswordLink
	 * @throws UnsupportedEncodingException
	 * @throws MessagingException
	 * @throws STPropertyAccessException
	 */
	public void sendResetPasswordRequestedMail(STUser user, String forgotPasswordLink)
			throws UnsupportedEncodingException, MessagingException, STPropertyAccessException {
		String text = "Dear " + USER_GIVEN_NAME_PH + " " + USER_FAMILY_NAME_PH + ","
				+ "<br>we've received a request to reset the password for the " + appName
				+ " account associated to this email address."
				+ "<br>Click the link below to be redirected to the reset password page."
				+ " This password reset is only valid for a limited time."
				+ "<br><br>" + forgotPasswordLink + "<br><br>"
				+ "If you did not request a password reset, please ignore this email."
				+ "<br><br>Kind regards.";
		text = replacePlaceholders(text, user, null);
		EmailSender.sendMail(coreSettingsManager, user.getEmail(), appName + " password reset", text);
	}


	/**
	 * Sends an email to tha administrator users for notifying them that a project has been created
	 * (it doesn't send to the user who created)
	 * @param creator
	 * @param project
	 * @throws UnsupportedEncodingException
	 * @throws MessagingException
	 * @throws STPropertyAccessException
	 * @throws UserException
	 */
	public void sendProjCreationMailToAdmin(STUser creator, Project project)
			throws UnsupportedEncodingException, MessagingException, STPropertyAccessException, UserException {
		List<String> adminEmailList = UsersManager.getAdminEmailList().stream()
				.filter(email -> !email.equals(creator.getEmail())).toList();

		String text = "Dear " + ADMIN_GIVEN_NAME_PH + " " + ADMIN_FAMILY_NAME_PH + ",<br>" +
				"a new project with name " + formatItalic(project.getName()) + " has been created by " +
				USER_GIVEN_NAME_PH + " " + USER_FAMILY_NAME_PH + " (" + USER_EMAIL_PH + ").<br><br>" +
				"Kind regards";

		for (String adminEmail: adminEmailList) {
			STUser admin = UsersManager.getUser(adminEmail);
			String emailContent = replacePlaceholders(text, creator, admin);
			EmailSender.sendMail(coreSettingsManager, adminEmail, appName + ": new project created", emailContent);
		}
	}


	protected static String formatBold(String text) {
		return "<b>" + text + "</b>";
	}
	protected static String formatItalic(String text) {
		return "<i>" + text + "</i>";
	}

	protected static String replacePlaceholders(String text, STUser user, STUser admin) {
		String replaced = text;
		if (user != null) {
			replaced = replaced.replace(USER_EMAIL_PH, user.getEmail());
			replaced = replaced.replace(USER_GIVEN_NAME_PH, user.getGivenName());
			replaced = replaced.replace(USER_FAMILY_NAME_PH, user.getFamilyName());
		}
		if (admin != null) {
			replaced = replaced.replace(ADMIN_EMAIL_PH, admin.getEmail());
			replaced = replaced.replace(ADMIN_GIVEN_NAME_PH, admin.getGivenName());
			replaced = replaced.replace(ADMIN_FAMILY_NAME_PH, admin.getFamilyName());
		}
		if (replaced.contains(ADMIN_LIST_PH)) {
			Collection<String> adminEmails = UsersManager.getAdminEmailList();
			replaced = replaced.replace(ADMIN_LIST_PH, String.join(", ", adminEmails));
		}
		return replaced;
	}

}
