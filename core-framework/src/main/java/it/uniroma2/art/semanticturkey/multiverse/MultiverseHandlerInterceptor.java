package it.uniroma2.art.semanticturkey.multiverse;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * An {@link HandlerInterceptor} that select the right <em>world</em> in the multiverse based on the request parameter
 * <code>ctx_world</code>.
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class MultiverseHandlerInterceptor implements HandlerInterceptor {
    public static final String CTX_WORLD_PARAM = "ctx_world";


    private final MultiverseManager multiverseManager;

    public MultiverseHandlerInterceptor(MultiverseManager multiverseManager) {
        this.multiverseManager = multiverseManager;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String world = request.getParameter(CTX_WORLD_PARAM);
        multiverseManager.setCurrentWorld(world);
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        multiverseManager.releaseCurrentWorld();
    }
}
