package it.uniroma2.art.semanticturkey.security;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import it.uniroma2.art.semanticturkey.servlet.JSONResponseREPLY;
import it.uniroma2.art.semanticturkey.servlet.ServiceVocabulary.RepliesStatus;
import it.uniroma2.art.semanticturkey.servlet.ServiceVocabulary.SerializationType;
import it.uniroma2.art.semanticturkey.servlet.ServletUtilities;

/**
 * 
 * @author Tiziano
 * Implementation of the LogoutSuccessHandler. Returns an HTTP status code of 200.
 * This is useful in REST-type scenarios where a redirect upon a successful logout is not desired.
 * (Referenced in WEB-INF/spring-security.xml)
 * 
 */
public class STLogoutSuccessHandler implements LogoutSuccessHandler {

	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException {

		//removes the user attribute from the session for keeping track of active users. See {@link LoggedUser}
		HttpSession session = request.getSession();
		if (session != null){
			session.removeAttribute("user");
		}

		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_OK);
		ServletOutputStream out = response.getOutputStream();
		JSONResponseREPLY jsonResp = (JSONResponseREPLY) ServletUtilities.getService()
				.createReplyResponse("logout", RepliesStatus.ok, SerializationType.json);
		out.print(jsonResp.toString());
	}
	
}
