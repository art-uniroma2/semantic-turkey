package it.uniroma2.art.semanticturkey.settings.core;

import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;

import java.util.Map;

public class PredicateLabelSettings implements STProperties {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.core.PredicateLabelSettings";

        public static final String shortName = keyBase + ".shortName";
        public static final String enabled$description = keyBase + ".enabled.description";
        public static final String enabled$displayName = keyBase + ".enabled.displayName";
        public static final String mappings$description = keyBase + ".mappings.description";
        public static final String mappings$displayName = keyBase + ".mappings.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + PredicateLabelSettings.MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + PredicateLabelSettings.MessageKeys.enabled$description + "}", displayName = "{" + PredicateLabelSettings.MessageKeys.enabled$displayName +"}")
    public Boolean enabled;

    @STProperty(description = "{" + PredicateLabelSettings.MessageKeys.mappings$description + "}", displayName = "{" + PredicateLabelSettings.MessageKeys.mappings$displayName +"}")
    public Map<String, Map<String, String>> mappings; //map lang => predIri => label

}
