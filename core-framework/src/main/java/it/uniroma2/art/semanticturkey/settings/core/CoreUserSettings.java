package it.uniroma2.art.semanticturkey.settings.core;

import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.properties.STProperty;

import java.util.List;

public class CoreUserSettings implements Settings {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.core.CoreUserSettings";

        public static final String shortName = keyBase + ".shortName";
        public static final String projectVisualization$description = keyBase + ".projectVisualization.description";
        public static final String projectVisualization$displayName = keyBase + ".projectVisualization.displayName";

        public static final String favoriteDatasets$description = keyBase + ".favoriteDatasets.description";
        public static final String favoriteDatasets$displayName = keyBase + ".favoriteDatasets.displayName";

        public static final String clickableValueStyle$description = keyBase + ".clickableValueStyle.description";
        public static final String clickableValueStyle$displayName = keyBase + ".clickableValueStyle.displayName";

        public static final String projectRendering$description = keyBase + ".projectRendering.description";
        public static final String projectRendering$displayName = keyBase + ".projectRendering.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + CoreUserSettings.MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + CoreUserSettings.MessageKeys.projectVisualization$description + "}",
            displayName = "{" + CoreUserSettings.MessageKeys.projectVisualization$displayName + "}")
    public ProjectVisualization projectVisualization;

    @STProperty(description = "{" + CoreUserSettings.MessageKeys.favoriteDatasets$description + "}",
            displayName = "{" + CoreUserSettings.MessageKeys.favoriteDatasets$displayName + "}")
    public List<String> favoriteDatasets;

    @STProperty(description = "{" + CoreUserSettings.MessageKeys.clickableValueStyle$description + "}",
            displayName = "{" + CoreUserSettings.MessageKeys.clickableValueStyle$displayName + "}")
    public ClickableValueStyle clickableValueStyle;

    @STProperty(description = "{" + CoreUserSettings.MessageKeys.projectRendering$description + "}",
            displayName = "{" + CoreUserSettings.MessageKeys.projectRendering$displayName + "}")
    public Boolean projectRendering;

}
