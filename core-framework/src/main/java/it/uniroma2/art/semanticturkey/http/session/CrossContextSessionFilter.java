package it.uniroma2.art.semanticturkey.http.session;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CrossContextSessionFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
        CrossContextSessionRequestWrapper customRequest =
                new CrossContextSessionRequestWrapper((HttpServletRequest)request, (HttpServletResponse)response);
		chain.doFilter(customRequest, response);
	}

	@Override
	public void destroy() {
		// nothing to do
	}
	
}
