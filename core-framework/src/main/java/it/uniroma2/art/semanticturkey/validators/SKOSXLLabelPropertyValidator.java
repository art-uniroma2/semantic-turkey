package it.uniroma2.art.semanticturkey.validators;

import it.uniroma2.art.semanticturkey.constraints.SKOSXLLabelProperty;
import it.uniroma2.art.semanticturkey.services.STServiceContext;
import it.uniroma2.art.semanticturkey.services.support.STServiceContextUtils;
import it.uniroma2.art.semanticturkey.tx.RDF4JRepositoryUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.SKOSXL;
import org.eclipse.rdf4j.query.BooleanQuery;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.springframework.beans.factory.annotation.Autowired;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

/**
 * Validators associated with the constraint {@link SKOSXLLabelProperty}
 * 
 * @author Tiziano Lorenzetti
 */
public class SKOSXLLabelPropertyValidator implements ConstraintValidator<SKOSXLLabelProperty, Resource> {

	private SKOSXLLabelProperty annotation;

	@Autowired
	private STServiceContext serviceContext;

	@Override
	public void initialize(SKOSXLLabelProperty value) {
		this.annotation = value;
	}

	@Override
	public boolean isValid(Resource value, ConstraintValidatorContext context) {
		try {
			if (value == null) {
				return true;
			}
			
			SimpleValueFactory vf = SimpleValueFactory.getInstance();
			List<IRI> xlLexProps = Arrays.asList(SKOSXL.PREF_LABEL, SKOSXL.ALT_LABEL, SKOSXL.HIDDEN_LABEL);

			if (xlLexProps.contains(value)) { //a prop is subPropOf itself
				return true; //avoid to perform the sparql query
			}
			
			try (RepositoryConnection repoConn = RDF4JRepositoryUtils
					.getConnection(STServiceContextUtils.getRepository(serviceContext))) {
				String query = "ASK { \n"
						+ "VALUES ?xlProp {\n";
						for (IRI p : xlLexProps) {
							query += NTriplesUtil.toNTriplesString(p) + " ";
						}
				query += "}\n"
						+ NTriplesUtil.toNTriplesString(value) + " "
						+ NTriplesUtil.toNTriplesString(RDFS.SUBPROPERTYOF) + "* "
						+ "?xlProp \n"
						+ " }";
				BooleanQuery bq = repoConn.prepareBooleanQuery(query);
				return bq.evaluate();
			}
				
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
