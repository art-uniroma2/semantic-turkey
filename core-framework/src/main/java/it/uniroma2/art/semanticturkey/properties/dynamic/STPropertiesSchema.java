package it.uniroma2.art.semanticturkey.properties.dynamic;

import java.lang.reflect.AnnotatedType;
import java.net.URL;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.BooleanUtils;
import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.semanticturkey.constraints.LanguageTaggedString;
import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import it.uniroma2.art.semanticturkey.properties.Schema;
import it.uniroma2.art.semanticturkey.properties.dynamic.DynamicSTProperties.PropertyDefinition;

/**
 * A schema for an {@link STProperties} object. It should be used together with the annotation {@link Schema}
 * to implement dynamically typed properties fields.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class STPropertiesSchema implements Settings {

	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.properties.STPropertiesSchema";

		public static final String shortName = keyBase + ".shortName";
		public static final String properties$description = keyBase + ".properties.description";
		public static final String properties$displayName = keyBase + ".properties.displayName";
	}

	@Override
	public String getShortName() {
		return "{" + MessageKeys.shortName + "}";
	}

	@STProperty(description = "{" + MessageKeys.properties$description + "}", displayName = "{"
			+ MessageKeys.properties$displayName + "}")
	public List<STPropertySchema> properties;

	/**
	 * Returns an {@link STProperties} object that conforms to this schema
	 * 
	 * @return
	 */
	public STProperties toSTProperties() {
		DynamicSTProperties rv = new DynamicSTProperties();

		if (properties != null) {
			for (STPropertySchema propSchema : properties) {
				boolean required = propSchema.required;
				java.util.List<@LanguageTaggedString Literal> description = propSchema.description;
				java.util.List<@LanguageTaggedString Literal> displayName = propSchema.displayName;

				AnnotatedType annotatedType = makeElementType(propSchema.type);

				PropertyDefinition dynPropDef = new PropertyDefinition(displayName, description, required,
						annotatedType);
				if (propSchema.enumeration != null) {
					dynPropDef.setEnumeration(propSchema.enumeration.values, propSchema.enumeration.open);
				}

				rv.addProperty(propSchema.name, dynPropDef);
			}
		}

		return rv;
	}

	protected AnnotatedType makeElementType(STPropertyType type) {
		Class<?> baseType = null;
		switch (type.name) {
		case "boolean":
			baseType = Boolean.class;
		case "integer":
			baseType = Integer.class;
		case "short":
			baseType = Short.class;
		case "long":
			baseType = Long.class;
		case "float":
			baseType = Float.class;
		case "double":
			baseType = Double.class;
		case "IRI":
			baseType = IRI.class;
		case "Literal":
			baseType = Literal.class;
		case "RDFValue":
			baseType = Value.class;
		case "URL":
			baseType = URL.class;
		case "String":
			baseType = String.class;
		}

		if (baseType == null) {
			throw new IllegalArgumentException("Unsupported type " + type.name);
		}

		AnnotatedType annotatedType = new DynamicSTProperties.AnnotatedTypeBuilder().withType(baseType).build();

		if (BooleanUtils.isTrue(type.set)) { // optionally wrap the type inside a set
			annotatedType = new DynamicSTProperties.AnnotatedTypeBuilder().withType(Set.class).withTypeArgument(annotatedType).build();
		}

		return annotatedType;
	}
}
