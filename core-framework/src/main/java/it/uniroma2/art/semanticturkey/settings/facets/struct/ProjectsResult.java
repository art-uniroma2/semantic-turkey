package it.uniroma2.art.semanticturkey.settings.facets.struct;

import java.util.ArrayList;
import java.util.List;

public class ProjectsResult {
    List<String> projectNameList = new ArrayList<>();

    public ProjectsResult() {
    }

    public void addProjectName(String projectName) {
        if (!projectNameList.contains(projectName)) {
            projectNameList.add(projectName);
        }
    }

    public List<String> getProjectNameList() {
        return projectNameList;
    }
}
