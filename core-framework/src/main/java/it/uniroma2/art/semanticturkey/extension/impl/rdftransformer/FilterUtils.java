package it.uniroma2.art.semanticturkey.extension.impl.rdftransformer;

import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;

import java.util.stream.Stream;

/**
 * Utility class for the implementation of filters.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
public abstract class FilterUtils {

	/**
	 * Returns the provided array of graphs, or return every graph (the name of which is an IRI) in case of an
	 * empty array.
	 * 
	 * @param workingRepositoryConnection
	 * @param graphs
	 * @return
	 */
	public static IRI[] expandGraphs(RepositoryConnection workingRepositoryConnection, IRI[] graphs) {
		if (graphs.length != 0) {
			return graphs;
		}

		try(RepositoryResult<Resource> contextIDs = workingRepositoryConnection.getContextIDs();
			Stream<Resource> stream = QueryResults.stream(contextIDs)) {
			return stream.filter(IRI.class::isInstance).toArray(IRI[]::new);
		}
	}
}
