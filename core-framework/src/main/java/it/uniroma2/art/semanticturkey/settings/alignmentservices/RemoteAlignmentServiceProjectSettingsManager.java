package it.uniroma2.art.semanticturkey.settings.alignmentservices;

import it.uniroma2.art.semanticturkey.extension.settings.ProjectSettingsManager;
import org.springframework.stereotype.Component;

/**
 * A settings manager for the association of a project with a remote alignment service.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
@Component
public class RemoteAlignmentServiceProjectSettingsManager
		implements ProjectSettingsManager<RemoteAlignmentServiceProjectSettings> {

	@Override
	public String getId() {
		return RemoteAlignmentServiceProjectSettingsManager.class.getName();
	}

}
