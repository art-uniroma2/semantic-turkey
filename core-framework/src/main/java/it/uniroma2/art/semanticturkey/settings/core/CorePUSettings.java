package it.uniroma2.art.semanticturkey.settings.core;

import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.properties.Enumeration;
import it.uniroma2.art.semanticturkey.properties.STProperty;
import org.eclipse.rdf4j.model.IRI;

import java.util.List;
import java.util.Map;

public class CorePUSettings implements Settings {

	public static class MessageKeys {
		public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.core.CorePUSettings";

		public static final String shortName = keyBase + ".shortName";
		public static final String editingLanguage$description = keyBase + ".editingLanguage.description";
		public static final String editingLanguage$displayName = keyBase + ".editingLanguage.displayName";
        public static final String filterValueLanguages$description = keyBase + ".filterValueLanguages.description";
        public static final String filterValueLanguages$displayName = keyBase + ".filterValueLanguages.displayName";
        public static final String activeSchemes$description = keyBase + ".activeSchemes.description";
        public static final String activeSchemes$displayName = keyBase + ".activeSchemes.displayName";
        public static final String activeLexicon$description = keyBase + ".activeLexicon.description";
        public static final String activeLexicon$displayName = keyBase + ".activeLexicon.displayName";
        public static final String showFlags$description = keyBase + ".showFlags.description";
        public static final String showFlags$displayName = keyBase + ".showFlags.displayName";
		public static final String projectTheme$description = keyBase + ".projectTheme.description";
		public static final String projectTheme$displayName = keyBase + ".projectTheme.displayName";
		public static final String classTree$description = keyBase + ".classTree.description";
		public static final String classTree$displayName = keyBase + ".classTree.displayName";
		public static final String hiddenDataPanelsRdfsOwl$description = keyBase + ".hiddenDataPanelsRdfsOwl.description";
		public static final String hiddenDataPanelsRdfsOwl$displayName = keyBase + ".hiddenDataPanelsRdfsOwl.displayName";
		public static final String hiddenDataPanelsSkos$description = keyBase + ".hiddenDataPanelsSkos.description";
		public static final String hiddenDataPanelsSkos$displayName = keyBase + ".hiddenDataPanelsSkos.displayName";
		public static final String hiddenDataPanelsOntolex$description = keyBase + ".hiddenDataPanelsOntolex.description";
		public static final String hiddenDataPanelsOntolex$displayName = keyBase + ".hiddenDataPanelsOntolex.displayName";
		public static final String instanceList$description = keyBase + ".instanceList.description";
		public static final String instanceList$displayName = keyBase + ".instanceList.displayName";
		public static final String conceptTree$description = keyBase + ".conceptTree.description";
		public static final String conceptTree$displayName = keyBase + ".conceptTree.displayName";
		public static final String lexEntryList$description = keyBase + ".lexEntryList.description";
		public static final String lexEntryList$displayName = keyBase + ".lexEntryList.displayName";
		public static final String customTree$description = keyBase + ".customTree.description";
		public static final String customTree$displayName = keyBase + ".customTree.displayName";
		public static final String graphViewPartitionFilter$description = keyBase + ".graphViewPartitionFilter.description";
		public static final String graphViewPartitionFilter$displayName = keyBase + ".graphViewPartitionFilter.displayName";
		public static final String resourceView$description = keyBase + ".resourceView.description";
		public static final String resourceView$displayName = keyBase + ".resourceView.displayName";
		public static final String resViewPartitionFilter$description = keyBase + ".resViewPartitionFilter.description";
		public static final String resViewPartitionFilter$displayName = keyBase + ".resViewPartitionFilter.displayName";
		public static final String resViewHideLang$description = keyBase + ".resViewHideLang.description";
		public static final String resViewHideLang$displayName = keyBase + ".resViewHideLang.displayName";
		public static final String resViewShowSectionHeader$description = keyBase + ".resViewShowSectionHeader.description";
		public static final String resViewShowSectionHeader$displayName = keyBase + ".resViewShowSectionHeader.displayName";
		public static final String hideLiteralGraphNodes$description = keyBase + ".hideLiteralGraphNodes.description";
		public static final String hideLiteralGraphNodes$displayName = keyBase + ".hideLiteralGraphNodes.displayName";
		public static final String searchSettings$description = keyBase + ".searchSettings.description";
		public static final String searchSettings$displayName = keyBase + ".searchSettings.displayName";
		public static final String notificationsStatus$description = keyBase + ".notificationsStatus.description";
		public static final String notificationsStatus$displayName = keyBase + ".notificationsStatus.displayName";
		public static final String sheet2rdfSettings$description = keyBase + ".sheet2rdfSettings.description";
		public static final String sheet2rdfSettings$displayName = keyBase + ".sheet2rdfSettings.displayName";
		public static final String hiddenShowVocTabs$description = keyBase + ".hiddenShowVocTabs.description";
		public static final String hiddenShowVocTabs$displayName = keyBase + ".hiddenShowVocTabs.displayName";
		public static final String renderingClassTree$description = keyBase + ".renderingClassTree.description";
		public static final String renderingClassTree$displayName = keyBase + ".renderingClassTree.displayName";
		public static final String renderingInstanceList$description = keyBase + ".renderingInstanceList.description";
		public static final String renderingInstanceList$displayName = keyBase + ".renderingInstanceList.displayName";
		public static final String renderingDatatypeList$description = keyBase + ".renderingDatatypeList.description";
		public static final String renderingDatatypeList$displayName = keyBase + ".renderingDatatypeList.displayName";
		public static final String renderingPropertyTree$description = keyBase + ".renderingPropertyTree.description";
		public static final String renderingPropertyTree$displayName = keyBase + ".renderingPropertyTree.displayName";
		public static final String renderingCollectionTree$description = keyBase + ".renderingCollectionTree.description";
		public static final String renderingCollectionTree$displayName = keyBase + ".renderingCollectionTree.displayName";
		public static final String renderingConceptTree$description = keyBase + ".renderingConceptTree.description";
		public static final String renderingConceptTree$displayName = keyBase + ".renderingConceptTree.displayName";
		public static final String renderingSchemeList$description = keyBase + ".renderingSchemeList.description";
		public static final String renderingSchemeList$displayName = keyBase + ".renderingSchemeList.displayName";
		public static final String renderingLexEntryList$description = keyBase + ".renderingLexEntryList.description";
		public static final String renderingLexEntryList$displayName = keyBase + ".renderingLexEntryList.displayName";
		public static final String renderingLexiconList$description = keyBase + ".renderingLexiconList.description";
		public static final String renderingLexiconList$displayName = keyBase + ".renderingLexiconList.displayName";
	}

	@Override
	public String getShortName() {
		return "{" + MessageKeys.shortName + "}";
	}

	@STProperty(description = "{" + MessageKeys.editingLanguage$description
			+ "}", displayName = "{" + MessageKeys.editingLanguage$displayName + "}")
	public String editingLanguage;

    @STProperty(description = "{" + MessageKeys.filterValueLanguages$description
            + "}", displayName = "{" + MessageKeys.filterValueLanguages$displayName + "}")
    public ValueFilterLanguages filterValueLanguages;

    @STProperty(description = "{" + MessageKeys.activeSchemes$description
            + "}", displayName = "{" + MessageKeys.activeSchemes$displayName + "}")
    public List<IRI> activeSchemes;

    @STProperty(description = "{" + MessageKeys.activeLexicon$description
            + "}", displayName = "{" + MessageKeys.activeLexicon$displayName + "}")
    public IRI activeLexicon;

    @STProperty(description = "{" + MessageKeys.showFlags$description
            + "}", displayName = "{" + MessageKeys.showFlags$displayName + "}")
    public Boolean showFlags;

	@STProperty(description = "{" + MessageKeys.projectTheme$description
			+ "}", displayName = "{" + MessageKeys.projectTheme$displayName + "}")
	public String projectTheme;

	@STProperty(description = "{" + MessageKeys.hiddenDataPanelsRdfsOwl$description
			+ "}", displayName = "{" + MessageKeys.hiddenDataPanelsRdfsOwl$displayName + "}")
	public List<String> hiddenDataPanelsRdfsOwl;

	@STProperty(description = "{" + MessageKeys.hiddenDataPanelsSkos$description
			+ "}", displayName = "{" + MessageKeys.hiddenDataPanelsSkos$displayName + "}")
	public List<String> hiddenDataPanelsSkos;

	@STProperty(description = "{" + MessageKeys.hiddenDataPanelsOntolex$description
			+ "}", displayName = "{" + MessageKeys.hiddenDataPanelsOntolex$displayName + "}")
	public List<String> hiddenDataPanelsOntolex;

	@STProperty(description = "{" + MessageKeys.classTree$description
			+ "}", displayName = "{" + MessageKeys.classTree$displayName + "}")
	public ClassTreePreferences classTree;

	@STProperty(description = "{" + MessageKeys.instanceList$description
			+ "}", displayName = "{" + MessageKeys.instanceList$displayName + "}")
	public InstanceListPreferences instanceList;

	@STProperty(description = "{" + MessageKeys.conceptTree$description
			+ "}", displayName = "{" + MessageKeys.conceptTree$displayName + "}")
	public ConceptTreePreferences conceptTree;

	@STProperty(description = "{" + MessageKeys.lexEntryList$description
			+ "}", displayName = "{" + MessageKeys.lexEntryList$displayName + "}")
	public LexEntryListPreferences lexEntryList;

	@STProperty(description = "{" + MessageKeys.customTree$description
			+ "}", displayName = "{" + MessageKeys.customTree$displayName + "}")
	public CustomTreeSettings customTree;

	@STProperty(description = "{" + MessageKeys.resourceView$description
			+ "}", displayName = "{" + MessageKeys.resourceView$displayName + "}")
	public ResourceViewPreferences resourceView;

	@STProperty(description = "{" + MessageKeys.resViewPartitionFilter$description
			+ "}", displayName = "{" + MessageKeys.resViewPartitionFilter$displayName + "}")
	public Map<String, List<String>> resViewPartitionFilter;

	@STProperty(description = "{" + MessageKeys.resViewHideLang$description
			+ "}", displayName = "{" + MessageKeys.resViewHideLang$displayName + "}")
	public Boolean resViewHideLang;

	@STProperty(description = "{" + MessageKeys.resViewShowSectionHeader$description
			+ "}", displayName = "{" + MessageKeys.resViewShowSectionHeader$displayName + "}")
	public Boolean resViewShowSectionHeader;

	@STProperty(description = "{" + MessageKeys.graphViewPartitionFilter$description
			+ "}", displayName = "{" + MessageKeys.graphViewPartitionFilter$displayName + "}")
	public Map<String, List<String>> graphViewPartitionFilter;

	@STProperty(description = "{" + MessageKeys.hideLiteralGraphNodes$description
			+ "}", displayName = "{" + MessageKeys.hideLiteralGraphNodes$displayName + "}")
	public Boolean hideLiteralGraphNodes;

	@STProperty(description = "{" + MessageKeys.searchSettings$description
			+ "}", displayName = "{" + MessageKeys.searchSettings$displayName + "}")
	public SearchSettings searchSettings;

	@STProperty(description = "{" + MessageKeys.notificationsStatus$description
			+ "}", displayName = "{" + MessageKeys.notificationsStatus$displayName + "}")
	@Enumeration({"no_notifications", "in_app_only", "email_instant", "email_daily_digest"})
	public String notificationsStatus;

	@STProperty(description = "{" + MessageKeys.sheet2rdfSettings$description
			+ "}", displayName = "{" + MessageKeys.sheet2rdfSettings$displayName + "}")
	public Sheet2RdfSettings sheet2rdfSettings;

	@STProperty(description = "{" + MessageKeys.hiddenShowVocTabs$description
			+ "}", displayName = "{" + MessageKeys.hiddenShowVocTabs$displayName + "}")
	public List<String> hiddenShowVocTabs;

	//trees/lists rendering
	@STProperty(description = "{" + MessageKeys.renderingClassTree$description
			+ "}", displayName = "{" + MessageKeys.renderingClassTree$displayName + "}")
	public Boolean renderingClassTree;

	@STProperty(description = "{" + MessageKeys.renderingInstanceList$description
			+ "}", displayName = "{" + MessageKeys.renderingInstanceList$displayName + "}")
	public Boolean renderingInstanceList;

	@STProperty(description = "{" + MessageKeys.renderingDatatypeList$description
			+ "}", displayName = "{" + MessageKeys.renderingDatatypeList$displayName + "}")
	public Boolean renderingDatatypeList;

	@STProperty(description = "{" + MessageKeys.renderingPropertyTree$description
			+ "}", displayName = "{" + MessageKeys.renderingPropertyTree$displayName + "}")
	public Boolean renderingPropertyTree;

	@STProperty(description = "{" + MessageKeys.renderingCollectionTree$description
			+ "}", displayName = "{" + MessageKeys.renderingCollectionTree$displayName + "}")
	public Boolean renderingCollectionTree = true;

	@STProperty(description = "{" + MessageKeys.renderingConceptTree$description
			+ "}", displayName = "{" + MessageKeys.renderingConceptTree$displayName + "}")
	public Boolean renderingConceptTree = true;

	@STProperty(description = "{" + MessageKeys.renderingSchemeList$description
			+ "}", displayName = "{" + MessageKeys.renderingSchemeList$displayName + "}")
	public Boolean renderingSchemeList = true;

	@STProperty(description = "{" + MessageKeys.renderingLexEntryList$description
			+ "}", displayName = "{" + MessageKeys.renderingLexEntryList$displayName + "}")
	public Boolean renderingLexEntryList = true;

	@STProperty(description = "{" + MessageKeys.renderingLexiconList$description
			+ "}", displayName = "{" + MessageKeys.renderingLexiconList$displayName + "}")
	public Boolean renderingLexiconList = true;

}
