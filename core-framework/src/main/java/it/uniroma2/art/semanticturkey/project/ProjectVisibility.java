package it.uniroma2.art.semanticturkey.project;

public enum ProjectVisibility {
    PUBLIC, AUTHORIZED, PRISTINE
}
