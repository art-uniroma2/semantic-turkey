package it.uniroma2.art.semanticturkey.settings.core;

import it.uniroma2.art.semanticturkey.properties.Enumeration;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;

public class ProjectVisualization implements STProperties {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.core.ProjectVisualization";

        public static final String shortName = keyBase + ".shortName";
        public static final String mode$description = keyBase + ".mode.description";
        public static final String mode$displayName = keyBase + ".mode.displayName";
        public static final String facetBagOf$description = keyBase + ".facetBagOf.description";
        public static final String facetBagOf$displayName = keyBase + ".facetBagOf.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + ProjectVisualization.MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + ProjectVisualization.MessageKeys.mode$description
            + "}", displayName = "{" + ProjectVisualization.MessageKeys.mode$displayName + "}")
    @Enumeration({"list", "facet"})
    @Required
    public String mode = "list";

    @STProperty(description = "{" + ProjectVisualization.MessageKeys.facetBagOf$description
            + "}", displayName = "{" + ProjectVisualization.MessageKeys.facetBagOf$displayName + "}")
    public String facetBagOf;

}
