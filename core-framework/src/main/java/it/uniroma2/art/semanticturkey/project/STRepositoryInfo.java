package it.uniroma2.art.semanticturkey.project;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import jakarta.annotation.Nullable;

import org.apache.commons.lang3.EnumUtils;
import org.eclipse.rdf4j.common.transaction.IsolationLevel;
import org.eclipse.rdf4j.common.transaction.IsolationLevels;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.eclipse.rdf4j.model.util.Values;

public class STRepositoryInfo {
	private final String backendType;
	private final String username;
	private final String password;
	private final IsolationLevel defaultReadIsolationLevel;
	private final IsolationLevel defaultWriteIsolationLevel;
	private final SearchStrategies searchStrategy;

	public static enum SearchStrategies {
		REGEX, GRAPH_DB
	}

	@JsonCreator
	public STRepositoryInfo(@JsonProperty("backendType") String backendType,
			@JsonProperty("username") String username, @JsonProperty("password") String password,
			@JsonProperty("defaultReadIsolationLevel") @JsonDeserialize(using = IsolationLevelDeserializer.class) IsolationLevel defaultReadIsolationLevel,
			@JsonProperty("defaultWriteIsolationLevel") @JsonDeserialize(using = IsolationLevelDeserializer.class) IsolationLevel defaultWriteIsolationLevel,
			@JsonProperty("searchStrategy") SearchStrategies searchStrategy) {
		this.backendType = backendType;
		this.username = username;
		this.password = password;
		this.defaultReadIsolationLevel = defaultReadIsolationLevel;
		this.defaultWriteIsolationLevel = defaultWriteIsolationLevel;
		this.searchStrategy = searchStrategy;
	}

	public @Nullable String getBackendType() {
		return backendType;
	}

	public @Nullable String getUsername() {
		return username;
	}

	public @Nullable String getPassword() {
		return password;
	}

	@JsonSerialize(using = IsolationLevelSerializer.class)
	public IsolationLevel getDefaultReadIsolationLevel() {
		return defaultReadIsolationLevel;
	}

	@JsonSerialize(using = IsolationLevelSerializer.class)
	public IsolationLevel getDefaultWriteIsolationLevel() {
		return defaultWriteIsolationLevel;
	}

	public @Nullable SearchStrategies getSearchStrategy() {
		return searchStrategy;
	}

	private static class IsolationLevelSerializer extends StdSerializer<IsolationLevel> {

		private static final long serialVersionUID = 1L;

		protected IsolationLevelSerializer() {
			super(IsolationLevel.class);
		}

		@Override
		public void serialize(IsolationLevel value, JsonGenerator gen, SerializerProvider provider)
				throws IOException {
			if (!IsolationLevel.NAME.equals(value.getName())) {
				provider.reportMappingProblem("unsupported insolation levels class: " + value.getName());
			}
			gen.writeString("http://www.openrdf.org/schema/sesame#" + value.getValue());
		}

	}

	private static class IsolationLevelDeserializer extends StdDeserializer<IsolationLevel> {

		private static final long serialVersionUID = 1L;

		protected IsolationLevelDeserializer() {
			super(IsolationLevel.class);
		}

		@Override
		public IsolationLevel deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			String levelName = p.getValueAsString();

			if (levelName == null)
				return null;

			var levelIRI = Values.iri(levelName);

			if (!levelIRI.getNamespace().equals("http://www.openrdf.org/schema/sesame#")) {
				throw new JsonMappingException(p,"Not a recognized isolation levels namespace: " + levelIRI.getNamespace());
			}

			return Optional.ofNullable(EnumUtils.getEnum(IsolationLevels.class, levelIRI.getLocalName())).orElseThrow(() -> new JsonMappingException(p,
					"Not a recognized isolation level: " + levelName));
		}

	}

	public static STRepositoryInfo createDefault() {
		return new STRepositoryInfo(null, null, null, null, null, null);
	}

	public STRepositoryInfo withNewAccessCredentials(String newUsername, String newPassword) {
		return new STRepositoryInfo(backendType, newUsername, newPassword, defaultReadIsolationLevel,
				defaultWriteIsolationLevel, searchStrategy);
	}

}