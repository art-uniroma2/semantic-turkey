package it.uniroma2.art.semanticturkey.spring;

import org.springframework.context.ApplicationContext;

/**
 * Holds the main {@link org.springframework.context.ApplicationContext}. This is intended to be used as a fallback
 * by those portions of code that can't use dependency injection (e.g. static methods).
 *
 * @author <a href="mailto:manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
public class RootApplicationContextHolder {
    private static ApplicationContext _instance;

    public static void set(ApplicationContext applicationContext) {
        _instance = applicationContext;
    }

    public static ApplicationContext get() {
        return _instance;
    }
}
