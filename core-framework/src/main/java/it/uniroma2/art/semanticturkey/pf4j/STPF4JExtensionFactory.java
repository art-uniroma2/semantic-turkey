package it.uniroma2.art.semanticturkey.pf4j;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.pf4j.ExtensionFactory;
import org.pf4j.PluginWrapper;
import org.pf4j.spring.SpringExtensionFactory;
import org.pf4j.spring.SpringPlugin;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Map;

public class STPF4JExtensionFactory implements ExtensionFactory {
    protected final STPluginManager pluginManager;

    public STPF4JExtensionFactory(STPluginManager pluginManager) {
        this.pluginManager = pluginManager;
    }

    @Override
    public <T> T create(Class<T> extensionClass) {
        PluginWrapper pluginWrapper = pluginManager.whichPlugin(extensionClass);

        // determines the application context to use for the instantiation.
        // - if the class is defined by a plugin, then use its application context if any.
        // - otherwise, for classpath extensions, use the pluginManger application context
        // If there is an application context, two cases can happen:
        // - the context contains a suitable bean, which is returned
        // - otherwise, one is assembled on the fly
        // If there is no application context,just instantiate the class using the default
        // constructor
        ApplicationContext applicationContext;
        if (pluginWrapper != null) {
            if (pluginWrapper.getPlugin() instanceof SpringPlugin springPlugin) {
                applicationContext = springPlugin.getApplicationContext();
            } else {
                applicationContext = null;
            }
        } else {
            applicationContext = pluginManager.getApplicationContext();
        }

        if (applicationContext != null) {
            Collection<?> beansOfType = applicationContext.getBeansOfType(extensionClass).values();
            if (beansOfType.isEmpty()) {
                return applicationContext.getAutowireCapableBeanFactory().createBean(extensionClass);
            } else if (beansOfType.size() == 1) {
                return extensionClass.cast(beansOfType.iterator().next());
            } else {
                throw new IllegalStateException("Multiple bean instances of the same class: " + extensionClass);
            }
        } else {
            try {
                return extensionClass.getDeclaredConstructor().newInstance();
            } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                ExceptionUtils.rethrow(e);
                return null; // never executed
            }
        }
    }
}
