package it.uniroma2.art.semanticturkey.multiverse;

import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.AliasFor;
import org.springframework.transaction.event.TransactionalApplicationListener;

import java.lang.annotation.*;

/**
 * Annotation that marks a method to receive events originated from different world in the multiverse. This annotation
 * should only be applied to world-scoped objects.
 *
 * @author <a href="mailto:manuel.fiorelli">Manuel Fiorelli</a>
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@EventListener
public @interface MultiverseEventListener {

    /**
     * Alias for {@link #classes}.
     */
    @AliasFor(annotation = EventListener.class, attribute = "classes")
    Class<?>[] value() default {};

    /**
     * The event classes that this listener handles.
     * <p>If this attribute is specified with a single value, the annotated
     * method may optionally accept a single parameter. However, if this
     * attribute is specified with multiple values, the annotated method
     * must <em>not</em> declare any parameters.
     */
    @AliasFor(annotation = EventListener.class, attribute = "classes")
    Class<?>[] classes() default {};

    /**
     * Spring Expression Language (SpEL) attribute used for making the event
     * handling conditional.
     * <p>The default is {@code ""}, meaning the event is always handled.
     * @see EventListener#condition
     */
    @AliasFor(annotation = EventListener.class, attribute = "condition")
    String condition() default "";

    /**
     * An optional identifier for the listener, defaulting to the fully-qualified
     * signature of the declaring method (e.g. "mypackage.MyClass.myMethod()").
     * @since 5.3
     * @see EventListener#id
     * @see TransactionalApplicationListener#getListenerId()
     */
    @AliasFor(annotation = EventListener.class, attribute = "id")
    String id() default "";

}

