package it.uniroma2.art.semanticturkey.settings.core;

import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;

import java.util.List;

public class ProjectsSearchGroup  implements STProperties {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.core.ProjectsSearchGroup";

        public static final String shortName = keyBase + ".shortName";

        public static final String groupName$description = keyBase + ".groupName.description";
        public static final String groupName$displayName = keyBase + ".groupName.displayName";
        public static final String groupDescription$description = keyBase + ".groupDescription.description";
        public static final String groupDescription$displayName = keyBase + ".groupDescription.displayName";
        public static final String projectNames$description = keyBase + ".projectNames.description";
        public static final String projectNames$displayName = keyBase + ".projectNames.displayName";

    }

    @Override
    public String getShortName() {
        return "{" + MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.groupName$description + "}", displayName = "{"
            + MessageKeys.groupName$displayName + "}")
    @Required
    public String groupName;

    @STProperty(description = "{" + MessageKeys.groupDescription$description + "}", displayName = "{"
            + MessageKeys.groupDescription$displayName + "}")
    public String groupDescription;

    @STProperty(description = "{" + MessageKeys.projectNames$description + "}", displayName = "{"
            + MessageKeys.projectNames$displayName + "}")
    public List<String> projectNames;

}
