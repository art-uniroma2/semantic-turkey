package it.uniroma2.art.semanticturkey.services.http;

import it.uniroma2.art.semanticturkey.exceptions.ProjectUpdateException;
import it.uniroma2.art.semanticturkey.mvc.ResolvedRequestHolder;
import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.project.ProjectConsumer;
import it.uniroma2.art.semanticturkey.project.ProjectManager;
import it.uniroma2.art.semanticturkey.services.InvalidContextException;
import it.uniroma2.art.semanticturkey.services.STRequest;
import it.uniroma2.art.semanticturkey.services.STServiceContext;
import it.uniroma2.art.semanticturkey.showvoc.ShowVocConstants;
import it.uniroma2.art.semanticturkey.user.STUser;
import it.uniroma2.art.semanticturkey.user.UsersManager;
import jakarta.annotation.Nullable;
import jakarta.servlet.http.HttpServletRequest;
import org.eclipse.rdf4j.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;

import java.util.Arrays;

public class STServiceHTTPContext implements STServiceContext {

	private static final String HTTP_PARAM_PREFIX = "ctx_";
	private static final String HTTP_PARAM_PROJECT_CONSUMER = HTTP_PARAM_PREFIX + "consumer";
	private static final String HTTP_PARAM_PROJECT = HTTP_PARAM_PREFIX + "project";
	private static final String HTTP_PARAM_WGRAPH = HTTP_PARAM_PREFIX + "wgraph";
	private static final String HTTP_PARAM_RGRAPHS = HTTP_PARAM_PREFIX + "rgraphs";
	private static final String HTTP_PARAM_TOKEN = HTTP_PARAM_PREFIX + "token";
	private static final String HTTP_PARAM_LANGUAGES = HTTP_PARAM_PREFIX + "langs";
	private static final String HTTP_PARAM_VERSION = HTTP_PARAM_PREFIX + "version";

	private static final String HTTP_ARG_DEFAULT_GRAPH = "DEFAULT";
	private static final String HTTP_ARG_ANY_GRAPH = "ANY";

	protected static Logger logger = LoggerFactory.getLogger(STServiceHTTPContext.class);
	
	@Autowired(required = false)
	private STPlugin stPlugin;

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	@Lazy
	private ConversionService conversionService;

	public STServiceHTTPContext() {

	}

	protected HttpServletRequest currentRequest() {
		return ResolvedRequestHolder.get();
	}

	@Override
	public ProjectConsumer getProjectConsumer() {
		String consumerParameter = currentRequest().getParameter(HTTP_PARAM_PROJECT_CONSUMER);

		if (consumerParameter == null || consumerParameter.equals(ProjectConsumer.SYSTEM.getName())) {
			return ProjectConsumer.SYSTEM;
		} else {
			Project project = ProjectManager.getProject(consumerParameter);

			if (project == null) {
				throw new InvalidContextException(
						"the provided consumer is not an open project: " + consumerParameter);
			}

			return project;
		}

	}


	@Override
	public Project getProject() {
		String projectParameter = currentRequest().getParameter(HTTP_PARAM_PROJECT);
		Project project;

		if (projectParameter == null) {
			throw new InvalidContextException(
					"either this context is meant to have no project information, or it has not been passed through the request");
		} else {
			project = ProjectManager.getProject(projectParameter);
		}

		logger.trace("project = " + project);

		if (project == null) {
			throw new InvalidContextException(
					"not an open project: " + (projectParameter.equals("null") ? "<current>" : projectParameter));
		} else {
			try {
				STUser loggedUser = UsersManager.getLoggedUser();
				if (!loggedUser.isAnonymous()) {
					//update connection info only if user is not showvoc visitor, which is a mockup user
					project.updateLastConnectionInfo(loggedUser, UsersManager.getLoggedUserSessionId());
				}
			} catch (ProjectUpdateException ignored) {}
		}

		return project;
	}

	/**
	 * Returns the working graph. If none is indicated as a parameter of the request URL (see
	 * {@value #HTTP_PARAM_WGRAPH}), then it returns the base URI of the current project.
	 */
	@Override
	public Resource getWGraph() {
		String wgraphParameter = currentRequest().getParameter(HTTP_PARAM_WGRAPH);

		if (wgraphParameter == null) {
			wgraphParameter = "<" + getProject().getBaseURI() + ">";
		}

		Resource wgraph = conversionService.convert(wgraphParameter, Resource.class);

		logger.trace("wgraph = " + wgraph);

		return wgraph;
	}

	@Override
	public Resource[] getRGraphs() {
		String rgraphsParameter = currentRequest().getParameter(HTTP_PARAM_RGRAPHS);

		Resource[] rgraphs;
		if (rgraphsParameter == null) {
			rgraphs = new Resource[0];
		} else {
			rgraphs = conversionService.convert(rgraphsParameter, Resource[].class);
		}

		logger.trace("rgraphs = " + Arrays.toString(rgraphs));

		return rgraphs;
	}

	@Deprecated
	@Override
	public Project getProject(int index) {
		String projectPar;
		if (index == 0) {
			projectPar = HTTP_PARAM_PROJECT;
		} else {
			projectPar = new StringBuilder(HTTP_PARAM_PROJECT).append("_").append(index).toString();
		}
		return ProjectManager.getProject(currentRequest().getParameter(projectPar));
	}

	@Override
	public String getExtensionPathComponent() {

		if (stPlugin == null) {
			return "it.uniroma2.art.semanticturkey/st-core-services";
		} else {
			return stPlugin.getExtensionPath();
		}
	}

	@Override
	public STRequest getRequest() {
		return new STHTTPRequest(currentRequest());
	}

	@Override
	public String getSessionToken() {
		String token = currentRequest().getParameter(HTTP_PARAM_TOKEN);
		if (token == null) {
			throw new InvalidContextException("The token has not been passed through the request");
		}
		return token;
	}

	@Override
	public String getVersion() {
		return currentRequest().getParameter(HTTP_PARAM_VERSION);
	}

	@Override
	public String getLanguages() {
		return currentRequest().getParameter(HTTP_PARAM_LANGUAGES);
	}

	@Override
	public boolean hasContextParameter(String parameter) {
		return getContextParameter(parameter) != null;
	}

	@Override
	public @Nullable String getContextParameter(String parameter) {
		return currentRequest().getParameter("ctx_" + parameter);
	}
}
