package it.uniroma2.art.semanticturkey.extension.extpts.datasetmetadata;

import it.uniroma2.art.semanticturkey.extension.ExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.mdr.core.MetadataRegistryStateException;
import it.uniroma2.art.semanticturkey.mdr.core.NoSuchDatasetMetadataException;
import it.uniroma2.art.semanticturkey.resources.Scope;
import it.uniroma2.art.semanticturkey.utilities.ReflectionUtilities;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.semanticturkey.extension.Extension;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;

import java.lang.reflect.Method;

/**
 * Extension point for the dataset metadata exporters
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 */
public interface DatasetMetadataExporter extends Extension {

	interface ExtensionFactoryIntrospection {
		default boolean getMetadataStoredInsideDataset() {
			Class<?> extClass = ReflectionUtilities.getInterfaceArgumentTypeAsClass(this.getClass(), ExtensionFactory.class, 0);
			try {
				Method m = extClass.getMethod("addMetadataToDataset", Project.class, RepositoryConnection.class, IRI.class);
				return m.getDeclaringClass() != DatasetMetadataExporter.class;
			} catch (NoSuchMethodException e) {
				throw new IllegalStateException(e);
			}

		}
	}

	Model produceDatasetMetadata(Project project, RepositoryConnection conn, IRI dataGraph)
			throws DatasetMetadataExporterException, STPropertyAccessException;

	default void addMetadataToDataset(Project project, RepositoryConnection conn, IRI dataGraph)
			throws DatasetMetadataExporterException {
		throw new UnsupportedOperationException("This dataset metadata exporter does not support storing metadata inside the ontology");
	}

	default Settings importFromMetadataRegistry(Project project, Scope scope) throws NoSuchDatasetMetadataException, MetadataRegistryStateException {
		return null;
	}
}