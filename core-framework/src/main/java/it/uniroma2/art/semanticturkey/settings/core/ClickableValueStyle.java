package it.uniroma2.art.semanticturkey.settings.core;

import it.uniroma2.art.semanticturkey.properties.STProperties;
import it.uniroma2.art.semanticturkey.properties.STProperty;

public class ClickableValueStyle  implements STProperties {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.core.ClickableValueStyle";

        public static final String shortName = keyBase + ".shortName";
        public static final String bold$description = keyBase + ".bold.description";
        public static final String bold$displayName = keyBase + ".bold.displayName";
        public static final String underlined$description = keyBase + ".underlined.displayName";
        public static final String underlined$displayName = keyBase + ".underlined.description";
        public static final String widget$description = keyBase + ".widget.description";
        public static final String widget$displayName = keyBase + ".widget.displayName";
        public static final String color$description = keyBase + ".color.displayName";
        public static final String color$displayName = keyBase + ".color.description";
    }

    @Override
    public String getShortName() {
        return "{" + ClickableValueStyle.MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + ClickableValueStyle.MessageKeys.bold$description
            + "}", displayName = "{" + ClickableValueStyle.MessageKeys.bold$displayName + "}")
    public boolean bold;

    @STProperty(description = "{" + ClickableValueStyle.MessageKeys.underlined$description
            + "}", displayName = "{" + ClickableValueStyle.MessageKeys.underlined$displayName + "}")
    public boolean underlined;

    @STProperty(description = "{" + ClickableValueStyle.MessageKeys.widget$description
            + "}", displayName = "{" + ClickableValueStyle.MessageKeys.widget$displayName + "}")
    public boolean widget;

    @STProperty(description = "{" + ClickableValueStyle.MessageKeys.color$description
            + "}", displayName = "{" + ClickableValueStyle.MessageKeys.color$displayName + "}")
    public String color;


}