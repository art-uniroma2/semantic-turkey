package it.uniroma2.art.semanticturkey.settings.connections.showvoc;

import it.uniroma2.art.semanticturkey.extension.settings.ProjectSettingsManager;
import it.uniroma2.art.semanticturkey.extension.settings.SystemSettingsManager;
import org.springframework.stereotype.Component;


@Component
public class ConnectedShowVocSettingsManager implements SystemSettingsManager<ConnectedShowVocSystemSettings>, ProjectSettingsManager<ConnectedShowVocProjectSettings> {

	@Override
	public String getId() {
		return ConnectedShowVocSettingsManager.class.getName();
	}

}
