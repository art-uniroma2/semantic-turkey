package it.uniroma2.art.semanticturkey.extension.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import it.uniroma2.art.semanticturkey.config.Configuration;
import it.uniroma2.art.semanticturkey.config.ConfigurationManager;
import it.uniroma2.art.semanticturkey.config.ConfigurationNotFoundException;
import it.uniroma2.art.semanticturkey.config.InvalidConfigurationException;
import it.uniroma2.art.semanticturkey.extension.ConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.Extension;
import it.uniroma2.art.semanticturkey.extension.ExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.ExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.ExtensionPointManager;
import it.uniroma2.art.semanticturkey.extension.ExtensionPointManagerHolder;
import it.uniroma2.art.semanticturkey.extension.NoSuchConfigurationManager;
import it.uniroma2.art.semanticturkey.extension.NoSuchExtensionException;
import it.uniroma2.art.semanticturkey.extension.NoSuchExtensionPointException;
import it.uniroma2.art.semanticturkey.extension.NoSuchSettingsManager;
import it.uniroma2.art.semanticturkey.extension.NonConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.extpts.collaboration.CollaborationBackend;
import it.uniroma2.art.semanticturkey.extension.extpts.collaboration.CollaborationBackendExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetcatalog.DatasetCatalogConnector;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetcatalog.DatasetCatalogConnectorExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetmetadata.DatasetMetadataExporter;
import it.uniroma2.art.semanticturkey.extension.extpts.datasetmetadata.DatasetMetadataExporterExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.extpts.rdftransformer.RDFTransformer;
import it.uniroma2.art.semanticturkey.extension.extpts.rdftransformer.RDFTransformerExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.extpts.rendering.RenderingEngine;
import it.uniroma2.art.semanticturkey.extension.extpts.rendering.RenderingEngineExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.extpts.repositoryimplconfigurer.RepositoryImplConfigurer;
import it.uniroma2.art.semanticturkey.extension.extpts.repositoryimplconfigurer.RepositoryImplConfigurerExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.extpts.search.SearchStrategy;
import it.uniroma2.art.semanticturkey.extension.extpts.search.SearchStrategyExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.extpts.urigen.URIGenerator;
import it.uniroma2.art.semanticturkey.extension.extpts.urigen.URIGeneratorExtensionPoint;
import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.extension.settings.SettingsManager;
import it.uniroma2.art.semanticturkey.extension.settings.impl.SettingsSupport;
import it.uniroma2.art.semanticturkey.plugin.PluginSpecification;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.project.ProjectManager;
import it.uniroma2.art.semanticturkey.properties.PropertyNotFoundException;
import it.uniroma2.art.semanticturkey.properties.STPropertiesChecker;
import it.uniroma2.art.semanticturkey.properties.STPropertiesManager;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.properties.STPropertyUpdateException;
import it.uniroma2.art.semanticturkey.properties.WrongPropertiesException;
import it.uniroma2.art.semanticturkey.resources.Reference;
import it.uniroma2.art.semanticturkey.resources.Scope;
import it.uniroma2.art.semanticturkey.user.STUser;
import it.uniroma2.art.semanticturkey.user.UsersGroup;
import it.uniroma2.art.semanticturkey.utilities.ReflectionUtilities;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.stream.Collectors.toList;

public class ExtensionPointManagerImpl implements ExtensionPointManager{

    private ConcurrentHashMap<String, Class<?>> configClassName2Class = new ConcurrentHashMap<>();

    private ApplicationContext context;

    private ConcurrentHashMap<String, ExtensionPoint> extensionPoints = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, ExtensionFactory<?>> extensionFactories = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, SettingsManager> settingsManagers = new ConcurrentHashMap<>();

    private ConcurrentHashMap<String, ConfigurationManager<?>> configurationManagers = new ConcurrentHashMap<>();

    public ExtensionPointManagerImpl(ApplicationContext context) {
        this.context = context;
    }

    @PostConstruct
    public void init() {
        ProjectManager.setExtensionPointManager(this);
        SettingsSupport.setEventPublisher(context);
        ExtensionPointManagerHolder.setExtensionPointManager(this);
    }

    @PreDestroy
    public void destroy() {
        ProjectManager.setExtensionPointManager(null);
        SettingsSupport.setEventPublisher(null);
        ExtensionPointManagerHolder.setExtensionPointManager(null);
    }

    public void indexApplicationContext(ApplicationContext pluginContext) {
        for (ExtensionPoint expt : pluginContext.getBeansOfType(ExtensionPoint.class).values()) {
            extensionPoints.put(expt.getId(), expt);
        }

        for (ExtensionFactory<?> extFact : pluginContext.getBeansOfType(ExtensionFactory.class).values()) {
            extensionFactories.put(extFact.getId(), extFact);
        }

        for (SettingsManager settingsManager : pluginContext.getBeansOfType(SettingsManager.class).values()) {
            settingsManagers.put(settingsManager.getId(), settingsManager);
        }

        for (ConfigurationManager<?> configurationManager : pluginContext.getBeansOfType(ConfigurationManager.class).values()) {
            configurationManagers.put(configurationManager.getId(), configurationManager);
        }
    }
    @Override
    public Collection<ExtensionPoint> getExtensionPoints(Scope... scopes) {
/*
        Collection<ExtensionPoint> rv = new ArrayList<>();
        Set<Scope> filter = new HashSet<>();
        Arrays.stream(scopes.length == 0 ? Scope.values() : scopes).forEach(filter::add);
        for (Object expt : extensionPointTracker.getServices()) {
            ExtensionPoint expt2 = ((ExtensionPoint) expt);
            if (filter.contains(expt2.getScope())) {
                rv.add(expt2);
            }
        }
        return rv;
*/
        return extensionPoints.values()
                .stream()
                .filter(f -> ArrayUtils.contains(scopes, f.getScope())).collect(toList());
    }

    @Override
    public ExtensionPoint getExtensionPoint(String identifier) throws NoSuchExtensionPointException {
/*
        for (Object expt : extensionPointTracker.getServices()) {
            ExtensionPoint expt2 = ((ExtensionPoint) expt);

            if (expt2.getInterface().getName().equals(identifier)) {
                return expt2;
            }
        }
*/
        return Optional.ofNullable(extensionPoints.get(identifier))
                .orElseThrow(() -> new NoSuchExtensionPointException("Unrecognized extension point: " + identifier));
    }

    @Override
    public CollaborationBackendExtensionPoint getCollaborationBackend() {
        return ((CollaborationBackendExtensionPoint) getExtensionPoint(CollaborationBackend.class.getName()));
    }

    @Override
    public DatasetCatalogConnectorExtensionPoint getDatasetCatalogConnector() {
        return ((DatasetCatalogConnectorExtensionPoint) getExtensionPoint(
                DatasetCatalogConnector.class.getName()));
    }

    @Override
    public DatasetMetadataExporterExtensionPoint getDatasetMetadataExporter() {
        return ((DatasetMetadataExporterExtensionPoint) getExtensionPoint(
                DatasetMetadataExporter.class.getName()));
    }

    @Override
    public RenderingEngineExtensionPoint getRenderingEngine() {
        return ((RenderingEngineExtensionPoint) getExtensionPoint(RenderingEngine.class.getName()));
    }

    @Override
    public RDFTransformerExtensionPoint getRDFTransformer() {
        return ((RDFTransformerExtensionPoint) getExtensionPoint(RDFTransformer.class.getName()));
    }

    @Override
    public RepositoryImplConfigurerExtensionPoint getRepositoryImplConfigurer() {
        return ((RepositoryImplConfigurerExtensionPoint) getExtensionPoint(
                RepositoryImplConfigurer.class.getName()));
    }

    @Override
    public SearchStrategyExtensionPoint getSearchStrategy() {
        return ((SearchStrategyExtensionPoint) getExtensionPoint(SearchStrategy.class.getName()));
    }

    @Override
    public URIGeneratorExtensionPoint getURIGenerator() {
        return (URIGeneratorExtensionPoint) getExtensionPoint(URIGenerator.class.getName());
    }

    @Override
    public ConfigurationManager<?> getConfigurationManager(String componentIdentifier)
            throws NoSuchConfigurationManager {

        return Optional.ofNullable(configurationManagers.get(componentIdentifier))
                .orElseThrow(() ->new NoSuchConfigurationManager("Unrecognized configuration manager: " + componentIdentifier));
    }

    @Override
    public Collection<ConfigurationManager<?>> getConfigurationManagers() {
        return configurationManagers
                .values()
                .stream()
                .map(o -> (ConfigurationManager<?>) o)
                .collect(toList());
    }

    @Override
    public Collection<Reference> getConfigurationReferences(Project project, STUser user,
                                                            String componentIdentifier) throws NoSuchConfigurationManager {
        return getConfigurationManager(componentIdentifier).getConfigurationReferences(project, user);
    }

    @Override
    public Configuration getConfiguration(String componentIdentifier, Reference reference)
            throws NoSuchConfigurationManager, STPropertyAccessException {
        return getConfigurationManager(componentIdentifier).getConfiguration(reference);
    }

    @Override
    public SettingsManager getSettingsManager(String componentIdentifier) throws NoSuchSettingsManager {
        return Optional.ofNullable(settingsManagers.get(componentIdentifier))
                .orElseThrow(() -> new NoSuchSettingsManager("Unrecognized settings manager: " + componentIdentifier));
    }

    @Override
    public Collection<SettingsManager> getSettingsManagers() {
        return settingsManagers.values();
    }

    @Override
    public Collection<Scope> getSettingsScopes(String componentIdentifier) throws NoSuchSettingsManager {
        return getSettingsManager(componentIdentifier).getSettingsScopes();
    }

    @Override
    public Settings getSettings(Project project, STUser user, UsersGroup group, String componentIdentifier, Scope scope)
            throws STPropertyAccessException, NoSuchSettingsManager {
        return getSettingsManager(componentIdentifier).getSettings(project, user, group, scope);
    }

    @Override
    public Settings getSettingsDefault(Project project, STUser user, UsersGroup group, String componentID, Scope scope, Scope defaultScope) throws STPropertyAccessException, NoSuchSettingsManager {
        return getSettingsManager(componentID).getSettingsDefault(project, user, group, scope, defaultScope);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void storeConfiguration(String componentIdentifier, Reference reference, ObjectNode configuration)
            throws IOException, WrongPropertiesException, NoSuchConfigurationManager,
            STPropertyUpdateException, STPropertyAccessException {
        ConfigurationManager<?> configurationManager = getConfigurationManager(componentIdentifier);
        Class<? extends Configuration> configBaseClass = ReflectionUtilities
                .<Configuration>getInterfaceArgumentTypeAsClass(configurationManager.getClass(),
                        ConfigurationManager.class, 0);

        Configuration configObj = STPropertiesManager.loadSTPropertiesFromObjectNodes(configBaseClass, true,
                STPropertiesManager.createObjectMapper(this), configuration);
        ((ConfigurationManager) configurationManager).storeConfiguration(reference, configObj);
    }

    @Override
    public void deleteConfiguraton(String componentIdentifier, Reference reference)
            throws NoSuchConfigurationManager, ConfigurationNotFoundException {
        ConfigurationManager<?> configurationManager = getConfigurationManager(componentIdentifier);
        ((ConfigurationManager) configurationManager).deleteConfiguration(reference);
    }

    @Override
    public void storeSettings(String componentIdentifier, Project project, STUser user, UsersGroup group, Scope scope,
                              ObjectNode settings) throws NoSuchSettingsManager, STPropertyUpdateException,
            WrongPropertiesException, STPropertyAccessException {
        SettingsManager settingsManager = getSettingsManager(componentIdentifier);
        Settings settingsObj = SettingsSupport.createSettings(settingsManager, scope, settings);
        settingsManager.storeSettings(project, user, group, scope, settingsObj);
    }

    @Override
    public void storeSetting(String componentID, Project project, STUser loggedUser, UsersGroup group, Scope scope, String property, JsonNode propertyValue) throws NoSuchSettingsManager, STPropertyUpdateException, WrongPropertiesException, STPropertyAccessException, PropertyNotFoundException, IOException {
        // A non-atomic read-update of a single settings property
        SettingsManager settingsManager = getSettingsManager(componentID);
        Settings explicitSettings = settingsManager.getSettings(project, loggedUser, group, scope, true);
        Type propertyType = explicitSettings.getPropertyType(property);
        ObjectMapper om = STPropertiesManager.createObjectMapper();
        Object parsedPropertyValue = null;
        if (propertyValue != null) {
            parsedPropertyValue = om.readValue(om.treeAsTokens(propertyValue), om.constructType(propertyType));
        }
        explicitSettings.setPropertyValue(property, parsedPropertyValue);
        settingsManager.storeSettings(project, loggedUser, group, scope, explicitSettings);
    }

    @Override
    public void storeSettingsDefault(String componentIdentifier, Project project, STUser user, UsersGroup group, Scope scope, Scope defaultScope,
                                     ObjectNode settings) throws NoSuchSettingsManager, STPropertyUpdateException,
            WrongPropertiesException, STPropertyAccessException {
        SettingsManager settingsManager = getSettingsManager(componentIdentifier);
        Settings settingsObj = SettingsSupport.createSettings(settingsManager, scope, settings);
        settingsManager.storeSettingsDefault(project, user, group, scope, defaultScope, settingsObj);
    }

    @Override
    public void storeSettingDefault(String componentID, Project project, STUser user, UsersGroup group, Scope scope, Scope defaultScope,
                                    String property, JsonNode propertyValue) throws NoSuchSettingsManager, STPropertyUpdateException,
            WrongPropertiesException, STPropertyAccessException, PropertyNotFoundException, IOException {
        // A non-atomic read-update of a single settings default property
        SettingsManager settingsManager = getSettingsManager(componentID);
        Settings settingsDefault = settingsManager.getSettingsDefault(project, user, group, scope, defaultScope);
        Type propertyType = settingsDefault.getPropertyType(property);
        ObjectMapper om = STPropertiesManager.createObjectMapper();
        Object parsedPropertyValue = propertyValue == null ? null : om.readValue(om.treeAsTokens(propertyValue), om.constructType(propertyType));
        settingsDefault.setPropertyValue(property, parsedPropertyValue);
        settingsManager.storeSettingsDefault(project, user, group, scope, defaultScope, settingsDefault);
    }

    @Override
    public Collection<ExtensionFactory<?>> getExtensions(String extensionPoint) {
/*        ExtensionPoint expt = getExtensionPoint(extensionPoint);
        Class<?> exptInt = expt.getInterface();
        Collection<ExtensionFactory<?>> rv = new ArrayList<>();
        for (Object extFactory : extensionFactoryTracker.getServices()) {
            if (exptInt.isAssignableFrom(((ExtensionFactory<?>) extFactory).getExtensionType())) {
                rv.add((ExtensionFactory<?>) extFactory);
            }
        }
        return rv;*/
        ExtensionPoint expt = getExtensionPoint(extensionPoint);
        Class<?> exptInt = expt.getInterface();
        List<ExtensionFactory<?>> rv = new ArrayList<>();
        extensionFactories
                .values()
                .stream()
                .filter(f -> exptInt.isAssignableFrom(f.getExtensionType())).forEach(f -> rv.add(f));
        return rv;
    }

    @Override
    public ExtensionFactory<?> getExtension(String componentID) throws NoSuchExtensionException {
/*
        for (Object extFactory : extensionFactoryTracker.getServices()) {
            if (((ExtensionFactory<?>) extFactory).getId().equals(componentID)) {
                return (ExtensionFactory<?>) extFactory;
            }
        }
*/
        return Optional.ofNullable(extensionFactories.get(componentID))
                .orElseThrow(() -> new NoSuchExtensionException("Unrecognized extension: " + componentID));
    }

    @Override
    public <T extends Extension, C extends Configuration> T instantiateExtension(Class<T> targetInterface,
                                                                                 PluginSpecification spec,
                                                                                 Project project,
                                                                                 STUser user)  throws IllegalArgumentException, NoSuchExtensionException, WrongPropertiesException,
            STPropertyAccessException, InvalidConfigurationException {
        ExtensionFactory<?> extFactory = this.getExtension(spec.getFactoryId());
        if (!targetInterface.isAssignableFrom(extFactory.getExtensionType())) {
            throw new IllegalArgumentException("Extension \"" + spec.getFactoryId()
                    + "\" is not assignable to interface \"" + targetInterface.getName() + "\"");
        }

        ObjectNode config = spec.getConfiguration();

        if (spec.getConfigType() != null && config != null
                && !config.hasNonNull(STPropertiesManager.SETTINGS_TYPE_PROPERTY)) {
            config = config.deepCopy();
            config.put(STPropertiesManager.SETTINGS_TYPE_PROPERTY, spec.getConfigType());
        }
        T obj;

        if (config == null) {
            if (extFactory instanceof NonConfigurableExtensionFactory) {
                obj = ((NonConfigurableExtensionFactory<T>) extFactory).createInstance();
            } else {
                throw new IllegalArgumentException("Missing configuration");
            }
        } else {
            if (extFactory instanceof ConfigurableExtensionFactory) {
                Class<? extends Configuration> configBaseClass = ReflectionUtilities
                        .getInterfaceArgumentTypeAsClass(extFactory.getClass(), ConfigurationManager.class,
                                0);

                Configuration configObj = STPropertiesManager.loadSTPropertiesFromObjectNodes(configBaseClass,
                        true, STPropertiesManager.createObjectMapper(this), config);
                try {
                    STPropertiesManager.loadDefaultsFromSettings(configObj, project, user);
                } catch (PropertyNotFoundException e) {
                    throw new RuntimeException(e); // TODO: use a more specific exception type, requiring to change the throws clause
                }
                STPropertiesChecker checker = STPropertiesChecker.getModelConfigurationChecker(configObj);
                if (!checker.isValid()) {
                    throw new InvalidConfigurationException(checker.getErrorMessage());
                }

                obj = (T) ((ConfigurableExtensionFactory<T, Configuration>) extFactory)
                        .createInstance(configObj);
            } else {
                throw new IllegalArgumentException(
                        "Provided configuration for a non configurable extension factory");
            }
        }
        return obj;
    }

    @Override
    public Optional<Class<?>> getConfigurationClassFromName(String configClassName) {
        for (ExtensionFactory<?> f : extensionFactories.values()) {
            if (f instanceof ConfigurableExtensionFactory<?,?> cf) {
                for (Configuration c : (Collection<Configuration>) cf.getConfigurations()) {
                    if (c.getClass().getName().equals(configClassName)) {
                        return Optional.of(c.getClass());
                    }
                }
            }
        }
        return Optional.empty();
    }

}
