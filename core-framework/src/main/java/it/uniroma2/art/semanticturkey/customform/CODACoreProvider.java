package it.uniroma2.art.semanticturkey.customform;

import it.uniroma2.art.coda.pf4j.PF4JComponentProvider;
import it.uniroma2.art.coda.provisioning.ComponentProvider;
import org.pf4j.PluginManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.uniroma2.art.coda.core.CODACore;

@Component
public class CODACoreProvider {
	
	private CODACore codaCore;
	private PluginManager pluginManager;
	
	protected CODACoreProvider() {
		// This constructor is only required for the generation of a CGLIB proxy
	}
	
	@Autowired
	public CODACoreProvider(PluginManager pluginManager){
		this.pluginManager = pluginManager;
	}

	public CODACore getCODACore(){
		var componentProvider = new PF4JComponentProvider(pluginManager);
		codaCore = new CODACore(componentProvider);
		codaCore.setGlobalContractBinding("http://art.uniroma2.it/coda/contracts/randIdGen",
				"http://semanticturkey.uniroma2.it/coda/converters/randIdGen");
		return codaCore;
	}
}
