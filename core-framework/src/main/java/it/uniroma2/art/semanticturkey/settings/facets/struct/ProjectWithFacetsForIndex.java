package it.uniroma2.art.semanticturkey.settings.facets.struct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectWithFacetsForIndex {
    Map<String, List<String>> facetNameToFaceValueListMap = new HashMap<>();

    public ProjectWithFacetsForIndex() {

    }

    public void addFacetAndValueList(String facetName, List<String> facetValueList) {
        if (facetValueList == null) {
            return;
        }
        for (String facetValue : facetValueList) {
            addFacetAndValue(facetName, facetValue);
        }
    }

    public void addFacetAndValue(String facetName, String facetValue) {
        if (!facetNameToFaceValueListMap.containsKey(facetName)) {
            facetNameToFaceValueListMap.put(facetName, new ArrayList<>());
        }
        facetNameToFaceValueListMap.get(facetName).add(facetValue);
    }

    public Map<String, List<String>> getFacetNameToFaceValueListMap() {
        return facetNameToFaceValueListMap;
    }
}
