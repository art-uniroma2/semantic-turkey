package it.uniroma2.art.semanticturkey.security;

import it.uniroma2.art.semanticturkey.user.STUser;
import jakarta.servlet.http.HttpSessionBindingEvent;
import jakarta.servlet.http.HttpSessionBindingListener;
import org.springframework.stereotype.Component;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Component
public class LoggedUser implements HttpSessionBindingListener, Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    private STUser user;
    private ActiveUserStore activeUserStore;

    public LoggedUser(STUser user, ActiveUserStore activeUserStore) {
        this.user = user;
        this.activeUserStore = activeUserStore;
    }

    public LoggedUser() {}

    /**
     * called when the user logs in. Inserts logged-in user into the active users store
     * @param event
     */
    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        List<STUser> users = activeUserStore.getUsers();
        LoggedUser user = (LoggedUser) event.getValue();
        if (!users.contains(user.getUser())) {
            users.add(user.getUser());
        }
    }

    /**
     * called when the user logs out or when the session expires. Removes user from the active users store
     * @param event
     */
    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        List<STUser> users = activeUserStore.getUsers();
        LoggedUser user = (LoggedUser) event.getValue();
        if (user != null) {
            users.remove(user.getUser());
        }
    }

    public STUser getUser() {
        return user;
    }

    public void setUser(STUser user) {
        this.user = user;
    }

}
