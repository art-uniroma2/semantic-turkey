package it.uniroma2.art.semanticturkey.settings.facets.struct;

public class ProjectsAndFacetsResult {
    FacetsResultStruct facetsResultStruct;
    ProjectsResult projectsResult;

    public ProjectsAndFacetsResult(FacetsResultStruct facetsResultStruct, ProjectsResult projectsResult) {
        this.facetsResultStruct = facetsResultStruct;
        this.projectsResult = projectsResult;
    }

    public FacetsResultStruct getFacetsResult() {
        return facetsResultStruct;
    }

    public ProjectsResult getProjectsResult() {
        return projectsResult;
    }
}
