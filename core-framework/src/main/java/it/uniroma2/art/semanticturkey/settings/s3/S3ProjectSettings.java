package it.uniroma2.art.semanticturkey.settings.s3;

import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.properties.STProperty;

/**
 * @author <a href="mailto:tiziano.dicondina@devit.cloud">Tiziano Di Condina</a>
 */
public class S3ProjectSettings implements Settings {

    public static class MessageKeys {
        public static final String keyBase = "it.uniroma2.art.semanticturkey.settings.s3.S3ProjectSettings";

        public static final String shortName = keyBase + ".shortName";

        public static final String region$description = keyBase + ".region.description";
        public static final String region$displayName = keyBase + ".region.displayName";
        public static final String accessKey$description = keyBase + ".accessKey.description";
        public static final String accessKey$displayName = keyBase + ".accessKey.displayName";
        public static final String secretKey$description = keyBase + ".secretKey.description";
        public static final String secretKey$displayName = keyBase + ".secretKey.displayName";
        public static final String bucket$description = keyBase + ".bucket.description";
        public static final String bucket$displayName = keyBase + ".bucket.displayName";
        public static final String versioning$description = keyBase + ".versioning.description";
        public static final String versioning$displayName = keyBase + ".versioning.displayName";
        public static final String rootPath$description = keyBase + ".rootPath.description";
        public static final String rootPath$displayName = keyBase + ".rootPath.displayName";
        public static final String localPath$description = keyBase + ".localPath.description";
        public static final String localPath$displayName = keyBase + ".localPath.displayName";
        public static final String filename$description = keyBase + ".filename.description";
        public static final String filename$displayName = keyBase + ".filename.displayName";
    }

    @Override
    public String getShortName() {
        return "{" + S3ProjectSettings.MessageKeys.shortName + "}";
    }

    @STProperty(description = "{" + MessageKeys.region$description + "}", displayName = "{" + MessageKeys.region$displayName + "}")
    public String region;

    @STProperty(description = "{" + MessageKeys.accessKey$description + "}", displayName = "{" + MessageKeys.accessKey$displayName + "}")
    public String accessKey;

    @STProperty(description = "{" + MessageKeys.secretKey$description + "}", displayName = "{" + MessageKeys.secretKey$displayName + "}")
    public String secretKey;

    @STProperty(description = "{" + MessageKeys.bucket$description + "}", displayName = "{" + MessageKeys.bucket$displayName + "}")
    public String bucket;

    @STProperty(description = "{" + MessageKeys.versioning$description + "}", displayName = "{" + MessageKeys.versioning$displayName + "}")
    public Boolean versioning;

    @STProperty(description = "{" + MessageKeys.rootPath$description + "}", displayName = "{" + MessageKeys.rootPath$displayName + "}")
    public String rootPath;

    @STProperty(description = "{" + MessageKeys.localPath$description + "}", displayName = "{" + MessageKeys.localPath$displayName + "}")
    public String localPath;

    @STProperty(description = "{" + MessageKeys.filename$description + "}", displayName = "{" + MessageKeys.filename$displayName + "}")
    public String filename;

}

