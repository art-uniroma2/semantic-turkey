package it.uniroma2.art.semanticturkey.security;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.uniroma2.art.semanticturkey.user.STUser;

/**
 * @author Tiziano
 * Specifies what happen when a user successes to login.
 * Simply returns a 200 response (ok) with a user Json object
 * (Referenced in WEB-INF/spring-security.xml)
 */
public class STAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	@Autowired
	ActiveUserStore activeUserStore;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException {

		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_OK);
		ServletOutputStream out = response.getOutputStream();
		
		STUser loggedUser = (STUser) authentication.getPrincipal();
		JsonNodeFactory jsonFactory = JsonNodeFactory.instance;
		ObjectNode responseNode = jsonFactory.objectNode();
		responseNode.set("result", loggedUser.getAsJsonObject());

		//adds the user attribute to the session for keeping track of active users. See {@link LoggedUser}
		HttpSession session = request.getSession(false);
		if (session != null) {
			LoggedUser user = new LoggedUser(loggedUser, activeUserStore);
			session.setAttribute("user", user);
		}
		
		out.print(responseNode.toString());
		
	}
}