package it.uniroma2.art.semanticturkey.config.resourcemetadata;

import it.uniroma2.art.semanticturkey.extension.ProjectScopedConfigurableComponent;
import org.apache.poi.util.IOUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class ResourceMetadataPatternStore implements ProjectScopedConfigurableComponent<ResourceMetadataPattern> {

	public static final String DCT_METADATA_FACTORY_PATTERN_REF = "factory:DublinCore metadata";
	private List<String> factoryConfigurationReferences = Collections.singletonList(DCT_METADATA_FACTORY_PATTERN_REF);

	@Override
	public String getId() {
		return ResourceMetadataPatternStore.class.getName();
	}

	public List<String> getFactoryConfigurationReferences() {
		return factoryConfigurationReferences;
	}


	public File getFactoryConfigurationFile(String fileName) throws IOException {
		File cfgFile;
		try (InputStream is = ResourceMetadataPatternStore.class.getResourceAsStream(fileName)) {
			cfgFile = File.createTempFile("configFile", ".cfg");
			try (OutputStream os = new FileOutputStream(cfgFile)) {
				IOUtils.copy(is, os);
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}
		return cfgFile;
	}

}
