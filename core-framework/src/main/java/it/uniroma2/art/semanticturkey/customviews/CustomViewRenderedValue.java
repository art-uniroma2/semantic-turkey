package it.uniroma2.art.semanticturkey.customviews;

import it.uniroma2.art.semanticturkey.services.AnnotatedValue;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CustomViewRenderedValue {

    private String field;
//    private Value resource;
    private AnnotatedValue<Value> resource;
    private Map<String, Value> pivots;
    private UpdateInfo updateInfo;
//    private Set<IRI> graphs;

    //default constructor for json serialization
    public CustomViewRenderedValue() {
//        graphs = new HashSet<>();
    }

    public CustomViewRenderedValue(AnnotatedValue<Value> resource) {
        this(null, resource);
    }

    public CustomViewRenderedValue(String field, AnnotatedValue<Value> resource) {
        this();
        this.field = field;
        this.resource = resource;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public AnnotatedValue<Value> getResource() {
        return resource;
    }

    public void setResource(AnnotatedValue<Value> resource) {
        this.resource = resource;
    }

    public Map<String, Value> getPivots() {
        return pivots;
    }

    public void setPivots(Map<String, Value> pivots) {
        this.pivots = pivots;
    }

    public UpdateInfo getUpdateInfo() {
        return updateInfo;
    }

    public void setUpdateInfo(UpdateInfo updateInfo) {
        this.updateInfo = updateInfo;
    }

//    public Set<IRI> getGraphs() {
//        return graphs;
//    }
//
//    public void setGraphs(Set<IRI> graphs) {
//        this.graphs = graphs;
//    }
//
//    public void addGraphs(IRI graph) {
//        this.graphs.add(graph);
//    }

}
