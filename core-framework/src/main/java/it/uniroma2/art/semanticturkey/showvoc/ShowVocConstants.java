package it.uniroma2.art.semanticturkey.showvoc;

public class ShowVocConstants {

	/*
	Used for mockup visitor user when anonymous access is allowed
	 */
	public static final String SHOWVOC_VISITOR_EMAIL = "public@showvoc.eu";

}
