package it.uniroma2.art.semanticturkey.user;

import it.uniroma2.art.semanticturkey.services.STServiceContext;
import org.springframework.aop.Pointcut;
import org.springframework.aop.framework.AbstractAdvisingBeanPostProcessor;
import org.springframework.aop.support.ComposablePointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMethodMatcher;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import it.uniroma2.art.semanticturkey.aop.STServiceOperationPointcut;
import it.uniroma2.art.semanticturkey.services.annotations.Write;
import org.springframework.context.annotation.Lazy;

@SuppressWarnings("serial")
public class SchemesOwnershipPostProcessor extends AbstractAdvisingBeanPostProcessor
		implements InitializingBean {

	private STServiceContext stServiceContext;
	public SchemesOwnershipPostProcessor(STServiceContext stServiceContext) {
		this.stServiceContext = stServiceContext;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Pointcut pointcut = new ComposablePointcut(new STServiceOperationPointcut())
				.intersection(new AnnotationMethodMatcher(Write.class));
		SchemesOwnershipCheckerInterceptor advice = new SchemesOwnershipCheckerInterceptor(stServiceContext);
		this.advisor = new DefaultPointcutAdvisor(pointcut, advice);
	}

}
