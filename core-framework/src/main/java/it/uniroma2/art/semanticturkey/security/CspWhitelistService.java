package it.uniroma2.art.semanticturkey.security;

import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.settings.core.CoreSystemSettings;
import it.uniroma2.art.semanticturkey.settings.core.SemanticTurkeyCoreSettingsManager;
import it.uniroma2.art.semanticturkey.settings.core.SepiaSettings;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CspWhitelistService {

    @Autowired
    private SemanticTurkeyCoreSettingsManager coreSettingsManager;

    private volatile String cachedWhitelist = "";

    @PostConstruct
    public void loadWhitelist() {
        initWhitelist();
    }

    @Scheduled(fixedRate = 600000) // Check every 10 minutes
    public void periodicalRefresh() {
        initWhitelist();
    }

    public void refreshWhitelist() {
        initWhitelist();
    }

    private synchronized void initWhitelist() {
        try {
            CoreSystemSettings systemSettings = coreSettingsManager.getSystemSettings();
            SepiaSettings sepiaSettings = systemSettings.sepiaSettings;
            if (sepiaSettings != null) {
                cachedWhitelist = sepiaSettings.endpointURL;
            }
        } catch (STPropertyAccessException e) {
            e.printStackTrace();
        }
    }

    public String getWhitelist() {
        return cachedWhitelist;
    }
}
