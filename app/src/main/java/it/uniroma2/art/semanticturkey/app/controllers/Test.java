package it.uniroma2.art.semanticturkey.app.controllers;

import it.uniroma2.art.semanticturkey.extension.ExtensionPointManager;
import it.uniroma2.art.semanticturkey.extension.NoSuchSettingsManager;
import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.extension.settings.SettingsManager;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.resources.Scope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
public class Test {

    public Test() {
    }
    @Autowired
    private ExtensionPointManager exptMgr;

    @GetMapping("/list")
    public Collection<String> list() {
        return exptMgr.getSettingsManagers().stream().map(SettingsManager::getId).collect(Collectors.toList());
    }

    @GetMapping("/get")
    public Settings get(@RequestParam("id") String id) throws NoSuchSettingsManager, STPropertyAccessException {
        return exptMgr.getSettings(null, null, null, id, Scope.SYSTEM);
    }

}
