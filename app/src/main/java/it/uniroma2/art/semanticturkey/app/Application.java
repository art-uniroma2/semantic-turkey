package it.uniroma2.art.semanticturkey.app;

import it.uniroma2.art.semanticturkey.SemanticTurkey;
import it.uniroma2.art.semanticturkey.mdr.bindings.configuration.MDRBindingsConfiguration;
import it.uniroma2.art.semanticturkey.pf4j.STRootWebApplicationContext;
import it.uniroma2.art.semanticturkey.spring.STCoreFrameworkConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.event.ApplicationFailedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

/**
 * Main class of the Semantic Turkey application.
 * 
 * @author <a href="manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
@SpringBootApplication
@Import({
        STCoreFrameworkConfiguration.class, // configuration of the core framework
        MDRBindingsConfiguration.class
})
public class Application {

    public static void main(String[] args) {
        var app = new SpringApplication(Application.class) {
            @Override
            protected ConfigurableApplicationContext createApplicationContext() {
                return new STRootWebApplicationContext();
            }
        };
        app.addListeners((ApplicationEnvironmentPreparedEvent event) -> SemanticTurkey.initialize(event.getEnvironment()));
        app.addListeners((ApplicationReadyEvent event) -> System.out.println("ST Started"));
        app.addListeners((ApplicationFailedEvent event) -> System.out.println("ST Failed to Start"));
        app.run(args);
    }
}
