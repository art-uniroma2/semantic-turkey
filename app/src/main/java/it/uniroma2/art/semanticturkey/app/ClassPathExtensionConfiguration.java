package it.uniroma2.art.semanticturkey.app;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * A @Configuration class introduced to support extensions available on the class path .
 *
 * @author <a href="manuel.fiorelli@uniroma2.it">Manuel Fiorelli</a>
 */
@Configuration
@ComponentScan("it.uniroma2.art.semanticturkey.extension.impl")
public class ClassPathExtensionConfiguration {
}
